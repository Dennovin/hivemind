#!/bin/bash
/usr/local/bin/b2 sync b2://kqhivemind-certs /data/certs/

cd /data/hivemind
docker compose -f prod/docker-compose.app.yaml exec web nginx -s reload
