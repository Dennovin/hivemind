#!/bin/bash
set -e

find . -name '*.py' -not -path '*/migrations/*' | xargs isort
find . -name '*.py' -not -path '*/migrations/*' | xargs pylint
