#!/usr/bin/env python
import re
import sys

import inflection

replacements = {
    "theme.palette.text.primary": "textPrimary",
    "theme.palette.text.disabled": "textDisabled",
    "theme.palette.divider": "divider",
    "theme.palette.blue.light1": "blueLight1",
    "theme.palette.blue.light2": "blueLight2",
    "theme.palette.blue.light3": "blueLight3",
    "theme.palette.blue.light4": "blueLight4",
    "theme.palette.blue.light5": "blueLight5",
    "theme.palette.blue.dark1": "blueDark1",
    "theme.palette.blue.dark2": "blueDark2",
    "theme.palette.blue.dark3": "blueDark3",
    "theme.palette.blue.dark4": "blueDark4",
    "theme.palette.blue.dark5": "blueDark5",
    "theme.palette.gold.light1": "goldLight1",
    "theme.palette.gold.light2": "goldLight2",
    "theme.palette.gold.light3": "goldLight3",
    "theme.palette.gold.light4": "goldLight4",
    "theme.palette.gold.light5": "goldLight5",
    "theme.palette.gold.dark1": "goldDark1",
    "theme.palette.gold.dark2": "goldDark2",
    "theme.palette.gold.dark3": "goldDark3",
    "theme.palette.gold.dark4": "goldDark4",
    "theme.palette.gold.dark5": "goldDark5",
    "theme.palette.background.default": "defaultBg",
    "theme.palette.grey['800']": "gray800",
}

gradients = {
    "theme.gradients.blue.light": ["135deg", "blueLight3", "blueLight2"],
    "theme.gradients.blue.main": ["blueLight1", "blueDark1"],
    "theme.gradients.blue.dark": ["blueDark1", "blueDark3"],
    "theme.gradients.gold.light": ["135deg", "goldLight3", "goldLight2"],
    "theme.gradients.gold.main": ["goldLight1", "goldDark1"],
    "theme.gradients.gold.dark": ["goldDark1", "goldDark3"],
    "theme.gradients.gray.light": ["#ccc", "#aaa"],
    "theme.gradients.primary": ["goldLight3", "goldLight1"],
}

for source_fn in sys.argv[1:]:
    css_fn = source_fn.replace(".jsx", ".module.css")
    css_rel_fn = css_fn.split("/")[-1]
    print(f"import styles from './{css_rel_fn}';")

    with open(source_fn, "r") as source_fh, open(css_fn, "w") as css_fh:
        groups = []
        used_vars = set()
        in_block = False
        last_class_name = None

        for line in source_fh:
            if line.startswith("const useStyles ="):
                in_block = True
                continue

            if line.startswith("}));"):
                in_block = False
                continue

            if not in_block:
                continue

            result = re.match("^\s*(\w+)\: \{\s*$", line)
            if result:
                groups.append((f".{result.group(1)}", []))
                continue

            if len(groups) == 0:
                continue

            result = re.match("^\s*'(.*?)': \{\s*$", line)
            if result:
                class_name = result.group(1).replace("&", groups[-1][0])
                groups.append((class_name, []))
                continue

            result = re.match("^\s*\},\s*$", line)
            if result:
                continue

            result = re.match("^\s*(\w+)\: \{\}\,\s*$", line)
            if result:
                continue

            result = re.match("^\s*(\w+)\: '(.*?)',\s*$", line)
            if result:
                prop_name = inflection.underscore(result.group(1)).replace("_", "-")
                groups[-1][1].append(f"    {prop_name}: {result.group(2)};\n")
                continue

            result = re.match("^\s*(\w+)\: `(.*?)`,\s*$", line)
            if result:
                prop_name = inflection.underscore(result.group(1)).replace("_", "-")
                prop_value = result.group(2)

                while True:
                    prop_value_match = re.search("\$\{(.*?)\}", prop_value)
                    if not prop_value_match:
                        break
                    inner_val = prop_value_match.group(1)

                    if inner_val in gradients:
                        for v in gradients[prop_value]:
                            if v in replacements.values():
                                used_vars.add(v)
                        inner_val = "linear-gradient({})".format(
                            ", ".join(gradients[inner_val])
                        )

                    if inner_val in replacements:
                        inner_val = replacements[inner_val]
                        used_vars.add(inner_val)

                    prop_value = prop_value.replace(prop_value_match.group(0), inner_val)

                groups[-1][1].append(f"    {prop_name}: {prop_value};\n")
                continue

            result = re.match("^\s*(\w+)\: (.*?),\s*$", line)
            if result:
                prop_name = inflection.underscore(result.group(1)).replace("_", "-")
                prop_value = result.group(2)

                if prop_value in gradients:
                    for v in gradients[prop_value]:
                        if v in replacements.values():
                            used_vars.add(v)
                    prop_value = "linear-gradient({})".format(
                        ", ".join(gradients[prop_value])
                        )

                if prop_value in replacements:
                    prop_value = replacements[prop_value]
                    used_vars.add(prop_value)

                prop_result = re.match("^theme.spacing\(([\d\.\,\s]+)\)$", prop_value)
                if prop_result:
                    spacings = [int(8*float(i)) for i in prop_result.group(1).split(",")]
                    spacings = [f"{i}px" if i > 0 else "0" for i in spacings]
                    prop_value = " ".join(spacings)

                groups[-1][1].append(f"    {prop_name}: {prop_value};\n")
                continue

            print(line)

        if len(used_vars) > 0:
            used_vars_str = ", ".join(sorted(used_vars))
            css_fh.write("@value colors: 'theme/colors.css';\n")
            css_fh.write(f"@value {used_vars_str} from colors;\n")
            css_fh.write("\n")

        for group in groups:
            css_fh.write(f"{group[0]} {{\n")
            for prop in group[1]:
                css_fh.write(prop)

            css_fh.write("}\n\n")
