#!/bin/bash

basedir="/mnt/volume_nyc1_01/backups"
backupdate="$( date +%Y%m%d_%H%M%S )"
outputdir="${basedir}/${backupdate}"
b2="/usr/local/bin/b2"

cd /data/hivemind
pg_dump --file "$outputdir" --format directory hivemind

for dir in $( ls -t "$basedir" | tail -n +14 ); do
    rm -rf "${basedir}/${dir}"
done

"$b2" sync --delete "${basedir}" b2://kqhivemind-backups
