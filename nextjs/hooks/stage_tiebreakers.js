import React, { useEffect, useState } from 'react';
import { getAxios } from '@/util/axios';

export function useStageTiebreakerOptions() {
  const [tiebreakers, setTiebreakers] = useState(null);
  const axios = getAxios();

  useEffect(() => {
    axios.get('/api/tournament/tournament/tiebreaker-types/').then(response => {
      setTiebreakers(response.data.tiebreakerTypes);
    });
  }, [setTiebreakers]);

  return tiebreakers;
}
