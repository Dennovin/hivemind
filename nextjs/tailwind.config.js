/** @type {import('tailwindcss').Config} */
const { join } = require('path');

const colors = {
  divider: '#e0e0e0',
  light: {
    body: '#EEEEEE',
    container: '#FFFFFF',
    header: '#232323',
    text: '#212121',
    grass: '#116611',
    dirtTop: '#806810',
    dirtBottom: '#543b04',
  },
  dark: {
    body: '#292929',
    container: '#131313',
    header: '#000000',
    text: '#D7DADC',
  },
  map: {
    day: {
      top: '#5bacdc',
      bottom: '#98d8fb',
    },
    night: {
      top: '#17113f',
      bottom: '#6361d2',
    },
    dusk: {
      top: '#885527',
      bottom: '#cb9544',
    },
    twilight: {
      top: '#061d33',
      bottom: '#8d0b8f',
    },
  },
  blue: {
    light5: '#EBF4FF',
    light4: '#cce5ff',
    light3: '#99ccff',
    light2: '#66b2ff',
    light1: '#3299ff',
    main: '#287fff',
    dark1: '#2860e8',
    dark2: '#10489D',
    dark3: '#143366',
    dark4: '#12264D',
    dark5: '#101933',
  },
  // e0fcf9,b8f8f1,83f2e6,19e6d1,1dc5d7,21a3dd,2860e8
  gold: {
    main: '#ffb330',
    dark1: '#ffa70e',
    dark2: '#eb9500',
    dark3: '#c97f00',
    dark4: '#a76a00',
    dark5: '#855400',
    light1: '#ffbf52',
    light2: '#ffcc74',
    light3: '#ffd896',
    light4: '#ffe5b8',
    light5: '#fff1da',
  },
  secondary: {
    main: '#C856C8', //baseTheme.palette.gold.dark5,
    light: '#C58194', //baseTheme.palette.gold.main,
    dark: '#8C4056',
    lighter: '#D8ABB8',
  },
};

module.exports = {
  content: [
    join(
      __dirname,
      '{components,pages,overlay,src,theme,providers,util,theme}/**/*.{js,ts,jsx,tsx}',
    ),
    join(__dirname, 'components/forms/**/*.{js,ts,jsx,tsx}'),
  ],
  important: true,
  theme: {
    extend: {
      colors,
      keyframes: {
        'slab-down': {
          '0%': { transform: 'translateY(-100%)' },
          '100%': { transform: 'translateY(0)' },
        },
        'fade-in': {
          '0%': { opacity: 0 },
          '100%': { opacity: 1 },
        },
      },
      transitionTimingFunction: {
        'spring-slab': `linear(0, 0.01, 0.03, 0.11 4%, 0.61 11%, 0.82, 0.89, 0.96, 1, 1.03, 1.05 26% 33%,1.01 44%, 1 51% 100%)`,
      },
      animation: {
        'slab-down': 'slab-down 1s cubic-bezier(1,.78,1,1.03)',
        'fade-in': 'fade-in 500s cubic-bezier(1,.78,1,1.03)',
      },
    },
  },
  plugins: [],
  corePlugins: {
    preflight: false,
  },
};
