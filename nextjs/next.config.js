module.exports = {
  trailingSlash: false,
  publicRuntimeConfig: {
    OAUTH_CLIENT_ID: process.env.OAUTH_CLIENT_ID,
    PATREON_CLIENT_ID: process.env.PATREON_CLIENT_ID,
    DISCORD_CLIENT_ID: process.env.DISCORD_CLIENT_ID,
    TWITCH_CLIENT_ID: process.env.TWITCH_CLIENT_ID,
  },
  webpack: config => {
    config.resolve.fallback = {
      canvas: false,
      crypto: false,
      dgram: false,
      net: false,
      os: false,
      stream: false,
      tls: false,
      zlib: false,
    };

    config.module.rules.push({
      test: /\.svg$/,
      use: ['@svgr/webpack'],
    });

    return config;
  },
  eslint: {
    ignoreDuringBuilds: true,
  },
  async redirects() {
    return [
      {
        source: '/wiki',
        destination: '/wiki/Main_Page',
        permanent: true,
      },
      {
        source: '/activate',
        destination: '/wiki/Player_Card_Registration',
        permanent: true,
      },
      {
        source: '/setup/cabinet',
        destination: '/wiki/Basic_Client_Setup',
        permanent: true,
      },
      {
        source: '/setup/obs',
        destination: '/wiki/OBS_Configuration',
        permanent: true,
      },
    ];
  },
};
