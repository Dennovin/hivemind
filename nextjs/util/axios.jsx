import axios from 'axios';
import applyCaseMiddleware from 'axios-case-converter';
import { applyAuthTokenInterceptor } from 'axios-jwt';
import qs from 'qs';

import isServer from 'util/isServer';

const serverBaseURL = 'http://app:8080/';
const clientBaseURL = '/';
// const clientBaseURL = 'http://192.168.129.155:8087/';

axios.defaults.headers['Access-Control-Allow-Origin'] = '*';

export function requestRefresh(refreshToken) {
  return axios.post('/api/user/refresh-token/', { refresh: refreshToken }).then(response => {
    return Promise.resolve({
      accessToken: response.data.access,
      refreshToken: response.data.refresh,
    });
  });
}

export function getAxios(params) {
  params ??= {};
  const { authenticated } = params;

  const axiosInstance = applyCaseMiddleware(
    axios.create({
      // xsrfCookieName: 'hivemind_csrftoken',
      // xsrfHeaderName: 'X-CSRFTOKEN',
      withCredentials: false,
      baseURL: '/',
    }),
  );

  axiosInstance.getAllPages = async (url, opts) => {
    opts ??= {};
    opts.params ??= {};
    opts.params.rowsPerPage ??= 100;

    let response = await axiosInstance.get(url, opts);
    let rows = response.data.results;

    while (response.data.next) {
      response = await axiosInstance.get(response.data.next);
      rows = [...rows, ...response.data.results];
    }

    return rows;
  };

  axiosInstance.interceptors.request.use(config => {
    const newConfig = {
      ...config,
      paramsSerializer: p => {
        return qs.stringify(p, { arrayFormat: 'comma' });
      },
    };

    newConfig.baseURL = isServer() ? serverBaseURL : clientBaseURL;
    return newConfig;
  });

  if (!isServer() && authenticated) {
    applyAuthTokenInterceptor(axiosInstance, { requestRefresh });
  }

  return axiosInstance;
}
