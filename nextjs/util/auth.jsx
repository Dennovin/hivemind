import React, { useContext, useState, useEffect } from 'react';
import { clearAuthTokens, isLoggedIn, setAuthTokens } from 'axios-jwt';
import PropTypes from 'prop-types';
import { getAxios } from 'util/axios';

import { PERMISSION_TYPES } from 'util/constants';
import isServer from 'util/isServer';

export const UserContext = React.createContext({
  user: null,
  hasPermission: (sceneId, permissionType) => false,
  hasAnyPermission: (sceneId, permissionTypes) => false,
  isAdminOf: sceneId => false,
});

export const useUser = () => useContext(UserContext).user;
export const useAuth = () => useContext(UserContext);

export async function onLoginSuccess({ accessToken, tokenId }) {
  const axios = getAxios();
  const response = await axios.post('/api/user/exchange-token/google-oauth2/', {
    accessToken: accessToken,
    idToken: tokenId,
  });

  await setAuthTokens({
    accessToken: response.data.accessToken,
    refreshToken: response.data.refreshToken,
  });
}

export async function onLoginFailure({ ...props }) {
  clearAuthTokens();
}

export function logOut() {
  clearAuthTokens();
}

export async function getCurrentUser() {
  const axios = getAxios({ authenticated: true });
  try {
    const response = await axios.get('/api/user/me/');
    return response.data;
  } catch (e) {
    return null;
  }
}

export function UserProvider({ children }) {
  const [user, setUser] = useState(null);
  const [userLoading, setUserLoading] = useState(true);

  useEffect(() => {
    if (user === null && !isServer()) {
      const fromLocalStorage = localStorage.getItem('user');
      if (fromLocalStorage) {
        try {
          setUser(JSON.parse(fromLocalStorage));
          setUserLoading(false);
        } catch (err) {
          console.log('Invalid user data in local storage. Clearing.');
          console.log(err);
          localStorage.clear();
        }
      }

      getCurrentUser().then(response => {
        setUser(response);
        setUserLoading(false);
        localStorage.setItem('user', JSON.stringify(response));
      });
    }
  }, []);

  const hasAnyPermission = (sceneId, permissionTypes) => {
    if (user?.permissions) {
      for (const permission of user.permissions) {
        if (sceneId === permission.scene && permissionTypes.includes(permission.permission)) {
          return true;
        }
      }
    }

    return false;
  };

  const hasPermission = (sceneId, permissionType) => hasAnyPermission(sceneId, [permissionType]);

  const reloadUser = async () => {
    const response = await getCurrentUser();
    setUser(response);
    localStorage.setItem('user', JSON.stringify(response));
  };

  const isAdminOf = sceneId => hasPermission(sceneId, PERMISSION_TYPES.ADMIN);

  const userState = {
    user,
    userLoading,
    hasPermission,
    hasAnyPermission,
    isAdminOf,
    reloadUser,
  };

  return <UserContext.Provider value={userState}>{children}</UserContext.Provider>;
}

export function UserConsumer(component) {
  return componentProps => (
    <div suppressHydrationWarning>
      <UserContext.Consumer>
        {props => React.createElement(component, { ...props, ...componentProps })}
      </UserContext.Consumer>
    </div>
  );
}

export function isAdminOf(sceneId) {
  return hasPermission(sceneId, PERMISSION_TYPES.ADMIN);
}

export function hasPermission(sceneId, permissionType) {
  const user = useUser();
  if (user?.permissions) {
    for (const permission of user.permissions) {
      if (sceneId === permission.scene && permission.permission === permissionType) {
        return true;
      }
    }
  }

  return false;
}

export const IfLoggedIn = React.forwardRef(({ state, children }, ref) => (
  <div suppressHydrationWarning>
    {(!isServer() && isLoggedIn()) === state && <React.Fragment>{children}</React.Fragment>}
  </div>
));

IfLoggedIn.propTypes = {
  state: PropTypes.bool,
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.element), PropTypes.element])
    .isRequired,
};

IfLoggedIn.defaultProps = {
  state: true,
};
