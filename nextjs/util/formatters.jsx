import { STAT_FORMATTING_TYPE } from 'util/constants';
import { formatInterval, formatTime } from 'util/dates';

export function formatPercent(value, decimals = 2) {
  if (value === undefined) return '';
  return value.toLocaleString(undefined, {
    style: 'percent',
    minimumFractionDigits: decimals,
    maximumFractionDigits: decimals,
  });
}

export function formatStatValue(value, formattingType) {
  if (value === undefined) return '';

  if (formattingType === STAT_FORMATTING_TYPE.INTEGER) {
    return value.toLocaleString(undefined, {
      style: 'decimal',
      minimumFractionDigits: 0,
      maximumFractionDigits: 0,
    });
  }

  if (formattingType === STAT_FORMATTING_TYPE.TIME) {
    return formatInterval(value);
  }

  if (formattingType === STAT_FORMATTING_TYPE.THOUSANDTHS) {
    if (!value) return '';

    return (value / 1000).toLocaleString(undefined, {
      style: 'decimal',
      minimumFractionDigits: 3,
      maximumFractionDigits: 3,
    });
  }

  return value;
}

export function formatStatValueShort(value, formattingType) {
  if (formattingType === STAT_FORMATTING_TYPE.TIME) {
    return formatTime(value);
  }

  return formatStatValue(value, formattingType);
}
