import React, { useState, useEffect } from 'react';
import { toast } from 'react-toastify';

import { getAxios } from './axios';

export function useGet(url, params) {
  const [data, setData] = useState(null);
  const [error, setError] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const axios = getAxios({ authenticated: true });

  const getData = () => {
    setIsLoading(true);
    axios
      .get(url, params)
      .then(response => {
        setIsLoading(false);
        setError(false);
        setData(response.data);
      })
      .catch(() => {
        setIsLoading(false);
        setError(true);
      });
  };

  useEffect(() => {
    getData();
  }, [url]);

  return { data, setData, error, isLoading, reload: getData };
}

export function useGetAllPages(url, params) {
  const [data, setData] = useState(null);
  const axios = getAxios({ authenticated: true });

  const getData = () =>
    axios
      .getAllPages(url, params)
      .then(setData)
      .catch(() => {
        toast.error('API request failed.');
      });

  useEffect(() => {
    getData();
  }, [url]);

  return { data, setData, reload: getData };
}
