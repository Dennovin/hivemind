const FEATURE_FLAGS = {};

export function hasFeatureFlag(flagName, sceneName) {
  return flagName in FEATURE_FLAGS && FEATURE_FLAGS[flagName].includes(sceneName);
}
