import React from 'react';

import { PermissionInvitePage } from '@/components/scene-permissions';

export default function PermissionInvite({ token }) {
  return <PermissionInvitePage token={token} />;
}

export async function getServerSideProps({ params }) {
  return {
    props: { token: params.token },
  };
}
