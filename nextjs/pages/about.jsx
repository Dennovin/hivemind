import { makeStyles } from '@material-ui/core/styles';
import { Typography, Grid, Paper, Link } from '@material-ui/core';
import Carousel from 'react-material-ui-carousel';

const useStyles = makeStyles(theme => ({
  carouselContainer: {
    margin: theme.spacing(2, 0),
    height: '800px',
  },
  carouselItem: {
    padding: theme.spacing(2),
    textAlign: 'center',
    '& img': {
      maxHeight: '600px',
      margin: '0 auto',
      objectFit: 'contain',
    },
  },
  carouselImageContainer: {
    height: '620px',
    flexBasis: '620px',
    flexGrow: 0,
  },
  textContainer: {
    margin: theme.spacing(2, 0),
    textAlign: 'center',
  },
}));

export function AboutPageItem({ title, imgSrc, imgAlt, description }) {
  const classes = useStyles();
  return (
    <Paper className={classes.carouselItem}>
      <Grid item>
        <Typography variant="h2">{title}</Typography>
      </Grid>
      <Grid item className={classes.carouselImageContainer}>
        <img src={imgSrc} alt={imgAlt} />
      </Grid>
      <Grid item>
        <Typography>{description}</Typography>
      </Grid>
    </Paper>
  );
}

export default function About() {
  const classes = useStyles();
  return (
    <>
      <Typography variant="h1">HiveMind: Stats for Killer Queen</Typography>
      <Grid container spacing={4}>
        <Grid item container xs={12} direction="column" className={classes.carouselContainer}>
          <Carousel interval={15000} navButtonsAlwaysVisible={true}>
            <AboutPageItem
              title="All your stats from all your games"
              imgSrc="/static/screenshots/game_summary.png"
              imgAlt="Screenshot with example of game statistics"
              description="Your game stats are sent to HiveMind in real time, and are always available from your phone or computer."
            />
            <AboutPageItem
              title="All your stats from all your games"
              imgSrc="/static/screenshots/player_stats.png"
              imgAlt="Screenshot of a game's player stats and charts"
              description="Stats for every game are available from your computer or phone, with charts showing your team's progress towards victory."
            />
            <AboutPageItem
              title="Streaming Overlays"
              imgSrc="/static/screenshots/overlay_theme.png"
              imgAlt="Screenshot of themed overlay"
              description="Show real-time stats in your stream, with a variety of available themes."
            />
            <AboutPageItem
              title="Streaming Overlays"
              imgSrc="/static/screenshots/postgame_overlay.png"
              imgAlt="Screenshot of themed postgame overlay"
              description="Automatically show stats, charts, and awards at the end of each game."
            />
            <AboutPageItem
              title="Tournament Tracking"
              imgSrc="/static/screenshots/match_summary.png"
              imgAlt="Screenshot of tournament match stats"
              description="Track your tournaments easily with Challonge integration."
            />
            <AboutPageItem
              title="Tournament Tracking"
              imgSrc="/static/screenshots/match_summary_overlay.png"
              imgAlt="Screenshot of tournament post-match overlay"
              description="Your stream will automatically update with team names and match summaries."
            />
            <AboutPageItem
              title="Tournament Registration"
              imgSrc="/static/screenshots/tournament_registration.png"
              imgAlt="Screenshot of a tournament registration page"
              description="Tournament registration allows players to register for your event, create teams, and pay for their registration through Stripe."
            />
            <AboutPageItem
              title="Player Sign-In"
              imgSrc="/static/screenshots/player_last_session.png"
              imgAlt="Screenshot of stats from a player's last session"
              description="Players can sign in using a hardware NFC device or through a web interface."
            />
            <AboutPageItem
              title="Player Leaderboard"
              imgSrc="/static/screenshots/leaderboard.png"
              imgAlt="Screenshot of a player leaderboard"
              description="Sign in to make it to the national leaderboard!"
            />
          </Carousel>
        </Grid>

        <Grid item container xs={12} direction="column" className={classes.textContainer}>
          <Typography>
            HiveMind is a free open-source stat tracker for{' '}
            <Link href="https://killerqueenarcade.com/">Killer Queen Arcade</Link>.
          </Typography>

          <Typography>
            <Link href="/scene-request">Sign up</Link>, follow the{' '}
            <Link href="/wiki/Basic_Client_Setup">setup instructions</Link>, and join{' '}
            <b>#hivemind</b> on Killer Queen Discord to connect your cabinet.
          </Typography>

          <Typography>
            Source code is available on{' '}
            <Link href="https://gitlab.com/kqhivemind/hivemind">GitLab</Link>. Contributions and
            feature requests are welcome.
          </Typography>
        </Grid>
      </Grid>
    </>
  );
}
