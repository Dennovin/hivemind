import { useRouter } from 'next/router';

import SceneRequestForm from 'components/forms/SceneRequestForm';
import LoginButtons from 'components/LoginButtons';
import { UserConsumer } from 'util/auth';

const SceneRequest = UserConsumer(({ user }) => {
  const router = useRouter();

  return (
    <>
      <h1 className='mt-0'>Request a New Scene</h1>

      <p className='text-base'>Is Killer Queen Arcade new to your city and you want to help build a community? Or maybe you are a venue operator who wants to let your players track their stats?
      Then join the HiveMind!</p>

      <p className='text-base'>A scene is used to manage cabinets, organize tournaments, and more.
      Once your scene has been created, check out the <a href='/wiki'>wiki</a> for guides on connecting your cabinets to HiveMind.</p>

      {user?.id && (
        <>
          <p className='mb-2 text-base'>
            <b>This is not the player registration form.</b> To edit your player profile, go to the{' '}
            <a href={`/user/${user.id}`}>user profile page</a> and click the Edit button.
          </p>

          <SceneRequestForm />
        </>
      )}

      {!user?.id && (
        <>
          <LoginButtons>Log in to continue</LoginButtons>
        </>
      )}
    </>
  );
});

export default SceneRequest;
