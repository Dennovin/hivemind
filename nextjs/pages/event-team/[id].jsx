import { makeStyles } from '@material-ui/core/styles';
import { Typography, Breadcrumbs, Link, Grid } from '@material-ui/core';
import { getAxios } from 'util/axios';
import { format } from 'date-fns';
import PropTypes from 'prop-types';
import clsx from 'clsx';

import { formatUTC } from 'util/dates';
import PlayerListTable from 'components/tables/PlayerListTable';
import MatchListTable from 'components/tables/MatchListTable';

const useStyles = makeStyles(theme => ({
}));

export default function TeamPage({ team }) {
  const classes = useStyles();
  const completedMatches = team.matches.filter(match => match.isComplete);
  const remainingMatches = team.matches.filter(match => !match.isComplete);

  return (
    <>

      <Breadcrumbs>
        <Link href={`/scene/${team.event.season.scene.name}`}>{team.event.season.scene.displayName}</Link>
        <Link href={`/season/${team.event.season.id}`}>{team.event.season.name}</Link>
        <Link href={`/event/${team.event.id}`}>{formatUTC(new Date(team.event.eventDate), 'MMMM d, yyyy')}</Link>
        <Link href={`/event-team/${team.id}`}>{team.name}</Link>
      </Breadcrumbs>
      <Typography variant="h1">{team.name}</Typography>

      <PlayerListTable data={team.players} />

      {remainingMatches.length > 0 && (
        <MatchListTable title="Remaining Matches" data={remainingMatches} />
      )}

      {completedMatches.length > 0 && (
        <MatchListTable title="Completed Matches" data={completedMatches} />
      )}
    </>
  );
}

export async function getServerSideProps({ params }) {
  const axios = getAxios();
  let response = await axios.get(`/api/league/team/${params.id}`);
  const team = response.data;

  if (!team) return { notFound: true };

  response = await axios.get('/api/league/match/', { params: { teamId: team.id } });
  team.matches = response.data.results;

  response = await axios.get('/api/league/player/', { params: { teamId: team.id } });
  team.players = response.data.results;

  response = await axios.get(`/api/league/event/${team.event}`);
  team.event = response.data;

  response = await axios.get(`/api/league/season/${team.event.season}`);
  team.event.season = response.data;

  response = await axios.get(`/api/game/scene/${team.event.season.scene}`);
  team.event.season.scene = response.data;

  return {
    props: { team, title: `${team.name} - ${team.event.season.name}` },
  };
}

