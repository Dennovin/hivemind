import React, { useState, useEffect } from 'react';
import { Typography, Breadcrumbs, Link, CircularProgress } from '@material-ui/core';
import { useRouter } from 'next/router';
import clsx from 'clsx';

import { BLUE_TEAM, POSITIONS_BY_TEAM, SIGN_IN_ACTIONS, CABINET_POSITIONS } from 'util/constants';
import { useUser } from 'util/auth';
import { getAxios } from 'util/axios';
import { useWebSocket } from 'util/websocket';
import Title from 'components/Title';
import LoginButtons from 'components/LoginButtons';

import styles from './[...position].module.css';

export default function SignInPage({ cabinet }) {
  const axios = getAxios({ authenticated: true });
  const router = useRouter();
  const user = useUser();
  const [loading, setLoading] = useState(false);
  const [redirecting, setRedirecting] = useState(false);
  const [signInDataLoaded, setSignInDataLoaded] = useState(false);

  const player = {};
  const setPlayer = {};
  const { position } = router.query;
  const [team, playerPos] = position;

  for (const pos of Object.values(CABINET_POSITIONS)) {
    [player[pos.ID], setPlayer[pos.ID]] = useState(null);
  }

  useEffect(() => {
    axios.get(`/api/game/cabinet/${cabinet.id}/signin/`).then(response => {
      for (const player of response.data.signedIn) {
        setPlayer[player.playerId](player);
      }
      setSignInDataLoaded(true);
    });
  }, []);

  useEffect(() => {
    if (signInDataLoaded && user?.id) {
      const position = POSITIONS_BY_TEAM[team].find(position => position.POSITION === playerPos);
      if (position) {
        setRedirecting(true);

        const parameters = {
          player: position.ID,
          cabinetId: cabinet.id,
          action: SIGN_IN_ACTIONS.SIGN_IN,
        };

        axios.post(`/api/user/signin/qr/`, parameters).then(() => {
          router.push(`/cabinet/${cabinet.scene.name}/${cabinet.name}/signin`);
        });
      }
    }
  }, [signInDataLoaded, user?.id]);

  const ws = useWebSocket('/ws/signin');
  ws.onJsonMessage(message => {
    if (message.cabinetId !== cabinet.id) {
      return;
    }

    if (message.action === SIGN_IN_ACTIONS.SIGN_IN) {
      setPlayer[message.playerId](message);
    }

    if (message.action === SIGN_IN_ACTIONS.SIGN_OUT) {
      setPlayer[message.playerId](null);
    }
  });

  const login = async pos => {
    setLoading(true);

    if (user?.id && !loading) {
      const action =
        player[pos.ID] && player[pos.ID].userId === user.id
          ? SIGN_IN_ACTIONS.SIGN_OUT
          : SIGN_IN_ACTIONS.SIGN_IN;
      const response = await axios.post(`/api/user/signin/qr/`, {
        player: pos.ID,
        cabinetId: cabinet.id,
        action,
      });
    }

    setLoading(false);
  };

  const PlayerSignIn = ({ pos }) => {
    return (
      <>
        <div
          className={clsx(styles.cabinetPosition, pos.TEAM, { active: player[pos.ID] !== null })}
          onClick={() => login(pos)}
        >
          <div className={styles.icon}>
            <img src={pos.IMAGE} />
          </div>
          <div className={styles.name}>
            {player[pos.ID] ? (
              player[pos.ID].userName
            ) : (
              <span className="italic font-normal text-gray-500">
                <span className="md:hidden">Tap</span>
                <span className="hidden md:inline">Click</span> to sign-in
              </span>
            )}
          </div>
        </div>
      </>
    );
  };

  return (
    <>
      <Breadcrumbs>
        <Link href={`/scene/${cabinet.scene.name}`}>{cabinet.scene.displayName}</Link>
        <Link href={`/cabinet/${cabinet.scene.name}/${cabinet.name}`}>{cabinet.displayName}</Link>
        <Link href={`/cabinet/${cabinet.scene.name}/${cabinet.name}/signin`}>Sign In</Link>
        <Link href={`/cabinet/${cabinet.scene.name}/${cabinet.name}/signin/${team}`}>{team}</Link>
        {playerPos && <span>{playerPos}</span>}
      </Breadcrumbs>

      <Title title={`${cabinet.displayName}: Sign In`} />

      {cabinet.allowQrSignin && (
        <>
          {!user?.id && playerPos ? (
            <div className={styles.container}>
              {POSITIONS_BY_TEAM[team]
                .filter(pos => (playerPos ? pos.POSITION === playerPos : true))
                .map(pos => (
                  <LoginButtons>
                    <div className={styles.loginLink}>
                      <div className={styles.icon}>
                        <img src={pos.IMAGE} />
                      </div>
                      <div className={styles.name}>Tap to sign in</div>
                    </div>
                  </LoginButtons>
                ))}
            </div>
          ) : redirecting ? (
            <div className={styles.container}>
              {POSITIONS_BY_TEAM[team]
                .filter(pos => (playerPos ? pos.POSITION === playerPos : true))
                .map(pos => (
                  <div className={styles.redirecting}>
                    <div className={styles.icon}>
                      <img src={pos.IMAGE} />
                    </div>
                    <div className={styles.name}>
                      <CircularProgress /> Signing you in...
                    </div>
                  </div>
                ))}
            </div>
          ) : (
            <div className={styles.container}>
              <div className={styles.teams}>
                <div className={clsx(styles.team, team)}>
                  {POSITIONS_BY_TEAM[team]
                    .filter(pos => (playerPos ? pos.POSITION === playerPos : true))
                    .map(pos => (
                      <PlayerSignIn key={pos.ID} pos={pos} />
                    ))}
                </div>
              </div>
            </div>
          )}
        </>
      )}

      {!cabinet.allowQrSignin && (
        <Typography>This cabinet is not configured to allow sign-ins.</Typography>
      )}
    </>
  );
}

export async function getServerSideProps({ params }) {
  const axios = getAxios();
  let response = await axios.get(`/api/game/scene/`, { params: { name: params.sceneName } });
  const scene = response.data.results[0];

  if (!scene) return { notFound: true };

  response = await axios.get(`/api/game/cabinet/`, {
    params: { sceneId: scene.id, name: params.cabinetName },
  });
  const cabinet = response.data.results[0];

  if (!cabinet) return { notFound: true };

  cabinet.scene = scene;

  return {
    props: {
      cabinet,
      title: `${scene.displayName} - ${cabinet.displayName}`,
      description: cabinet.description,
    },
  };
}
