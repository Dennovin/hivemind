import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Typography, Breadcrumbs, Link, CircularProgress } from '@material-ui/core';
import { useRouter } from 'next/router';
import clsx from 'clsx';

import {
  TEAMS,
  BLUE_TEAM,
  GOLD_TEAM,
  POSITIONS_BY_TEAM,
  SIGN_IN_ACTIONS,
  CABINET_POSITIONS,
} from 'util/constants';
import { useUser } from 'util/auth';
import { getAxios } from 'util/axios';
import { useWebSocket } from 'util/websocket';
import Title from 'components/Title';
import LoginButtons from 'components/LoginButtons';

const useStyles = makeStyles(theme => ({
  container: {
    margin: `${theme.spacing(2)}px auto 0 auto`,
    maxWidth: '800px',
  },
  teams: {
    display: 'flex',
    flexDirection: 'row',
  },
  blueTeam: {
    flexGrow: 1,
    flexBasis: 0,
    display: 'flex',
    flexDirection: 'column',
  },
  goldTeam: {
    flexGrow: 1,
    flexBasis: 0,
    display: 'flex',
    flexDirection: 'column',
  },
  cabinetPosition: {
    flexGrow: 1,
    flexBasis: 0,
    padding: theme.spacing(1),
    display: 'flex',
    flexDirection: 'row',
    borderWidth: '0 1px 1px 1px',
    borderStyle: 'solid',
    borderColor: theme.palette.divider,
    marginTop: '-1px',
    '&:first-of-type': {
      borderTopWidth: '1px',
    },
    '&.blue': {
      paddingRight: '8px',
    },
    '&.gold': {
      flexDirection: 'row-reverse',
      paddingLeft: '8px',
    },
    '&:hover': {
      cursor: 'pointer',
      borderBottom: `2px solid ${theme.palette.text.primary}`,
      marginBottom: '-1px',
    },
    '&.active.blue': {
      background: theme.gradients.blue.light,
    },
    '&.active.gold': {
      background: theme.gradients.gold.light,
    },
  },
  icon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: theme.spacing(0, 1),
    '& img': {
      height: '25px',
    },
  },
  name: {
    flexGrow: 1,
    flexBasis: 0,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: '16px',
    fontWeight: 'bold',
  },
}));

export default function SignInPage({ cabinet }) {
  const axios = getAxios({ authenticated: true });
  const router = useRouter();
  const user = useUser();
  const [loading, setLoading] = useState(false);
  const [initialized, setInitialized] = useState(false);
  const classes = useStyles({ user, loading });

  const player = {};
  const setPlayer = {};
  const { query } = router;
  console.log(router);

  for (const pos of Object.values(CABINET_POSITIONS)) {
    [player[pos.ID], setPlayer[pos.ID]] = useState(null);
  }

  if (!initialized) {
    axios.get(`/api/game/cabinet/${cabinet.id}/signin/`).then(response => {
      setInitialized(true);
      for (const player of response.data.signedIn) {
        setPlayer[player.playerId](player);
      }
    });
  }

  const ws = useWebSocket('/ws/signin');
  ws.onJsonMessage(message => {
    if (message.cabinetId !== cabinet.id) {
      return;
    }

    if (message.action === SIGN_IN_ACTIONS.SIGN_IN) {
      setPlayer[message.playerId](message);
    }

    if (message.action === SIGN_IN_ACTIONS.SIGN_OUT) {
      setPlayer[message.playerId](null);
    }
  });

  const login = async pos => {
    setLoading(true);

    if (user?.id && !loading) {
      const action =
        player[pos.ID] && player[pos.ID].userId === user.id
          ? SIGN_IN_ACTIONS.SIGN_OUT
          : SIGN_IN_ACTIONS.SIGN_IN;
      const response = await axios.post(`/api/user/signin/qr/`, {
        player: pos.ID,
        cabinetId: cabinet.id,
        action,
      });
    }

    setLoading(false);
  };

  const PlayerSignIn = ({ pos }) => {
    return (
      <>
        <div
          className={clsx(classes.cabinetPosition, pos.TEAM, { active: player[pos.ID] !== null })}
          onClick={() => login(pos)}
        >
          <div className={classes.icon}>
            <img src={pos.ICON} />
          </div>
          <div className={classes.name}>{player[pos.ID] ? player[pos.ID].userName : ''}</div>
        </div>
      </>
    );
  };

  return (
    <>
      <Breadcrumbs>
        <Link href={`/scene/${cabinet.scene.name}`}>{cabinet.scene.displayName}</Link>
        <Link href={`/cabinet/${cabinet.scene.name}/${cabinet.name}`}>{cabinet.displayName}</Link>
      </Breadcrumbs>

      <Title title={`${cabinet.displayName}: Sign In`} />

      {cabinet.allowQrSignin && (
        <>
          {!user?.id && (
            <>
              <LoginButtons>Log in to continue</LoginButtons>
            </>
          )}

          <div className={classes.container}>
            <div className={classes.teams}>
              <div className={classes.blueTeam}>
                {POSITIONS_BY_TEAM[BLUE_TEAM].map(pos => (
                  <PlayerSignIn key={pos.ID} pos={pos} />
                ))}
              </div>

              <div className={classes.goldTeam}>
                {POSITIONS_BY_TEAM[GOLD_TEAM].map(pos => (
                  <PlayerSignIn key={pos.ID} pos={pos} />
                ))}
              </div>
            </div>
          </div>
        </>
      )}

      {!cabinet.allowQrSignin && (
        <Typography>This cabinet is not configured to allow sign-ins.</Typography>
      )}
    </>
  );
}

export async function getServerSideProps({ params }) {
  const axios = getAxios();
  let response = await axios.get(`/api/game/scene/`, { params: { name: params.sceneName } });
  const scene = response.data.results[0];

  if (!scene) return { notFound: true };

  response = await axios.get(`/api/game/cabinet/`, {
    params: { sceneId: scene.id, name: params.cabinetName },
  });
  const cabinet = response.data.results[0];

  if (!cabinet) return { notFound: true };

  cabinet.scene = scene;

  return {
    props: {
      cabinet,
      title: `${scene.displayName} - ${cabinet.displayName}`,
      description: cabinet.description,
    },
  };
}
