import { Breadcrumbs, Button, IconButton, Link, Paper, Typography } from '@material-ui/core';
import SettingsIcon from '@material-ui/icons/Settings';
import { mdiCalendar } from '@mdi/js';
import PropTypes from 'prop-types';
import { getAxios } from 'util/axios';

import Markdown from 'components/Markdown';
import { SummaryTable, SummaryTableRow } from 'components/tables/SummaryTable';
import TournamentTeamTable from 'components/tables/TournamentTeamTable';
import { GroupResultsTables } from 'components/tournament/group-results';
import Title from 'components/Title';
import { useAuth } from 'util/auth';
import { formatUTC } from 'util/dates';
import { TOURNAMENT_LINK_TYPES } from 'util/constants';
import { TournamentAdminProvider, MenuIcon } from '@/components/tournament-admin';
import { TournamentLinks } from '@/components/tournament';
import styles from './[tournamentSlug].module.css';

export default function TournamentPage({ tournament }) {
  const { isAdminOf } = useAuth();

  const buttons = (
    <div className="space-x-4">
      <Button variant="contained" color="secondary" href={`/tournament/${tournament.id}/register`}>
        Register
      </Button>
      <Button variant="contained" color="secondary" href={`/tournament/${tournament.id}/dashboard`}>
        Dashboard
      </Button>
      {tournament.awards && (
        <Button variant="contained" color="secondary" href={`/tournament/${tournament.id}/awards`}>
          Awards
        </Button>
      )}

      <MenuIcon tournament={tournament} />
    </div>
  );

  return (
    <TournamentAdminProvider id={tournament.id}>
      <div className="space-y-8">
        <Breadcrumbs>
          <Link href={`/scene/${tournament.scene.name}`}>{tournament.scene.displayName}</Link>
        </Breadcrumbs>

        <Title title={tournament.name} form={buttons} />

        <Paper className={styles.description}>
          <div className={styles.summaryLinks}>
            <SummaryTable>
              <SummaryTableRow icon={mdiCalendar} label="Start Date">
                {formatUTC(tournament.date, 'MMM d, yyyy')}
              </SummaryTableRow>
            </SummaryTable>
          </div>

          <Markdown className={styles.descriptionText} contents={tournament.description} />

          <TournamentLinks tournament={tournament} />
        </Paper>

        {tournament.teams.length > 0 && (
          <TournamentTeamTable title="Teams" tournament={tournament} />
        )}

        {tournament.brackets.some(b => b.matches.length > 0) && (
          <>
            <GroupResultsTables tournament={tournament} />
          </>
        )}
      </div>
    </TournamentAdminProvider>
  );
}

TournamentPage.propTypes = {
  tournament: PropTypes.object.isRequired,
};

export async function getServerSideProps({ params }) {
  const axios = getAxios();
  let response = await axios.get(`/api/game/scene/`, { params: { name: params.sceneName } });
  const scene = response.data?.results?.[0];

  if (!scene) return { notFound: true };

  response = await axios.get(`/api/tournament/tournament/`, {
    params: { sceneId: scene.id, slug: params.tournamentSlug },
  });
  const tournament = response.data?.results?.[0];

  if (!tournament) return { notFound: true };

  tournament.scene = scene;

  tournament.teams = await axios.getAllPages(`/api/tournament/team/`, {
    params: { tournamentId: tournament.id },
  });

  response = await axios.get(`/api/tournament/bracket/`, {
    params: { tournamentId: tournament.id },
  });
  tournament.brackets = [];

  for (const bracket of response.data.results) {
    bracket.matches = await axios.getAllPages(`/api/tournament/match/`, {
      params: { bracketId: bracket.id },
    });
    bracket.matches.sort((a, b) => a?.roundName?.localeCompare(b?.roundName) || a.id - b.id);
    tournament.brackets.push(bracket);
  }

  tournament.links = await axios.getAllPages(`/api/tournament/links/`, {
    params: { tournament: tournament.id },
  });

  return {
    props: { tournament, title: tournament.name, description: tournament.description },
  };
}
