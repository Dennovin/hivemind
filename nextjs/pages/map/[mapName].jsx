import React from 'react';
import { getAxios } from 'util/axios';

import { MapInfoPage } from '@/components/map-info';

export default function Page({ map, heatmaps }) {
  return <MapInfoPage {...{ map, heatmaps }} />;
}

export async function getServerSideProps({ params }) {
  const axios = getAxios();

  let response = await axios.get(`/api/game/map/`, { params: { name: `map_${params.mapName}` } });
  const map = response.data?.results?.[0];

  if (!map) return { notFound: true };

  response = await axios.get(`/api/game/map/${map.id}/heatmaps/`);
  const heatmaps = {};
  for (const heatmap of response.data) {
    heatmaps[heatmap.eventType] = heatmap;
  }

  return {
    props: { map, heatmaps, title: `${map.displayName} Map` },
  };
}
