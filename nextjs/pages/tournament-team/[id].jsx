import { makeStyles } from '@material-ui/core/styles';
import { Typography, Breadcrumbs, Link, Grid } from '@material-ui/core';
import { getAxios } from 'util/axios';
import { format } from 'date-fns';
import PropTypes from 'prop-types';
import clsx from 'clsx';

import { formatUTC } from 'util/dates';
import PlayerCard from 'components/PlayerCard';
import TournamentMatchListTable from 'components/tables/TournamentMatchListTable';
import RecordByMapTable from 'components/tables/RecordByMapTable';
import TournamentTeamStatsTable from 'components/tables/TournamentTeamStatsTable';

const useStyles = makeStyles(theme => ({}));

export default function TeamPage({ team }) {
  const classes = useStyles();

  return (
    <>
      <Breadcrumbs>
        <Link href={`/scene/${team.tournament.scene.name}`}>
          {team.tournament.scene.displayName}
        </Link>
        <Link href={`/t/${team.tournament.sceneName}/${team.tournament.slug}`}>
          {team.tournament.name}
        </Link>
        <Link href={`/tournament-team/${team.id}`}>{team.name}</Link>
      </Breadcrumbs>

      <Typography variant="h1">{team.name}</Typography>

      {team?.players.length > 0 && (
        <>
          <Grid container spacing={2}>
            {team.players
              .sort((a, b) => a.name.localeCompare(b.name))
              .map(player => (
                <PlayerCard
                  key={player.id}
                  player={player}
                  href={`/tournament-player/${player.id}`}
                />
              ))}
          </Grid>

          <TournamentTeamStatsTable team={team} />
        </>
      )}

      {team.tournament.brackets.some(b => b.matches.length > 0) && (
        <>
          <RecordByMapTable title="Record By Map" team={team} />
          <TournamentMatchListTable
            title="Matches"
            data={team.matches}
            tournament={team.tournament}
          />
        </>
      )}
    </>
  );
}

export async function getServerSideProps({ params }) {
  const axios = getAxios();
  let response = await axios.get(`/api/tournament/team/${params.id}`);
  const team = response.data;

  if (!team) return { notFound: true };

  team.matches = {};

  response = await axios.get(`/api/tournament/tournament/${team.tournament}`);
  team.tournament = response.data;

  response = await axios.get(`/api/game/scene/${team.tournament.scene}`);
  team.tournament.scene = response.data;

  team.tournament.brackets = await axios.getAllPages(`/api/tournament/bracket/`, {
    params: { tournamentId: team.tournament.id },
  });

  team.tournament.teams = await axios.getAllPages(`/api/tournament/team/`, {
    params: { tournamentId: team.tournament.id },
  });

  team.players = await axios.getAllPages(`/api/tournament/player/`, {
    params: { teamId: team.id },
  });

  const allMatches = await axios.getAllPages('/api/tournament/match/', {
    params: { teamId: team.id },
  });
  for (const bracket of team.tournament.brackets) {
    bracket.matches = allMatches.filter(match => match.bracket == bracket.id);
  }

  return {
    props: {
      team,
      title: '',
      description: `${team.name}, a team playing in ${team.tournament.name} in ${team.tournament.scene.displayName}.`,
    },
  };
}
