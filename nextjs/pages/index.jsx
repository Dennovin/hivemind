import { Button, CircularProgress, Grid, Typography } from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';
import { useEffect, useState } from 'react';

import { Leaderboards } from '@/components/leaderboards';
import SessionStatsBox from 'components/SessionStatsBox';
import GamesByWeekChart from 'components/charts/GamesByWeekChart';
import WinConditionsChart from 'components/charts/WinConditionsChart';
import SceneSelector from 'components/forms/SceneSelector';
import GameTable from 'components/tables/GameTable';
import SceneTable from 'components/tables/SceneTable';
import UserScenesTable from 'components/tables/UserScenesTable';
import UpcomingTournamentsTable from 'components/tables/UpcomingTournamentsTable';
import { useUser } from 'util/auth';
import { getAxios } from 'util/axios';

export default function Index() {
  const requestUser = useUser();
  const [user, setUser] = useState(null);

  useEffect(() => {
    async function getUserInfo() {
      if (requestUser?.id) {
        const axios = getAxios({ authenticated: true });
        const response = await axios.get(`/api/user/user/${requestUser.id}/`);
        setUser(response.data);
      }
    }
    getUserInfo();
  }, [requestUser?.id]);

  const now = new Date();
  const recapAvailable = now.getUTCMonth() == 0;
  const recapYear = now.getUTCFullYear() - 1;

  return (
    <>
      <header className="flex justify-between items-center gap-4 flex-wrap md:flex-nowrap">
        <Typography variant="h1" element="h1" className="flex-grow">
          HiveMind: Stats for Killer Queen
        </Typography>

        <Button href="https://patreon.com/kqhivemind" variant="outlined" color="secondary">
          Support HiveMind
        </Button>
      </header>

      {user && !Boolean(user?.name) && (
        <Alert severity="error" className="items-center content-stretch">
          <h3 className="my-0 flex justify-between items-center w-full">
            <span>Your profile is missing your name! Edit your profile to add one.</span>
            <Button
              className=" ml-8"
              size="small"
              variant="outlined"
              color="secondary"
              href={`/user/${user.id}`}
            >
              Edit Profile
            </Button>
          </h3>
        </Alert>
      )}

      {recapAvailable && (
        <Typography variant="h2" className="p-6 bg-blue-light3 rounded">
          {requestUser?.id && (
            <>
              Happy New Year{requestUser.name && `, ${requestUser.name}`}! The{' '}
              <a href={`/recap/${recapYear}`}>HiveMind {recapYear} recap</a> and{' '}
              <a href={`/user/${requestUser.id}/recap/${recapYear}`}>
                your personal {recapYear} recap
              </a>{' '}
              are now available.
            </>
          )}
          {!requestUser?.id && (
            <>
              Happy New Year! The <a href={`/recap/${recapYear}`}>HiveMind {recapYear} recap</a> is
              now available. You can also sign in to see your personal {recapYear} recap!
            </>
          )}
        </Typography>
      )}

      {!recapAvailable && requestUser?.id && (
        <Typography variant="h2" className="p-6 bg-blue-light3 rounded">
          {requestUser.name ? `Welcome Back, ${requestUser.name}` : 'Welcome Back'}
        </Typography>
      )}

      <Grid container spacing={4} style={{ marginTop: '1rem' }}>
        <Grid item xs={12} md={7} className="space-y-8">
          {requestUser?.id && (
            <>
              {user === null && <CircularProgress />}
              {user !== null && (
                <>
                  {user.userScenes && (
                    <UserScenesTable title="Your Scenes" scenes={user.userScenes} />
                  )}
                </>
              )}
            </>
          )}

          <UpcomingTournamentsTable title="Current & Upcoming Tournaments" />

          <SceneTable title="All Scenes" />

          {requestUser && !requestUser.id && (
            <>
              <GamesByWeekChart />
              <WinConditionsChart />
            </>
          )}
        </Grid>
        <Grid item xs={12} md={5} className="space-y-8">
          <Leaderboards />
          <GameTable title="Recent Games" rowsPerPage={10} />
        </Grid>
      </Grid>
    </>
  );
}

export async function getServerSideProps({ params }) {
  return { props: {} };
}
