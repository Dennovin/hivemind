import { makeStyles } from '@material-ui/core/styles';
import { Link } from '@material-ui/core';

import { P, H2, List, ListItem } from 'components/Layout';
import Title from 'components/Title';

const useStyles = makeStyles(theme => ({
  paper: {
    padding: theme.spacing(1),
  },
}));

export default function PrivacyPage() {
  const classes = useStyles();
  return (
    <>
      <Title title="HiveMind Privacy Policy" />

      <P>
        This is a description of what data HiveMind collects and when, and who that data is
        available to. By using the site, you agree to the collection and use of information in
        accordance with this policy. This page was last updated on 9 May 2023.
      </P>

      <H2>For All Website Visitors</H2>

      <P>
        When visiting the website, we collect information that your browser sends whenever you visit
        our site. This may include information such as your computer's IP address, browser type,
        browser version, the pages you visit, the time and date of your visit, the time spent on
        those pages, and other statistics. This data is only available to HiveMind staff and is used
        to diagnose errors and improve service.
      </P>

      <P>
        We use Google Analytics to collect information about how users interact with our website.
        Google Analytics collects information such as your IP address, the type of device you are
        using, and the pages you visit. We use this information to improve our website and
        understand how users are engaging with our content. Google Analytics may also use cookies
        and similar technologies to collect and store information about your usage of the site.
      </P>

      <P>
        You can opt-out of Google Analytics by installing the{' '}
        <Link href="https://tools.google.com/dlpage/gaoptout">
          Google Analytics Opt-out Browser Add-on
        </Link>
        , which prevents your data from being collected and used by Google Analytics.
      </P>

      <P>
        For more information about how Google Analytics collects and uses data, please review{' '}
        <Link href="https://policies.google.com/privacy?hl=en-US">Google's Privacy Policy</Link>.
      </P>

      <H2>When You Sign In</H2>

      <P>
        We use third-party systems such as Google's OAuth to allow users to sign in to our website.
        When you sign in with these systems, we may collect certain information about your account,
        such as your name and email address. We use this information to create and maintain your
        account on our website.
      </P>

      <P>
        We do not collect your password or any other sensitive information from your linked account.
        We only use the information provided by the authentication system to authenticate you and
        provide you with access to our website.
      </P>

      <P>
        For more information about how these third parties handle your information when you sign in,
        please review their Privacy Policy:
      </P>

      <List>
        <ListItem>
          <Link href="https://policies.google.com/privacy?hl=en-US">Google</Link>
        </ListItem>
        <ListItem>
          <Link href="https://discord.com/privacy">Discord</Link>
        </ListItem>
        <ListItem>
          <Link href="https://www.twitch.tv/p/en/legal/privacy-notice/">Twitch</Link>
        </ListItem>
        <ListItem>
          <Link href="https://privacy.patreon.com/">Patreon</Link>
        </ListItem>
        <ListItem>
          <Link href="https://challonge.com/privacy_policy">Challonge</Link>
        </ListItem>
      </List>

      <H2>Your User Profile</H2>

      <P>
        When you create an account, you can add personal information to your user profile. If you
        add your name, picture, home scene, and pronouns, these are always visible on your user
        profile, and may be shown on video streams if you are signed in.
      </P>

      <P>
        By default, your statistics, games played, and achievements are only shown to you. If you
        choose to make your user profile public, it will be shown on your public profile page.
      </P>

      <P>
        If you would like to delete your user profile and all associated user data, please contact
        an administrator at <Link href="mailto:contact@kqhivemind.com">contact@kqhivemind.com</Link>{' '}
        or in the <b>#hivemind</b> channel on Killer Queen Discord.
      </P>

      <H2>Game Data and Public API</H2>

      <P>
        When a game system is connected to our service, we collect all events provided by the Killer
        Queen system's websocket interface. These events are always public and available on our
        website, through our API, and on any video stream that uses our API.
      </P>

      <P>
        Additional aggregated data calculated using these events, such as total statistics, game
        times, and players who are signed in, are also available through our site and the API.
      </P>

      <P>
        We cannot control how third parties use data from our public API, and we do not assume
        responsibility for the actions of these third parties.
      </P>

      <H2>NFC Cards</H2>

      <P>
        When you register a NFC card, we store the serial number of the card you registered. This is
        used to associate each NFC card with its respective user account. We do not share this
        information with third parties.
      </P>

      <H2>Phone Numbers</H2>

      <P>
        If you choose to enable SMS notifications, your phone number will be stored on our servers.
        This will only be used for notifications and will not be sold or shared with a third party
        for any other purpose. You may opt out from text messages at any time by removing your phone
        number from your user profile, or by replying "STOP" to any message.
      </P>

      <H2>Tournament Registration and Payments</H2>

      <P>
        When you register for a tournament, any information you provide is made available to the
        tournament organizer. This includes any additional custom questions that the tournament
        organizers may request. Any picture you provide during registration is always available to
        tournament organizers, and may be shown on video streams unless you choose to hide it.
      </P>

      <P>
        We use Stripe, a third-party payment processor, to process payments for registration and
        other services. When you make a payment through our website, we may collect certain
        information about you, such as your name, email address, and payment information, including
        your credit card number, expiration date, and billing address.
      </P>

      <P>
        We use this information to process your payment and complete your registration. We do not
        store your credit card information on our servers. All credit card transactions are
        processed through Stripe's secure servers.
      </P>

      <P>
        We take measures to secure and protect the information we collect through our Stripe
        integration. We do not share your payment information with third parties, except where
        required by law. For more information about how Stripe handles your information, please
        review <Link href="https://stripe.com/privacy">their Privacy Policy</Link>.
      </P>

      <H2>Data Security</H2>

      <P>
        We use industry-standard security measures to protect the confidentiality, integrity, and
        availability of your personal information. These measures include:
      </P>

      <List>
        <ListItem>
          Encryption of data in transit: We use Transport Layer Security (TLS) encryption to protect
          data transmitted between our servers and your device.
        </ListItem>
        <ListItem>
          Sensitive data: No user passwords or credit card information are ever stored on our
          servers.
        </ListItem>
        <ListItem>
          Regular software updates: We keep our software up to date with the latest security patches
          and updates.
        </ListItem>
        <ListItem>
          Access control: We restrict access to personal information to authorized personnel only,
          and require them to maintain the confidentiality of the information.
        </ListItem>
        <ListItem>
          Data backups: We regularly back up our data to ensure that it can be restored in case of a
          data loss incident.
        </ListItem>
      </List>

      <P>
        In the event of a data breach, we will notify affected users as soon as possible, and take
        all necessary steps to mitigate the impact of the breach.
      </P>

      <P>
        If you have any questions about our data security practices or suspect that your personal
        information has been compromised, please contact us immediately.
      </P>

      <H2>Data Retention</H2>

      <P>
        All game data and events will be retained for as long as the service is active. All user
        data is retained for as long as the user has an account, and can be deleted on request.
      </P>

      <H2>Contact Us</H2>

      <P>
        HiveMind administrators can be contacted at{' '}
        <Link href="mailto:contact@kqhivemind.com">contact@kqhivemind.com</Link> or in the{' '}
        <b>#hivemind</b> channel on Killer Queen Discord.
      </P>
    </>
  );
}
