import { makeStyles } from '@material-ui/core/styles';
import { useTheme } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  container: {
    margin: theme.spacing(0, 0, 2, 2),
  },
  containerTitle: {
    fontSize: '14px',
    marginLeft: '22px',
  },
  color: {
    border: `1px solid ${theme.palette.divider}`,
    height: '14px',
    width: '14px',
    display: 'inline-block',
    verticalAlign: 'middle',
    marginRight: theme.spacing(1),
  },
  text: {
    fontSize: '14px',
  },
}));

export default function Palette() {
  const classes = useStyles();
  const theme = useTheme();

  const Colors = ({ data, prefix }) => {
    if (typeof data == 'object') {
      return (
        <>
          <div className={classes.containerTitle}>{prefix}</div>
          <div className={classes.container}>
            {Object.keys(data).sort().map(key => (
              <Colors key={key} data={data[key]} prefix={`${prefix}.${key}`} />
            ))}
          </div>
        </>
      );
    } else {
      return (
        <div className={classes.entry}>
          <div className={classes.color} style={{ backgroundColor: data }} />
          <span className={classes.text}>{prefix}</span>
        </div>
      );
    }
  };

  return (
    <Colors data={theme.palette} prefix="theme.palette" />
  );
}
