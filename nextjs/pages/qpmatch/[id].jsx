import { makeStyles } from '@material-ui/core/styles';
import { Typography, Breadcrumbs, Link, Grid } from '@material-ui/core';
import { getAxios } from 'util/axios';
import { format } from 'date-fns';
import PropTypes from 'prop-types';
import clsx from 'clsx';

import { formatUTC } from 'util/dates';
import QPMatchResultTable from 'components/tables/QPMatchResultTable';
import { GameSummaryTableList } from 'components/tables/GameSummaryTable';

const useStyles = makeStyles(theme => ({
}));

export default function QPMatchPage({ match }) {
  const classes = useStyles({ match });

  return (
    <>
      <Breadcrumbs>
        <Link href={`/scene/${match.event.season.scene.name}`}>{match.event.season.scene.displayName}</Link>
        <Link href={`/season/${match.event.season.id}`}>{match.event.season.name}</Link>
        <Link href={`/event/${match.event.id}`}>{format(new Date(match.event.eventDate), 'MMMM d, yyyy')}</Link>
        <Link href={`/qpmatch/${match.id}`}>Match #{match.matchNumber}</Link>
      </Breadcrumbs>

      <Typography variant="h1">Quickplay Match #{match.matchNumber}</Typography>

      <Typography variant="h2">Match Results</Typography>
      <QPMatchResultTable match={match} />

      <Typography variant="h2">Game Results</Typography>

      {match.games.length === 0 && (
        <Typography className={classes.noGames}>Game details not available for this match.</Typography>
      )}

      {match.games.length > 0 && (
        <GameSummaryTableList games={match.games} />
      )}
    </>
  );
}

export async function getServerSideProps({ params }) {
  const axios = getAxios();
  let response = await axios.get(`/api/league/qpmatch/${params.id}`);
  const match = response.data;

  if (!match) return { notFound: true };

  match.games = [];
  response = await axios.get('/api/game/game/', { params: { qpMatchId: match.id } });

  for (const game of response.data.results) {
    response = await axios.get(`/api/game/game/${game.id}/stats`);
    match.games.push(response.data);
  }

  response = await axios.get(`/api/league/qpmatch/${match.id}/players`);
  match.players = response.data;

  response = await axios.get(`/api/league/event/${match.event}`);
  match.event = response.data;

  response = await axios.get(`/api/league/season/${match.event.season}`);
  match.event.season = response.data;

  response = await axios.get(`/api/game/scene/${match.event.season.scene}`);
  match.event.season.scene = response.data;

  return {
    props: {
      match,
      title: `Quickplay Match #${match.matchNumber}`,
      description: `A quickplay match on ${formatUTC(match.event.eventDate, 'MMMM d, yyyy')}, an event in ${match.event.season.name} in ${match.event.season.scene.displayName}.`,

    },
  };
}

