import { makeStyles } from '@material-ui/core/styles';
import { Typography, Paper, Breadcrumbs, Link, Grid, Box } from '@material-ui/core';
import { getAxios } from 'util/axios';
import { format } from 'date-fns';
import PropTypes from 'prop-types';
import clsx from 'clsx';

import PlayerResultsTable from 'components/tables/PlayerResultsTable';

const useStyles = makeStyles(theme => ({
}));

export default function PlayerPage({ player }) {
  const classes = useStyles({ player });

  return (
    <>

      <Breadcrumbs>
        <Link href={`/scene/${player.scene.name}`}>{player.scene.displayName}</Link>
        <Link href={`/player/${player.id}`}>{player.name}</Link>
      </Breadcrumbs>
      <Typography variant="h1">{player.name}</Typography>

      <PlayerResultsTable player={player} />
    </>
  );
}

PlayerPage.propTypes = {
  player: PropTypes.object.isRequired,
};

export async function getServerSideProps({ params }) {
  const axios = getAxios();
  let response = await axios.get(`/api/league/player/${params.id}`);
  const player = response.data;

  if (!player) return { notFound: true };

  response = await axios.get(`/api/league/player/${params.id}/results`);
  player.results = response.data;

  response = await axios.get(`/api/game/scene/${player.scene}/`);
  player.scene = response.data;

  return {
    props: { player, title: player.name },
  };
}

