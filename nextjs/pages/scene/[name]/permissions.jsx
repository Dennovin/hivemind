import React from 'react';

import { ScenePermissionsProvider, ScenePermissionsPage } from '@/components/scene-permissions';
import { getAxios } from '@/util/axios';

export default function ScenePermissionComponentPage({ scene }) {
  return (
    <ScenePermissionsProvider scene={scene}>
      <ScenePermissionsPage />
    </ScenePermissionsProvider>
  );
}

export async function getServerSideProps({ params }) {
  const axios = getAxios();
  const response = await axios.get(`/api/game/scene`, { params: { name: params.name } });
  const scene = response.data.results[0];

  if (!scene) return { notFound: true };

  return {
    props: { scene, title: scene.displayName, description: scene.description },
  };
}
