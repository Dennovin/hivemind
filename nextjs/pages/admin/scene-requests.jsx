import React from 'react';

import Title from 'components/Title';
import SceneRequestTable from 'components/tables/SceneRequestTable';

export default function EventPage({ user, initialEvent }) {
  return (
    <>
      <Title title="New Scene Registrations" />

      <SceneRequestTable />
    </>
  );
}
