import React from 'react';
import { Typography } from '@material-ui/core';
import { useRouter } from 'next/router';

import Title from 'components/Title';
import LoginButtons from 'components/LoginButtons';
import ClientDevicesTable from 'components/tables/ClientDevicesTable';
import { useUser } from 'util/auth';

export default function DevicesPage() {
  const user = useUser();
  const router = useRouter();

  return (
    <div className="space-y-8">
      <Title title="Manage Client Devices" />

      {user?.id && <ClientDevicesTable />}
      {!user?.id && (
        <>
          <LoginButtons>Log in to continue</LoginButtons>
        </>
      )}
    </div>
  );
}
