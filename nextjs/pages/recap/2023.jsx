import React, { useState, useEffect, useMemo } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { CircularProgress } from '@material-ui/core';
import { format } from 'date-fns';

import { mapColor } from 'theme/colors';
import { getAxios } from 'util/axios';
import { formatPercent } from 'util/formatters';
import {
  Recap,
  RecapItem,
  RecapCaption,
  RecapMainItem,
  RecapSubItem,
  RecapList,
  RecapListItem,
  RecapBarChart,
  RecapPieChart,
} from 'components/recap';
import SceneColorBox from 'components/SceneColorBox';

export default function RecapPage() {
  const axios = getAxios();
  const [recapResponse, setRecapResponse] = useState(null);

  const getRecap = () => {
    if (recapResponse === null) {
      axios.get(`/api/stats/recap/2023/`).then(response => {
        if (response.data.success) {
          setRecapResponse(response.data);
        } else {
          setTimeout(getRecap, 1000);
        }
      });
    }
  };

  useEffect(() => {
    getRecap();
  }, []);

  const byMap = useMemo(() => {
    if (!recapResponse?.data?.byMap) return null;
    const gamesByMap = [];
    let bonusGames = 0;

    for (const row of recapResponse.data.byMap) {
      if (row.name in mapColor && !row.betaOf) {
        gamesByMap.push({
          name: row.name,
          value: row.games,
          color: mapColor[row.name],
        });
      }

      if (row.isBonus) {
        bonusGames += row.games;
      }
    }

    gamesByMap.push({
      name: 'Bonus Maps',
      value: bonusGames,
      color: '#8f8f8f',
    });

    return gamesByMap;
  }, [recapResponse?.data?.byMap]);

  if (recapResponse === null) {
    return <CircularProgress />;
  }

  if (!recapResponse.success) {
    return <>Could not retrieve recap: {recapResponse.error}</>;
  }

  const recapData = recapResponse.data;

  const colorByMap = mapName => mapColor[mapName];

  return (
    <Recap title="HiveMind 2023 Recap">
      <RecapItem>
        <RecapMainItem>
          <b>{recapData.games.total.toLocaleString()}</b> total games were recorded
        </RecapMainItem>

        <RecapSubItem>
          That's over <b>{recapData.games.gameTimeDays.toLocaleString()}</b> days of game time.
        </RecapSubItem>

        <RecapList title="Most Games by Scene">
          {recapData.games.topScenes.map(scene => (
            <RecapListItem
              key={scene.scene}
              name={
                <>
                  <SceneColorBox color={scene.backgroundColor} /> {scene.scene}
                </>
              }
              value={scene.games}
            />
          ))}
        </RecapList>

        <RecapList title={`${recapData.newScenes.length} scenes recorded their first game`}>
          {recapData.newScenes.map(scene => (
            <RecapListItem
              key={scene.id}
              style={{ width: '50%' }}
              name={
                <>
                  <SceneColorBox color={scene.backgroundColor} /> {scene.name}
                </>
              }
              value={format(new Date(scene.firstGame), 'MMM d')}
            />
          ))}
        </RecapList>
      </RecapItem>

      <RecapItem>
        <RecapSubItem>
          <b>{recapData.games.scenes}</b> scenes logged games on <b>{recapData.games.cabinets}</b>{' '}
          cabinets.
        </RecapSubItem>

        <RecapSubItem>
          The busiest month was <b>{recapData.games.topMonth.month}</b>, with{' '}
          <b>{recapData.games.topMonth.count.toLocaleString()}</b> games recorded.
        </RecapSubItem>

        <RecapBarChart
          title="Games by Month"
          data={recapData.byMonth.data}
          stacks={recapData.byMonth.scenes}
          labelKey="month"
          stackLabelKey="name"
          stackColorKey="backgroundColor"
        />
      </RecapItem>

      <RecapItem>
        <RecapSubItem>
          <b>{recapData.games.topMap.count.toLocaleString()}</b> games were played on the most
          popular map, <b>{recapData.games.topMap.name}</b>.
        </RecapSubItem>

        <RecapSubItem>
          Twilight was only played{' '}
          <b>
            {recapData.byMap
              .filter(i => i.name === 'Twilight')
              .map(i => i.games)
              .reduce((a, b) => a + b, 0)
              .toLocaleString()}
          </b>{' '}
          times.
          <RecapCaption>I'm not mad, I'm just disappointed.</RecapCaption>
        </RecapSubItem>

        {byMap && (
          <RecapPieChart
            title="Games by Map"
            data={byMap}
            getLabel={i => i.name}
            getValue={i => i.value}
            getColor={i => i.color}
            getTooltipValue={i =>
              `${i.toLocaleString()} (${formatPercent(i / recapData.games.total, 1)})`
            }
          />
        )}
      </RecapItem>

      <RecapItem>
        <RecapMainItem>
          Users logged in for <b>{recapData.loggedIn.games.toLocaleString()}</b> games
        </RecapMainItem>

        <RecapSubItem>
          <b>{recapData.loggedIn.users.toLocaleString()}</b> different users logged in for at least
          one game.
        </RecapSubItem>

        <RecapSubItem>
          <b>{recapData.loggedIn.scenes.toLocaleString()}</b> different scenes had players sign in.
        </RecapSubItem>

        <RecapSubItem>
          Players registered <b>{recapData.loggedIn.nfcTags.toLocaleString()}</b> new NFC tags.
        </RecapSubItem>

        <RecapSubItem>
          Queens accounted for <b>{formatPercent(recapData.loggedIn.queenPct)}</b> of signed-in
          games.
        </RecapSubItem>
      </RecapItem>

      <RecapItem>
        <RecapMainItem>
          <b>{recapData.tournaments.tournaments}</b> tournaments were managed using HiveMind
        </RecapMainItem>

        <RecapSubItem>
          Tournaments accounted for <b>{recapData.tournaments.games.toLocaleString()}</b> of this
          year's games.
        </RecapSubItem>

        <RecapSubItem>
          <b>{recapData.tournaments.scenes}</b> scenes hosted at least one tournament.
        </RecapSubItem>

        <RecapSubItem>
          <b>{formatPercent(recapData.tournaments.signInPct)}</b> of players were signed in for
          their tournament games.
        </RecapSubItem>

        <RecapList title="Most Games by Tournament">
          {recapData.tournaments.topTournaments.map(tournament => (
            <RecapListItem
              key={tournament.name}
              name={
                <>
                  <SceneColorBox color={tournament.backgroundColor} /> {tournament.name}
                </>
              }
              value={tournament.games}
            />
          ))}
        </RecapList>
      </RecapItem>

      <RecapItem>
        <RecapMainItem>Thanks to the Patrons</RecapMainItem>

        <RecapSubItem>
          Subscribers on Patreon contributed <b>$2,698.19</b> to cover server costs.
        </RecapSubItem>

        <RecapSubItem>
          You can join these <b>24</b> awesome patrons at{' '}
          <a href="https://www.patreon.com/kqhivemind">patreon.com/kqhivemind</a>.
        </RecapSubItem>

        <RecapSubItem>
          Your donations allowed HiveMind to add extra server infrastructure, with monthly server
          costs increasing from <b>$88.50</b> in January to <b>$132.00</b> in December.
        </RecapSubItem>

        <RecapSubItem>
          <b>9</b> rounds of drinks, <b>1</b> round of ice cream, and <b>100</b> custom-printed NFC
          cards were purchased on the HiveMind Software LLC debit card.
        </RecapSubItem>
      </RecapItem>
    </Recap>
  );
}
