import DateFnsUtils from '@date-io/date-fns';
import { Paper } from '@material-ui/core';
import CssBaseline from '@material-ui/core/CssBaseline';
import { makeStyles, StylesProvider, ThemeProvider } from '@material-ui/core/styles';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import clsx from 'clsx';
import Head from 'next/head';
import PropTypes from 'prop-types';
import React, { useEffect, useMemo } from 'react';
import {
  defaults,
  Chart,
  ArcElement,
  CategoryScale,
  LinearScale,
  BarElement,
  PointElement,
  LineElement,
} from 'chart.js';
import { ToastContainer } from 'react-toastify';

import 'react-toastify/dist/ReactToastify.min.css';
import 'styles/global.css';

import Header from 'components/Header';
import Footer from 'components/Footer';
import theme from 'theme/theme';
import { UserProvider } from 'util/auth';

Chart.register(ArcElement, BarElement, CategoryScale, LinearScale, LineElement, PointElement);

const useStyles = makeStyles(theme => ({
  container: {
    maxWidth: '1400px',
    margin: '0 auto',
    borderWidth: '0 1px',
    borderRadius: '0',
    background: 'white',
    padding: theme.spacing(8),
    [theme.breakpoints.down('sm')]: {
      padding: theme.spacing(1),
    },
  },
}));

const Body = ({ Component, hideHeader, hideContainer, hideFooter, ...pageProps }) => {
  const classes = useStyles();

  if (hideHeader) {
    return <Component {...pageProps} />;
  }

  return (
    <>
      <Header />
      {hideContainer ? (
        <Component {...pageProps} />
      ) : (
        <div className="clouds py-8 px-0 md:p-12">
          <div
            className={clsx(
              classes.container,
              'min-h-[calc(100vh-50px)] md:min-h-0 p-4 md:p-10 pb-12 space-y-8 shadow-lg shadow-sky-500/40',
            )}
          >
            <Component {...pageProps} />
          </div>
        </div>
      )}
      {!hideFooter && <Footer />}
    </>
  );
};

export default function App(props) {
  const classes = useStyles();
  const { Component, pageProps } = props;

  // Chart.js defaults
  defaults.animation = false;
  defaults.font.family = 'Roboto';

  useEffect(() => {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector('#jss-server-side');
    if (jssStyles) {
      jssStyles.parentElement.removeChild(jssStyles);
    }
  }, []);

  const imageURL = pageProps?.image ?? 'https://kqhivemind.com/static/hivemind.png';
  const title = pageProps?.title
    ? pageProps.title + ' ⬡ HiveMind'
    : 'HiveMind: Stats for Killer Queen';
  const description =
    pageProps?.description ??
    'HiveMind is a free open-source stat tracker for Killer Queen Arcade.';

  if (Component.App) {
    return (
      <Component.App>
        <Component {...pageProps} />
      </Component.App>
    );
  }

  return (
    <React.Fragment>
      <Head>
        <title>{title}</title>
        <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width" />
        <meta property="og:image" content={imageURL} />
        <meta property="og:title" content={title} />
        <meta property="og:description" content={description} />
      </Head>
      <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <StylesProvider injectFirst>
          <ThemeProvider theme={theme}>
            <UserProvider>
              <CssBaseline />
              <Body Component={Component} {...pageProps} />
              <ToastContainer />
            </UserProvider>
          </ThemeProvider>
        </StylesProvider>
      </MuiPickersUtilsProvider>
    </React.Fragment>
  );
}

App.propTypes = {
  Component: PropTypes.elementType.isRequired,
  pageProps: PropTypes.object.isRequired,
};
