import React, { useEffect } from 'react';
import { useRouter } from 'next/router';

import { useAuth } from '@/util/auth';
import { getAxios } from '@/util/axios';

export default function PatreonAuthPage() {
  const router = useRouter();
  const axios = getAxios({ authenticated: true });

  const { user } = useAuth();

  useEffect(() => {
    if (!router?.query || !user?.id) return;

    axios
      .post('/api/user/exchange-token/patreon/', {
        code: router.query.code,
        state: router.query.state,
      })
      .then(() => {
        router.push(`/user/${user.id}`);
      });
    console.log(router?.query, user?.id);
  }, [router?.query, user?.id]);
  return null;
}
