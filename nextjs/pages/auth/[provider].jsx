import React, { useEffect } from 'react';
import { useRouter } from 'next/router';
import { clearAuthTokens, isLoggedIn, setAuthTokens } from 'axios-jwt';

import { useAuth } from '@/util/auth';
import { getAxios } from '@/util/axios';

export default function GenericAuthPage({ provider }) {
  const router = useRouter();
  const axios = getAxios({ authenticated: true });
  const { user, reloadUser } = useAuth();

  useEffect(() => {
    if (!router?.query?.code) return;

    axios
      .post(`/api/user/exchange-token/${provider}/`, {
        code: router.query.code,
      })
      .then(async response => {
        if (!user) {
          await setAuthTokens({
            accessToken: response.data.accessToken,
            refreshToken: response.data.refreshToken,
          });
        }

        await reloadUser();

        if (router.query.state) {
          router.push(router.query.state);
        }
      });
  }, [router?.query]);
  return null;
}

export async function getServerSideProps({ params }) {
  return {
    props: {
      provider: params.provider,
    },
  };
}
