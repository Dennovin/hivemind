import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Paper } from '@material-ui/core';

import { getAxios } from 'util/axios';
import Title from 'components/Title';
import WikiMarkdown from 'components/WikiMarkdown';
import WikiPageForm from 'components/forms/WikiPageForm';

const loadPage = async slug => {
  const axios = getAxios();
  let response = await axios.get(`/api/wiki/page/`, { params: { slug } });
  const page = response.data.results[0] ?? null;

  return page;
};

const useStyles = makeStyles(theme => ({
  markdown: {
    '& h1': {
      fontSize: '28px',
    },
    '& h2': {
      fontSize: '20px',
    },
    '& code': {
      fontSize: '14px',
      border: `1px solid ${theme.palette.divider}`,
      backgroundColor: theme.palette.action.selected,
      padding: theme.spacing(0, 1),
    },
    '& pre': {
      fontSize: '14px',
      border: `1px solid ${theme.palette.divider}`,
      backgroundColor: theme.palette.action.selected,
      padding: theme.spacing(1),
      boxShadow: '3px 3px 2px rgba(0, 0, 0, 0.5)',
    },
    '& pre code': {
      border: 'none',
      backgroundColor: 'transparent',
      padding: 0,
    },
  },
}));

export default function WikiPage({ page, slug }) {
  const classes = useStyles();
  const [revision, setRevision] = useState(page?.current ?? {});
  revision.slug = slug;

  const reload = () => {
    loadPage(slug).then(newPage => setRevision(newPage?.current ?? {}));
  };

  const form = (<WikiPageForm page={revision} onSave={reload} />);

  return (
    <>
      <Title title={revision.title ?? 'New Page'} form={form} />

      <WikiMarkdown
        className={classes.markdown}
        contents={revision.pageText ?? 'This page has not yet been created.'}
      />
    </>
  );
}

export async function getServerSideProps({ params }) {
  const page = await loadPage(params.slug);

  return {
    props: { page, slug: params.slug, title: page?.current?.title || null },
  };
}

