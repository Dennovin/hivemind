import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Typography, Breadcrumbs, Link } from '@material-ui/core';
import { format } from 'date-fns';
import PropTypes from 'prop-types';

import { isAdminOf } from 'util/auth';
import { getAxios } from 'util/axios';
import { SEASON_TYPES } from 'util/constants';
import { formatUTC } from 'util/dates';
import { useWebSocket } from 'util/websocket';
import Title from 'components/Title';
import EventStandingsTable from 'components/tables/EventStandingsTable';
import EventPlayersTable from 'components/tables/EventPlayersTable';
import QPEventStandingsTable from 'components/tables/QPEventStandingsTable';
import MatchListTable from 'components/tables/MatchListTable';
import QPMatchListTable from 'components/tables/QPMatchListTable';
import QPMatchResultTable from 'components/tables/QPMatchResultTable';
import EventForm from 'components/forms/EventForm';
import EventActiveToggleButton from 'components/forms/EventActiveToggleButton';

const useStyles = makeStyles(theme => ({
}));

async function loadEvent(id) {
  const axios = getAxios();
  let response = await axios.get(`/api/league/event/${id}`);
  const event = response.data;

  if (!event) return { notFound: true };

  response = await axios.get(`/api/league/event/${id}/standings`);
  event.standings = response.data;

  const matchesUrl = event.isQuickplay ? '/api/league/qpmatch/' : '/api/league/match/';
  response = await axios.get(matchesUrl, { params: { eventId: event.id } });
  event.matches = response.data.results;

  if (event.isQuickplay) {
    for (const match of event.matches) {
      response = await axios.get(`/api/league/qpmatch/${match.id}/players`);
      match.players = response.data;
    }
  }

  event.unassignedPlayers = await axios.getAllPages('/api/league/player/', { params: { eventId: event.id } });

  response = await axios.get(`/api/league/season/${event.season}`);
  event.season = response.data;

  response = await axios.get(`/api/game/scene/${event.season.scene}`);
  event.season.scene = response.data;

  event.canStart = false;

  if (event.season.seasonType == SEASON_TYPES.SHUFFLE && event.isQuickplay && event.unassignedPlayers.length > 0) {
    event.canStart = true;
  }
  if (event.season.seasonType == SEASON_TYPES.SHUFFLE && !event.isQuickplay && event.matches.length > 0 && !event.isComplete) {
    event.canStart = true;
  }
  if (event.season.seasonType == SEASON_TYPES.BYOT && event.matches.length > 0 && !event.isComplete) {
    event.canStart = true;
  }

  return event;
}

export default function EventPage({ initialEvent }) {
  const [event, setEvent] = useState(initialEvent);
  const classes = useStyles({ event });
  const form = (<EventForm event={event} />);

  const completedMatches = event.matches.filter(match => match.isComplete);
  const remainingMatches = event.matches.filter(match => !match.isComplete);
  const title = event.name ?? formatUTC(event.eventDate, 'MMMM d, yyyy');

  const onOpen = () => {
    ws.sendJsonMessage({ scene_name: event.season.scene.name });
  };

  const reload = async () => {
    const response = await loadEvent(event.id);
    setEvent(response);
  };

  const ws = useWebSocket('/ws/gamestate', { onOpen });
  ws.onJsonMessage(message => {
    if (message.type == 'gameend') {
      reload();
    }
  });

  return (
    <>
      <Breadcrumbs>
        <Link href={`/scene/${event.season.scene.name}`}>{event.season.scene.displayName}</Link>
        <Link href={`/season/${event.season.id}`}>{event.season.name}</Link>
        <Link href={`/event/${event.id}`}>
          {formatUTC(event.eventDate, 'MMMM d, yyyy')} - Event {event.eventNumber}
        </Link>
      </Breadcrumbs>

			<Title title={title} form={form} />

      {(event.isQuickplay && remainingMatches.length > 0) && (
        <QPMatchResultTable title="Current Match" match={remainingMatches[0]} />
      )}

      {event.canStart && (
        <EventActiveToggleButton event={event} onSave={reload} />
      )}

      {event.isQuickplay && (
        <QPEventStandingsTable data={event.standings} />
      )}

      {(!event.isQuickplay && (event.standings.length > 0 || event.season.seasonType == SEASON_TYPES.BYOT)) && (
        <EventStandingsTable event={event} />
      )}

      {(!event.isComplete && event.season.seasonType == SEASON_TYPES.SHUFFLE && event.standings.length == 0) && (
        <EventPlayersTable title={event.isQuickplay ? 'Players' : 'Unassigned Players'} event={event} />
      )}

      {(event.isQuickplay && completedMatches.length > 0) && (
        <QPMatchListTable title="Completed Matches" data={completedMatches} />
      )}

      {(!event.isQuickplay && remainingMatches.length > 0) && (
        <MatchListTable title="Remaining Matches" data={remainingMatches} />
      )}
      {(!event.isQuickplay && completedMatches.length > 0) && (
        <MatchListTable title="Completed Matches" data={completedMatches} />
      )}
    </>
  );
}

EventPage.propTypes = {
  initialEvent: PropTypes.object.isRequired,
};

export async function getServerSideProps({ params }) {
  const event = await loadEvent(params.id);

  return {
    props: { initialEvent: event, title: event.name ?? formatUTC(event.eventDate, 'MMMM d, yyyy') },
  };
}

