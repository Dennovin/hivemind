import React from 'react';
import { getAxios } from 'util/axios';

export default function TournamentPage() {
  return null;
}

export async function getServerSideProps({ params }) {
  const axios = getAxios();

  let response = await axios.get(`/api/tournament/tournament/${params.id}/`);
  const tournament = response.data;

  if (!tournament) return { notFound: true };

  return {
    redirect: {
      destination: `/t/${tournament.sceneName}/${tournament.slug}`,
      permanent: true,
    },
  };
}
