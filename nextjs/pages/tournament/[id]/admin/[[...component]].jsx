import React from 'react';

import { TournamentAdminProvider, TournamentAdminPage } from '@/components/tournament-admin';

export default function TournamentAdminComponentPage({ id }) {
  return (
    <TournamentAdminProvider id={id}>
      <TournamentAdminPage />
    </TournamentAdminProvider>
  );
}

export async function getServerSideProps({ params }) {
  return {
    props: { id: params.id, title: 'Tournament Admin' },
  };
}
