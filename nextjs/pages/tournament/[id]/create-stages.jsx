import { useState } from 'react';
import { Breadcrumbs, CircularProgress, Link, Grid } from '@material-ui/core';

import { TournamentAdminProvider, useTournamentAdmin } from '@/components/tournament-admin';
import Title from 'components/Title';
import {
  AwaitConfirm,
  AwaitSubmit,
  Complete,
  ConfigurePlacements,
  CreateStages,
  CreateStagesProvider,
  SetFinalStandings,
  Step,
  useCreateStages,
} from 'components/tournament/create-stages';

const ComponentByStep = {
  [Step.CreateStages]: CreateStages,
  [Step.ConfigurePlacements]: ConfigurePlacements,
  [Step.SetFinalStandings]: SetFinalStandings,
  [Step.AwaitConfirm]: AwaitConfirm,
  [Step.AwaitSubmit]: AwaitSubmit,
  [Step.Complete]: Complete,
};

function CurrentStep() {
  const { step } = useCreateStages();
  const Component = ComponentByStep[step];

  return <Component />;
}

function TournamentAdminContents() {
  const { tournament } = useTournamentAdmin();

  if (tournament === null) {
    return <CircularProgress />;
  }

  return (
    <div className="space-y-8">
      <Breadcrumbs>
        <Link href={`/scene/${tournament.scene.name}`}>{tournament.scene.displayName}</Link>
        <Link href={`/t/${tournament.sceneName}/${tournament.slug}`}>{tournament.name}</Link>
      </Breadcrumbs>

      <Title title={`Create Stages: ${tournament.name}`} />

      <CurrentStep />
    </div>
  );
}

export default function TournamentCreateStagesPage({ id }) {
  return (
    <TournamentAdminProvider id={id}>
      <CreateStagesProvider>
        <TournamentAdminContents />
      </CreateStagesProvider>
    </TournamentAdminProvider>
  );
}

export async function getServerSideProps({ params }) {
  return {
    props: { id: params.id, title: 'Create Tournament Stages' },
  };
}
