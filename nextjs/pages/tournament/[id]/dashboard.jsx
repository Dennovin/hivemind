import {
  Breadcrumbs,
  Button,
  CircularProgress,
  Grid,
  IconButton,
  Link,
  Typography,
} from '@material-ui/core';
import SettingsIcon from '@material-ui/icons/Settings';
import TournamentMatchListTable from 'components/tables/TournamentMatchListTable';
import TournamentTeamTable from 'components/tables/TournamentTeamTable';
import Title from 'components/Title';
import { useAuth } from 'util/auth';
import {
  TournamentRefereeCabinet,
  TournamentAdminProvider,
  useTournamentAdmin,
  MenuIcon,
} from '@/components/tournament-admin';
import TournamentQueueTable from 'components/tables/TournamentQueueTable';

const TournamentAdminContents = () => {
  const { isAdminOf } = useAuth();
  const {
    tournament,
    cabinets,
    visibleQueues,
    reloadTournament,
    hideInactiveCabs,
    setHideInactiveCabs,
  } = useTournamentAdmin();
  if (tournament === null || cabinets === null) {
    return <CircularProgress />;
  }

  const form = (
    <Grid container direction="row" alignItems="center" spacing={2}>
      <MenuIcon />
    </Grid>
  );

  return (
    <div className="space-y-8">
      <Breadcrumbs>
        <Link href={`/scene/${tournament.scene.name}`}>{tournament.scene.displayName}</Link>
        <Link href={`/t/${tournament.sceneName}/${tournament.slug}`}>{tournament.name}</Link>
      </Breadcrumbs>

      <Title title={'Dashboard: ' + tournament.name} form={form} />
      <div className="space-y-0 py-4 px-4 pb-2 bg-gray-100 rounded-lg">
        <div className="flex justify-between items-center">
          <Typography variant="h2" className="m-0">
            Cabinets
          </Typography>
          {isAdminOf(tournament.scene.id) && (
            <>
              <Button
                variant="outlined"
                color="default"
                size="small"
                onClick={() => setHideInactiveCabs(i => !i)}
              >
                {hideInactiveCabs ? 'Show' : 'Hide'} Inactive Cabinets
              </Button>
            </>
          )}
        </div>
        <Grid container direction="row" spacing={4}>
          {cabinets.map(cabinet => (
            <TournamentRefereeCabinet cabinet={cabinet} />
          ))}

          <Grid item xs={12} className="m-0">
            <Typography variant="h2" className="m-0 mt-2">
              Queues
            </Typography>
          </Grid>

          {visibleQueues?.map(queue => (
            <Grid item xs={12} md={6}>
              <TournamentQueueTable title={queue.name} queue={queue} isEditable={true} />
            </Grid>
          ))}
        </Grid>
      </div>
    </div>
  );
};

export default function TournamentAdminPage({ id }) {
  return (
    <TournamentAdminProvider id={id}>
      <TournamentAdminContents />
    </TournamentAdminProvider>
  );
}

export async function getServerSideProps({ params }) {
  return {
    props: { id: params.id, title: 'Dashboard' },
  };
}
