import { getAxios } from 'util/axios';
import { WhiteboardAdmin } from '@/components/whiteboard';

export default function WhiteboardAdminPage({ session, scene }) {
  return <WhiteboardAdmin session={session} scene={scene} />;
}

export async function getServerSideProps({ params }) {
  const axios = getAxios();
  let response = await axios.get(`/api/whiteboard/session/${params.id}/`);
  const session = response.data;

  if (!session) return { notFound: true };

  response = await axios.get(`/api/game/scene/${session.scene}/`);
  const scene = response.data;

  return {
    props: {
      session,
      scene,
      title: `Whiteboard Session: ${scene.displayName}`,
      description: scene.description,
    },
  };
}
