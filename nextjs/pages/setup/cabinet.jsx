import { makeStyles } from '@material-ui/core/styles';
import { Typography, Grid, Paper, Link, List, ListItem, ListItemText } from '@material-ui/core';

import Title from 'components/Title';
import InlineCode from 'components/InlineCode';

const useStyles = makeStyles(theme => ({
}));

export default function SetupCabinetPage() {
  const classes = useStyles();
  return (
    <>
      <Title title="Cabinet Setup Instructions" />

      <Typography>The client can run on any system that supports <Link href="https://nodejs.org/en/">Node.js</Link>. The recommended hardware is a <Link href="https://www.raspberrypi.org/products/raspberry-pi-4-model-b/">Raspberry Pi 4</Link> running <Link href="https://www.raspberrypi.org/downloads/raspbian/">Raspbian</Link>.</Typography>

      <List>
        <ListItem>
          <ListItemText>
            On the client's page, click "Edit", then "Download Client Configuration" to download the configuration file.
          </ListItemText>
        </ListItem>
        <ListItem>
          <ListItemText>
            Install <Link href="https://nodejs.org/en/">Node.js</Link> if it is not already installed on your system.
          </ListItemText>
        </ListItem>
        <ListItem>
          <ListItemText>
            Run <InlineCode>npm install @kqhivemind/hivemind-client</InlineCode> to download and install the client program.
          </ListItemText>
        </ListItem>
        <ListItem>
          <ListItemText>
            Run <InlineCode>npm install -g pm2</InlineCode> to install the <Link href="https://pm2.keymetrics.io/">PM2</Link> process manager.
          </ListItemText>
        </ListItem>
        <ListItem>
          <ListItemText>
            Run <InlineCode>pm2 start node_modules/.bin/hivemind-client -- config.json</InlineCode> to start the client.
          </ListItemText>
        </ListItem>
        <ListItem>
          <ListItemText>
            Run <InlineCode>pm2 startup</InlineCode> and follow the instructions to generate a startup script.
          </ListItemText>
        </ListItem>
        <ListItem>
          <ListItemText>
            Run <InlineCode>pm2 save</InlineCode> to run the startup script when the system boots.
          </ListItemText>
        </ListItem>
      </List>
    </>
  );
}
