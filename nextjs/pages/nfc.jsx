import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Button, Grid, FormControl, InputLabel, Select, MenuItem, TextField, IconButton,
       Divider } from '@material-ui/core';
import Icon from '@mdi/react';
import { mdiCardPlus, mdiCardRemove, mdiCreditCardWireless, mdiRadioboxBlank,
         mdiRadioboxMarked } from '@mdi/js';
import crypto from 'crypto';
import { toast } from 'react-toastify';

import { BLUE_TEAM, GOLD_TEAM, POSITIONS_BY_TEAM, SIGN_IN_ACTIONS } from 'util/constants';
import { getAxios } from 'util/axios';
import { useWebSocket } from 'util/websocket';
import usePersistentState from 'util/persistentState';

const useStyles = makeStyles(theme => ({
  buttons: {
    textAlign: 'center',
  },
  blueButton: {
    color: theme.palette.blue.dark1,
  },
  goldButton: {
    color: theme.palette.gold.dark1,
  },
}));

export default function NfcTestPage({ }) {
  const classes = useStyles();
  const axios = getAxios({ authenticated: true });
  const [scene, setScene] = usePersistentState('nfc.scene', 0);
  const [cabinet, setCabinet] = usePersistentState('nfc.cabinet', 0);
  const [readerName, setReaderName] = usePersistentState('nfc.reader-name', 'registration');
  const [cards, setCards] = usePersistentState('nfc.cards', []);
  const [scenes, setScenes] = useState(null);
  const [cabinets, setCabinets] = useState(null);
  const [selectedCard, setSelectedCard] = useState(null);
  const [token, setToken] = useState('');
  const [signedIn, setSignedIn] = useState({});
  const [regData, setRegData] = useState(null);

  const setFromList = (items, setFunc) => {
    return evt => {
      for (const item of items) {
        if (item.id === evt.target.value) {
          return setFunc(item);
        }
      }

      setFunc(null);
    };
  };

  const generateCard = () => {
    const id = crypto.randomBytes(8).toString('hex');
    setCards(c => c ? [...c, id] : [id]);
  };

  const clearCards = () => {
    setCards([]);
  };

  const tapSelectedCard = () => {
    const postdata = {
      sceneName: scene.name,
      cabinetName: cabinet.name,
      token,
      card: selectedCard,
      action: SIGN_IN_ACTIONS.NFC_REGISTER_TAPPED,
      ...regData,
    };

    axios.post('/api/stats/signin/nfc/', postdata).catch(err => {
      if (err.response.data.error) {
        toast.error(err.response.data.error);
      }
    });
  };

  const pressButton = player => {
    return () => {
      const postdata = {
        sceneName: scene.name,
        cabinetName: cabinet.name,
        token,
        player,
      };

      if (selectedCard) {
        postdata.card = selectedCard;
        postdata.action = SIGN_IN_ACTIONS.SIGN_IN;
      } else {
        postdata.action = SIGN_IN_ACTIONS.SIGN_OUT;
      }

      return axios.post('/api/stats/signin/nfc/', postdata);
    };
  };

  useEffect(() => {
    if (scenes === null) {
      axios.getAllPages('/api/game/scene/').then(setScenes);
    }
  }, []);

  useEffect(() => {
    axios.getAllPages('/api/game/cabinet/', { params: { sceneId: scene?.id } }).then(setCabinets);
  }, [scene]);

  useEffect(() => {
    if (cabinet?.id) {
      axios.get(`/api/game/cabinet/${cabinet.id}/`).then(response => {
        setToken(response?.data?.token ?? '');
      });

      axios.get(`/api/game/cabinet/${cabinet.id}/signin/`).then(response => {
        const s = {};
        for (const row of response.data.signedIn) {
          s[row.playerId] = true;
        }

        setSignedIn(s);
      });
    }
  }, [cabinet]);

  const ws = useWebSocket('/ws/signin');
  ws.onJsonMessage(message => {
    if (message.sceneName !== scene?.name || message.cabinetName !== cabinet?.name) {
      return;
    }

    if (message.type === SIGN_IN_ACTIONS.NFC_REGISTER) {
      const data = {...message};
      delete data.cabinetName;
      delete data.sceneName;
      delete data.readerId;
      delete data.type;

      setRegData(data);
    }

    if (message.action === SIGN_IN_ACTIONS.SIGN_IN) {
      setSignedIn(s => ({ ...s, [message.playerId]: true }));
    }

    if (message.action === SIGN_IN_ACTIONS.SIGN_OUT) {
      setSignedIn(s => ({ ...s, [message.playerId]: false }));
    }
  });

  return (
    <>
      <Grid container spacing={2}>

        <Grid item xs={12} md={4}>
          {scenes && (
            <FormControl fullWidth>
              <InputLabel>Scene</InputLabel>
              <Select fullWidth name="scene" value={scene?.id ?? 0} label="Scene"
                      onChange={setFromList(scenes, setScene)}>
                <MenuItem value={0} />
                {scenes?.map(s => (
                  <MenuItem value={s.id} key={s.id}>{s.displayName}</MenuItem>
                ))}
              </Select>
            </FormControl>
          )}
        </Grid>

        <Grid item xs={12} md={4}>
          {cabinets && (
            <FormControl fullWidth>
              <InputLabel>Cabinet</InputLabel>
              <Select fullWidth name="cabinet" value={cabinet?.id ?? 0} label="Cabinet"
                      onChange={setFromList(cabinets, setCabinet)}>
                <MenuItem value={0} />
                {cabinets?.map(c => (
                  <MenuItem value={c.id} key={c.id}>{c.displayName}</MenuItem>
                ))}
              </Select>
            </FormControl>
          )}
        </Grid>

        <Grid item xs={12} md={4}>
          <FormControl fullWidth>
            <TextField fullWidth name="token" value={token} label="Cabinet Token"
                       onChange={evt => setToken(evt.target.value)} />
          </FormControl>
        </Grid>

        <Grid item xs={12} md={12}>
          <FormControl fullWidth>
            <TextField fullWidth name="reader-name" value={readerName ?? ''} label="Reader Name"
                       onChange={evt => setReaderName(evt.target.value)} />
          </FormControl>
        </Grid>

        <Grid item xs={12} md={3}>
          <IconButton onClick={generateCard}>
            <Icon path={mdiCardPlus} size={2} />
          </IconButton>
          <IconButton onClick={clearCards}>
            <Icon path={mdiCardRemove} size={2} />
          </IconButton>
        </Grid>

        <Grid item xs={11} md={8}>
          <FormControl fullWidth>
            <InputLabel>NFC Card ID</InputLabel>
            <Select fullWidth name="card" value={selectedCard ?? 0} label="NFC Card ID"
                    onChange={evt => setSelectedCard(evt.target.value)}>
              <MenuItem value={0} />
              {cards?.map(c => (
                <MenuItem value={c} key={c}>{c}</MenuItem>
              ))}
            </Select>
          </FormControl>
        </Grid>

        <Grid item xs={1}>
          <IconButton onClick={tapSelectedCard}>
            <Icon path={mdiCreditCardWireless} size={2} />
          </IconButton>
        </Grid>

        <Grid item xs={12} md={6} className={classes.buttons}>
          {POSITIONS_BY_TEAM[BLUE_TEAM].map(pos => (
            <IconButton key={pos.ID} onClick={pressButton(pos.ID)}>
              <Icon className={classes.blueButton} size={2}
                    path={signedIn[pos.ID] ? mdiRadioboxMarked : mdiRadioboxBlank} />
            </IconButton>
          ))}
        </Grid>

        <Grid item xs={12} md={6} className={classes.buttons}>
          {POSITIONS_BY_TEAM[GOLD_TEAM].map(pos => (
            <IconButton key={pos.ID} onClick={pressButton(pos.ID)}>
              <Icon className={classes.goldButton} size={2}
                    path={signedIn[pos.ID] ? mdiRadioboxMarked : mdiRadioboxBlank} />
            </IconButton>
          ))}
        </Grid>

      </Grid>
    </>
  );
}
