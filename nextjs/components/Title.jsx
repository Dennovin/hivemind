import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { Avatar, Grid, Typography } from '@material-ui/core';
import clsx from 'clsx';

const useStyles = makeStyles(theme => ({
  container: {
    justifyContent: 'space-between',
    marginBottom: theme.spacing(2),
    gap: '1rem',
  },
  title: {
    display: 'flex',
    alignItems: 'center',
  },
  avatar: {
    width: '64px',
    height: '64px',
    marginRight: theme.spacing(1),
  },
}));

export default function Title({ title, subtitle, form, avatarSrc, className, ...props }) {
  const classes = useStyles();

  return (
    <Grid container className={clsx(classes.container, className)}>
      <Grid item className={classes.title}>
        {avatarSrc && <Avatar src={avatarSrc} alt={title} className={classes.avatar} />}
        <Grid item container direction="column">
          <Typography variant="h1">{title}</Typography>
          {subtitle && <Typography variant="subtitle1">{subtitle}</Typography>}
        </Grid>
      </Grid>
      {form !== null && (
        <Grid item className={classes.form}>
          {form}
        </Grid>
      )}
    </Grid>
  );
}

Title.propTypes = {
  title: PropTypes.oneOfType([PropTypes.string, PropTypes.node]).isRequired,
  subtitle: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  form: PropTypes.node,
  avatar: PropTypes.node,
};

Title.defaultProps = {
  form: null,
};
