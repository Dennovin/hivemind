import { AppBar, Link, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import MenuIcon from '@material-ui/icons/Menu';
import clsx from 'clsx';
import { bindTrigger, usePopupState } from 'material-ui-popup-state/hooks';

import MenuPopup from './MenuPopup';
import UserLoggedIn from './UserLoggedIn';

const useStyles = makeStyles(theme => ({
  container: {
    background: 'black',
    borderBottom: '1px solid #232323',
    margin: 0,
    verticalAlign: 'middle',
    alignItems: 'center',
  },
  toolbar: {
    width: '100%',
    padding: 0,
    minHeight: '48px',
  },
  toolbarGrid: {
    justifyContent: 'space-between',
    flexWrap: 'nowrap',
  },
  toolbarSection: {
    width: 'auto',
    alignItems: 'center',
    display: 'flex',
    justifyContent: 'center',
    flexGrow: 0,
    [theme.breakpoints.up('md')]: {
      margin: theme.spacing(0, 2),
    },
  },
  menuIcon: {
    color: 'white',
    margin: theme.spacing(0, 2),
    cursor: 'pointer',
  },
  toolbarIcon: {
    color: 'white',
  },
  logo: {
    height: '36px',
    [theme.breakpoints.down('sm')]: {
      height: '24px',
    },
  },
  title: {
    color: theme.palette.primary.light,
    marginLeft: theme.spacing(1),
    verticalAlign: 'middle',
    fontSize: '24px',
    [theme.breakpoints.down('sm')]: {
      fontSize: '18px',
      marginLeft: theme.spacing(1),
    },
  },
}));

export default function Header() {
  const classes = useStyles();
  const menuState = usePopupState({ popupId: 'menu' });

  return (
    <AppBar position="static" className={classes.container}>
      {/* <Grid container direction="row" className={clsx(classes.toolbarGrid, 'justify-between')}> */}
      <div className="flex justify-between items-center w-full h-12">
        <div className="flex justify-center">
          <Link href="/" className="flex items-center hover:no-underline">
            <img src="/static/hivemind.png" className={clsx('h-9 w-9 ml-4')} />
            <Typography
              variant="h1"
              component="h1"
              className="text-primary-light text-[18px] md:text-lg ml-2 align-middle hidden sm:block"
            >
              HiveMind
            </Typography>
          </Link>
        </div>

        <div className="flex items-center justify-center ml-auto">
          <UserLoggedIn />

          {/* <Link {...bindTrigger(menuState)}> */}
          <MenuIcon className={classes.menuIcon} {...bindTrigger(menuState)} />
          {/* </Link> */}

          <MenuPopup menuState={menuState} />
        </div>
      </div>
    </AppBar>
  );
}
