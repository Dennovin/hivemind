import { Grid, Typography } from '@material-ui/core';
import { PieChart, Pie, Sector, Cell, Legend, Tooltip, ResponsiveContainer, Label } from 'recharts';
import clsx from 'clsx';

import SceneColorBox from 'components/SceneColorBox';

import styles from './Recap.module.css';

export default function RecapPieChart({
  title,
  data,
  getLabel,
  getValue,
  getColor,
  getTooltipValue,
}) {
  const chartData = data.map(row => ({
    name: getLabel(row),
    value: getValue(row),
  }));

  const colors = data.map(row => getColor(row));
  const tooltipValue = value =>
    getTooltipValue ? getTooltipValue(value) : value?.toLocaleString();

  const formatLabel = ({ fill, x, y, ...props }) => {
    const lx = x + 7 * Math.cos((props.midAngle * 2 * Math.PI) / 360);
    const ly = y + 6 - 7 * Math.sin((props.midAngle * 2 * Math.PI) / 360);

    return (
      <text
        {...props}
        x={lx}
        y={ly}
        fill={`color-mix(in oklab, ${fill} 70%, black)`}
        className={styles.legendText}
      >
        {props.name}
      </text>
    );
  };
  const formatTooltip = (value, name, props) => [
    <>
      <SceneColorBox color={props.payload.fill} />
      <span className={styles.tooltipLabel}>{name}:</span>
      <span className={styles.tooltipValue}>{tooltipValue(value)}</span>
    </>,
  ];

  return (
    <Grid item xs={12} className={clsx(styles.gridItem, styles.chartContainer)}>
      <Typography className={styles.listTitle}>{title}</Typography>

      <Grid item className={styles.chart}>
        <ResponsiveContainer width="100%" height="100%">
          <PieChart>
            <Tooltip separator=": " formatter={formatTooltip} />
            <Pie label={formatLabel} data={chartData} dataKey="value" nameKey="name">
              {data.map(row => (
                <Cell key={`cell-${row.id}`} fill={getColor(row)} />
              ))}
            </Pie>
          </PieChart>
        </ResponsiveContainer>
      </Grid>
    </Grid>
  );
}
