import { Grid, Paper } from '@material-ui/core';

import styles from './Recap.module.css';

export default function RecapItem({ children }) {
  return (
    <Paper className={styles.recapItem}>
      <Grid container className={styles.itemContainer} spacing={2}>
        {children}
      </Grid>
    </Paper>
  );
}
