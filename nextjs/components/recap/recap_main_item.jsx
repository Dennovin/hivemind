import { Typography, Grid } from '@material-ui/core';

import styles from './Recap.module.css';

export default function RecapMainItem({ children }) {
  return (
    <Grid item xs={12} className={styles.gridItem}>
      <Typography variant="h3" className={styles.largeText}>
        {children}
      </Typography>
    </Grid>
  );
}
