import { Typography } from '@material-ui/core';
import 'react-responsive-carousel/lib/styles/carousel.min.css';

import Carousel from 'components/carousel';
import styles from './Recap.module.css';

export default function Recap({ title, children }) {
  return (
    <>
      <Typography variant="h1" element="h1" className="flex-grow">
        {title}
      </Typography>

      <Carousel autoPlay={false} className={styles.recapCarousel}>
        {children}
      </Carousel>
    </>
  );
}
