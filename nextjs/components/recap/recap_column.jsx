import { Grid } from '@material-ui/core';
import clsx from 'clsx';

import styles from './Recap.module.css';

export default function RecapColumn({ className, children, ...props }) {
  return (
    <Grid item xs={6} className={clsx(styles.recapColumn, className)} {...props}>
      {children}
    </Grid>
  );
}
