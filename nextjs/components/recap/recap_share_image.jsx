import React, { useEffect, useRef, useState } from 'react';
import { Button } from '@material-ui/core';
import Icon from '@mdi/react';
import { mdiContentCopy, mdiFacebook, mdiTwitter } from '@mdi/js';
import { FacebookShareButton, TwitterShareButton } from 'react-share';

import styles from './Recap.module.css';
import { colors } from 'theme/colors';
import { getAxios } from 'util/axios';
import { useUser } from 'util/auth';
import isServer from 'util/isServer';

export default function RecapShareImage({ user, recapData, mostPlayedWith, favoritePosition }) {
  const axios = getAxios({ authenticated: true });
  const requestUser = useUser();
  const canvasRef = useRef(null);

  const [imagesToLoad, setImagesToLoad] = useState(0);
  const [canvasDone, setCanvasDone] = useState(false);

  const drawImage = (ctx, url, x, y, width, height, square = true) => {
    return new Promise((resolve, reject) => {
      setImagesToLoad(v => v + 1);
      const image = new Image();
      image.setAttribute('crossorigin', 'anonymous');
      image.onload = () => {
        ctx.save();

        if (square) {
          const sx = Math.max(0, (image.width - image.height) / 2);
          const sy = Math.max(0, (image.height - image.width) / 2);
          const sSize = Math.min(image.width, image.height);
          ctx.drawImage(image, sx, sy, sSize, sSize, x, y, width, height);
        } else {
          ctx.drawImage(image, x, y, width, height);
        }

        ctx.restore();

        setImagesToLoad(v => v - 1);
        resolve();
      };
      image.src = url;
    });
  };

  const generateImage = async () => {
    const canvas = canvasRef.current;
    const ctx = canvas.getContext('2d');

    ctx.clearRect(0, 0, canvas.width, canvas.height);

    for (const friend of mostPlayedWith) {
      try {
        const response = await axios.get(`/api/user/user/${friend.id}/public-data/`);
        friend.image = response.data.image;
      } catch (err) {}
    }

    const gradient = ctx.createLinearGradient(1200, 0, 0, 630);
    gradient.addColorStop(0, colors.blue.light1);
    gradient.addColorStop(1, colors.blue.dark2);

    ctx.fillStyle = gradient;
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    ctx.globalAlpha = 0.12;
    await drawImage(ctx, '/static/hivemind.png', 500, 30, 570, 570);
    ctx.globalAlpha = 1;

    ctx.font = '32px Lato';
    ctx.fillStyle = '#efefef';
    ctx.textAlign = 'center';
    ctx.strokeStyle = '#1010103f';
    ctx.lineWidth = 3;
    ctx.strokeText(`HiveMind ${recapData.year} Recap`, 200, 534);
    ctx.fillText(`HiveMind ${recapData.year} Recap`, 200, 534);

    let width = Number.MAX_VALUE;
    let size = 48;

    while (width > 320) {
      size--;
      ctx.font = `bold ${size}px Lato, Noto Color Emoji`;
      width = ctx.measureText(user.name).width;
    }

    ctx.strokeText(user.name, 200, 450);
    ctx.fillText(user.name, 200, 450);

    const userImage = user.image || '/static/hivemind.png';
    drawImage(ctx, userImage, 40, 40, 320, 320);

    ctx.font = '26px Lato';
    ctx.textAlign = 'left';

    const statBlockStart = 150;
    const statLineHeight = 50;
    const statLabelLeft = 420;
    const statTop = rowNum => statBlockStart + statLineHeight * rowNum;
    const beeFFsLeft = 860;
    const beeFFsTop = 220;

    ctx.font = '30px Lato';
    ctx.textAlign = 'left';

    ctx.fillText('Total Stats', statLabelLeft, statBlockStart - 80);
    ctx.fillText('Top BeeFFs', beeFFsLeft, statBlockStart - 80);

    ctx.fillText('Games Played', statLabelLeft, statTop(0));
    ctx.fillText('Total Kills', statLabelLeft, statTop(1));
    ctx.fillText('Military Kills', statLabelLeft, statTop(2));
    ctx.fillText('Bump Assists', statLabelLeft, statTop(3));
    ctx.fillText('Berries Deposited', statLabelLeft, statTop(4));
    ctx.fillText('Snail Meters', statLabelLeft, statTop(5));
    ctx.fillText('Berries Kicked In', statLabelLeft, statTop(6));

    ctx.font = 'bold 34px Lato';
    ctx.textAlign = 'right';

    const statValueLeft = 780;

    ctx.fillText(recapData.statTotals.games.toLocaleString(), statValueLeft, statTop(0));
    ctx.fillText(recapData.statTotals.kills.toLocaleString(), statValueLeft, statTop(1));
    ctx.fillText(recapData.statTotals.militaryKills.toLocaleString(), statValueLeft, statTop(2));
    ctx.fillText(recapData.statTotals.bumpAssists.toLocaleString(), statValueLeft, statTop(3));
    ctx.fillText(recapData.statTotals.berries.toLocaleString(), statValueLeft, statTop(4));
    ctx.fillText(recapData.statTotals.snailMeters.toLocaleString(), statValueLeft, statTop(5));
    ctx.fillText(recapData.statTotals.berriesKicked.toLocaleString(), statValueLeft, statTop(6));

    ctx.font = '24px Lato';
    ctx.textAlign = 'left';
    mostPlayedWith.map((friend, idx) => {
      ctx.fillText(`${idx + 1}.`, beeFFsLeft, beeFFsTop + 58 * idx);
    });

    if (mostPlayedWith.length > 0 && mostPlayedWith[0].image) {
      const heartsTop = statBlockStart - 140;
      drawImage(ctx, '/static/hearts.png', beeFFsLeft + 60, heartsTop, 300, 210, false);
      drawImage(ctx, mostPlayedWith[0].image, beeFFsLeft + 200, heartsTop + 36, 120, 120);
    }

    ctx.font = 'bold 30px Lato, Noto Color Emoji';
    mostPlayedWith.map((friend, idx) => {
      ctx.fillText(friend.name, beeFFsLeft + 40, beeFFsTop + 58 * idx);
    });

    ctx.textAlign = 'left';

    if (favoritePosition) {
      ctx.font = '28px Lato';
      ctx.fillText('Most played', statLabelLeft, 530);

      ctx.font = 'bold 36px Lato';
      ctx.fillText(
        favoritePosition.charAt(0).toUpperCase() + favoritePosition.slice(1),
        statLabelLeft,
        580,
      );
    }

    ctx.font = '28px Lato';
    ctx.fillText('Minutes played', beeFFsLeft, 530);

    const minutesPlayed = Math.floor((recapData.statTotals?.timePlayed || 0) / 60000);

    ctx.font = 'bold 36px Lato';
    ctx.fillText(minutesPlayed.toLocaleString(), beeFFsLeft, 580);

    ctx.font = '16px Lato';
    ctx.textAlign = 'center';
    ctx.fillText(`Visit kqhivemind.com to see your ${recapData.year} recap!`, 270, 655);

    // Resize to fit smaller screens
    const containerWidth = 300;
    if (containerWidth < 540) {
      ctx.scale = containerWidth / 540;
    }

    setCanvasDone(true);
  };

  useEffect(() => {
    Promise.all([document.fonts.load('16px Lato'), document.fonts.load('bold 16px Lato')]).then(
      generateImage,
    );
  }, []);

  useEffect(() => {
    if (imagesToLoad > 0 || !canvasDone) return;

    const canvas = canvasRef.current;

    if (requestUser?.id === user.id && !recapData.shareImage) {
      axios.post('/api/stats/user-recap/image/', {
        year: recapData.year,
        image: canvas.toDataURL(),
      });
    }
  }, [imagesToLoad, canvasDone]);

  const pageURL = `https://kqhivemind.com/user/${user.id}/recap/${recapData.year}`;

  return (
    <div className={styles.recapImagePage}>
      <div className={styles.canvasContainer}>
        <canvas
          className={styles.canvas}
          ref={canvasRef}
          id="recap-image"
          height="630"
          width="1200"
        ></canvas>
      </div>

      <div className={styles.recapImageShareButtons}>
        <Button
          variant="contained"
          color="primary"
          startIcon={<Icon path={mdiContentCopy} size={1} />}
          onClick={() => navigator?.clipboard?.writeText(pageURL)}
        >
          Copy URL to Clipboard
        </Button>

        <FacebookShareButton url={pageURL}>
          <Button
            variant="contained"
            color="primary"
            startIcon={<Icon path={mdiFacebook} size={1} />}
          >
            Share on Facebook
          </Button>
        </FacebookShareButton>
      </div>
    </div>
  );
}
