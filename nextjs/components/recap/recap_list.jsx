import { Grid, Typography, List } from '@material-ui/core';
import clsx from 'clsx';

import styles from './Recap.module.css';

export default function RecapList({ title, children, ...params }) {
  return (
    <Grid item xs={12} className={clsx(styles.gridItem, styles.listContainer)}>
      <Typography className={styles.listTitle}>{title}</Typography>
      <List className={styles.list} {...params}>
        {children}
      </List>
    </Grid>
  );
}
