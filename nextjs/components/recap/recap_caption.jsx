import styles from './Recap.module.css';

export default function RecapCaption({ children }) {
  return <div className={styles.caption}>{children}</div>;
}
