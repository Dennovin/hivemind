import React from 'react';
import { Button } from '@material-ui/core';
import Icon from '@mdi/react';
import { mdiPause, mdiPlay } from '@mdi/js';

import styles from './Whiteboard.module.css';
import { getAxios } from '@/util/axios';
import { useWhiteboard } from './context';

export function TogglePauseButton() {
  const axios = getAxios({ authenticated: true });
  const { session, status } = useWhiteboard();

  const togglePause = () => {
    axios.patch(`/api/whiteboard/session/${session.id}/`, {
      isPaused: !status.isPaused,
    });
  };

  const icon = <Icon path={status.isPaused ? mdiPlay : mdiPause} size={1} />;

  return (
    <div>
      <Button
        variant="outlined"
        startIcon={icon}
        onClick={togglePause}
        color="primary"
        className={styles.togglePauseButton}
      >
        {status.isPaused ? 'Unpause Session' : 'Pause Session'}
      </Button>
    </div>
  );
}
