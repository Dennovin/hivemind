import React from 'react';

import styles from './Whiteboard.module.css';
import { useAuth } from '@/util/auth';
import { useWhiteboard } from './context';
import { SignupForm } from './signup_form';

export function PlayerStatus() {
  const { currentPlayer } = useWhiteboard();
  const { user } = useAuth();

  return (
    <>
      <div className={styles.playerStatus}>
        {currentPlayer?.isReady ? (
          <>
            You are signed up for role(s): <b>{currentPlayer.roles.join(', ')}</b>
          </>
        ) : (
          <>You are not currently signed up to play.</>
        )}
      </div>

      {currentPlayer && user && (
        <div className={styles.playerStatus}>
          You can configure SMS notifications on your{' '}
          <a href={`/user/${user.id}#sms-config`}>user profile page</a>.
        </div>
      )}

      <SignupForm />
    </>
  );
}
