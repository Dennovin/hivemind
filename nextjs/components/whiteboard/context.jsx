import React, { createContext, useContext, useEffect, useState, useMemo } from 'react';

import Loading from '@/components/Loading';
import { getAxios } from 'util/axios';
import { useUser } from 'util/auth';
import { useWebSocket } from 'util/websocket';

export const WhiteboardContext = createContext({});

export function WhiteboardProvider({ session, children }) {
  const axios = getAxios({ authenticated: true });
  const user = useUser();
  const [scene, setScene] = useState(null);
  const [status, setStatus] = useState(null);
  const [currentMatch, setCurrentMatch] = useState(null);
  const [currentPlayer, setCurrentPlayer] = useState(null);
  const [needsSelfRating, setNeedsSelfRating] = useState(true);
  const [playerList, setPlayerList] = useState(null);

  const websocket = useWebSocket(`/ws/whiteboard/${session.id}`);
  websocket.onJsonMessage(message => {
    if (message.type === 'status') {
      setStatus({
        cabinet: message.data.cabinet,
        isActive: message.data.isActive,
        isPaused: message.data.isPaused,
        playersQueued: message.data.playersQueued,
        roundsPerMatch: message.data.roundsPerMatch,
        winsPerMatch: message.data.winsPerMatch,
      });

      setCurrentMatch(message.data.currentMatch);
    }

    if (message.type === 'players') {
      setPlayerList(message.data);
    }
  });

  websocket.onJsonMessage(
    message => {
      if (message.type === 'player') {
        if (message.data.userId === user?.id) {
          setCurrentPlayer(message.data);
        }
      }
    },
    [user?.id],
  );

  useEffect(() => {
    if (user?.id && playerList) {
      setCurrentPlayer(playerList.filter(i => i.user.id === user.id).pop());
    }
  }, [user?.id, playerList]);

  useEffect(() => {
    if (user?.id) {
      axios.get('/api/whiteboard/player/me/').then(response => {
        setNeedsSelfRating(!response?.data?.id);
      });
    }
  }, [user?.id]);

  useEffect(() => {
    if (scene === null) {
      axios.get(`/api/whiteboard/session/${session.id}/`).then(wbResponse => {
        axios.get(`/api/game/scene/${wbResponse.data.scene}/`).then(sceneResponse => {
          setScene(sceneResponse.data);
        });
      });
    }
  }, [scene]);

  const bluePlayerIds = useMemo(() => {
    if (currentMatch?.bluePlayers) {
      return new Set(currentMatch.bluePlayers.map(p => p.playerId));
    }
  }, [currentMatch?.bluePlayers]);

  const goldPlayerIds = useMemo(() => {
    if (currentMatch?.goldPlayers) {
      return new Set(currentMatch.goldPlayers.map(p => p.playerId));
    }
  }, [currentMatch?.goldPlayers]);

  if (!session || !scene) return null;

  const contextValue = {
    scene,
    status,
    session,
    currentMatch,
    currentPlayer,
    needsSelfRating,
    setNeedsSelfRating,
    playerList,
    bluePlayerIds,
    goldPlayerIds,
  };

  return <WhiteboardContext.Provider value={contextValue}>{children}</WhiteboardContext.Provider>;
}

export function useWhiteboard() {
  return useContext(WhiteboardContext);
}

export function WhiteboardProviderByCabinet({ cabinetId, children }) {
  const axios = getAxios({ authenticated: true });
  const [session, setSession] = useState(null);
  const websocket = useWebSocket(`/ws/gamestate`);

  useEffect(() => {
    axios
      .get(`/api/whiteboard/session/`, { params: { isActive: true, cabinetId } })
      .then(response => {
        if (response.data.count > 0) {
          setSession(response.data.results[0]);
        }
      });
  }, []);

  websocket.onJsonMessage(message => {
    if (
      message.type === 'match' &&
      message.matchType === 'whiteboard' &&
      message.cabinetId === cabinetId &&
      session === null
    ) {
      axios.get(`/api/whiteboard/session/${message.sessionId}/`).then(response => {
        setSession(response.data);
      });
    }
  });

  if (session) {
    return <WhiteboardProvider session={session}>{children}</WhiteboardProvider>;
  }

  return <>{children}</>;
}
