import React, { useEffect, useState } from 'react';
import { Grid, Typography } from '@material-ui/core';
import * as Yup from 'yup';

import Form from '@/components/forms/Form';
import Field, { Conditional, Switch, TextField } from '@/components/fields/Field';
import { useAuth } from '@/util/auth';
import { getAxios } from '@/util/axios';
import { PLAYER_ROLE } from '@/util/constants';

import { roleIcon } from './constants';
import { useWhiteboard } from './context';
import { RoleSelector } from './role_selector';
import { SelfRatingSlider } from './self_rating_slider';
import styles from './Whiteboard.module.css';

export function SignupForm() {
  const axios = getAxios({ authenticated: true });
  const { user, reloadUser } = useAuth();
  const { session, currentPlayer, needsSelfRating, setNeedsSelfRating } = useWhiteboard();

  const SignupSchema = Yup.object().shape({
    userName: user?.name ? undefined : Yup.string().required('Name is required'),
  });

  const playerData = {
    userName: '',
    isReady: true,
    requeue: true,
    queenSelfRating: 0,
    warriorSelfRating: 0,
    objectiveSelfRating: 0,
    roles: [],
    ...currentPlayer,
  };

  for (const role of Object.values(PLAYER_ROLE)) {
    playerData[role] = currentPlayer?.roles.includes(role);
  }

  const formProps = {
    buttonText: currentPlayer ? 'Edit Signup' : 'Sign Up',
    dialogTitle: 'Session Registration',
    canEdit: () => true,
    object: playerData,
    url: `/api/whiteboard/session/${session.id}/signup/`,
    method: 'PUT',
    reloadOnSave: false,
    validationSchema: SignupSchema,
    getPostData: {
      roles: values => Object.values(PLAYER_ROLE).filter(v => values[v]),
    },
    beforeSave: async values => {
      if (needsSelfRating) {
        await axios.post('/api/whiteboard/player/create/', {
          queen: values.queenSelfRating,
          warrior: values.warriorSelfRating,
          objective: values.objectiveSelfRating,
        });

        setNeedsSelfRating(false);
      }

      if (values.userName) {
        await axios.patch(`/api/user/user/${user.id}/`, { name: values.userName });
        await reloadUser();
      }
    },
  };

  if (!user) return null;

  return (
    <Form {...formProps}>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Field
            name="isReady"
            label="Ready to Play"
            component={Switch}
            helperText="When selected, you will be added to the next available match."
          />
        </Grid>

        <Grid item xs={12}>
          <Field
            name="requeue"
            label="Requeue"
            component={Switch}
            helperText="If selected, you will be added back to the queue after your next match ends."
          />
        </Grid>

        {!user.name && (
          <>
            <Grid item xs={12} className={styles.userNameText}>
              Please set your name before signing up.
            </Grid>

            <Grid item xs={12}>
              <Field component={TextField} name="userName" label="Name" />
            </Grid>
          </>
        )}

        <Grid item xs={12}>
          <Typography variant="h3">Role Selection</Typography>
        </Grid>

        <div className={styles.roleSelectorRow}>
          <RoleSelector name="queen" label="Queen" iconPath={roleIcon.queen} />
          <RoleSelector name="warrior" label="Warrior" iconPath={roleIcon.warrior} />
          <RoleSelector name="objective" label="Objective" iconPath={roleIcon.objective} />
        </div>

        {needsSelfRating && (
          <>
            <Grid item xs={12}>
              <Typography variant="h3">Experience</Typography>
            </Grid>

            <Grid item xs={12} className={styles.selfRatingText}>
              Looks like this is your first time playing in a whiteboard session. To help with
              matchmaking for your first few games, give yourself a rating in each of these roles.
            </Grid>

            <Grid item xs={12}>
              <Field component={SelfRatingSlider} name="queenSelfRating" label="Queen" />
            </Grid>

            <Grid item xs={12}>
              <Field component={SelfRatingSlider} name="warriorSelfRating" label="Warrior" />
            </Grid>

            <Grid item xs={12}>
              <Field component={SelfRatingSlider} name="objectiveSelfRating" label="Objective" />
            </Grid>
          </>
        )}
      </Grid>
    </Form>
  );
}
