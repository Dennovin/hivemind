import { mdiCrown, mdiSword, mdiSnail } from '@mdi/js';

export const ROLES = ['queen', 'warrior', 'objective'];

export const roleIcon = {
  queen: mdiCrown,
  warrior: mdiSword,
  objective: mdiSnail,
};
