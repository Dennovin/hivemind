import React from 'react';
import clsx from 'clsx';
import Icon from '@mdi/react';

import styles from './Whiteboard.module.css';
import { useWhiteboard } from './context';
import { roleIcon } from './constants';

export function PlayerInfo({ player }) {
  if (!player) return null;

  return (
    <div className={styles.player}>
      <div className={styles.imageCell}>
        {player.image && (
          <img
            className={styles.image}
            src={player.image}
            alt={`Player image for ${player.name}`}
          />
        )}
      </div>

      <div className={styles.playerInfo}>
        <div className={styles.name}>
          {player.name} ({player.scene})
        </div>
      </div>

      <div className={styles.roleImageCell}>
        <Icon path={roleIcon[player.role]} />
      </div>
    </div>
  );
}

export function CurrentMatch() {
  const { currentMatch, status } = useWhiteboard();

  return (
    <>
      <div className={styles.matchScore}>
        <div className={clsx(styles.teamName, styles.blueTeam)}>Blue Team</div>
        <div className={clsx(styles.teamScore, styles.blueTeam)}>{currentMatch?.blueScore}</div>
        <div className={clsx(styles.teamScore, styles.goldTeam)}>{currentMatch?.goldScore}</div>
        <div className={clsx(styles.teamName, styles.goldTeam)}>Gold Team</div>
      </div>

      {currentMatch && (
        <div className={styles.currentMatch}>
          <div className={clsx(styles.team, styles.blueTeam)}>
            {currentMatch.bluePlayers.map(player => (
              <PlayerInfo key={player.playerId} player={player} />
            ))}
          </div>
          <div className={clsx(styles.team, styles.goldTeam)}>
            {currentMatch.goldPlayers.map(player => (
              <PlayerInfo key={player.playerId} player={player} />
            ))}
          </div>
        </div>
      )}

      {!currentMatch && <div className={styles.noMatch}>No match is currently active.</div>}
    </>
  );
}
