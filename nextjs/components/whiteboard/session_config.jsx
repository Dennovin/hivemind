import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { MenuItem, Grid } from '@material-ui/core';

import { DEFAULT_ROUNDS_PER_MATCH } from '@/util/constants';
import Form from '@/components/forms/Form';
import Field, { TextField, Select, Switch } from '@/components/fields/Field';
import { getAxios } from '@/util/axios';
import { useAuth } from '@/util/auth';
import { useWhiteboard } from './context';

export function SessionConfig({ scene }) {
  const axios = getAxios({ authenticated: true });
  const router = useRouter();
  const [cabinets, setCabinets] = useState(null);
  const whiteboard = useWhiteboard();
  const { isAdminOf } = useAuth();

  const { status, session } = whiteboard;
  const sceneId = scene?.id ?? whiteboard?.scene?.id;
  const sessionConfig = {
    scene: sceneId,
    cabinetId: null,
    roundsPerMatch: DEFAULT_ROUNDS_PER_MATCH,
    winsPerMatch: 0,
    isActive: true,
    isPaused: false,
    isLastSet: false,
  };

  if (session !== null) {
    Object.assign(sessionConfig, {
      session,
      ...status,
    });
  }

  const formProps = {
    buttonText: session?.id ? 'Configure Session' : 'Create Whiteboard Session',
    dialogTitle: session?.id ? 'Configure Whiteboard Session' : 'Create Whiteboard Session',
    canEdit: () => true,
    object: sessionConfig,
    url: session?.id ? `/api/whiteboard/session/${session.id}/` : '/api/whiteboard/session/',
    method: session?.id ? 'PUT' : 'POST',
    onSave: result => {
      if (!session?.id) {
        router.push(`/whiteboard/${result.id}`);
      }
    },
  };

  useEffect(() => {
    if (cabinets === null) {
      axios.getAllPages(`/api/game/cabinet/`, { params: { sceneId } }).then(setCabinets);
    }
  }, []);

  return (
    <Form {...formProps}>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Field name="cabinet" label="Cabinet" component={Select}>
            <MenuItem value="">(None)</MenuItem>
            {cabinets !== null &&
              cabinets.map(cabinet => (
                <MenuItem key={cabinet.id} value={cabinet.id}>
                  {cabinet.displayName}
                </MenuItem>
              ))}
          </Field>
        </Grid>

        <Grid item xs={12}>
          <Field name="isActive" label="Active" component={Switch} />
        </Grid>
        <Grid item xs={12}>
          <Field name="isPaused" label="Paused" component={Switch} />
        </Grid>
        <Grid item xs={12}>
          <Field name="isLastSet" label="Last Set" component={Switch} />
        </Grid>

        <Grid container spacing={2}>
          <Grid item xs={12} md={6}>
            <Field
              name="roundsPerMatch"
              label="Rounds per Match"
              component={TextField}
              type="number"
              helperText="The match will end when this number of rounds have been played. Set to 0 to disable."
            />
          </Grid>
          <Grid item xs={12} md={6}>
            <Field
              name="winsPerMatch"
              label="Wins per Match"
              component={TextField}
              type="number"
              helperText="The match will end when one team has won this many rounds. Set to 0 to disable."
            />
          </Grid>
        </Grid>
      </Grid>
    </Form>
  );
}
