import { useAuth } from '@/util/auth';
import { WhiteboardProvider } from './context';
import { CurrentMatch } from './current_match';
import { QR } from './qr';
import { SessionConfig } from './session_config';
import { SessionPlayerList } from './session_player_list';
import { SessionStatus } from './session_status';
import styles from './Whiteboard.module.css';

export function WhiteboardAdmin({ session, scene }) {
  const { isAdminOf } = useAuth();

  if (!isAdminOf(scene.id)) {
    return <>You do not have permission to update settings.</>;
  }

  return (
    <WhiteboardProvider session={session}>
      <div className={styles.page}>
        <div className={styles.columns}>
          <div className={styles.leftColumn}>
            <CurrentMatch />
            <SessionPlayerList />
          </div>

          <div className={styles.rightColumn}>
            <QR />
            <SessionStatus />
            <SessionConfig />
          </div>
        </div>
      </div>
    </WhiteboardProvider>
  );
}
