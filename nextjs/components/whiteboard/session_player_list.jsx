import React, { useEffect, useState, useMemo } from 'react';
import { CircularProgress } from '@material-ui/core';
import Icon from '@mdi/react';
import { mdiCheck, mdiClose, mdiSquare } from '@mdi/js';
import clsx from 'clsx';

import styles from './Whiteboard.module.css';
import { getAxios } from '@/util/axios';
import { useAuth } from '@/util/auth';
import { useWhiteboard } from './context';
import { ToggleActiveButton } from './toggle_active_button';
import { roleIcon } from './constants';

export function SessionPlayer({ signup }) {
  const axios = getAxios({ authenticated: true });
  const { currentMatch, session, bluePlayerIds, goldPlayerIds } = useWhiteboard();

  const icon = useMemo(() => {
    if (bluePlayerIds?.has(signup.playerId))
      return <Icon className={clsx(styles.isReady, styles.blue)} size={1} path={mdiSquare} />;
    if (goldPlayerIds?.has(signup.playerId))
      return <Icon className={clsx(styles.isReady, styles.gold)} size={1} path={mdiSquare} />;

    return null;
  }, [signup, bluePlayerIds, goldPlayerIds]);

  return (
    <div className={styles.player}>
      <img className={styles.playerImage} src={signup.user.image} />
      <div className={styles.playerName}>{signup.user.name}</div>
      <div className={styles.roles}>
        {signup.roles.map(role => (
          <Icon key={role} path={roleIcon[role]} size={1} />
        ))}
      </div>

      <div className={styles.playerActiveStatus}>
        {icon}

        <ToggleActiveButton signup={signup} />
      </div>
    </div>
  );
}

export function SessionPlayerList() {
  const axios = getAxios({ authenticated: true });
  const { isAdminOf } = useAuth();
  const { scene, currentMatch, playerList, bluePlayerIds, goldPlayerIds } = useWhiteboard();

  const booleanSort = (a, b, fn) => {
    if (fn(a) && !fn(b)) return -1;
    if (fn(b) && !fn(a)) return 1;
    return 0;
  };

  const arraySort = (a, b) => {
    if (a.length === 0 && b.length === 0) return 0;
    if (a.length === 0) return -1;
    if (b.length === 0) return 1;
    if (!isNaN(a[0]) && !isNaN(b[0]) && a[0] !== b[0]) return parseFloat(a[0]) - parseFloat(b[0]);
    if (a[0] !== b[0]) return a[0].localeCompare(b[0]);
    return arraySort(a.slice(1, a.length), b.slice(1, b.length));
  };

  const sortFunction = (a, b) => {
    return (
      (bluePlayerIds && booleanSort(a, b, v => bluePlayerIds.has(v.playerId))) ||
      (goldPlayerIds && booleanSort(a, b, v => goldPlayerIds.has(v.playerId))) ||
      booleanSort(a, b, v => v.isReady) ||
      arraySort(a.priority, b.priority) ||
      a?.user?.name?.localeCompare(b?.user?.name)
    );
  };

  const sortedSignups = useMemo(
    () => playerList?.sort(sortFunction),
    [playerList, bluePlayerIds, goldPlayerIds],
  );

  if (!isAdminOf(scene.id)) return null;
  if (playerList === null) return <CircularProgress />;

  return (
    <div className={styles.playerList}>
      <div className={styles.title}>Registered Players</div>

      {sortedSignups.map(signup => (
        <SessionPlayer signup={signup} key={signup.id} />
      ))}
    </div>
  );
}
