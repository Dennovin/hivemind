import React from 'react';
import { Button } from '@material-ui/core';

import { useAuth } from '@/util/auth';
import { useWhiteboard } from './context';
import styles from './Whiteboard.module.css';

export function SessionAdminButton() {
  const { scene, session } = useWhiteboard();
  const { isAdminOf } = useAuth();

  if (!isAdminOf(scene?.id)) return null;

  return (
    <Button
      className={styles.sessionAdminButton}
      variant="outlined"
      color="primary"
      href={`/whiteboard/${session.id}/admin`}
    >
      Session Admin
    </Button>
  );
}
