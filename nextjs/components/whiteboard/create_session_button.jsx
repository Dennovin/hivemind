import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { Button, CircularProgress, MenuItem, Grid } from '@material-ui/core';

import { DEFAULT_ROUNDS_PER_MATCH } from '@/util/constants';
import Form from '@/components/forms/Form';
import Field, { TextField, Select, Switch } from '@/components/fields/Field';
import { getAxios } from '@/util/axios';
import { useAuth } from '@/util/auth';
import { useWhiteboard } from './context';

export function CreateSessionButton({ scene }) {
  const axios = getAxios({ authenticated: true });
  const router = useRouter();
  const [cabinets, setCabinets] = useState(null);
  const [sessionExists, setSessionExists] = useState(null);
  const [existingSessionId, setExistingSessionId] = useState(null);
  const whiteboard = useWhiteboard();
  const { isAdminOf } = useAuth();

  const sceneId = scene?.id;
  const sessionConfig = {
    scene: sceneId,
    cabinetId: null,
    roundsPerMatch: DEFAULT_ROUNDS_PER_MATCH,
    winsPerMatch: 0,
    isActive: true,
    isPaused: false,
    isLastSet: false,
  };

  const formProps = {
    buttonText: 'Create Whiteboard Session',
    dialogTitle: 'Create Whiteboard Session',
    canEdit: () => true,
    object: sessionConfig,
    url: '/api/whiteboard/session/',
    method: 'POST',
    onSave: result => {
      router.push(`/whiteboard/${result.id}`);
    },
  };

  useEffect(() => {
    if (cabinets === null) {
      axios.getAllPages(`/api/game/cabinet/`, { params: { sceneId } }).then(setCabinets);
    }
  }, []);

  useEffect(() => {
    if (sessionExists === null) {
      axios
        .get(`/api/whiteboard/session/`, { params: { sceneId: scene.id, isActive: true } })
        .then(response => {
          if (response?.data?.results?.length > 0) {
            setSessionExists(true);
            setExistingSessionId(response.data.results[0].id);
          } else {
            setSessionExists(false);
          }
        });
    }
  }, [scene, sessionExists]);

  if (sessionExists) {
    return (
      <Button variant="outlined" color="secondary" href={`/whiteboard/${existingSessionId}`}>
        Join Whiteboard Session
      </Button>
    );
  }

  if (sessionExists === null) return <CircularProgress />;

  return (
    <Form {...formProps}>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Field name="cabinet" label="Cabinet" component={Select}>
            <MenuItem value="">(None)</MenuItem>
            {cabinets !== null &&
              cabinets.map(cabinet => (
                <MenuItem key={cabinet.id} value={cabinet.id}>
                  {cabinet.displayName}
                </MenuItem>
              ))}
          </Field>
        </Grid>

        <Grid container spacing={2}>
          <Grid item xs={12} md={6}>
            <Field
              name="roundsPerMatch"
              label="Rounds per Match"
              component={TextField}
              type="number"
              helperText="The match will end when this number of rounds have been played. Set to 0 to disable."
            />
          </Grid>
          <Grid item xs={12} md={6}>
            <Field
              name="winsPerMatch"
              label="Wins per Match"
              component={TextField}
              type="number"
              helperText="The match will end when one team has won this many rounds. Set to 0 to disable."
            />
          </Grid>
        </Grid>
      </Grid>
    </Form>
  );
}
