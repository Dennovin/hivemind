import { Slider } from '@material-ui/core';

import { useForm } from '@/components/forms/Form';
import styles from './Whiteboard.module.css';

export function SelfRatingSlider({ name, label, ...props }) {
  const form = useForm();
  const steps = [
    'I have never tried this role before.',
    'I have played in this role, but not regularly.',
    'I play in this role regularly at a local league night.',
    'I play this role often in regional or national tournaments.',
    'I am one of the best in the world at this role.',
  ];

  const onChange = (event, value) => {
    form.setFieldValue(name, value);
  };

  const value = form.values[name];
  const stepCaption = steps[value];

  return (
    <div className={styles.slider}>
      <div className={styles.label}>{label}</div>
      <Slider
        name={name}
        label={label}
        aria-label={label}
        marks
        min={0}
        max={4}
        {...props}
        onChange={onChange}
      />
      <div className={styles.caption}>{stepCaption}</div>
    </div>
  );
}
