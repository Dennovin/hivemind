import { utcDateFromString } from 'util/dates';

const Wrapper = ({ children }) => (
  <div className="rounded border border-solid border-primary p-12 text-center">
    {children}
    <div className="text-sm">Contact the scene organizers for more info</div>
  </div>
);

export const displayRegistrationClosedMessage = (closeDate, allowRegistration) =>
  !allowRegistration ||
  closeDate === null ||
  closeDate === undefined ||
  utcDateFromString(closeDate) < Date.now();

export default function RegistrationClosedMessage({ closeDate, allowRegistration }) {
  const registrationCloseDate = utcDateFromString(closeDate);

  if (!allowRegistration)
    return (
      <Wrapper>
        <h2>Registration is elsewhere</h2>
      </Wrapper>
    );
  if (closeDate === undefined || closeDate === null)
    return (
      <Wrapper>
        <h2>Registration has not yet opened.</h2>
      </Wrapper>
    );

  if (registrationCloseDate < Date.now())
    return (
      <Wrapper>
        <h2>
          Registration has closed as of{' '}
          {registrationCloseDate.toLocaleString('en-US', { style: 'medium' })}
        </h2>
      </Wrapper>
    );

  return null;
}
