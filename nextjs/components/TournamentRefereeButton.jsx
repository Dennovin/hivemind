import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Card, Grid, Typography, CircularProgress, Button } from '@material-ui/core';
import Icon from '@mdi/react';
import {
  mdiFlagCheckered,
  mdiFruitGrapes,
  mdiMap,
  mdiPencil,
  mdiThemeLightDark,
  mdiTimer,
  mdiWeatherNight,
  mdiWeatherSunny,
  mdiWeatherSunset,
} from '@mdi/js';
import clsx from 'clsx';

import { MAP_NAMES } from '@/util/constants';
import { getAxios } from '@/util/axios';
import { useWebSocket } from '@/util/websocket';
import { formatUTC } from '@/util/dates';
import { isAdminOf } from '@/util/auth';
import { useTournamentAdmin } from '@/components/tournament-admin';
import TournamentMatchFormReferee from '@/components/forms/TournamentMatchFormReferee';
import TournamentQueueTable from '@/components/tables/TournamentQueueTable';

const useStyles = makeStyles(theme => ({
  row: {
    padding: theme.spacing(1, 2),

    alignItems: 'center',
    '&:first-child': {
      borderWidth: 0,
    },
  },
  roundNameRow: {
    backgroundColor: theme.palette.background.default,
    height: '2.25rem',
    fontStyle: 'italic',
    borderBottom: `1px solid ${theme.palette.divider}`,
    textAlign: 'right',
  },
  teamRow: {
    borderColor: theme.palette.divider,
    borderStyle: 'solid',
    borderWidth: '0 0 1px',
    padding: 0,
    '&:last-child': {
      borderBottom: 0,
    },
    '&:before': {
      content: '""',
      width: '1.75rem',
      height: '1.75rem',
      marginLeft: '1rem',
      display: 'block',
    },
  },
  blue: {
    '&:before': {
      background: theme.gradients.blue.light,
    },
  },
  gold: {
    '&:before': {
      background: theme.gradients.gold.light,
    },
  },
  cabinetNameRow: {
    display: 'flex',
    justifyContent: 'space-between',
    backgroundColor: theme.palette.gold.light3,
    '.online &': {
      backgroundColor: theme.palette.gold.light3,
    },
  },
  cabinetName: {
    fontWeight: 'bold',
  },
  gameStats: {
    backgroundColor: theme.palette.grey['800'],
  },
  teamName: {
    flexGrow: 1,
    flexBasis: 0,
    padding: theme.spacing(1.5, 2),
  },
  teamScore: {
    flexGrow: 0,
    flexBasis: '100px',
    borderLeft: `1px solid ${theme.palette.divider}`,
    textAlign: 'right',
    padding: theme.spacing(1.5, 2),
  },
  subtitleItem: {
    flexBasis: 0,
    flexGrow: 1,
    textAlign: 'center',
    background: theme.palette.grey['800'],
    color: 'white',
    '& svg': {
      width: '24px',
      height: '24px',
      verticalAlign: 'middle',
      '&:first-child': {
        verticalAlign: 'top',
      },
    },
  },
}));

export default function TournamentRefereeButton({ cabinet }) {
  const axios = getAxios({ authenticated: true });
  const [activeMatch, setActiveMatch] = useState(undefined);
  const [online, setOnline] = useState(cabinet.clientStatus === 'online');
  const [stats, setStats] = useState(null);
  const [formOpen, setFormOpen] = useState(false);
  const { tournament, wsGameState, teamsById, hideInactiveCabs } = useTournamentAdmin();
  const classes = useStyles();

  const mapIcons = {
    Day: <Icon size={0.8} path={mdiWeatherSunny} />,
    Night: <Icon size={0.8} path={mdiWeatherNight} />,
    Dusk: <Icon size={0.8} path={mdiWeatherSunset} />,
    Twilight: <Icon size={0.8} path={mdiThemeLightDark} />,
  };

  const allTeams = {};
  for (const team of tournament.teams) {
    allTeams[team.id] = team;
  }

  const finishMatch = async () => {
    let response = await axios.get(`/api/tournament/match/${activeMatch.id}/`);
    const data = response.data;

    data.isComplete = true;
    data.activeCabinet = null;

    response = await axios.put(`/api/tournament/match/${activeMatch.id}/`, data);

    setFormOpen(false);
    loadActiveMatch();
  };

  wsGameState.onJsonMessage(message => {
    if (message.type == 'match' && message.cabinetId == cabinet.id) {
      loadActiveMatch();
    }

    if (message.type == 'cabinetOnline' && message.cabinetId == cabinet.id) {
      setOnline(true);
    }

    if (message.type == 'cabinetOffline' && message.cabinetId == cabinet.id) {
      setOnline(false);
    }
  });

  const onOpenStats = () => {
    wsStats.sendJsonMessage({ type: 'subscribe', cabinet_id: cabinet.id });
  };

  const wsStats = useWebSocket('/ws/ingame_stats', { onOpen: onOpenStats });
  wsStats.onJsonMessage(message => {
    setStats(message);
  });

  const handleClick = () => {
    setFormOpen(true);
  };

  const loadActiveMatch = () => {
    axios
      .get(`/api/tournament/match/`, { params: { activeCabinetId: cabinet.id } })
      .then(response => {
        if (response.data?.count > 0) {
          setActiveMatch(response.data.results[0]);
        } else {
          setActiveMatch({
            tournament: tournament.id,
            activeCabinet: cabinet.id,
            blueScore: 0,
            goldScore: 0,
          });
        }
      });
  };

  const handleClose = () => {
    setFormOpen(false);
  };

  useEffect(() => {
    if (activeMatch === undefined) {
      loadActiveMatch();
    }
  }, []);

  if (activeMatch === undefined) {
    return <CircularProgress />;
  }

  const blueTeamName = activeMatch?.blueTeam ? allTeams[activeMatch.blueTeam]?.name : '(no game)';
  const goldTeamName = activeMatch?.goldTeam ? allTeams[activeMatch.goldTeam]?.name : '(no game)';

  // TODO: add this button to ref dash
  const dialogActions = activeMatch?.id && (
    <Button variant="outlined" color="primary" onClick={finishMatch}>
      Match Complete
    </Button>
  );

  return (
    <>
      <Button
        variant="outlined"
        color="secondary"
        size="small"
        onClick={handleClick}
        startIcon={<Icon path={mdiFlagCheckered} size={0.8} />}
      >
        Ref
      </Button>
      <TournamentMatchFormReferee
        tournament={tournament}
        cabinet={cabinet}
        cabinetID={cabinet.id}
        activeMatch={activeMatch}
        open={formOpen}
        onClose={handleClose}
        dialogActions={dialogActions}
        stats={stats}
      />
    </>
  );
}
