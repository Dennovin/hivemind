import 'react-phone-number-input/style.css';
import React, { useState, useMemo } from 'react';
import PhoneInput, { isPossiblePhoneNumber } from 'react-phone-number-input';
import { Button } from '@material-ui/core';

import styles from './sms-config.module.css';
import { useSMSConfig } from './context';

export function PhoneNumberInput() {
  const { submitPhoneNumber } = useSMSConfig();
  const [value, setValue] = useState('');

  const isValid = useMemo(() => {
    if (value) {
      return isPossiblePhoneNumber(value);
    }

    return false;
  }, [value]);

  const onKeyDown = evt => {
    if (evt?.key === 'Enter') {
      submitPhoneNumber(value);
    }
  };

  return (
    <div className={styles.container}>
      <div>
        Set up SMS notifications to receive a text message when your team is about to play. Message
        and data rates may apply. Reply STOP to opt-out.
      </div>

      <div>
        <PhoneInput
          defaultCountry="US"
          placeholder="Enter phone number"
          value={value}
          onChange={setValue}
          onKeyDown={onKeyDown}
          autoFocus={true}
        />

        <div className={styles.buttons}>
          <Button
            variant="outlined"
            color="primary"
            disabled={!isValid}
            onClick={() => submitPhoneNumber(value)}
          >
            Save
          </Button>
        </div>
      </div>
    </div>
  );
}
