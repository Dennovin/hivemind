import React from 'react';
import {
  Checkbox,
  FormControl,
  FormControlLabel,
  FormHelperText,
  IconButton,
} from '@material-ui/core';
import Icon from '@mdi/react';
import { mdiDelete } from '@mdi/js';

import styles from './sms-config.module.css';
import { useSMSConfig } from './context';

export function ConfigForm() {
  const { config, remove, update } = useSMSConfig();

  return (
    <div className={styles.container}>
      <div>
        <span>
          Notifications will be sent to <b>{config.phoneNumber}</b>.
        </span>

        <div className={styles.iconButtons}>
          <IconButton onClick={remove}>
            <Icon path={mdiDelete} size={1} title="Remove" />
          </IconButton>
        </div>
      </div>

      <div>
        <FormControl>
          <FormControlLabel
            label="Notify on Tournament Match"
            control={
              <Checkbox
                checked={config.notifyOnTournamentMatch}
                onChange={() =>
                  update({ notifyOnTournamentMatch: !config.notifyOnTournamentMatch })
                }
              />
            }
          />
          <FormHelperText>Send a text message when your team is about to play.</FormHelperText>
        </FormControl>
      </div>

      <div>
        <FormControl>
          <FormControlLabel
            label="Notify on Whiteboard Match"
            control={
              <Checkbox
                checked={config.notifyOnWhiteboardMatch}
                onChange={() =>
                  update({ notifyOnWhiteboardMatch: !config.notifyOnWhiteboardMatch })
                }
              />
            }
          />
          <FormHelperText>
            Send a text message when you are selected to play in a whiteboard match.
          </FormHelperText>
        </FormControl>
      </div>

      <div>
        <FormControl>
          <FormControlLabel
            label="Notify on Message"
            control={
              <Checkbox
                checked={config.notifyOnMessage}
                onChange={() => update({ notifyOnMessage: !config.notifyOnMessage })}
              />
            }
          />
          <FormHelperText>
            Send a text message when you have a new message on HiveMind.
          </FormHelperText>
        </FormControl>
      </div>
    </div>
  );
}
