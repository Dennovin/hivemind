import React from 'react';

import { SMSConfigProvider, useSMSConfig } from './context';
import { PhoneNumberInput } from './phone-number-input';
import { ValidateInput } from './validate-input';
import { ConfigForm } from './config-form';

export function SMSConfigStep() {
  const { config } = useSMSConfig();

  if (!config?.id) return <PhoneNumberInput />;
  if (!config.isValidated) return <ValidateInput />;
  return <ConfigForm />;
}

export function SMSConfig() {
  return (
    <SMSConfigProvider>
      <SMSConfigStep />
    </SMSConfigProvider>
  );
}
