import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Typography, Grid, CircularProgress } from '@material-ui/core';
import { format } from 'date-fns';

import UserStatsSummaryTable from 'components/tables/UserStatsSummaryTable';
import { getAxios } from 'util/axios';

export default function SessionStatsBox({ title, session, stats, ...props }) {
  const axios = getAxios();
  const [cabinet, setCabinet] = useState(null);

  useEffect(() => {
    if (session?.cabinet) {
      axios.get(`/api/game/cabinet/${session.cabinet}/`).then(response => {
        setCabinet(response.data);
      });
    }
  }, [session?.cabinet]);

  const subtitle = format(new Date(session.startTime), 'MMM d, yyyy') + ' at ' +
        (session.cabinet ? (cabinet?.displayName ?? '') : 'multiple cabinets');

  return (
    <UserStatsSummaryTable title="Your Last Session" subtitle={subtitle} stats={stats} {...props} />
  );
}
