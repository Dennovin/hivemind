import React from 'react';

import { TOURNAMENT_LINK_TYPES } from 'util/constants';
import TournamentMatchFormManual from './TournamentMatchFormManual';
import TournamentMatchFormChallonge from './TournamentMatchFormChallonge';
import TournamentMatchFormHiveMind from './TournamentMatchFormHiveMind';

export default function TournamentMatchForm({ tournament, ...props }) {
  if (tournament.linkType == TOURNAMENT_LINK_TYPES.MANUAL) {
    return <TournamentMatchFormManual tournament={tournament} {...props} />;
  }

  if (tournament.linkType == TOURNAMENT_LINK_TYPES.CHALLONGE) {
    return <TournamentMatchFormChallonge tournament={tournament} {...props} />;
  }

  if (tournament.linkType == TOURNAMENT_LINK_TYPES.HIVEMIND) {
    return <TournamentMatchFormHiveMind tournament={tournament} {...props} />;
  }
}
