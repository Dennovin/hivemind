import React, { useState } from 'react';
import { Typography } from '@material-ui/core';
import { useRouter } from 'next/router';

import Form from './Form';
import Field, { Switch } from '../fields/Field';
import { getAxios } from 'util/axios';

export default function EventBYOTeamSelectForm({ event }) {
  const axios = getAxios({ authenticated: true });
  const router = useRouter();
  const [teams, setTeams] = useState(null);

  if (teams === null) {
    axios.get('/api/league/byoteam/', { params: { seasonId: event.season.id } }).then(response => {
      setTeams(response.data.results);
    });

    return (<></>);
  }

  const postdata = {numTeams: 0, teamsToInclude: []};
  for (const team of teams) {
    postdata[team.id] = true;
  }

  const props = {
    buttonText: 'Select Teams',
    dialogTitle: 'Select Teams',
    canEdit: async () => true,
    object: postdata,
    url: `/api/league/event/${event.id}/select-teams/`,
    method: 'PUT',
    saveButtonText: 'Generate Matches',
    onSave: router.reload,
  };

  return (
    <Form {...props}>
      {teams.sort((a, b) => a.name.localeCompare(b.name)).map(team => (
        <Field key={team.id} name={team.id} label={team.name} component={Switch} />
      ))}
    </Form>
  );
}
