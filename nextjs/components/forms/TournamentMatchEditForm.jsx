import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Icon from '@mdi/react';
import { Button, Typography, Grid } from '@material-ui/core';
import { mdiCloudSync, mdiSwapHorizontalBold } from '@mdi/js';
import { toast } from 'react-toastify';
import clsx from 'clsx';
import { format } from 'date-fns';

import { WIN_CONDITIONS, TOURNAMENT_LINK_TYPES, PERMISSION_TYPES } from '@/util/constants';
import { formatGameTime } from '@/util/dates';
import Form, { useForm } from './Form';
import List from './List';
import Field, { TextField, Switch } from '../fields/Field';
import { GameMeta } from '@/components/tables/GameTable';
import { useAuth } from '@/util/auth';
import { getAxios } from '@/util/axios';
import { useTournamentAdmin } from '@/components/tournament-admin';
import {
  mdiFruitGrapes,
  mdiSnail,
  mdiSword,
  mdiThemeLightDark,
  mdiWeatherNight,
  mdiWeatherSunny,
  mdiWeatherSunset,
} from '@mdi/js';

const useStyles = makeStyles(theme => ({}));
const validPermissions = [PERMISSION_TYPES.ADMIN, PERMISSION_TYPES.TOURNAMENT];

export function ReportResultFunction({ match }) {
  const axios = getAxios({ authenticated: true });
  const formik = useForm();
  const [isLoading, setIsLoading] = useState(false);

  const reportResult = async () => {
    setIsLoading(true);
    await axios.put(`/api/tournament/match/${match.id}/report/`);
    setIsLoading(false);
    toast.success('Match result sent.');
  };

  return (
    <Button
      variant="outlined"
      color="secondary"
      onClick={reportResult}
      disabled={isLoading}
      startIcon={<Icon size={1} path={mdiCloudSync} />}
    >
      Report Result
    </Button>
  );
}

export function SwapTeamsFunction({ match }) {
  const axios = getAxios({ authenticated: true });
  const { reloadTournament } = useTournamentAdmin();
  const formik = useForm();
  const [isLoading, setIsLoading] = useState(false);

  const swapTeams = async () => {
    setIsLoading(true);

    await axios.patch(`/api/tournament/match/${match.id}/`, {
      blueTeam: match.goldTeam,
      goldTeam: match.blueTeam,
      isFlipped: !match.isFlipped,
    });

    await axios.put(`/api/tournament/match/${match.id}/report/`);
    reloadTournament();

    setIsLoading(false);
    toast.success('Teams swapped and match result sent.');
    console.log(match);
  };

  return (
    <Button
      variant="outlined"
      color="secondary"
      onClick={swapTeams}
      disabled={isLoading}
      startIcon={<Icon size={1} path={mdiSwapHorizontalBold} />}
    >
      Swap Blue/Gold
    </Button>
  );
}

export default function TournamentMatchEditForm({ tournament, match, isOpen, onClose }) {
  const classes = useStyles();
  const axios = getAxios({ authenticated: true });
  const [games, setGames] = useState([]);
  const { hasAnyPermission } = useAuth();

  const blueTeamName = tournament?.teamsById[match.blueTeam]?.name ?? '(Unknown Team)';
  const goldTeamName = tournament?.teamsById[match.goldTeam]?.name ?? '(Unknown Team)';

  const winConditionIcons = {
    Military: <Icon size={0.8} className={classes.gameIcon} path={mdiSword} />,
    Economic: <Icon size={0.8} className={classes.gameIcon} path={mdiFruitGrapes} />,
    Snail: <Icon size={0.8} className={classes.gameIcon} path={mdiSnail} />,
  };

  const mapIcons = {
    Day: <Icon size={0.8} className={classes.gameIcon} path={mdiWeatherSunny} />,
    Night: <Icon size={0.8} className={classes.gameIcon} path={mdiWeatherNight} />,
    Dusk: <Icon size={0.8} className={classes.gameIcon} path={mdiWeatherSunset} />,
    Twilight: <Icon size={0.8} className={classes.gameIcon} path={mdiThemeLightDark} />,
  };

  const addGame = async gameId => {
    let response = await axios.get(`/api/game/game/${gameId}/`);
    const game = response.data;

    if (game?.cabinet?.scene !== tournament.scene.id) {
      toast.error('This game is not associated with the same scene as this tournament.');
      return false;
    }

    if (game?.tournamentMatch !== null) {
      toast.error('This game is already part of a tournament match.');
      return false;
    }

    response = await axios.patch(`/api/game/game/${gameId}/`, { tournamentMatch: match.id });

    setGames(v => [...v, game]);
  };

  const deleteGame = async game => {
    await axios.patch(`/api/game/game/${game.id}/`, { tournamentMatch: null });
    setGames(v => v.filter(g => g.id !== game.id));
  };

  const formProps = {
    dialogTitle: `Editing Tournament Match: ${blueTeamName} vs. ${goldTeamName}`,
    canEdit: match => hasAnyPermission(tournament.scene.id, validPermissions),
    object: match,
    url: `/api/tournament/match/${match.id}/`,
    method: 'PATCH',
    enableReinitialize: true,
    onClose,
    open: isOpen,
    showButton: false,
  };

  if (tournament.linkType == TOURNAMENT_LINK_TYPES.CHALLONGE) {
    formProps.dialogActions = (
      <>
        <SwapTeamsFunction match={match} />
        <ReportResultFunction match={match} />
      </>
    );
  } else {
    formProps.dialogActions = <SwapTeamsFunction match={match} />;
  }

  useEffect(() => {
    axios
      .getAllPages(`/api/game/game/`, { params: { tournamentMatchId: match.id } })
      .then(setGames);
  }, [isOpen]);

  if (!isOpen) {
    return <></>;
  }

  return (
    <Form {...formProps}>
      <Grid container spacing={2}>
        <Grid item xs={12} md={6}>
          <Field name="blueScore" label="Blue Score" component={TextField} type="number" />
        </Grid>
        <Grid item xs={12} md={6}>
          <Field name="goldScore" label="Gold Score" component={TextField} type="number" />
        </Grid>
      </Grid>

      <Field name="isComplete" label="Match is Complete" component={Switch} />
      <Field name="videoLink" label="Video URL" component={TextField} />

      <List
        title="Game IDs"
        getItemText={game => (
          <div className="grid grid-cols-10">
            <div className="col-span-1">{game.id}</div>
            <div className="col-span-1">{mapIcons[game.mapName]}</div>
            <div className="col-span-1">{winConditionIcons[WIN_CONDITIONS[game.winCondition] ?? game.winCondition]}</div>
            <div className="col-span-3">{game.cabinet.displayName}</div>
            <div className="col-span-2">{formatGameTime(game)}</div>
            <div className="col-span-2">{format(new Date(game.endTime), 'MMM d - hh:mm aa')}</div>
	  </div>
        )}
        items={games}
        onAdd={addGame}
        onDelete={deleteGame}
        newItemPlaceholderText="Game ID"
      />
    </Form>
  );
}
