import React, { useRef, useState, useEffect } from 'react';
import { CircularProgress, TextField, Button } from '@material-ui/core';

import Form from './Form';
import Field from '../fields/Field';
import PlayerListAutocomplete from '../fields/PlayerListAutocomplete';
import { getAxios } from 'util/axios';
import { isAdminOf } from 'util/auth';

export default function AddPlayerForm({ event, players, ...props }) {
  const axios = getAxios({ authenticated: true });

  const getUnassignedPlayers = async (values) => {
    const ids = [];
    for(const player of values.unassignedPlayers) {
      if (player.id) {
        ids.push(player.id);
      } else {
        const response = await axios.post('/api/league/player/', { name: player.value, scene: event.season.scene.id });
        ids.push(response.data.id);
      }
    }

    return ids;
  };

  const formObj = {
    id: event.id,
    unassignedPlayers: players,
  };

  useEffect(() => {
    formObj.unassignedPlayers = players;
  }, [players]);

  const formProps = {
    buttonText: 'Edit Player List',
    dialogTitle: 'Edit Player List',
    canEdit: formObj => isAdminOf(event.season.scene.id),
    object: formObj,
    url: `/api/league/event/${event.id}/`,
    method: 'PATCH',
    enableReinitialize: true,
    getPostData: {
      unassignedPlayers: getUnassignedPlayers,
    },
    ...props,
  };

  return (
    <Form {...formProps}>
      <PlayerListAutocomplete name="unassignedPlayers" scene={event.season.scene} />
    </Form>
  );
}


