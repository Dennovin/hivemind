import React, { useState, useMemo } from 'react';
import { MenuItem } from '@material-ui/core';
import { mdiPencil } from '@mdi/js';

import { getAxios } from 'util/axios';
import { useUser } from 'util/auth';
import Form from './Form';
import Field, { Select, TextField, CabinetSelect, Conditional } from '../fields/Field';

export default function ClientDeviceForm({ device, onClose, allScenes }) {
  const axios = getAxios({ authenticated: true });
  const user = useUser();

  const userScenes = useMemo(() => {
    if (!user?.permissions || !allScenes) return [];
    return user.permissions
      .filter(p => p.permission === 'admin')
      .map(p => allScenes.filter(s => s.id === p.scene)[0] || p.scene)
      .sort((a, b) => a.displayName?.localeCompare(b.displayName));
  }, [user?.permissions, allScenes]);

  return (
    <Form
      buttonText={device?.id ? 'Edit' : 'Add New Device'}
      dialogTitle={device?.id ? `Editing Device: ${device.name}` : 'Add New Client Device'}
      canEdit={() => true}
      object={device}
      url={device?.id ? `/api/game/client-device/${device.id}/` : '/api/game/client-device/'}
      method={device?.id ? 'PUT' : 'POST'}
      onClose={onClose}
      buttonIconPath={device?.id ? mdiPencil : undefined}
    >
      <Field name="name" label="Device Name" component={TextField} />
      <Field name="owner" label="Owner" component={Select}>
        {userScenes.map(scene => (
          <MenuItem value={scene.id}>{scene.displayName}</MenuItem>
        ))}
      </Field>
      <Field
        name="cabinet"
        label="Assigned Cabinet"
        component={CabinetSelect}
        sceneFilter={scene => userScenes.map(s => s.id).includes(scene.id)}
      />

      {device?.id && (
        <>
          <Field
            name="id"
            label="Device ID"
            component={TextField}
            InputProps={{ readOnly: true }}
            withCopyButton
          />

          <Field
            name="token"
            label="Client Token"
            component={TextField}
            InputProps={{ readOnly: true }}
            withCopyButton
          />
        </>
      )}
    </Form>
  );
}
