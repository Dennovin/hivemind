import React, { useState } from 'react';
import { MenuItem, Grid } from '@material-ui/core';
import PropTypes from 'prop-types';
import * as Yup from 'yup';
import { toast } from 'react-toastify';
import { useRouter } from 'next/router';

import Form from './Form';
import List from './List';
import Field, { Radio, RadioGroup, TextField, DateField } from '../fields/Field';
import { isAdminOf } from 'util/auth';
import { getAxios } from 'util/axios';
import { SEASON_TYPES } from 'util/constants';

const SeasonSchema = Yup.object().shape({
  name: Yup.string()
    .required('Season name is required'),
  startDate: Yup.string().nullable(true).transform(v => v || null),
  endDate: Yup.string().nullable(true).transform(v => v || null),
  dropLowestScores: Yup.number().integer().transform(v => v || 0),
});

export default function SeasonForm({ season }) {
  const axios = getAxios({ authenticated: true });
  const router = useRouter();

  const props = {
    buttonText: season.id ? 'Edit' : 'Create New',
    dialogTitle: season.id ? `Editing Season: ${season.name}` : 'Create New Season',
    canEdit: season => isAdminOf(season.scene.id),
    object: season,
    validationSchema: SeasonSchema,
    url: season.id ? `/api/league/season/${season.id}/` : '/api/league/season/',
    method: season.id ? 'PUT' : 'POST',
    getPostData: {
      'scene': values => values.scene.id,
    },
    onSave: result => {
      if (!season.id) {
        router.push(`/season/${result.id}`);
      }
    },
  };

  return (
    <Form {...props}>
      <Field name="name" label="Season Name" component={TextField} />

      <Grid container spacing={2}>
        <Grid item xs={12} md={6}>
          <Field name="startDate" label="Start Date" component={DateField} />
        </Grid>
        <Grid item xs={12} md={6}>
          <Field name="endDate" label="End Date" component={DateField} />
        </Grid>
      </Grid>

      {season.seasonType == SEASON_TYPES.SHUFFLE && (
        <Field name="dropLowestScores" label="Drop Lowest Scores" component={TextField} type="number" />
      )}

      {!season.id && (
        <Field name="seasonType" label="League Type" component={RadioGroup}>
          <Radio value={SEASON_TYPES.SHUFFLE} label="Shuffle"
                 helperText="Teams are selected by HiveMind. Teams can be shuffled at the start of an event or at the start of each round." />
          <Radio value={SEASON_TYPES.BYOT} label="Bring Your Own Team"
                 helperText="Players select their own teams and stay with the same team for the entire season. Free agents can be assigned to a team at the beginning of the event." />
        </Field>
      )}
    </Form>
  );
}

SeasonForm.propTypes = {
  season: PropTypes.object,
};

SeasonForm.defaultProps = {
  season: null,
};
