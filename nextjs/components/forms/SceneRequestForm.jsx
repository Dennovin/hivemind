import React, { useState } from 'react';
import { Paper, Typography } from '@material-ui/core';
import PropTypes from 'prop-types';
import * as Yup from 'yup';
import { toast } from 'react-toastify';

import Form from './Form';
import List from './List';
import Field, { TextField } from '../fields/Field';
import { getAxios } from 'util/axios';

const SceneRequestSchema = Yup.object().shape({
  userName: Yup.string()
    .required('Name is required'),
  name: Yup.string()
    .required('Scene name is required')
    .min(3, 'Too short')
    .max(20, 'Too long')
    .matches('^[a-z0-9]+$', 'Letters and numbers only'),
  displayName: Yup.string().required()
    .required('Display name is required')
    .max(30, 'Too long'),
  comments: Yup.string(),
});

export default function SceneRequestForm({ }) {
  const axios = getAxios({ authenticated: true });
  const [submitted, setSubmitted] = useState(false);

  const onSave = async (response) => {
    setSubmitted(true);
  };

  const data = {};
  for (const key of Object.keys(SceneRequestSchema.fields)) {
    data[key] = '';
  }

  const props = {
    dialog: false,
    object: data,
    validationSchema: SceneRequestSchema,
    url: `/api/user/scene-request/`,
    method: 'post',
    saveButtonText: 'Submit',
    onSave,
  };

  if (submitted) {
    return (
      <Typography>
        Your scene registration has been submitted. You will receive a message once it is approved.
      </Typography>
    );
  }

  return (
    <Form {...props}>
      <Field name="userName" label="Your Name" component={TextField} placeholder="Your name" />
      <Field name="name" label="Scene Name" component={TextField}
             helperText={`Lowercase letters and numbers only. Must be unique. (e.g. "bmore")`} />
      <Field name="displayName" label="Scene Display Name" component={TextField}
             helperText={`Your scene's name. (e.g. "Baltimore")`}/>
      <Field name="comments" label="Comments" component={TextField} multiline rows={5} />
    </Form>
  );
}
