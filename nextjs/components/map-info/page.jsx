import React from 'react';
import { getAxios } from 'util/axios';
import { mdiFruitGrapes, mdiSnail, mdiMap, mdiCrown, mdiSword, mdiClipboardOutline } from '@mdi/js';

import { Tabs, TabSection } from '@/components/tabs';
import Title from '@/components/Title';
import { SummaryTable, SummaryTableRow } from '@/components/tables/SummaryTable';
import styles from './map-info.module.css';

import { SnailTrack } from './snail-track';
import { Clipboard } from './clipboard';
import { HelperText } from './helper-text';

export function MapInfoPage({ map, heatmaps }) {
  return (
    <div>
      <Title title={`${map.displayName} Map`} />

      <Tabs>
        <TabSection name="clipboard" title="Clipboard" isDefault={true} icon={mdiClipboardOutline}>
          <Clipboard map={map} />
        </TabSection>
        <TabSection name="queen" title="Queen Deaths" icon={mdiCrown}>
          <img
            className={styles.mapImage}
            src={heatmaps.queen.image}
            alt={`Heatmap of queen deaths on ${map.displayName} map`}
          />
          <HelperText dateGenerated={heatmaps.queen.dateGenerated} />
        </TabSection>
        <TabSection name="warrior" title="Warrior Deaths" icon={mdiSword}>
          <img
            className={styles.mapImage}
            src={heatmaps.warrior.image}
            alt={`Heatmap of warrior deaths on ${map.displayName} map`}
          />
          <HelperText dateGenerated={heatmaps.warrior.dateGenerated} />
        </TabSection>
        <TabSection name="worker" title="Worker Deaths" icon={mdiFruitGrapes}>
          <img
            className={styles.mapImage}
            src={heatmaps.worker.image}
            alt={`Heatmap of worker deaths on ${map.displayName} map`}
          />
          <HelperText dateGenerated={heatmaps.worker.dateGenerated} />
        </TabSection>
        {map.snailTrackYLoc && (
          <TabSection name="snail" title="Snail Rider Deaths" icon={mdiSnail}>
            <SnailTrack map={map} heatmap={heatmaps.snail} />
            <HelperText
              dateGenerated={heatmaps.snail.dateGenerated}
              addText="Markings are in snail meters. 1 snail meter represents the approximate distance traveled by a vanilla snail rider in 1 second."
            />
          </TabSection>
        )}
      </Tabs>

      <SummaryTable>
        <SummaryTableRow icon={mdiFruitGrapes} label="Total Berries">
          {map.totalBerries}
        </SummaryTableRow>
        <SummaryTableRow icon={mdiSnail} label="Snail Track Width (Center to Net)">
          {map.snailTrackWidth} pixels
        </SummaryTableRow>
      </SummaryTable>
    </div>
  );
}
