import React, { useState, useMemo } from 'react';
import clsx from 'clsx';

import { useClipboard } from './context';
import { Draggable, useDraggable } from './draggable';
import styles from './clipboard.module.css';

function SnailInner({ map }) {
  const { flipped } = useDraggable();
  const { fullscreen } = useClipboard();

  return (
    <div>
      <img
        className={clsx(styles.snail, styles.flippable, { flipped })}
        src="/static/sprites/snail.png"
        alt="Snail"
        style={{ top: 1000 - map.snailTrackYLoc }}
      />
    </div>
  );
}

export function Snail({ ...props }) {
  return (
    <Draggable axis="x">
      <SnailInner {...props} />
    </Draggable>
  );
}
