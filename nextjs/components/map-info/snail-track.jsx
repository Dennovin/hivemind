import React, { useEffect, useRef } from 'react';

import styles from './snail-track.module.css';

export function SnailTrack({ map, heatmap }) {
  const canvasRef = useRef(null);

  useEffect(() => {
    if (!map.snailTrackYLoc) return;

    const canvas = canvasRef.current;
    const ctx = canvas.getContext('2d');

    const drawText = (text, x, y) => {
      ctx.font = 'bold 24px Roboto';
      ctx.textAlign = 'center';
      ctx.strokeStyle = '#000';
      ctx.fillStyle = '#ee8';
      ctx.strokeText(text, x, y);
      ctx.fillText(text, x, y);
    };

    const drawTick = (length, x, y) => {
      ctx.strokeStyle = '#ee8';
      ctx.beginPath();
      ctx.moveTo(x, y - length);
      ctx.lineTo(x, y + length);
      ctx.stroke();
    };

    ctx.clearRect(0, 0, canvas.width, canvas.height);

    const y = 1050 - map.snailTrackYLoc;

    const image = new Image();
    image.setAttribute('crossorigin', 'anonymous');
    image.onload = () => {
      ctx.drawImage(image, 0, 0, 1920, 1080);

      ctx.beginPath();
      ctx.lineWidth = 5;
      ctx.strokeStyle = '#ee8';

      ctx.moveTo(960 - map.snailTrackWidth, y);
      ctx.lineTo(960 + map.snailTrackWidth, y);
      ctx.stroke();

      let marker = 0;

      while (marker < map.snailTrackWidth / 21) {
        const centerDist = map.snailTrackWidth - marker * 21;

        if (marker % 10 == 0) {
          const tickLength = 20;

          drawTick(tickLength, 960 - centerDist, y);
          drawTick(tickLength, 960 + centerDist, y);

          drawText(marker, 960 - centerDist, y - tickLength - 10);
          drawText(marker, 960 + centerDist, y - tickLength - 10);
        } else {
          const tickLength = marker % 5 == 0 ? 15 : 10;

          drawTick(tickLength, 960 - centerDist, y);
          drawTick(tickLength, 960 + centerDist, y);
        }

        marker += 1;
      }

      drawTick(20, 960, y);
      drawText((map.snailTrackWidth / 21).toFixed(1), 960, y - 30);
    };

    image.src = heatmap?.image ?? map.image;
    ctx.scale = canvas.width / 1920;
  }, [canvasRef?.current]);

  return (
    <canvas
      className={styles.snailTrack}
      id="snail-track"
      ref={canvasRef}
      width="1920"
      height="1080"
    ></canvas>
  );
}
