import React, { createContext, useContext, useState } from 'react';
import { useFullScreenHandle } from 'react-full-screen';

const ClipboardContext = createContext({});
export const useClipboard = () => useContext(ClipboardContext);

export function ClipboardProvider({ map, children }) {
  const fullscreen = useFullScreenHandle();
  const ctx = {
    map,
    fullscreen,
  };

  return <ClipboardContext.Provider value={ctx}>{children}</ClipboardContext.Provider>;
}
