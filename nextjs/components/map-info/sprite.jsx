import React, { useState, useMemo } from 'react';
import clsx from 'clsx';

import { CABINET_POSITIONS } from '@/util/constants';
import { Draggable, useDraggable } from './draggable';
import styles from './clipboard.module.css';

const initialLocations = {
  [CABINET_POSITIONS.GOLD_STRIPES.ID]: { x: 1300, y: 33 },
  [CABINET_POSITIONS.GOLD_ABS.ID]: { x: 1370, y: 33 },
  [CABINET_POSITIONS.GOLD_QUEEN.ID]: { x: 1418, y: 20 },
  [CABINET_POSITIONS.GOLD_SKULLS.ID]: { x: 1510, y: 33 },
  [CABINET_POSITIONS.GOLD_CHECKS.ID]: { x: 1580, y: 33 },
  [CABINET_POSITIONS.BLUE_STRIPES.ID]: { x: 340, y: 33 },
  [CABINET_POSITIONS.BLUE_ABS.ID]: { x: 410, y: 33 },
  [CABINET_POSITIONS.BLUE_QUEEN.ID]: { x: 458, y: 20 },
  [CABINET_POSITIONS.BLUE_SKULLS.ID]: { x: 550, y: 33 },
  [CABINET_POSITIONS.BLUE_CHECKS.ID]: { x: 620, y: 33 },
};

function SpriteInner({ pos }) {
  const [warrior, setWarrior] = useState(false);
  const isQueen = pos.POSITION === 'queen';
  const team = pos.ID % 2 ? 'gold' : 'blue';
  const { flipped } = useDraggable();

  const imageURL = useMemo(() => {
    if (warrior) return `/static/sprites/${team}_${pos.POSITION}_warrior.png`;
    return `/static/sprites/${team}_${pos.POSITION}.png`;
  }, [warrior]);

  const myPosition = initialLocations[pos.ID];

  const onClick = evt => {
    if (evt.detail >= 2 && !isQueen) {
      setWarrior(v => !v);
    }
  };

  return (
    <div {...{ onClick }}>
      <img
        src={imageURL}
        alt={pos.POSITION}
        className={clsx(styles.unit, styles.flippable, {
          queen: isQueen,
          worker: !isQueen && !warrior,
          warrior,
          flipped: flipped !== (pos.ID % 2 === 1),
        })}
        draggable="true"
        style={{ left: myPosition.x, top: myPosition.y }}
      />
    </div>
  );
}

export function Sprite({ ...props }) {
  return (
    <Draggable>
      <SpriteInner {...props} />
    </Draggable>
  );
}
