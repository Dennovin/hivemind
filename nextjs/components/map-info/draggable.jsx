import React, { createContext, useContext, useState, useMemo } from 'react';
import ReactDraggable from 'react-draggable';
import clsx from 'clsx';

import { useClipboard } from './context';
import styles from './clipboard.module.css';

const DraggableContext = createContext({});
export const useDraggable = () => useContext(DraggableContext);

export function Draggable({ children, ...props }) {
  const { fullscreen } = useClipboard();
  const [lastLoc, setLastLoc] = useState(null);
  const [flipped, setFlipped] = useState(false);
  const [dragging, setDragging] = useState(false);

  const onStart = evt => {
    evt.preventDefault();
    setDragging(true);
  };

  const onDrag = evt => {
    if (lastLoc === null || Math.abs(lastLoc - evt.clientX) > 5) {
      setFlipped(lastLoc > evt.clientX || (lastLoc === evt.clientX && flipped));
      setLastLoc(evt.clientX);
    }
  };

  const onStop = evt => {
    setDragging(false);
  };

  return (
    <DraggableContext.Provider value={{ flipped }}>
      <ReactDraggable
        scale={fullscreen.active ? 1 : 0.6875}
        {...{ onStart, onDrag, onStop }}
        {...props}
      >
        <div className={clsx(styles.draggable, { dragging })}>{children}</div>
      </ReactDraggable>
    </DraggableContext.Provider>
  );
}
