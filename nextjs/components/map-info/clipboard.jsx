import React from 'react';

import { ClipboardProvider } from './context';
import { ClipboardMap } from './clipboard-map';
import { ClipboardButtons } from './clipboard-buttons';

export function Clipboard({ map }) {
  return (
    <ClipboardProvider map={map}>
      <ClipboardMap />
      <ClipboardButtons />
    </ClipboardProvider>
  );
}
