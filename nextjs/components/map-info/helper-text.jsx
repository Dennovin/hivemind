import React from 'react';
import { format } from 'date-fns';

import styles from './map-info.module.css';

export function HelperText({ addText, dateGenerated }) {
  return (
    <div className={styles.mapCaption}>
      {addText} Data from games with 10 human players over the past 60 days. Generated on{' '}
      {format(new Date(dateGenerated), 'MMM d, yyyy')}.
    </div>
  );
}
