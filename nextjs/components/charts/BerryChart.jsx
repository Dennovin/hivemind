import { Typography } from '@material-ui/core';

import GameChart from './GameChart';
import BaseBerryChart from './BaseBerryChart';

export default function BerryChart({ game }) {

  return (
    <GameChart title='Berries'>
      <BaseBerryChart game={game} datasetProps={{ pointRadius: 5 }} />
    </GameChart>
  );
}

