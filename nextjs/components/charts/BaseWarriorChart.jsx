import { Line } from 'react-chartjs-2';
import { colors } from 'theme/colors';
import { getBaseOptions } from './GameChart';
import PropTypes from 'prop-types';

export default function BaseWarriorChart({ game, datasetProps, blueLineColor, blueBackgroundColor, blueQueenDeathColor, goldLineColor, goldBackgroundColor, goldQueenDeathColor }) {
  const defaultDatasetProps = {
    borderWidth: 1,
    lineTension: 0,
    stepped: true,
    fill: 'origin',
  };
  const queenDatasetProps = {
    pointRadius: 7,
    hoverRadius: 7,
    borderWidth: 1,
  };

  const data = {
    datasets: [
      {
        label: 'Blue Queen Deaths',
        pointBackgroundColor: blueBackgroundColor,
        pointBorderColor: blueQueenDeathColor,
        pointBorderWidth: 16,
        pointStyle: 'rectRot',
        data: game.queenDeathData.blue,
        ...queenDatasetProps,
      },
      {
        label: 'Gold Queen Deaths',
        pointBackgroundColor: goldBackgroundColor,
        pointBorderColor: goldQueenDeathColor,
        pointBorderWidth: 16,
        pointStyle: 'rectRot',
        data: game.queenDeathData.gold,
        ...queenDatasetProps,
      },
      {
        label: 'Blue Warriors',
        borderColor: blueLineColor,
        backgroundColor: blueBackgroundColor,
        data: game.warriorData.blue,
        ...defaultDatasetProps,
        ...datasetProps,
      },
      {
        label: 'Gold Warriors',
        borderColor: goldLineColor,
        backgroundColor: goldBackgroundColor,
        data: game.warriorData.gold.map(v => ({ x: v.x, y: -v.y, text: v.text })),
        ...defaultDatasetProps,
        ...datasetProps,
      },
    ],
  };

  const options = getBaseOptions(game);
  options.scales.y = {
    display: false,
    type: 'linear',
    min: -4,
    max: 4,
    scaleLabel: {
      display: false,
      labelString: 'Pixels',
    },
  };

  return <Line options={options} data={data} />;
}

BaseWarriorChart.propTypes = {
  game: PropTypes.object.isRequired,
  datasetProps: PropTypes.object,
  blueBackgroundColor: PropTypes.string,
  blueLineColor: PropTypes.string,
  blueQueenDeathColor: PropTypes.string,
  goldBackgroundColor: PropTypes.string,
  goldLineColor: PropTypes.string,
  goldQueenDeathColor: PropTypes.string,
}

BaseWarriorChart.defaultProps = {
  datasetProps: {},
  blueBackgroundColor: colors.blue.light1,
  blueLineColor: colors.blue.dark2,
  blueQueenDeathColor: colors.blue.dark1,
  goldBackgroundColor: colors.gold.light1,
  goldLineColor: colors.gold.dark3,
  goldQueenDeathColor: colors.gold.dark3,
}
