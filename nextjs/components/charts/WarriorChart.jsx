import { Typography } from '@material-ui/core';

import GameChart from './GameChart';
import BaseWarriorChart from './BaseWarriorChart';

export default function WarriorChart({ game }) {

  return (
    <GameChart title='Warriors Up'>
      <BaseWarriorChart game={game} datasetProps={{ pointRadius: 5 }} />
    </GameChart>
  );
}

