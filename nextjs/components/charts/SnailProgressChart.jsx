import { Typography } from '@material-ui/core';

import GameChart from './GameChart';
import BaseSnailChart from './BaseSnailChart';

export default function SnailProgressChart({ game, datasetProps }) {
  return (
    <GameChart title='Snail Progress'>
      <BaseSnailChart game={game} datasetProps={{ pointRadius: 5 }} />
    </GameChart>
  );
}

