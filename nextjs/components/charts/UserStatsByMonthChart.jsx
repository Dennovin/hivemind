import { Bar } from 'react-chartjs-2';

import { colors } from 'theme/colors';
import { STAT_FORMATTING_TYPE } from 'util/constants';
import { formatTime } from 'util/dates';
import { formatStatValue } from 'util/formatters';

export default function UserStatsByMonthChart({ title, formatting, values }) {
  const formatter = value => {
    if (formatting === STAT_FORMATTING_TYPE.TIME) {
      return formatTime(value);
    }

    return formatStatValue(value, formatting);
  };

  const options = {
    maintainAspectRatio: true,
    plugins: {
      legend: {
        display: false,
      },
    },
    scales: {
      y: {
        display: true,
        type: 'linear',
        ticks: {
          callback: formatter,
        },
      },
    },
  };

  const data = {
    labels: values.map(v => v.month),
    datasets: [
      {
        label: title,
        data: values.map(v => v.value),
        backgroundColor: colors.gold.light1,
        borderColor: colors.gold.dark1,
      },
    ],
  };

  return <Bar options={options} data={data} />;
}
