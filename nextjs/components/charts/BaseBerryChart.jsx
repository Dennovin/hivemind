import { Line } from 'react-chartjs-2';
import { Typography } from '@material-ui/core';
import cloneDeep from 'lodash/cloneDeep';
import { colors } from 'theme/colors';
import PropTypes from 'prop-types';

import GameChart, { getBaseOptions } from './GameChart';

export default function BerryChart({ game, datasetProps, blueBackgroundColor, blueLineColor, goldBackgroundColor, goldLineColor }) {
  const defaultDatasetProps = {
    borderWidth: 1,
    lineTension: 0,
    stepped: true,
    fill: 'origin',
  };

  const data = {
    datasets: [
      {
        label: 'Blue Berries',
        borderColor: blueLineColor,
        backgroundColor: blueBackgroundColor,
        data: game.berryData.blue,
        ...defaultDatasetProps,
        ...datasetProps,
      },
      {
        label: 'Gold Berries',
        borderColor: goldLineColor,
        backgroundColor: goldBackgroundColor,
        data: game.berryData.gold.map((v) => ({x: v.x, y: -v.y, text: v.text})),
        ...defaultDatasetProps,
        ...datasetProps,
      },
    ],
  };

  const options = getBaseOptions(game);
  options.scales.y = {
    display: false,
    type: 'linear',
    min: -12,
    max: 12,
    scaleLabel: {
      display: false,
      labelString: 'Pixels',
    },
  };

  return (
    <Line options={options} data={data} />
  );
}

BerryChart.propTypes = {
  game: PropTypes.object.isRequired,
  datasetProps: PropTypes.object,
  blueBackgroundColor: PropTypes.string,
  blueLineColor: PropTypes.string,
  goldBackgroundColor: PropTypes.string,
  goldLineColor: PropTypes.string,
}

BerryChart.defaultProps = {
  datasetProps: {},
  blueBackgroundColor: colors.blue.light1,
  blueLineColor: colors.blue.dark2,
  goldBackgroundColor: colors.gold.light1,
  goldLineColor: colors.gold.dark3,
}

