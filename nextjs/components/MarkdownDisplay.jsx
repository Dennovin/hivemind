import { Remark } from 'react-remark';

function MarkdownDisplay({ children }) {
  return (
    <Remark
      rehypeReactOptions={{
        components: {
          p: props => <span className="my-0 inline-block" {...props} />,
          a: props => <a target="_blank" {...props} />,
          ul: props => <ul className="list-none pl-0 space-y-3" {...props} />,
          li: props => (
            <li className="border-0 border-solid border-l-4 border-gray-300 pl-3" {...props} />
          ),
        },
      }}
    >
      {children}
    </Remark>
  );
}

export default MarkdownDisplay;
