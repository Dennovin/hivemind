import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Icon from '@mdi/react';
import { mdiTarget } from '@mdi/js';

import { MAP_NAMES } from 'util/constants';

const useStyles = makeStyles(theme => ({
  mapDiv: {
    position: 'relative',
  },
  mapImage: {
    width: '100%',
  },
  target: {
    position: 'absolute',
    color: 'red',
    width: '50px',
    height: '50px',
    marginLeft: '-25px',
    marginTop: '-25px',
  },
}));

export default function GameEventsMap({ game, targetLoc }) {
  const classes = useStyles({ game });
  const mapName = game.mapName.toLowerCase();

  if (!['day', 'night', 'dusk', 'twilight'].includes(mapName)) {
    return (<></>);
  }

  const mapUrl = `/static/maps/${mapName}.png`;
  const targetStyle = {};

  if (targetLoc) {
    targetStyle.left = `${(100 * parseInt(targetLoc[0]) / 1920)}%`;
    targetStyle.top = `${100 - (100 * parseInt(targetLoc[1]) / 1080)}%`;
  }

  return (
    <div className={classes.mapDiv}>
      <img src={mapUrl} className={classes.mapImage} />
      {targetLoc && (
        <Icon path={mdiTarget} className={classes.target} style={targetStyle} />
      )}
    </div>
  );
}

GameEventsMap.propTypes = {
  game: PropTypes.object.isRequired,
  targetLoc: PropTypes.array,
};

GameEventsMap.defaultProps = {
  targetLoc: null,
};
