import React, { useEffect, useState } from 'react';
import { Select, MenuItem } from '@material-ui/core';
import { useRouter } from 'next/router';

import { STAGE_TYPE } from 'util/constants';
import { RoundRobinStandings } from './RoundRobinStandings';
import { SingleElimBracket } from './SingleElimBracket';
import { DoubleElimBracket } from './DoubleElimBracket';
import TournamentMatchListTable from 'components/tables/TournamentMatchListTable';
import { GroupResultsProvider } from './context';

export function GroupResults({ bracket, tournament }) {
  if (bracket?.stageType === STAGE_TYPE.ROUND_ROBIN) {
    return <RoundRobinStandings bracket={bracket} />;
  }

  if (bracket?.stageType === STAGE_TYPE.SINGLE_ELIM) {
    return <SingleElimBracket bracket={bracket} tournament={tournament} />;
  }

  if (bracket?.stageType === STAGE_TYPE.DOUBLE_ELIM) {
    return <DoubleElimBracket bracket={bracket} tournament={tournament} />;
  }

  return <></>;
}

export function GroupResultsTables({ tournament }) {
  const router = useRouter();
  const brackets = tournament.brackets
    .filter(b => b.matches.length > 0)
    .sort((a, b) => a.id - b.id);
  const [selectedBracket, setSelectedBracket] = useState(brackets?.[0]?.id);
  const bracket = brackets.filter(m => m.id === selectedBracket)?.[0];

  useEffect(() => {
    if (router?.isReady) {
      const selected = router?.asPath.split('#')[1];
      if (selected) setSelectedBracket(parseInt(selected));
    }
  }, [router?.isReady]);

  const onChange = evt => {
    router.replace(`${router.asPath.split('#')[0]}#${evt.target.value}`, null, {
      shallow: true,
    });
    setSelectedBracket(evt.target.value);
  };

  if (!router.isReady) return null;

  return (
    <GroupResultsProvider tournament={tournament}>
      <Select value={selectedBracket} onChange={onChange}>
        {brackets.map((bracket, idx) => (
          <MenuItem key={bracket.id} value={bracket.id}>
            {bracket.name}
          </MenuItem>
        ))}
      </Select>

      {bracket && (
        <>
          <GroupResults bracket={bracket} tournament={tournament} />
          <TournamentMatchListTable
            tournament={tournament}
            filter={m => m.bracket === bracket.id}
          />
        </>
      )}
    </GroupResultsProvider>
  );
}
