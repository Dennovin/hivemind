import React, { useState, useEffect, useMemo } from 'react';
import { Typography } from '@material-ui/core';

import { useTournamentAdmin } from '@/components/tournament-admin';
import { getAxios } from 'util/axios';
import Table, { TableRow, TableCell } from 'components/tables/Table';
import styles from './RoundRobinStandings.module.css';

export function RoundRobinStandings({ bracket }) {
  const axios = getAxios();
  const { standings: allStandings } = useTournamentAdmin();

  const standings = useMemo(() => allStandings?.[bracket?.id], [allStandings?.[bracket?.id]]);

  const hasTiebreak = standings?.filter(row => row.tiebreakDesc?.length > 0).length > 0;
  const getTiebreak = row => {
    if (row.tiebreakDesc?.length > 0) {
      return (
        <>
          {row.tiebreakDesc.map((txt, idx) => (
            <Typography className={styles.tiebreakText} key={idx}>
              {txt}
            </Typography>
          ))}
        </>
      );
    } else {
      return '';
    }
  };

  const columnHeaders = ['Place', 'Team Name', 'Games', 'Matches'];
  const cellClassNames = [styles.numeric, null, styles.numeric, styles.numeric];

  if (hasTiebreak) {
    columnHeaders.push('');
    cellClassNames.push(styles.tiebreak);
  }

  return (
    <Table
      title={bracket.name}
      columnHeaders={columnHeaders}
      cellClassNames={cellClassNames}
      isLoading={standings === null}
    >
      {standings &&
        standings.map((team, idx) => (
          <TableRow key={team.id} href={`/tournament-team/${team.id}`}>
            <TableCell className={styles.numeric}>{idx + 1}.</TableCell>
            <TableCell>{team.name}</TableCell>
            <TableCell className={styles.numeric}>
              {team.gameWins}-{team.gameLosses}
            </TableCell>
            <TableCell className={styles.numeric}>
              {team.matchWins}-{team.matchLosses}
            </TableCell>
            {hasTiebreak && (
              <TableCell className={styles.tiebreak}>
                {team.tiebreakDesc?.length > 0 && (
                  <>
                    {team.tiebreakDesc.map(txt => (
                      <Typography className={styles.tiebreakText} key={txt}>
                        {txt}
                      </Typography>
                    ))}
                  </>
                )}
              </TableCell>
            )}
          </TableRow>
        ))}
    </Table>
  );
}
