import { CircularProgress, Typography } from '@material-ui/core';

export function AwaitSubmit() {
  return (
    <>
      <Typography variant="h2">Creating Tournament Stages</Typography>

      <CircularProgress />
    </>
  );
}
