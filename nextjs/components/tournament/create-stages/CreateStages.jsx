import { useContext, createContext } from 'react';
import { useTournamentAdmin } from '@/components/tournament-admin';
import {
  Breadcrumbs,
  Button,
  CircularProgress,
  Grid,
  IconButton,
  Link,
  MenuItem,
  Select,
  TextField,
  Typography,
} from '@material-ui/core';
import Icon from '@mdi/react';
import { mdiArrowRight, mdiDelete, mdiPlus } from '@mdi/js';

import { STAGE_TYPE } from 'util/constants';
import { DraggableTable, DraggableTableRow } from 'components/tables/DraggableTable';
import { TableCell } from 'components/tables/Table';
import styles from './CreateStages.module.css';
import { useCreateStages } from './Context';
import { Step, Tiebreakers } from './Constants';

const StageRowContext = createContext({});
const useStageRow = () => useContext(StageRowContext);

function StageNameInput() {
  const { setStages } = useCreateStages();
  const { stage } = useStageRow();

  const onChange = evt => {
    setStages(val => {
      for (const row of val) {
        if (row.key === stage.key) {
          row.stageName = evt.target.value;
        }
      }

      return [...val];
    });
  };

  return (
    <TextField fullWidth value={stage.stageName} onChange={onChange} placeholder="Stage Name" />
  );
}

function StageTypeSelect() {
  const { setStages } = useCreateStages();
  const { stage } = useStageRow();

  const onChange = evt => {
    setStages(val => {
      for (const row of val) {
        if (row.key === stage.key) {
          row.stageType = evt.target.value;
        }
      }

      return [...val];
    });
  };

  return (
    <Select fullWidth value={stage.stageType} onChange={onChange}>
      <MenuItem value={STAGE_TYPE.ROUND_ROBIN}>Round Robin</MenuItem>
      <MenuItem value={STAGE_TYPE.SINGLE_ELIM}>Single Elimination</MenuItem>
      <MenuItem value={STAGE_TYPE.DOUBLE_ELIM}>Double Elimination</MenuItem>
      <MenuItem value={STAGE_TYPE.LADDER}>Ladder</MenuItem>
    </Select>
  );
}

function NumGroupsInput() {
  const { setStages } = useCreateStages();
  const { stage } = useStageRow();

  const onChange = evt => {
    setStages(val => {
      for (const row of val) {
        if (row.key === stage.key) {
          row.numGroups = evt.target.valueAsNumber;
        }
      }

      return [...val];
    });
  };

  return (
    <TextField
      fullWidth
      type="number"
      value={stage.numGroups}
      onChange={onChange}
      className={styles.numericInput}
    />
  );
}

function StageDeleteButton() {
  const { setStages } = useCreateStages();
  const { stage } = useStageRow();

  const onClick = evt => {
    setStages(val => {
      return val.filter(row => row.key !== stage.key);
    });
  };

  return (
    <IconButton aria-label="delete" onClick={onClick}>
      <Icon path={mdiDelete} size={1} />
    </IconButton>
  );
}

function StageRow({ stage, stageNum }) {
  return (
    <StageRowContext.Provider value={{ stage, stageNum }}>
      <DraggableTableRow draggableId={`${stage.key}`} index={stageNum - 1}>
        <TableCell className={styles.numeric}>{stageNum}.</TableCell>
        <TableCell>
          <StageNameInput />
        </TableCell>
        <TableCell>
          <StageTypeSelect />
        </TableCell>
        <TableCell className={styles.numeric}>
          <NumGroupsInput />
        </TableCell>
        <TableCell className={styles.deleteButton}>
          <StageDeleteButton />
        </TableCell>
      </DraggableTableRow>
    </StageRowContext.Provider>
  );
}

function AddStageButton() {
  const { setStages } = useCreateStages();

  const addStage = evt => {
    setStages(val => {
      const key = val.length === 0 ? 1 : Math.max(...val.map(r => r.key)) + 1;
      const newStage = {
        stageName: `Stage ${val.length + 1}`,
        key,
        stageType: STAGE_TYPE.ROUND_ROBIN,
        numGroups: 1,
      };

      val.push(newStage);
      return [...val];
    });
  };

  return (
    <Button
      variant="outlined"
      color="secondary"
      startIcon={<Icon path={mdiPlus} size={1} />}
      onClick={addStage}
    >
      Add Stage
    </Button>
  );
}

function NextStepButton() {
  const { stages, setStep, setStages } = useCreateStages();
  const letters = [...Array(26).keys()].map(i => String.fromCharCode(65 + i));
  const groupName = (stage, idx) => `${stage.stageName} Group ${letters[idx] ?? idx + 1}`;

  const nextStep = () => {
    setStages(val => {
      for (const stage of val) {
        stage.groups = [...Array(stage.numGroups).keys()].map(i => ({
          name: stage.numGroups > 1 ? groupName(stage, i) : stage.stageName,
          idx: i,
          placements: [],
          matches: [],
        }));

        stage.teamsIncluded = new Set();
        stage.placesIncluded = new Set();

        const isGroupStage = [STAGE_TYPE.ROUND_ROBIN, STAGE_TYPE.CUSTOM].includes(stage.stageType);

        stage.roundsPerMatch = isGroupStage ? 4 : 0;
        stage.winsPerMatch = isGroupStage ? 0 : 3;
        stage.matchesPerOpponent = stage.stageType === STAGE_TYPE.ROUND_ROBIN ? 1 : 0;
        stage.tiebreakers = [Tiebreakers.GameWins, Tiebreakers.HeadToHead, Tiebreakers.MatchWins];
        stage.addTiebreaker = false;
        stage.autoWarmup = true;
      }

      return [...val];
    });

    setStep(Step.ConfigurePlacements);
  };

  return (
    <Button
      variant="outlined"
      color="primary"
      endIcon={<Icon path={mdiArrowRight} size={1} disabled={stages.length === 0} />}
      onClick={nextStep}
    >
      Next
    </Button>
  );
}

export function CreateStages({}) {
  const { tournament } = useTournamentAdmin();
  const { stages, setStages } = useCreateStages();

  const columnHeaders = ['', 'Stage Name', 'Type', 'Groups', ''];
  const cellClassNames = [styles.numeric, null, null, styles.numeric, styles.deleteButton];

  const onDragEnd = result => {
    setStages(val => {
      const newVal = [...val];
      const row = newVal.splice(result.source.index, 1).shift();
      newVal.splice(result.destination.index, 0, row);

      return newVal;
    });
  };

  if (tournament.teams.length === 0) {
    return (
      <>
        <Typography>
          Teams must be added to this tournament before stages can be created.
        </Typography>
        <Button href={`/tournament/${tournament.id}/players`}>Manage Teams</Button>
      </>
    );
  }

  return (
    <>
      <DraggableTable
        title="Configure Stages"
        columnHeaders={columnHeaders}
        cellClassNames={cellClassNames}
        isLoading={false}
        onDragEnd={onDragEnd}
      >
        {stages.map((stage, idx) => (
          <StageRow key={stage.key} stage={stage} stageNum={idx + 1} />
        ))}
      </DraggableTable>

      <Grid container direction="row" justifyContent="space-between">
        <AddStageButton />
        <NextStepButton />
      </Grid>
    </>
  );
}
