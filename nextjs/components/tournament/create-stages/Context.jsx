import { createContext, useContext, useState, useEffect } from 'react';
import ordinal from 'ordinal';

import { Step } from './Constants';
import { useTournamentAdmin } from '@/components/tournament-admin';

const CreateStagesContext = createContext({});
export const useCreateStages = () => useContext(CreateStagesContext);

export function CreateStagesProvider({ children }) {
  const { tournament } = useTournamentAdmin();
  const [stages, setStages] = useState([]);
  const [standings, setStandings] = useState([]);
  const [step, setStep] = useState(Step.CreateStages);
  const [placementStageIdx, setPlacementStageIdx] = useState(0);
  const [showAssigned, setShowAssigned] = useState(false);
  const key = `tournament.${tournament?.id}.createStages`;

  const getAssignableTeams = () => {
    if (showAssigned) {
      const currentStage = stages[placementStageIdx];
      return tournament.teams.filter(t => !currentStage.teamsIncluded.has(t.id));
    }

    return tournament.teams.filter(t => stages.filter(s => s.teamsIncluded.has(t.id)).length === 0);
  };

  const getAssignablePlaces = (stage, group) => {
    const placements = [];

    for (const idx of [...group.placements.keys()]) {
      const placeId = `${stage.key}.${group.idx}.${idx + 1}`;
      const showPlace = showAssigned
        ? !stage.placesIncluded.has(placeId)
        : stages.filter(s => s.placesIncluded.has(placeId)).length === 0;
      if (showPlace) {
        placements.push({ id: placeId, name: `${group.name} - ${ordinal(idx + 1)} Place` });
      }
    }

    return placements;
  };

  const value = {
    stages,
    setStages,
    standings,
    setStandings,
    step,
    setStep,
    placementStageIdx,
    setPlacementStageIdx,
    showAssigned,
    setShowAssigned,
    getAssignableTeams,
    getAssignablePlaces,
  };

  return <CreateStagesContext.Provider value={value}>{children}</CreateStagesContext.Provider>;
}
