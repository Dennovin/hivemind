import { Button, Grid, Typography } from '@material-ui/core';
import Icon from '@mdi/react';
import { mdiArrowLeft, mdiArrowRight } from '@mdi/js';
import { DragDropContext } from 'react-beautiful-dnd';

import { getAxios } from 'util/axios';
import { useTournamentAdmin } from '@/components/tournament-admin';
import { useCreateStages } from './Context';
import { Step, PlaceType } from './Constants';
import { List, ListItem } from './Components';
import styles from './AwaitConfirm.module.css';

function Stages() {
  const { stages } = useCreateStages();

  return (
    <>
      {stages.map(stage => (
        <Grid item key={stage.key} className={styles.column}>
          {stage.groups.map(group => (
            <List
              id={group.idx}
              key={group.idx}
              name={group.name}
              droppable={false}
              className={styles.list}
            >
              {group.placements.map(place => (
                <ListItem
                  id={place.placeId}
                  key={place.placeId}
                  name={place.name}
                  draggable={false}
                />
              ))}
            </List>
          ))}
        </Grid>
      ))}
    </>
  );
}

function FinalStandings() {
  const { standings } = useCreateStages();

  return (
    <Grid item className={styles.column}>
      <List name="Final Standings" droppable={false} className={styles.list}>
        {standings.map(place => (
          <ListItem id={place.id} key={place.id} name={place.name} draggable={false} />
        ))}
      </List>
    </Grid>
  );
}

function PrevStepButton() {
  const { setStep, stages, setPlacementStageIdx } = useCreateStages();

  const prevStep = () => {
    setPlacementStageIdx(stages.length - 1);
    setStep(Step.SetFinalStandings);
  };

  return (
    <Button
      variant="outlined"
      color="primary"
      startIcon={<Icon path={mdiArrowLeft} size={1} />}
      onClick={prevStep}
    >
      Back
    </Button>
  );
}

function NextStepButton() {
  const axios = getAxios({ authenticated: true });
  const { tournament } = useTournamentAdmin();
  const { setStep, stages } = useCreateStages();

  const nextStep = async () => {
    setStep(Step.AwaitSubmit);

    const promises = [];
    const bracketIds = {};

    for (const stage of stages) {
      bracketIds[stage.key] = {};

      for (const [groupIdx, group] of stage.groups.entries()) {
        let response = await axios.post('/api/tournament/bracket/', {
          name: group.name,
          tournament: tournament.id,
          stageType: stage.stageType,
          roundsPerMatch: stage.roundsPerMatch,
          winsPerMatch: stage.winsPerMatch,
          matchesPerOpponent: stage.matchesPerOpponent,
          addTiebreaker: stage.addTiebreaker,
          autoWarmup: stage.autoWarmup,
        });

        bracketIds[stage.key][groupIdx] = response.data.id;
        for (const [placeIdx, place] of group.placements.entries()) {
          const data = {
            destBracket: response.data.id,
            destPlace: placeIdx + 1,
          };

          if (place.placeType === PlaceType.Team) {
            data.team = parseInt(place.placeId);
          }

          if (place.placeType === PlaceType.PrevStage) {
            const [srcStageIdx, srcGroupIdx, srcPlace] = place.placeId.split('.');
            data.srcBracket = bracketIds[parseInt(srcStageIdx)][parseInt(srcGroupIdx)];
            data.srcPlace = parseInt(srcPlace);
          }

          await axios.post('/api/tournament/stage-placement/', data);
        }
      }
    }

    for (const stage of Object.values(bracketIds)) {
      for (const bracketId of Object.values(stage)) {
        await axios.put(`/api/tournament/bracket/${bracketId}/create-matches/`, {});
      }
    }

    setStep(Step.Complete);
  };

  return (
    <Button
      variant="outlined"
      color="primary"
      endIcon={<Icon path={mdiArrowRight} size={1} />}
      onClick={nextStep}
    >
      Create Stages
    </Button>
  );
}

export function AwaitConfirm({}) {
  return (
    <DragDropContext onDragEnd={null}>
      <Typography variant="h2">Review Tournament Settings</Typography>

      <Grid container direction="row" spacing={2} className={styles.container}>
        <Stages />
        <FinalStandings />
      </Grid>

      <Grid container direction="row" justifyContent="space-between">
        <PrevStepButton />
        <NextStepButton />
      </Grid>
    </DragDropContext>
  );
}
