import { useEffect, useState, useRef } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, TextField } from '@material-ui/core';

import { useCreateStages } from '../Context';
import { CurrentStageContext, useCurrentStage } from './Context';
import { EditGroupNameButton } from './EditGroupNameButton';

export function GroupTableTitle({ group }) {
  const stage = useCurrentStage();
  const { setStages } = useCreateStages();
  const [isEditing, setIsEditing] = useState(false);
  const ref = useRef();

  useEffect(() => {
    if (isEditing && ref?.current) {
      ref.current.focus();
      ref.current.select();
    }
  }, [isEditing]);

  const onClick = () => {
    setIsEditing(true);
  };

  const onChange = evt => {
    setStages(val => {
      const currentStage = val.filter(i => i.key === stage.key)[0];
      const currentGroup = currentStage.groups.filter(i => i.idx === group.idx)[0];

      currentGroup.name = evt.target.value;

      return [...val];
    });
  };

  const onKeyUp = evt => {
    if (evt.keyCode === 13) {
      setIsEditing(false);
    }
  };

  if (isEditing) {
    return (
      <TextField
        value={group.name}
        onChange={onChange}
        onKeyUp={onKeyUp}
        onBlur={() => setIsEditing(false)}
        placeholder="Group Name"
        inputRef={ref}
      />
    );
  }

  return (
    <Grid item container alignItems="center" spacing={1}>
      <Grid item>{group.name}</Grid>
      <Grid item>
        <EditGroupNameButton onClick={onClick} />
      </Grid>
    </Grid>
  );
}
