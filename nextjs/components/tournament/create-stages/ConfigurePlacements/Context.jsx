import { createContext, useContext } from 'react';

export const CurrentStageContext = createContext({});
export const useCurrentStage = () => useContext(CurrentStageContext);
