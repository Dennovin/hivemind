import { makeStyles } from '@material-ui/core/styles';
import Icon from '@mdi/react';
import { mdiPencil } from '@mdi/js';

import styles from './ConfigurePlacements.module.css';

export function EditGroupNameButton({ group, ...props }) {
  return (
    <Icon
      title="Edit Group Name"
      className={styles.menuIcon}
      path={mdiPencil}
      size={1}
      {...props}
    />
  );
}
