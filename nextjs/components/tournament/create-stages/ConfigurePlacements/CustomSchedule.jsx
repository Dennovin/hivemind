import { Draggable, Droppable } from 'react-beautiful-dnd';
import { Button, Grid } from '@material-ui/core';

import { useCreateStages } from '../Context';
import { MatchList, MatchListItem } from '../Components';
import { GroupTableTitle } from './GroupTableTitle';
import { ClearGroupButton } from './ClearGroupButton';
import { CurrentStageContext, useCurrentStage } from './Context';

export function AddMatchButton({ group }) {
  const stage = useCurrentStage();
  const { setStages } = useCreateStages();

  const addMatch = () => {
    setStages(val => {
      for (const row of val) {
        if (row.key === stage.key) {
          stage.groups[group.idx].matches.push({
            key: stage.groups[group.idx].matches.length,
          });
        }
      }

      return [...val];
    });
  };

  return <Button onClick={addMatch}>Add Match</Button>;
}

export function CustomScheduleGroupTable({ group }) {
  return (
    <MatchList
      id={`group-${group.idx}`}
      name={<GroupTableTitle group={group} />}
      icons={<ClearGroupButton group={group} />}
      bottomButtons={<AddMatchButton group={group} />}
    >
      {group.matches.map((match, idx) => (
        <MatchListItem
          key={match.key}
          id={`match-${match.key}`}
          blueTeam={match.blueTeam}
          goldTeam={match.goldTeam}
          index={idx}
        />
      ))}
    </MatchList>
  );
}
