import { useState } from 'react';
import { Grid, Select, MenuItem } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Draggable, Droppable } from 'react-beautiful-dnd';
import Icon from '@mdi/react';
import { mdiMenuUp, mdiMenuDown, mdiDragVertical } from '@mdi/js';
import clsx from 'clsx';

import { useTournamentAdmin } from '@/components/tournament-admin';
import styles from './MatchList.module.css';

export function MatchList({ id, name, children, icons, bottomButtons, droppable = true } = {}) {
  const [collapsed, setCollapsed] = useState(false);

  const toggleCollapsed = () => setCollapsed(v => !v);

  return (
    <Grid container direction="column" className={styles.list}>
      <Grid
        item
        container
        direction="row"
        alignItems="center"
        justifyContent="space-between"
        className={clsx(styles.listTitle, styles.listItem, { collapsed })}
      >
        <Grid item>{name}</Grid>
        <Grid item>
          {!collapsed && icons}
          <Icon
            className={styles.menuIcon}
            onClick={toggleCollapsed}
            path={collapsed ? mdiMenuUp : mdiMenuDown}
            size={1}
          />
        </Grid>
      </Grid>

      {!collapsed && droppable && (
        <Droppable droppableId={id}>
          {(provided, snapshot) => (
            <Grid
              item
              container
              direction="column"
              ref={provided.innerRef}
              className={styles.listBody}
            >
              {children}
              {provided.placeholder}
            </Grid>
          )}
        </Droppable>
      )}

      {!collapsed && !droppable && (
        <Grid item container direction="column" className={styles.listBody}>
          {children}
        </Grid>
      )}

      {bottomButtons && (
        <Grid item container direction="row" className={styles.buttonsRow}>
          {bottomButtons}
        </Grid>
      )}
    </Grid>
  );
}

export function MatchListItem({ id, index, blueTeam, goldTeam, draggable = true } = {}) {
  const { tournament } = useTournamentAdmin();

  if (draggable) {
    return (
      <Draggable draggableId={id} index={index}>
        {(provided, snapshot) => (
          <Grid
            item
            container
            ref={provided.innerRef}
            className={styles.listItem}
            {...provided.draggableProps}
          >
            <Grid item className={styles.dragHandle} {...provided.dragHandleProps}>
              <Icon path={mdiDragVertical} size={1} />
            </Grid>
            <Grid item className={clsx(styles.team, styles.blueTeam)}>
              <Select value={blueTeam}>
                <MenuItem value={null} />
                {tournament.teams.map(team => (
                  <MenuItem key={team.id} value={team.id}>
                    {team.name}
                  </MenuItem>
                ))}
              </Select>
            </Grid>
            <Grid item className={clsx(styles.team, styles.goldTeam)}>
              <Select>
                {tournament.teams.map(team => (
                  <MenuItem key={team.id} value={team.id}>
                    {team.name}
                  </MenuItem>
                ))}
              </Select>
            </Grid>
          </Grid>
        )}
      </Draggable>
    );
  }

  return (
    <Grid item className={styles.listItem}>
      {name}
    </Grid>
  );
}
