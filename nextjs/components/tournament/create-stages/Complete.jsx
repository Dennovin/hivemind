import { Typography } from '@material-ui/core';

export function Complete() {
  return (
    <>
      <Typography variant="h2">Tournament Creation Complete</Typography>

      <Typography>
        Tournament stages have been created. The tournament is ready to begin.
      </Typography>
    </>
  );
}
