export const Step = {
  CreateStages: 1,
  ConfigurePlacements: 2,
  SetFinalStandings: 3,
  AwaitConfirm: 4,
  AwaitSubmit: 5,
  Complete: 6,
};

export const PlaceType = {
  Team: 'team',
  PrevStage: 'prevStage',
};

export const Tiebreakers = {
  GameWins: 'game_wins',
  MatchWins: 'match_wins',
  HeadToHead: 'head_to_head',
};
