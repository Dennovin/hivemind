import { Grid, Typography } from '@material-ui/core';

import styles from './BracketRound.module.css';

export function BracketRound({ name, children }) {
  return (
    <Grid item className={styles.round}>
      <Typography className={styles.name}>{name}</Typography>

      <Grid container direction="column" className={styles.matches}>
        {children}
      </Grid>
    </Grid>
  );
}
