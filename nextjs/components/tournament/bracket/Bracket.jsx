import { Grid } from '@material-ui/core';

import styles from './Bracket.module.css';

export function Bracket({ name, children }) {
  return (
    <>
      {name && <div className={styles.bracketTitle}>{name}</div>}
      <Grid container direction="row" spacing={2} wrap="nowrap" className={styles.bracket}>
        {children}
      </Grid>
    </>
  );
}
