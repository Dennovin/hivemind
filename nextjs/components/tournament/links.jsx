import React from 'react';

import styles from './links.module.css';

export function TournamentLinks({ tournament }) {
  if (!tournament?.links) return null;

  return (
    <div className={styles.links}>
      {tournament.links?.map(link => (
        <a href={link.url} className={styles.link}>
          {link.image && <img src={link.image} alt={link.title} width={32} height={32} />}
          <span>{link.title}</span>
        </a>
      ))}
    </div>
  );
}
