import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import Table from './Table';

const useStyles = makeStyles(theme => ({
  blue: {
    background: theme.gradients.blue.light,
  },
  gold: {
    background: theme.gradients.gold.light,
  },
  teamName: {
  },
  score: {
    ...theme.mixins.tableCell.numeric(theme),
  },
}));

export default function MatchListTable({ title, data }) {
  const classes = useStyles();
  const columnHeaders = ['Blue Team', 'Blue Score', 'Gold Team', 'Gold Score'];
  const getRowCells = row => [
    row.blueTeam.name,
    row.blueScore,
    row.goldTeam.name,
    row.goldScore,
  ];

  const cellClassNames = [
    clsx(classes.blue, classes.teamName),
    clsx(classes.blue, classes.score),
    clsx(classes.gold, classes.teamName),
    clsx(classes.gold, classes.score),
  ];

  const getCellStyle = row => {
    const bold = { fontWeight: 'bold' };

    if (row.isComplete && row.blueScore > row.goldScore)
      return [ bold, bold, {}, {} ];
    if (row.isComplete && row.goldScore > row.blueScore)
      return [ {}, {}, bold, bold ];

    return [ {}, {}, {}, {} ];
  };

  const rowLink = row => `/match/${row.id}`;

  return (
    <Table title={title} showColumnHeaders={false} columnHeaders={columnHeaders} data={data} getRowCells={getRowCells} isLoading={false} link={rowLink} getCellStyle={getCellStyle} cellClassNames={cellClassNames} />
  );
}

MatchListTable.propTypes = {
  title: PropTypes.string,
  data: PropTypes.array.isRequired,
};

MatchListTable.defaultProps = {
  title: 'Matches',
};
