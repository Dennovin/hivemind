import PlayerStatsRow, { PlayerStatsContainer, PlayerStatsHeaderRow } from './PlayerStatsRow';
import { formatStatValueShort } from 'util/formatters';

export default function UserStatsByPositionTable({ stats }) {
  return (
    <PlayerStatsContainer>
      <PlayerStatsHeaderRow />

      {stats.map(stat => (
        <PlayerStatsRow
          key={stat.name}
          label={stat.label}
          values={stat.values}
          format={v => formatStatValueShort(v, stat.formatting)}
        />
      ))}
    </PlayerStatsContainer>
  );
}
