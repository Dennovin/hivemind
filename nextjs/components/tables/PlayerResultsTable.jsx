import { useRouter } from 'next/router';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { NumericFormat } from 'react-number-format';

import { getAxios } from 'util/axios';
import { formatUTC } from 'util/dates';
import Table from './Table';

const useStyles = makeStyles(theme => ({
  numeric: {
    ...theme.mixins.tableCell.numeric(theme),
  },
}));

export default function PlayerResultsTable({ title, player }) {
  const classes = useStyles();
  const columnHeaders = ['Date', 'Team Name', 'Points', 'Rounds', 'Matches'];
  const getRowCells = row => [
    formatUTC(row.event.eventDate, 'MMM d, yyyy'),
    row.team?.name,
    <NumericFormat
      value={row.points}
      displayType="text"
      decimalScale={2}
      fixedDecimalScale={true}
    />,
    `${row.roundsWon}-${row.roundsLost}`,
    `${row.matchesWon}-${row.matchesLost}`,
  ];
  const getSubtitle = row => row.season.name;
  const getSectionRows = row => row.events;

  const cellClassNames = [null, null, classes.numeric, classes.numeric, classes.numeric];

  const rowLink = row => `/event/${row.event.id}`;
  const subtitleLink = row => `/season/${row.season.id}`;

  return (
    <Table
      title={title}
      columnHeaders={columnHeaders}
      data={player.results}
      getRowCells={getRowCells}
      getSubtitle={getSubtitle}
      getSectionRows={getSectionRows}
      isLoading={false}
      link={rowLink}
      subtitleLink={subtitleLink}
      cellClassNames={cellClassNames}
    />
  );
}

PlayerResultsTable.propTypes = {
  title: PropTypes.string,
  player: PropTypes.object.isRequired,
};

PlayerResultsTable.defaultProps = {
  title: 'Results By Season',
};
