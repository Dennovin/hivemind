import { formatTime } from 'util/dates';
import PlayerStatsRow, { PlayerStatsContainer, PlayerStatsHeaderRow } from './PlayerStatsRow';

export default function PlayerStatsTable({ game }) {
  if (Object.values(game.byPlayer).length === 0) return null;

  function getPlayerStat(key, pos, defaultValue) {
    if (game.byPlayer[key][pos] !== undefined) {
      const val = game.byPlayer[key][pos];
      if (game?.id && key === 'queenKills' && val === 3) return <span title={val}>🤠</span>;
      return val;
    }

    if (defaultValue !== undefined) return defaultValue;

    return '';
  }

  return (
    <PlayerStatsContainer>
      <PlayerStatsHeaderRow />

      <PlayerStatsRow
        label="K/D"
        getValue={pos => `${getPlayerStat('kills', pos, 0)}-${getPlayerStat('deaths', pos, 0)}`}
      />
      <PlayerStatsRow
        label="Military K/D"
        getValue={pos =>
          `${getPlayerStat('militaryKills', pos, 0)}-${getPlayerStat('militaryDeaths', pos, 0)}`
        }
      />
      <PlayerStatsRow label="Queen Kills" getValue={pos => getPlayerStat('queenKills', pos)} />
      <PlayerStatsRow label="Bump Assists" getValue={pos => getPlayerStat('bumpAssists', pos)} />
      <PlayerStatsRow
        label="Warrior Uptime"
        getValue={pos => formatTime(getPlayerStat('warriorUptime', pos))}
      />
      <PlayerStatsRow label="Berries Deposited" getValue={pos => getPlayerStat('berries', pos)} />
      <PlayerStatsRow
        label="Berries Kicked In"
        getValue={pos => getPlayerStat('berriesKicked', pos)}
      />
      <PlayerStatsRow label="Snail Meters" getValue={pos => getPlayerStat('snailMeters', pos)} />
    </PlayerStatsContainer>
  );
}
