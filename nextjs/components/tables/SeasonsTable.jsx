import { Button } from '@material-ui/core';
import { useState } from 'react';
import { IconButton } from '@material-ui/core';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import { format } from 'date-fns';

import { getAxios } from 'util/axios';
import Table from './Table';
import { isAdminOf } from 'util/auth';
import SeasonForm from 'components/forms/SeasonForm';

export default function SeasonsTable({ title, scene }) {
  const axios = getAxios();
  const [data, setData] = useState(null);

  const columnHeaders = ['Name', 'Start Date', 'End Date', 'Events'];
  const getRowCells = row => [
    row.name,
    row.startDate ? format(new Date(row.startDate), 'MMM d, yyyy') : '',
    row.endDate ? format(new Date(row.endDate), 'MMM d, yyyy') : '',
    row.eventCount,
  ];

  const rowLink = row => `/season/${row.id}`;

  const [showOld, setShowOld] = useState(false);

  if (data === null) {
    axios.get('/api/league/season/', { params: { sceneId: scene.id } }).then((response) => {
      const seasons = response.data.results.sort((a, b) =>
        Date(a.startDate) - Date(b.startDate)
      );

      setData(seasons);
    });
  }

  const props = {
    title,
    columnHeaders,
    data: data?.filter(seasons => showOld || new Date(seasons.endDate) > new Date(new Date().setFullYear(new Date().getFullYear() - 1))),
    getRowCells,
    isLoading: data === null,
    link: rowLink,
  };

  if (isAdminOf(scene.id)) {
    const emptySeason = { scene, seasonType: 'shuffle', startDate: format(new Date(), 'yyyy-MM-dd') };
    props.titleButton = (<SeasonForm season={emptySeason} />);
  }

  props.titleButton = (
    <>
      <div className="flex gap-2 items-center">
        <Button
          className=" ml-auto inline-block"
          size="small"
          variant="outlined"
          onClick={() => setShowOld(!showOld)}
        >
          {showOld ? 'Hide' : 'Show'} old
        </Button>
        {props?.titleButton}
      </div>
    </>
  );

  return (
    <Table {...props} />
  );
}

SeasonsTable.propTypes = {
  title: PropTypes.string,
  scene: PropTypes.object.isRequired,
};

SeasonsTable.defaultProps = {
  title: 'League Seasons',
};
