import React, { useEffect, useState } from 'react';

import { getAxios } from 'util/axios';
import Table from './Table';

export default function PaginatedTable({
  url,
  filters,
  ordering,
  tableClassName = null,
  afterGetData = () => undefined,
  rowsPerPage = 20,
  ...props
}) {
  const [page, setPage] = useState(0);
  const [totalCount, setTotalCount] = useState(null);
  const [data, setData] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const axios = getAxios();

  const getData = pageNum => {
    setIsLoading(true);
    axios
      .get(url, { params: { page: pageNum + 1, rowsPerPage, ordering, ...filters } })
      .then(response => {
        setData(response.data.results);
        afterGetData(response.data.results);
        setIsLoading(false);
        setPage(pageNum);
        setTotalCount(response.data.count);
      });
  };

  useEffect(() => {
    getData(page);
  }, []);

  const onPageChange = (event, newPageNum) => {
    getData(newPageNum);
  };

  return (
    <Table
      {...{
        data,
        isLoading,
        page,
        totalCount,
        onPageChange,
        tableClassName,
        rowsPerPage,
        ...props,
      }}
    />
  );
}
