import { useState, useEffect, useMemo } from 'react';
import { makeStyles } from '@material-ui/core/styles';

import { getAxios } from 'util/axios';
import Table, { TableRow, TableCell } from './Table';

const useStyles = makeStyles(theme => ({
  numeric: theme.mixins.tableCell.numeric(theme),
}));

export default function RecordByMapTable({ title, team }) {
  const axios = getAxios();
  const classes = useStyles();
  const [mapList, setMapList] = useState([]);
  const [recordByMap, setRecordByMap] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  const [isLoaded, setIsLoaded] = useState(false);

  useEffect(() => {
    if (isLoading || isLoaded || !team?.id) return;

    setIsLoading(true);

    axios.getAllPages(`/api/tournament/match/`, { params: { teamId: team.id } }).then(matches => {

      const promises = [];

      for (const match of matches) {
        const promise = axios.getAllPages(`/api/game/game/`, { params: { tournamentMatchId: match.id } })
              .then(games => {
                const isBlue = (match.blueTeam === team.id);

                for (const game of games) {
                  setRecordByMap(val => {
                    if (!(game.mapName in val)) {
                      val[game.mapName] = { wins: 0, losses: 0 };

                      const maps = Object.keys(val);
                      maps.sort();
                      setMapList(maps);
                    }

                    if ((game.winningTeam === 'blue') === isBlue) {
                      val[game.mapName].wins++;
                    } else {
                      val[game.mapName].losses++;
                    }

                    return val;
                  });
                }
              });

        promises.push(promise);
      }

      Promise.all(promises).then(() => {
        setIsLoaded(true);
        setIsLoading(false);
      });
    });
  }, [team]);

  return (
    <Table
      title={title}
      columnHeaders={['Map Name', 'Wins', 'Losses']}
      cellClassNames={['', classes.numeric, classes.numeric]}
      isLoading={!isLoaded}
    >
      {mapList.map(mapName => (
        <TableRow key={mapName}>
          <TableCell>{mapName}</TableCell>
          <TableCell className={classes.numeric}>{recordByMap[mapName].wins}</TableCell>
          <TableCell className={classes.numeric}>{recordByMap[mapName].losses}</TableCell>
        </TableRow>
      ))}
    </Table>
  );
}
