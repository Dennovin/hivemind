import { makeStyles } from '@material-ui/core/styles';
import { Typography, Grid, Link } from '@material-ui/core';
import clsx from 'clsx';

const useStyles = makeStyles(theme => ({
  matchTable: {
    margin: theme.spacing(2, 0),
    border: '1px solid #aaa',
  },
  teamLink: {
    flexGrow: 1,
    flexBasis: 0,
  },
  teamLinkGold: {
    flexDirection: 'row-reverse',
    textAlign: 'right',
  },
  blueTeam: ({ match }) => ({
    background: theme.gradients.blue.light,
    '& .MuiTypography-root': {
      fontWeight: (match.isComplete && match.blueScore > match.goldScore) ? 'bold' : 'normal',
    },
  }),
  goldTeam: ({ match }) => ({
    background: theme.gradients.gold.light,
    '& .MuiTypography-root': {
      fontWeight: (match.isComplete && match.blueScore < match.goldScore) ? 'bold' : 'normal',
    },
  }),
  queenIcon: {
    flexGrow: 0,
    flexBasis: '50px',
    padding: theme.spacing(2),
    display: 'flex',
    alignItems: 'center',
  },
  teamName: {
    flexGrow: 1,
    flexBasis: 0,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    padding: theme.spacing(2),
  },
  score: {
    flexGrow: 0,
    flexBasis: '50px',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    textAlign: 'center',
  },
  text: {
    margin: theme.spacing(0, 3),
    fontSize: '125%',
  },
  scoreText: {
    fontSize: '150%',
  },
  playerLink: {
    color: theme.palette.text.primary,
  },
}));

export default function QPMatchResultTable({ match }) {
  const classes = useStyles({ match });

  return (
    <Grid item container direction="row" className={classes.matchTable}>

      <Grid item container className={clsx(classes.teamLink, classes.teamLinkBlue)}>
        <Grid item className={clsx(classes.blueTeam, classes.queenIcon)}>
          <img className={classes.blueQueen} src="/static/blue_queen.png" />
        </Grid>

        <Grid item className={clsx(classes.blueTeam, classes.teamName)}>
          {match.players.blue.map(player => (
            <Link className={classes.playerLink} key={player.id} href={`/player/${player.id}`}>{player.name}</Link>
          ))}
        </Grid>

        <Grid item className={clsx(classes.blueTeam, classes.score)}>
          <Typography className={classes.scoreText}>{match.blueScore}</Typography>
        </Grid>
      </Grid>

      <Grid item container className={clsx(classes.teamLink, classes.teamLinkGold)}>
        <Grid item className={clsx(classes.goldTeam, classes.queenIcon)}>
          <img className={classes.goldQueen} src="/static/gold_queen.png" />
        </Grid>

        <Grid item className={clsx(classes.goldTeam, classes.teamName)}>
          {match.players.gold.map(player => (
            <Link className={classes.playerLink} key={player.id} href={`/player/${player.id}`}>{player.name}</Link>
          ))}
        </Grid>

        <Grid item className={clsx(classes.goldTeam, classes.score)}>
          <Typography className={classes.scoreText}>{match.goldScore}</Typography>
        </Grid>
      </Grid>

    </Grid>
  );
}
