import React, { useState } from 'react';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import { toast } from 'react-toastify';

import { isAdminOf } from 'util/auth';
import { TOURNAMENT_LINK_TYPES, AUTO_WARMUP } from 'util/constants';
import Table, { TableRow, TableCell } from './Table';
import TournamentBracketForm from 'components/forms/TournamentBracketForm';

export default function TournamentBracketTable({ title, tournament, onSave }) {
  const [formOpen, setFormOpen] = useState(false);
  const [selectedBracket, setSelectedBracket] = useState({});

  const columnHeaders = ['Stage Name'];

  const openForm = (e, bracket) => {
    e.preventDefault();

    setSelectedBracket(bracket);
    setFormOpen(true);
  };

  const closeForm = () => {
    onSave();
    setFormOpen(false);
  };

  const props = {
    title,
    columnHeaders,
    isLoading: false,
  };

  if (isAdminOf(tournament.scene.id)) {
    const blankBracket = {
      tournament: tournament.id,
      roundsPerMatch: 0,
      winsPerMatch: 0,
      matchesPerOpponent: 1,
      numGroups: 1,
      reportAsSets: false,
      addTiebreaker: false,
      thirdPlaceMatch: false,
      queueTimer: 30,
      autoWarmup: AUTO_WARMUP.NEVER,
      randomizeSides: false,
      stageTiebreakers: [
        'game_win_pct',
        'h2h_games',
        'match_win_pct',
        'game_time_in_losses',
        'dice_roll',
      ],
    };

    props.titleButton = (
      <TournamentBracketForm
        bracket={blankBracket}
        tournament={tournament}
        enableReinitialize={false}
        onClose={onSave}
      />
    );
  }

  return (
    <>
      <Table {...props}>
        {tournament.brackets.map(bracket => (
          <TableRow key={bracket.id} onClick={e => openForm(e, bracket)}>
            <TableCell>{bracket.name}</TableCell>
          </TableRow>
        ))}
      </Table>
      <TournamentBracketForm
        bracket={selectedBracket}
        tournament={tournament}
        showButton={false}
        open={formOpen}
        onClose={closeForm}
      />
    </>
  );
}

TournamentBracketTable.propTypes = {
  title: PropTypes.string,
  tournament: PropTypes.object.isRequired,
};

TournamentBracketTable.defaultProps = {
  title: 'Tournament Stages',
};
