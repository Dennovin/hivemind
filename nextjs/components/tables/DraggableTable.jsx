import Icon from '@mdi/react';
import { mdiDragVertical } from '@mdi/js';
import { makeStyles } from '@material-ui/core/styles';

import Table, { TableRow, TableCell } from './Table';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';

const useStyles = makeStyles(theme => ({
  handle: {
    width: '16px',
    cursor: 'grab',
  },
}));

export function DraggableTable({ columnHeaders, cellClassNames, onDragEnd, children, ...props }) {
  const classes = useStyles();

  return (
    <DragDropContext onDragEnd={onDragEnd}>
      <Droppable droppableId="drag-drop-table">
        {(provided, snapshot) => (
          <Table
            tbodyRef={provided.innerRef}
            columnHeaders={['', ...columnHeaders]}
            cellClassNames={[classes.handle, ...cellClassNames]}
            {...props}
          >
            {children}
            {provided.placeholder}
          </Table>
        )}
      </Droppable>
    </DragDropContext>
  );
}

export function DraggableTableRow({ draggableId, index, children, ...props }) {
  const classes = useStyles();

  return (
    <Draggable draggableId={draggableId} index={index}>
      {(provided, snapshot) => (
        <TableRow forwardedRef={provided.innerRef} {...props} {...provided.draggableProps}>
          <TableCell className={classes.handle} {...provided.dragHandleProps}>
            <Icon path={mdiDragVertical} size={1} />
          </TableCell>

          {children}
        </TableRow>
      )}
    </Draggable>
  );
}
