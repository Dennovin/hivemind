import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';

import Table, { TableRow, TableCell } from './Table';

const useStyles = makeStyles(theme => ({
}));

export default function TeamSheetPlayerList({ players, title }) {
  const classes = useStyles();
  const columnHeaders = ['Name', 'Scene'];

  return (
    <Table title={title} columnHeaders={columnHeaders} isLoading={false}>
      {players.map(player => (
        <TableRow key={player.id} href={`/tournament-player/${player.id}`}>
          <TableCell className={classes.playerName}>{player.name}</TableCell>
          <TableCell className={classes.scene}>{player.scene}</TableCell>
        </TableRow>
      ))}
    </Table>
  );
}

TeamSheetPlayerList.propTypes = {
  title: PropTypes.string,
  players: PropTypes.array.isRequired,
};

TeamSheetPlayerList.defaultProps = {
  title: null,
};
