import React, { useState, useEffect } from 'react';
import Icon from '@mdi/react';
import { mdiPlusMinus } from '@mdi/js';
import {
  mdiFruitGrapes,
  mdiSnail,
  mdiSword,
  mdiThemeLightDark,
  mdiWeatherNight,
  mdiWeatherSunny,
  mdiWeatherSunset,
} from '@mdi/js';
import PropTypes from 'prop-types';
import { format } from 'date-fns';
import { makeStyles } from '@material-ui/core/styles';
import { Button, CircularProgress } from '@material-ui/core';
import clsx from 'clsx';

import { BLUE_TEAM, GOLD_TEAM, TEAM_NAMES, WIN_CONDITIONS } from 'util/constants';
import { formatGameTime } from 'util/dates';
import { getAxios } from 'util/axios';
import Table, { TableRow, TableCell } from './Table';

const useStyles = makeStyles(theme => ({
  isLoading: {
    cursor: 'wait',
  },
  row: {
    backgroundColor: theme.palette.background.default,
  },
  blue: {
    background: theme.gradients.blue.light,
    '& .MuiTypography-root': {
      fontWeight: 'bold',
    },
  },
  gold: {
    background: theme.gradients.gold.light,
    '& .MuiTypography-root, & .MuiTableCell-root': {
      fontWeight: 'bold',
    },
  },
  gameIcon: {
    verticalAlign: 'top',
  },
  gameTable: {
    tableLayout: 'auto',
  },
  unavailable: {
    backgroundColor: theme.palette.action.disabled,
    opacity: 0.4,
  },
}));

export function formatGameEndTime(game) {
  if (!game?.endTime) return '';

  return format(new Date(game.endTime), 'h:mmaaa');
}

export default function TournamentGameSelector({ tournament, cabinet, activeMatch }) {
  const axios = getAxios({ authenticated: true });
  const classes = useStyles();
  const [games, setGames] = useState(null);
  const [isLoading, setIsLoading] = useState(false);

  const mapIcons = {
    Day: <Icon size={0.8} className={classes.gameIcon} path={mdiWeatherSunny} />,
    Night: <Icon size={0.8} className={classes.gameIcon} path={mdiWeatherNight} />,
    Dusk: <Icon size={0.8} className={classes.gameIcon} path={mdiWeatherSunset} />,
    Twilight: <Icon size={0.8} className={classes.gameIcon} path={mdiThemeLightDark} />,
  };

  const winConditionIcons = {
    Military: <Icon size={0.8} className={classes.gameIcon} path={mdiSword} />,
    Economic: <Icon size={0.8} className={classes.gameIcon} path={mdiFruitGrapes} />,
    Snail: <Icon size={0.8} className={classes.gameIcon} path={mdiSnail} />,
  };

  const loadGames = async () => {
    const response = await axios
      .get(`/api/game/game/recent/`, { params: { cabinetId: cabinet.id } })
      .catch(err => toast.error('Could not retrieve recent games list'));
    setGames(response.data.results);
  };

  useEffect(() => {
    loadGames();
  }, [tournament, activeMatch]);

  const canEdit = game =>
    isModifyMatchGames &&
    activeMatch?.id &&
    !isLoading &&
    (!game.tournamentMatch || game.tournamentMatch == activeMatch.id);

  const handleClick = async game => {
    if (!canEdit(game)) {
      return;
    }

    setIsLoading(true);

    const response = await axios
      .get(`/api/tournament/match/${activeMatch.id}/`)
      .catch(err => toast.error('Could not retrieve active match'));
    const match = response.data;

    const adj = game.tournamentMatch == activeMatch.id ? -1 : 1;
    const newMatchValues = {};

    if (game.winningTeam == BLUE_TEAM) newMatchValues.blueScore = match.blueScore + adj;
    if (game.winningTeam == GOLD_TEAM) newMatchValues.goldScore = match.goldScore + adj;

    await axios
      .patch(`/api/tournament/match/${activeMatch.id}/`, newMatchValues)
      .catch(err => toast.error('Could not update match scores'));

    const newMatchID = game.tournamentMatch == activeMatch.id ? null : activeMatch.id;
    await axios
      .patch(`/api/game/game/${game.id}/`, { tournamentMatch: newMatchID })
      .catch(err => toast.error('Could not add/remove game to current set'));

    await loadGames();
    setIsLoading(false);
  };

  const getClassName = game => {
    return clsx(classes.row, {
      [classes.blue]:
        activeMatch.id && game.tournamentMatch == activeMatch.id && game.winningTeam == BLUE_TEAM,
      [classes.gold]:
        activeMatch.id && game.tournamentMatch == activeMatch.id && game.winningTeam == GOLD_TEAM,
      [classes.unavailable]: game.tournamentMatch && game.tournamentMatch != activeMatch.id,
    });
  };

  const props = {
    title: 'Current Set',
    columnHeaders: ['Map', 'Time', 'Winner', 'Win', 'Ended'],
    isLoading: games === null,
    className: clsx(classes.gameTable, { [classes.isLoading]: isLoading }),
  };

  const [isModifyMatchGames, setIsModifyMatchGames] = useState(false);

  props.titleButton = (
    <>
      <Button
        size="small"
        variant="outlined"
        onClick={() => setIsModifyMatchGames(!isModifyMatchGames)}
      >
        {isModifyMatchGames ? 'Hide Recent Games' : 'Add/Remove Games'}
      </Button>
    </>
  );

  return (
    <Table {...props}>
      {games !== null &&
        games
          .filter(
            game =>
              isModifyMatchGames || (activeMatch.id && game.tournamentMatch == activeMatch.id),
          )
          .slice(0, 15)
          .map(game => (
            <TableRow
              key={game.id}
              className={getClassName(game)}
              onClick={canEdit(game) ? () => handleClick(game) : undefined}
            >
              <TableCell>
                {mapIcons[game.mapName]} {game.mapName}
              </TableCell>
              <TableCell>{formatGameTime(game)}</TableCell>
              <TableCell>{TEAM_NAMES[game.winningTeam] ?? game.winningTeam}</TableCell>
              <TableCell>
                {winConditionIcons[WIN_CONDITIONS[game.winCondition] ?? game.winCondition]}
              </TableCell>
              <TableCell>{formatGameEndTime(game)}</TableCell>
            </TableRow>
          ))}
    </Table>
  );
}
