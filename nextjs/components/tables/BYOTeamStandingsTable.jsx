import { makeStyles } from '@material-ui/core/styles';
import { useRouter } from 'next/router';
import { Typography } from '@material-ui/core';
import PropTypes from 'prop-types';

import { getAxios } from 'util/axios';
import { isAdminOf } from 'util/auth';
import Table from './Table';
import BYOTeamForm from '../forms/BYOTeamForm';

const useStyles = makeStyles(theme => ({
  tiebreak: {
    textAlign: 'right',
  },
  tiebreakText: {
    fontSize: '13px',
  },
  numeric: {
    ...theme.mixins.tableCell.numeric(theme),
  },
}));

export default function BYOTeamStandingsTable({ title, season, data }) {
  const classes = useStyles({ season });

  const hasTiebreak = data.filter(row => row.tiebreakDesc?.length > 0).length > 0;
  const getTiebreak = row => {
    if (row.tiebreakDesc?.length > 0) {
      return (
        <>
          {row.tiebreakDesc.map((txt, idx) => (
            <Typography className={classes.tiebreakText} key={idx}>{txt}</Typography>
          ))}
        </>
      );
    } else {
      return '';
    }
  };

  const columnHeaders = ['Team Name', 'Rounds', 'Matches'];
  const cellClassNames = [null, classes.numeric, classes.numeric];

  if (hasTiebreak) {
    columnHeaders.push('');
    cellClassNames.push(classes.tiebreak);
  }

  const getRowCells = row => {
    const cells = [
      row.team.name,
      `${row.roundsWon}-${row.roundsLost}`,
      `${row.matchesWon}-${row.matchesLost}`,
    ];

    if (hasTiebreak) {
      cells.push(getTiebreak(row));
    }

    return cells;
  };

  const rowLink = row => `/season-team/${row.team.id}`;

  const props = {
    title,
    columnHeaders,
    data,
    cellClassNames,
    getRowCells,
    isLoading: false,
    link: rowLink,
  };

  if (isAdminOf(season.scene.id)) {
    const emptyTeam = { season, name: '', players: [] };
    props.titleButton = (<BYOTeamForm team={emptyTeam} />);
  }

  return (
    <Table {...props} />
  );
}

BYOTeamStandingsTable.propTypes = {
  title: PropTypes.string,
  season: PropTypes.object.isRequired,
  data: PropTypes.array.isRequired,
};

BYOTeamStandingsTable.defaultProps = {
  title: 'Team Standings',
};
