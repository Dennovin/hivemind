import { makeStyles } from '@material-ui/core/styles';
import { Grid, List, ListItem, ListItemIcon, Typography } from '@material-ui/core';
import Icon from '@mdi/react';
import PropTypes from 'prop-types';
import clsx from 'clsx';

const useStyles = makeStyles(theme => ({
  rowGrid: {
    alignItems: 'start',
  },
  icon: {
    width: '32px',
    verticalAlign: 'middle',
  },
  label: {
    width: '50%',
    maxWidth: '12em',
    fontSize: 'inherit'

  },
  value: {
    flexBasis: 0,
    flexGrow: 1,
  },
}));

export function SummaryTableRow({ icon, label, children }) {
  const classes = useStyles();

  return (
    <ListItem className="px-0 py-2">
      <Grid container direction="row" spacing={2} className={clsx(classes.rowGrid, 'items-center')}>
        <Grid item className={classes.label}>
          <div className="flex gap-2 items-center">

          {icon && (
            <Icon path={icon} size={.8} className="text-gray-500 mix-blend-multiply" />
            )}

          <span className="font-bold">{label ? `${label}:` : ''}</span>
            </div>
        </Grid>
        <Grid item className={classes.value}>{children}</Grid>
      </Grid>
    </ListItem>
  );
}

SummaryTableRow.propTypes = {
  Icon: PropTypes.elementType,
  label: PropTypes.string,
  children: PropTypes.node,
};

SummaryTableRow.defaultProps = {
  icon: null,
  label: '',
  children: (<></>),
};

export function SummaryTable({ children }) {
  const classes = useStyles();

  return (
    <List>
      {children}
    </List>
  );
}
