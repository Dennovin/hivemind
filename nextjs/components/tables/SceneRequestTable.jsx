import React, { useState } from 'react';
import { format } from 'date-fns';

import { UserConsumer } from 'util/auth';
import { getAxios } from 'util/axios';
import Table from './Table';

const SceneRequestTable = UserConsumer(({ user }) => {
  const axios = getAxios({ authenticated: true });
  const [data, setData] = useState(null);

  const columnHeaders = ['Scene Name', 'Date Submitted', 'Status'];
  const getRowCells = row => [
    row.displayName,
    format(new Date(row.submitDate), 'MMM d - hh:mm aa'),
    row.completed ? 'Completed' : 'Pending',
  ];

  const rowLink = row => `/admin/scene-request/${row.id}`;

  if (!user?.isSiteAdmin) {
    return (
      <div>You do not have permission to approve scene registrations.</div>
    );
  }

  if (data === null) {
    axios.get('/api/user/scene-request/').then(response => { setData(response.data.results); });
    return (<></>);
  }

  return (
    <Table title="Scene Registrations" columnHeaders={columnHeaders} data={data} getRowCells={getRowCells} isLoading={data === null} link={rowLink} />
  );
});

export default SceneRequestTable;
