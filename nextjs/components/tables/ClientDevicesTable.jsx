import React, { useState, useEffect, useRef } from 'react';
import { Button, Dialog, DialogActions, DialogContent, IconButton, Link } from '@material-ui/core';
import QRCode from 'qrcode';
import { useReactToPrint } from 'react-to-print';
import { mdiQrcode } from '@mdi/js';
import Icon from '@mdi/react';

import { useUser } from 'util/auth';
import { getAxios } from 'util/axios';
import SceneColorBox from 'components/SceneColorBox';
import Table, { TableRow, TableCell } from './Table';
import ClientDeviceForm from '../forms/ClientDeviceForm';

import styles from './ClientDevicesTable.module.css';

export function RegisterLink({ device }) {
  const [open, setOpen] = useState(false);
  const [dataUrl, setDataUrl] = useState(null);
  const contentRef = useRef(null);
  const print = useReactToPrint({ contentRef });

  const url = `${window.location.origin}/register-nfc/${device.id}`;

  useEffect(() => {
    QRCode.toDataURL(url).then(setDataUrl);
  }, [url]);

  return (
    <>
      <IconButton variant="outlined" onClick={() => setOpen(true)}>
        <Icon path={mdiQrcode} size={1} />
      </IconButton>
      {open && (
        <Dialog
          open={true}
          onClose={() => setOpen(false)}
          className={styles.registerLinkDialog}
          maxWidth="md"
        >
          <DialogContent ref={contentRef}>
            <div className={styles.dialogContent}>
              <div className={styles.qrImage}>{dataUrl && <img src={dataUrl} />}</div>
              <div className={styles.bottomCaption}>Scan to register your NFC tag</div>
              <Link className={styles.link} href={url}>
                {url}
              </Link>
            </div>
          </DialogContent>

          <DialogActions>
            <Button variant="outlined" color="secondary" onClick={print}>
              Print
            </Button>
            <Button variant="outlined" color="default" onClick={() => setOpen(false)}>
              Close
            </Button>
          </DialogActions>
        </Dialog>
      )}
    </>
  );
}

export function CabinetName({ cabinetId, cabinet }) {
  if (cabinetId === null) {
    return <span className={styles.cabinetUnassigned}>Unassigned</span>;
  }

  if (cabinet === undefined) {
    return <span className={styles.loading}>...</span>;
  }

  return (
    <div className={styles.cabinet}>
      <SceneColorBox className={styles.sceneColorBox} color={cabinet.scene.backgroundColor} />
      <span className={styles.cabinetName}>
        {cabinet.scene.displayName} - {cabinet.displayName}
      </span>
      <span className={styles.cabinetNameShort}>{cabinet.displayName}</span>
    </div>
  );
}

export function CabinetNameCell({ cabinetId, cabinet }) {
  return (
    <TableCell className={styles.cabinetNameCell}>
      <CabinetName {...{ cabinetId, cabinet }} />
    </TableCell>
  );
}

export default function ClientDevicesTable() {
  const axios = getAxios({ authenticated: true });
  const user = useUser();
  const [devices, setDevices] = useState(null);
  const [allScenes, setAllScenes] = useState([]);
  const [cabinetsById, setCabinetsById] = useState({});

  const loadDevices = () => {
    axios.getAllPages('/api/game/client-device/').then(response => {
      setDevices(response.toSorted((a, b) => a.name.localeCompare(b.name)));
    });
  };

  useEffect(() => {
    loadDevices();
  }, []);

  useEffect(() => {
    axios.getAllPages(`/api/game/scene/`).then(setAllScenes);
  }, []);

  useEffect(() => {
    if (devices === null) return;
    const cabinetIds = new Set(devices.map(d => d.cabinet));
    cabinetIds.forEach(id => {
      if (!cabinetsById[id]) {
        axios.get(`/api/game/cabinet/${id}/`).then(response => {
          const cabinet = response.data;
          if (!cabinet) return;

          axios.get(`/api/game/scene/${cabinet.scene}/`).then(response => {
            setCabinetsById(val => ({ ...val, [id]: { ...cabinet, scene: response.data } }));
          });
        });
      }
    });
  }, [devices]);

  const emptyDevice = { name: '', cabinet: null, owner: '' };

  return (
    <Table
      title="Manage Client Devices"
      className={styles.table}
      columnHeaders={['ID', 'Name', 'Assigned To', '', '', '']}
      isLoading={devices === null}
      cellClassNames={[
        styles.idCell,
        styles.nameCell,
        styles.cabinetNameCell,
        styles.mobileTextCell,
        styles.editCell,
        styles.registerLinkCell,
      ]}
      titleButton={<ClientDeviceForm allScenes={allScenes} device={emptyDevice} />}
    >
      {devices &&
        devices.map(row => (
          <TableRow key={row.id}>
            <TableCell className={styles.idCell}>{row.id}</TableCell>
            <TableCell className={styles.nameCell}>{row.name}</TableCell>
            <CabinetNameCell cabinetId={row.cabinet} cabinet={cabinetsById[row.cabinet]} />
            <TableCell className={styles.mobileTextCell}>
              <div className={styles.mobileText}>
                <div className={styles.row}>{row.id}</div>
                <div className={styles.row}>
                  <b>{row.name}</b>
                </div>
                <div className={styles.row}>
                  <CabinetName cabinetId={row.cabinet} cabinet={cabinetsById[row.cabinet]} />
                </div>
              </div>
            </TableCell>
            <TableCell className={styles.editCell}>
              <ClientDeviceForm allScenes={allScenes} device={row} />
            </TableCell>
            <TableCell className={styles.registerLinkCell}>
              <RegisterLink device={row} />
            </TableCell>
          </TableRow>
        ))}
    </Table>
  );
}
