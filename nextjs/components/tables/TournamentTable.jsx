import { Button } from '@material-ui/core';
import PropTypes from 'prop-types';
import { useState } from 'react';

import { isAdminOf } from 'util/auth';
import { getAxios } from 'util/axios';
import { formatUTC } from 'util/dates';
import TournamentForm from '../forms/TournamentCreateForm';
import PaginatedTable from './PaginatedTable';

export default function TournamentTable({ title, scene }) {
  const axios = getAxios();

  const columnHeaders = ['Name', 'Date'];
  const getRowCells = row => [row.name, formatUTC(row.date, 'MMM d, yyyy')];

  const rowLink = row => `/t/${row.sceneName}/${row.slug}`;

  const props = {
    title,
    columnHeaders,
    getRowCells,
    link: rowLink,
    url: '/api/tournament/tournament/',
    filters: { sceneId: scene.id },
    rowsPerPage: 10,
  };

  if (isAdminOf(scene.id)) {
    const emptyTournament = { scene, assignedCabinets: [] };
    props.titleButton = (
      <>
        <TournamentForm tournament={emptyTournament} />
      </>
    );
  }

  return <PaginatedTable {...props} />;
}

TournamentTable.propTypes = {
  title: PropTypes.string,
  scene: PropTypes.object.isRequired,
};

TournamentTable.defaultProps = {
  title: 'Tournaments',
};
