import { getAxios } from 'util/axios';
import { formatUTC } from 'util/dates';
import PaginatedTable from './PaginatedTable';

export default function UpcomingTournamentsTable({ title }) {
  const axios = getAxios();

  const columnHeaders = ['Name', 'Scene', 'Register By', 'Date'];
  const getRowCells = row => [
    row.name,
    row.sceneDisplayName,
    row.registrationCloseDate ? formatUTC(row.registrationCloseDate, 'MMM d, yyyy') : '',
    formatUTC(row.date, 'MMM d, yyyy'),
  ];

  const rowLink = row => `/tournament/${row.id}`;

  const props = {
    title,
    columnHeaders,
    getRowCells,
    link: rowLink,
    url: '/api/tournament/tournament/',
    filters: { isFuture: true },
    ordering: 'date',
    rowsPerPage: 10,
    className: 'first:mt-0'
  };

  return <PaginatedTable {...props} />;
}
