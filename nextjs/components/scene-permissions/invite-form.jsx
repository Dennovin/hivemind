import React from 'react';
import { MenuItem } from '@material-ui/core';

import Form, { useForm } from '@/components/forms/Form';
import Field, { Select } from '@/components/fields/Field';
import { useScenePermissions } from './context';

import { PERMISSION_TYPES, PERMISSION_NAMES } from '@/util/constants';

const permissionDescriptions = {
  [PERMISSION_TYPES.ADMIN]:
    'A Scene Administrator has full access to all scene data and settings, and can add and remove all other permissions.',
  [PERMISSION_TYPES.TOURNAMENT]:
    'A Tournament Official has permission to update the score and status of tournament matches, and can modify the match queue.',
};

export function PermissionDescription() {
  const { values } = useForm();

  return <div>{permissionDescriptions[values?.permission]}</div>;
}

export function InviteForm() {
  const { scene, setInvites, setSelectedInvite } = useScenePermissions();
  const invite = {
    scene: scene.id,
    permission: PERMISSION_TYPES.ADMIN,
  };

  const formProps = {
    buttonText: 'Create Invite',
    dialogTitle: 'Create Permission Invite',
    canEdit: () => true,
    object: invite,
    url: `/api/user/permission-invite/`,
    method: 'POST',
    reloadOnSave: false,
    onSave: result => {
      setInvites(v => [...v, result]);
      setSelectedInvite(result);
    },
  };

  return (
    <Form {...formProps}>
      <Field name="permission" label="Permission Type" component={Select}>
        {[PERMISSION_TYPES.ADMIN, PERMISSION_TYPES.TOURNAMENT].map(value => (
          <MenuItem key={value} value={value}>
            {PERMISSION_NAMES[value]}
          </MenuItem>
        ))}
      </Field>

      <PermissionDescription />
    </Form>
  );
}
