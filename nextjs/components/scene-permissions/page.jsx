import React from 'react';
import { Breadcrumbs, Link } from '@material-ui/core';

import Title from '@/components/Title';
import { useScenePermissions } from './context';
import { ScenePermissionsTable } from './table';
import { PermissionInvitesTable } from './invites-table';
import { SelectedInvite } from './selected-invite';

export function ScenePermissionsPage() {
  const { scene } = useScenePermissions();
  if (!scene) return null;

  return (
    <div className="space-y-8">
      <Breadcrumbs>
        <Link href={`/scene/${scene.name}`}>{scene.displayName}</Link>
      </Breadcrumbs>

      <Title title={`User Permissions: ${scene.displayName}`} />

      <ScenePermissionsTable />
      <PermissionInvitesTable />
      <SelectedInvite />
    </div>
  );
}
