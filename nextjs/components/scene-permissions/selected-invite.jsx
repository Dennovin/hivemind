import React, { useState, useEffect, useMemo } from 'react';
import { Button, Dialog, DialogTitle, DialogContent, DialogActions } from '@material-ui/core';
import Icon from '@mdi/react';
import { mdiAccount, mdiCity, mdiShieldKey, mdiDelete } from '@mdi/js';
import QRCode from 'qrcode';
import { toast } from 'react-toastify';

import { SummaryTable, SummaryTableRow } from '@/components/tables/SummaryTable';
import { getAxios } from '@/util/axios';
import { PERMISSION_NAMES } from '@/util/constants';
import { useScenePermissions } from './context';

export function SelectedInvite() {
  const axios = getAxios({ authenticated: true });
  const [dataUrl, setDataUrl] = useState(null);
  const { setInvites, selectedInvite, setSelectedInvite } = useScenePermissions();

  const deleteInvite = () => {
    axios.delete(`/api/user/permission-invite/${selectedInvite.id}/`).then(() => {
      toast.success('Invite revoked.');
    });

    setInvites(invites => invites.filter(i => i.id !== selectedInvite.id));
    setSelectedInvite(null);
  };

  const url = useMemo(
    () => `${window.location.origin}/permission-invite/${selectedInvite?.token}`,
    [selectedInvite],
  );

  useEffect(() => {
    QRCode.toDataURL(url).then(setDataUrl);
  }, [url]);

  if (!selectedInvite) return null;

  return (
    <Dialog open={true} onClose={() => setSelectedInvite(null)} fullWidth={true} maxWidth="md">
      <DialogTitle>Permission Invite</DialogTitle>

      <DialogContent>
        <SummaryTable>
          <SummaryTableRow icon={mdiCity} label="Scene">
            <b>{selectedInvite.scene.displayName}</b>
          </SummaryTableRow>

          <SummaryTableRow icon={mdiShieldKey} label="Permission">
            {PERMISSION_NAMES[selectedInvite.permission]}
          </SummaryTableRow>

          <SummaryTableRow>Send this link to the user:</SummaryTableRow>

          <SummaryTableRow>
            <b>{url}</b>
          </SummaryTableRow>

          <SummaryTableRow>Or have them scan this QR code:</SummaryTableRow>

          <SummaryTableRow>{dataUrl && <img src={dataUrl} />}</SummaryTableRow>
        </SummaryTable>
      </DialogContent>

      <DialogActions>
        <Button
          variant="outlined"
          color="secondary"
          onClick={deleteInvite}
          startIcon={<Icon path={mdiDelete} size={1} />}
        >
          Revoke Invite
        </Button>
      </DialogActions>
    </Dialog>
  );
}
