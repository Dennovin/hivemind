import React, { createContext, useContext, useEffect, useState } from 'react';

import { getAxios } from '@/util/axios';
import { useAuth } from '@/util/auth';

export const ScenePermissionsContext = createContext({});
export const useScenePermissions = () => useContext(ScenePermissionsContext);

export function ScenePermissionsProvider({ scene, children }) {
  const axios = getAxios({ authenticated: true });
  const { isAdminOf } = useAuth();

  const [userPermissions, setUserPermissions] = useState(null);
  const [invites, setInvites] = useState(null);
  const [selectedInvite, setSelectedInvite] = useState(null);

  useEffect(() => {
    axios.getAllPages(`/api/game/scene/${scene.id}/permissions/`).then(setUserPermissions);
    axios.get(`/api/user/permission-invite/by-scene/${scene.id}/`).then(response => {
      setInvites(response.data);
    });
  }, []);

  if (!scene?.id || !isAdminOf(scene.id)) return null;

  const ctx = {
    scene,
    userPermissions,
    setUserPermissions,
    invites,
    setInvites,
    selectedInvite,
    setSelectedInvite,
  };

  return (
    <ScenePermissionsContext.Provider value={ctx}>{children}</ScenePermissionsContext.Provider>
  );
}
