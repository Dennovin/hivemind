import { Button, CircularProgress, Switch } from '@material-ui/core';
import BlockIcon from '@material-ui/icons/Block';
import CheckIcon from '@material-ui/icons/Check';
import { mdiAccountEditOutline } from '@mdi/js';
import {
  flexRender,
  getCoreRowModel,
  getSortedRowModel,
  useReactTable,
} from '@tanstack/react-table';
import { download, generateCsv, mkConfig } from 'export-to-csv';
import { Fragment, useEffect, useMemo, useState } from 'react';

import { useAuth } from '@/util/auth';
import { getAxios } from '@/util/axios';
import { useTournamentAdmin } from '../context';
import { Subtitle } from '../subtitle';
import TournamentPlayerAdminForm from '../teams/player-admin-form';

const csvOptions = {
  fieldSeparator: ',',
  quoteStrings: '"',
  decimalSeparator: '.',
  useBom: true,
};
const toTitleCase = str =>
  str
    .replace(/([A-Z])/g, ' $1')
    .trim()
    .split(' ')
    .map(word => word.charAt(0).toUpperCase() + word.slice(1).toLowerCase())
    .join(' ');
const parsePrice = value =>
  value
    ? (parseFloat(value) / 100).toLocaleString('en-US', {
        currency: 'USD',
        style: 'currency',
      })
    : '--';

export function RegisteredList() {
  const { isAdminOf } = useAuth();
  const { tournament } = useTournamentAdmin();
  const axios = getAxios({ authenticated: true });

  const [players, setPlayers] = useState(null);
  const [questions, setQuestions] = useState(null);
  const [sorting, setSorting] = useState([]);

  useEffect(() => {
    if (tournament?.id && players === null) {
      axios
        .getAllPages(`/api/tournament/player/`, {
          params: { tournamentId: tournament.id },
        })
        .then(setPlayers);
    }
  }, [tournament?.id]);

  useEffect(() => {
    if (tournament?.id && questions === null) {
      axios
        .getAllPages(`/api/tournament/player-info-field/`, {
          params: { tournamentId: tournament.id },
        })
        .then(setQuestions);
    }
  }, [tournament?.id]);

  const summary = useMemo(() => {
    if (players === null) return null;

    const value = { playersPaid: 0, totalPlayers: 0, totalPaid: 0 };
    players.forEach(player => {
      const playerData = {};
      value.playersPaid += player.paid ? 1 : 0;
      value.totalPlayers += 1;
      value.totalPaid += parseFloat(player.price) ? parseFloat(player.price) : 0;
    });

    return value;
  }, [players]);

  if (!isAdminOf(tournament.scene.id)) return <div>Not Authorized</div>;

  const questionsKeyMap = useMemo(() => {
    if (questions === null) return null;
    const val = {};
    questions.forEach(q => (q.fieldSlug ? (val[q.fieldName] = q.fieldSlug) : undefined));
    return val;
  }, [questions]);

  const columnHandlers = (key, value) => {
    if (key.includes('question')) {
      const returnVal = (function () {
        if (!value) return '--';
        const { fieldType } = questions.find(q => q.fieldName === key);
        if (fieldType === 'multi_choice') {
          return typeof value === 'string' ? JSON.parse(value)?.join(', ') : value?.join(', ');
        }
        return value;
      })();
      return <span className="text-xs whitespace-nowrap">{returnVal}</span>;
    }
    switch (key) {
      case 'price':
        return <span className="font-mono">{parsePrice(value)}</span>;
      case 'team':
        return tournament?.teams?.filter(i => i.id === value)?.pop()?.name || 'Free Agent';
      case 'registrationTime':
        return value ? new Date(value).toLocaleString('en-US') : '--';

      default:
        return typeof value === 'object' ? JSON.stringify(value) : value;
    }
  };

  const defaultColumns = useMemo(() => {
    if (questions === null) return;
    return [
      {
        id: 'checkedIn',
        header: 'Checked In',
        accessorKey: 'checkInTime',
        cell: ({ row }) => <>{<CheckInSwitch player={row.original} />}</>,
      },
      {
        id: 'player',
        accessorKey: 'name',
        cell: ({ row, getValue }) => (
          <div className="flex gap-6 items-center">
            <div className="w-12 h-12 rounded-lg bg-gray-300 overflow-hidden object-cover flex-shrink-0 relative">
              {row?.original?.doNotDisplay ? (
                <BlockIcon className="text-xl opacity-40 absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2" />
              ) : (
                row?.original?.image && (
                  <img src={row?.original?.image} className="block w-full h-full object-cover" />
                )
              )}
            </div>
            <div className="flex gap-3 text-base font-bold whitespace-nowrap">
              <div>{getValue()}</div>
              <div>
                <span className="rounded-sm bg-indigo-400/40 p-1 text-[10px] tracking-wider uppercase opacity-50">
                  {row?.original?.pronouns}
                </span>
              </div>
            </div>
          </div>
        ),
      },
      {
        id: 'paid',
        accessorKey: 'paid',
        sortingFn: a => (a.original.paid ? 1 : -1),
        cell: ({ row, getValue }) => {
          return !!getValue() ? (
            <div className="group relative">
              <span className="rounded-full w-8 h-8 bg-green-200 text-lg flex items-center justify-center tracking-wider uppercase whitespace-nowrap">
                <CheckIcon className="inline text-base" />
              </span>
              <div className="group-hover:block hidden absolute top-6 h-center p-3 rounded-lg bg-white shadow-lg z-50">
                {row.original?.selectedPaymentOptions?.map(po => (
                  <div
                    key={po.paymentOption?.name}
                    className="flex justify-between gap-6 text-xs font-mono whitespace-nowrap"
                  >
                    <span>{po.paymentOption?.name}</span>
                    <span>
                      {po.quantity} &times; {po.paymentOption?.price || po.customPrice}
                    </span>
                  </div>
                ))}
              </div>
            </div>
          ) : (
            <span className="rounded-full w-8 h-8 bg-red-200 text-lg flex items-center justify-center tracking-wider uppercase whitespace-nowrap">
              &times;
            </span>
          );
        },
      },
      {
        id: 'price',
        accessorKey: 'price',
        header: 'Price',
        cell: ({ getValue }) => columnHandlers('price', getValue()),
      },
      {
        id: 'registrationTime',
        accessorKey: 'registrationTime',
        header: 'Time of Payment',
        cell: ({ getValue }) => columnHandlers('registrationTime', getValue()),
      },
      ...['email', 'scene', 'team'].map(key => ({
        accessorKey: key,
        header: key,
        cell: ({ getValue }) => columnHandlers(key, getValue()),
      })),

      ...questions.map(q => ({
        accessorKey: q.fieldName,
        header: q?.fieldSlug || q?.fieldDescription || q.fieldName,
        cell: ({ getValue }) => columnHandlers(q.fieldName, getValue()),
      })),
      {
        id: 'edit',
        cell: ({ row }) => (
          <TournamentPlayerAdminForm
            player={row.original}
            team={tournament?.teams[row.original.team]}
            tournament={tournament}
            buttonIconPath={mdiAccountEditOutline}
          />
        ),
      },
    ];
  }, [questions]);

  const table = useReactTable({
    data: players,
    columns: defaultColumns,
    getCoreRowModel: getCoreRowModel(),
    state: {
      sorting,
    },
    onSortingChange: setSorting,
    getSortedRowModel: getSortedRowModel(),
  });

  const { getHeaderGroups, getRowModel } = table;

  const handleExportData = () => {
    const defaultColumns = [
      'user',
      'name',
      'pronouns',
      'scene',
      'team',
      { key: 'registrationTime', displayLabel: 'Time of Payment' },
      'checkInTime',
      'paid',
      'selectedPaymentOptions',
      'price',
      'image',
      'tidbit',
      'email',
    ];

    const columnHeaders = [
      ...defaultColumns.map(key => {
        if (typeof key === 'object' && key.key && key.displayLabel) return key;
        else return { key, displayLabel: toTitleCase(key) };
      }),
      ...Object.entries(questionsKeyMap).map(([key, value]) => ({ key, displayLabel: value })),
    ];

    const remap = players.map(player => {
      const cleanPlayer = {};

      columnHeaders.forEach((key, index) => {
        const realKey = key.key || key;
        if (Array.isArray(player[realKey])) {
          cleanPlayer[realKey] = player[realKey].join(', ');
        } else if (typeof player[realKey] === 'object') {
          cleanPlayer[realKey] = JSON.stringify(player[realKey]);
        } else {
          cleanPlayer[realKey] = player[realKey];
        }
      });

      return {
        ...cleanPlayer,
        selectedPaymentOptions: player.selectedPaymentOptions
          ?.map(po => `${po.paymentOption?.name}: ${po.quantity} x ${po.paymentOption?.price}`)
          .join('\n'),
        team: tournament?.teams?.filter(t => t.id === player.team)?.[0]?.name || '',
        price: parsePrice(player.price),
        image: player.doNotDisplay ? 'Do not display' : player.image,
        registrationTime: player.registrationTime
          ? new Date(player.registrationTime).toLocaleString('en-US', {
              day: '2-digit',
              month: '2-digit',
              year: 'numeric',
              hour: '2-digit',
              minute: '2-digit',
              hour12: true,
            })
          : '',
        checkInTime: player.checkInTime
          ? new Date(player.checkInTime).toLocaleString('en-US', {
              day: '2-digit',
              month: '2-digit',
              year: 'numeric',
              hour: '2-digit',
              minute: '2-digit',
              hour12: true,
            })
          : '',
      };
    });
    const csvConfig = mkConfig({
      ...csvOptions,
      filename: `${tournament.name} Registered Players`,
      columnHeaders,
    });
    const csv = generateCsv(csvConfig)(remap);
    download(csvConfig)(csv);
  };

  if (!summary || !questions) return <CircularProgress />;

  return (
    <>
      <Subtitle
        title={`Registered Players`}
        buttons={
          <Button variant="contained" color="secondary" onClick={handleExportData}>
            Export CSV
          </Button>
        }
      />

      <div className="rounded bg-blue-light4 p-6">
        <div className="grid grid-cols-2 md:grid-cols-4 gap-6">
          <h2 className="text-lg font-bold my-0">Summary</h2>
          <div>
            <h3 className="text-sm uppercase my-0 font-bold">Total Players</h3>
            <div className="text-2xl font-bold">{summary.totalPlayers}</div>
          </div>
          <div>
            <h3 className="text-sm uppercase my-0 font-bold">Players Paid</h3>
            <div className="text-2xl font-bold">{summary.playersPaid}</div>
          </div>
          <div>
            <h3 className="text-sm uppercase my-0 font-bold">Total Paid</h3>
            <div className="text-2xl font-bold">
              {tournament?.summary?.totalPaid &&
                (parseFloat(summary.totalPaid) / 100).toLocaleString('en-US', {
                  currency: 'USD',
                  style: 'currency',
                })}
            </div>
          </div>
        </div>
      </div>
      <div className="relative">
        <div className="overflow-auto w-full max-w-full h-[fit-content] max-h-[80vh] md:max-h-[60vh] md:relative sticky top-0">
          <table className="w-full border-collapse border-0 border-spacing-0">
            <thead>
              {getHeaderGroups().map(headerGroup => (
                <Fragment key={headerGroup.id}>
                  <tr className="bg-gray-800 text-gray-100">
                    {headerGroup.headers.map(header => (
                      <th
                        key={header.id}
                        className="p-2 px-3 text-left uppercase text-xs sticky top-0 z-50 whitespace-nowrap bg-gray-800 text-gray-100"
                      >
                        <div
                          {...{
                            className: header.column.getCanSort()
                              ? 'cursor-pointer select-none'
                              : '',
                            onClick: header.column.getToggleSortingHandler(),
                          }}
                        >
                          {flexRender(header.column.columnDef.header, header.getContext())}
                          {{
                            asc: ' 🔼',
                            desc: ' 🔽',
                          }[header.column.getIsSorted()] ?? null}
                        </div>
                      </th>
                    ))}
                  </tr>
                </Fragment>
              ))}
            </thead>
            <tbody>
              {getRowModel().rows.map(row => (
                <tr key={row.id} className="even:bg-gray-100">
                  {row.getVisibleCells().map(cell => (
                    <td key={cell.id} className="px-3 py-2 text-sm">
                      {flexRender(cell.column.columnDef.cell, cell.getContext())}
                    </td>
                  ))}
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </>
  );
}

const CheckInSwitch = ({ player }) => {
  const [checkedIn, setSetCheckedIn] = useState(!!player.checkInTime);
  return (
    <div className="flex items-center">
      <Switch
        name="checkInTime"
        checked={checkedIn}
        onChange={async e => {
          const axios = getAxios({ authenticated: true });
          setSetCheckedIn(!checkedIn);
          const response = await axios.patch(`/api/tournament/player/${player.id}/`, {
            checkInTime: e.target.checked ? new Date() : null,
          });
          if (response.status !== 200) {
            setSetCheckedIn(!checkedIn);
          }
        }}
      />
    </div>
  );
};

export async function getServerSideProps({ params }) {
  const axios = getAxios({ authenticated: true });
  let response = await axios.get(`/api/tournament/tournament/${params.id}/`);
  const tournament = response.data;

  if (!tournament) return { notFound: true };

  response = await axios.get(`/api/game/scene/${tournament.scene}/`);
  tournament.scene = response.data;

  tournament.teams = {};
  const teams = await axios.getAllPages(`/api/tournament/team/`, {
    params: { tournamentId: tournament.id },
  });
  teams.forEach(team => (tournament.teams[team.id] = team));
  const players = await axios.getAllPages(`/api/tournament/player/`, {
    params: { tournamentId: tournament.id },
  });

  const ignorekeys = ['stats', 'stripeSessionId', 'tournament'];
  const playerKeysFilter = key => !ignorekeys.includes(key);
  tournament.players = [];
  tournament.summary = {
    playersPaid: 0,
    totalPlayers: 0,
    totalPaid: 0,
  };

  const questions = response?.data?.results;
  const questionsKeyMap = {};

  return {
    props: {
      tournament,
      title: tournament.name,
      description: tournament.description,
      questions,
      questionsKeyMap,
    },
  };
}
