import React from 'react';

import { useTournamentAdmin } from '../context';
import { useTeamPlacement } from './context';
import { PlaceType } from './constants';
import { PreviousStageTable } from './previous-stage-table';

export function PreviousStages() {
  const { tournament } = useTournamentAdmin();
  const { stage } = useTeamPlacement();

  const stages = tournament.brackets.filter(b => b.id !== stage.id);

  return (
    <div>
      {stages.map(prevStage => (
        <React.Fragment key={prevStage.id}>
          <PreviousStageTable prevStage={prevStage} />
        </React.Fragment>
      ))}
    </div>
  );
}
