import React from 'react';
import { IconButton } from '@material-ui/core';
import Icon from '@mdi/react';
import { mdiMinusCircleOutline } from '@mdi/js';

import { STAGE_TYPE } from '@/util/constants';
import { List, ListItem } from './list';
import { PlaceType } from './constants';
import { ClearStageButton } from './clear-stage-button';
import { useTeamPlacement } from './context';

const showPlaceNumForTypes = [STAGE_TYPE.SINGLE_ELIM, STAGE_TYPE.DOUBLE_ELIM, STAGE_TYPE.LADDER];

export function RemoveItemButton({ place }) {
  const { setPlacements, setChanged } = useTeamPlacement();
  const onClick = () => {
    setPlacements(val => val.filter(p => p.key !== place.key));
    setChanged(true);
  };

  return (
    <IconButton onClick={onClick} aria-label="Remove from stage">
      <Icon path={mdiMinusCircleOutline} size={1} />
    </IconButton>
  );
}

export function StageTable() {
  const { stage, placements } = useTeamPlacement();
  const showPlaceNum = showPlaceNumForTypes.includes(stage.stageType);

  return (
    <List id={`stage-${stage.id}`} name={stage.name} icons={<ClearStageButton />}>
      {placements.map((place, idx) => (
        <ListItem
          name={place.name}
          key={place.key}
          id={place.key}
          place={place}
          index={idx}
          showPlaceNum={showPlaceNum}
          buttons={<RemoveItemButton place={place} />}
        />
      ))}
    </List>
  );
}
