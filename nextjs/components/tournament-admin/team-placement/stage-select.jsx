import React from 'react';
import { Select, MenuItem } from '@material-ui/core';
import { mdiAlert } from '@mdi/js';
import Icon from '@mdi/react';

import styles from './team-placement.module.css';
import { useTeamPlacement } from './context';
import { useTournamentAdmin } from '../context';

export function StageSelect() {
  const { tournament } = useTournamentAdmin();
  const { stage, setStage } = useTeamPlacement();

  const onChange = evt => {
    setStage(tournament?.brackets?.filter(b => b.id === evt.target.value)?.pop());
  };

  const showWarning = stage?.matches?.filter(m => m.blueScore + m.goldScore > 0).length > 0;

  return (
    <>
      <Select
        className={styles.stageSelect}
        displayEmpty={true}
        value={stage?.id ?? ''}
        onChange={onChange}
      >
        {!stage?.id && (
          <MenuItem className={styles.placeholder} value="">
            Select a stage...
          </MenuItem>
        )}
        {tournament.brackets.map(bracket => (
          <MenuItem className={styles.stageSelectItem} key={bracket.id} value={bracket.id}>
            {bracket.name}
          </MenuItem>
        ))}
      </Select>

      {showWarning && (
        <div className={styles.resetStageWarning}>
          <Icon className={styles.warningIcon} path={mdiAlert} size={1} />
          <span>
            WARNING: This stage has already begun. Modifying these assignments will reset the stage
            and delete any previously recorded results.
          </span>
        </div>
      )}
    </>
  );
}
