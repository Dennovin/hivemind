import React, { useMemo } from 'react';
import { IconButton } from '@material-ui/core';
import ordinal from 'ordinal';
import clsx from 'clsx';
import Icon from '@mdi/react';
import { mdiPlusCircleOutline } from '@mdi/js';

import { useTournamentAdmin } from '../context';
import { useTeamPlacement } from './context';
import { PlaceType } from './constants';
import { List, ListItem } from './list';
import { TeamTableMenu } from './team-table-menu';
import styles from './team-placement.module.css';

export function TeamEntry({ team, index }) {
  const { tournament } = useTournamentAdmin();
  const { stage, placementsByStage, standingsByStage } = useTeamPlacement();

  // pretend you didn't see this
  const teamInfo = useMemo(
    () =>
      tournament?.brackets
        ?.filter(b => b.id !== stage.id)
        ?.map(
          stage =>
            (stage.isComplete &&
              standingsByStage[stage.id]
                ?.map((t, idx) => ({ ...t, place: idx + 1 }))
                ?.filter(t => t.id === team.id)
                ?.map(t => (
                  <span>
                    {ordinal(t.place)} place in <b>{stage.name}</b>
                  </span>
                ))
                ?.shift()) ||
            placementsByStage[stage.id]
              ?.filter(t => t.team === team.id)
              ?.map(t => (
                <span>
                  Assigned to <b>{stage.name}</b>
                </span>
              ))
              ?.shift(),
        )
        ?.filter(i => i),
    [team, tournament?.brackets, placementsByStage, standingsByStage],
  );

  return (
    <div className={clsx(styles.teamEntry, { [styles.isAssigned]: teamInfo?.length > 0 })}>
      <div className={styles.teamName}>{team.name}</div>
      <div className={styles.teamInfo}>
        {teamInfo.map((row, idx) => (
          <div key={idx} className={styles.teamInfoRow}>
            {row}
          </div>
        ))}
      </div>
    </div>
  );
}

export function AddTeamButton({ team }) {
  const { setPlacements, setChanged } = useTeamPlacement();
  const placement = {
    placeType: PlaceType.Team,
    placeId: parseInt(team.id),
    name: team.name,
    key: `${PlaceType.Team}-${team.id}`,
  };

  const onClick = () => {
    setPlacements(val => [...val, placement]);
    setChanged(true);
  };

  return (
    <IconButton onClick={onClick} aria-label="Add to stage">
      <Icon path={mdiPlusCircleOutline} size={1} />
    </IconButton>
  );
}

export function TeamTable() {
  const { assignableTeams } = useTeamPlacement();
  if (assignableTeams.length === 0) return null;

  return (
    <List id="teams" name="Teams" icons={<TeamTableMenu />}>
      {assignableTeams.map((team, idx) => (
        <ListItem
          name={<TeamEntry team={team} />}
          key={team.id}
          id={`${PlaceType.Team}-${team.id}`}
          index={idx}
          draggable={false}
          buttons={<AddTeamButton team={team} />}
        />
      ))}
    </List>
  );
}
