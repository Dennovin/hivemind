import React from 'react';
import { Button } from '@material-ui/core';

import { useTeamPlacement } from './context';

export function SaveButton() {
  const { changed, saving, save } = useTeamPlacement();

  if (!changed) return null;

  return (
    <Button variant="contained" color="secondary" onClick={save}>
      Save
    </Button>
  );
}
