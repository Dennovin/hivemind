export const PlaceType = {
  Team: 'team',
  PrevStage: 'prevStage',
};

export const Tiebreakers = {
  GameWins: 'game_wins',
  MatchWins: 'match_wins',
  HeadToHead: 'head_to_head',
};
