import React, { createContext, useContext, useEffect, useState, useMemo } from 'react';
import { CircularProgress } from '@material-ui/core';
import { DragDropContext } from 'react-beautiful-dnd';
import { toast } from 'react-toastify';
import ordinal from 'ordinal';

import { useTournamentAdmin } from '../context';
import { PlaceType } from './constants';
import { getAxios } from '@/util/axios';

export const TeamPlacementContext = createContext({});

export function TeamPlacementProvider({ children }) {
  const { tournament, teamsById } = useTournamentAdmin();
  const [stage, setStage] = useState(null);
  const [placementsByStage, setPlacementsByStage] = useState({});
  const [standingsByStage, setStandingsByStage] = useState({});
  const [placements, setPlacements] = useState(null);
  const [changed, setChanged] = useState(false);
  const [saving, setSaving] = useState(false);
  const axios = getAxios({ authenticated: true });

  useEffect(() => {
    if (tournament?.brackets?.length > 0) {
      for (const bracket of tournament.brackets) {
        axios
          .getAllPages(`/api/tournament/stage-placement/`, {
            params: { destBracketId: bracket.id },
          })
          .then(response =>
            setPlacementsByStage(v => ({
              ...v,
              [bracket.id]: response.sort((a, b) => a.destPlace - b.destPlace),
            })),
          );

        axios
          .get(`/api/tournament/bracket/${bracket.id}/standings/`)
          .then(response =>
            setStandingsByStage(v => ({ ...v, [bracket.id]: response.data.standings })),
          );
      }
    }
  }, [tournament?.brackets]);

  useEffect(() => {
    setPlacements([]);
    setChanged(false);

    if (stage?.id) {
      axios
        .getAllPages(`/api/tournament/stage-placement/`, { params: { destBracketId: stage.id } })
        .then(response => {
          const stagePlacements = [];
          for (const [idx, row] of response.sort((a, b) => a.destPlace - b.destPlace).entries()) {
            if (row.team) {
              addTeamToStage({ stagePlacements, teamId: row.team, placeIdx: idx });
            } else {
              addPrevStagePlaceToStage({
                stagePlacements,
                placeId: `${row.srcBracket}.${row.srcPlace}`,
              });
            }
          }

          setPlacements(stagePlacements);
        });
      return () => setPlacements(null);
    }
  }, [stage?.id]);

  const assignableTeams = useMemo(() => {
    if (placements) {
      const assignedSet = new Set(placements.map(t => t.key));
      return tournament.teams.filter(t => !assignedSet.has(`${PlaceType.Team}-${t.id}`));
    }

    return [];
  }, [tournament?.teams, placements]);

  const addTeamToStage = ({ stagePlacements, teamId, placeIdx }) => {
    const placement = {
      placeType: PlaceType.Team,
      placeId: parseInt(teamId),
      name: teamsById[parseInt(teamId)].name,
      key: `${PlaceType.Team}-${teamId}`,
    };

    stagePlacements.splice(placeIdx ?? stagePlacements.length, 0, placement);
  };

  const addPrevStagePlaceToStage = ({ stagePlacements, placeId, placeIdx }) => {
    const [prevStageId, prevPlaceId] = placeId.split('.');
    const prevStage = tournament.brackets.filter(i => i.id === parseInt(prevStageId)).shift();
    if (!prevStage) return null;

    const prevPlace = `${ordinal(parseInt(prevPlaceId))} Place`;
    const name = `${prevStage.name} - ${prevPlace}`;

    const placement = {
      placeType: PlaceType.PrevStage,
      srcBracket: parseInt(prevStageId),
      srcPlace: parseInt(prevPlaceId),
      placeId,
      name,
      key: `${PlaceType.PrevStage}-${placeId}`,
    };

    stagePlacements.splice(placeIdx ?? stagePlacements.length, 0, placement);
  };

  const removeFromStage = ({ stagePlacements, placeIdx }) => {
    stagePlacements.splice(placeIdx, 1);
  };

  const onDragEnd = result => {
    setChanged(true);

    const [placeType, placeId] = result.draggableId.split('-');
    const [srcType, srcId] = result.source.droppableId.split('-');

    setPlacements(stagePlacements => {
      if (srcType === 'stage') {
        removeFromStage({ stagePlacements, placeIdx: result.source.index });
      }

      if (!result.destination) return [...stagePlacements];
      const [destType, destId] = result.destination.droppableId.split('-');

      if (destType === 'stage') {
        if (placeType === PlaceType.Team) {
          addTeamToStage({ stagePlacements, teamId: placeId, placeIdx: result.destination.index });
        }

        if (placeType === PlaceType.PrevStage) {
          addPrevStagePlaceToStage({
            stagePlacements,
            placeId,
            placeIdx: result.destination.index,
          });
        }
      }

      return [...stagePlacements];
    });
  };

  const save = async () => {
    if (saving) return;
    setSaving(true);

    const existingData = await axios.getAllPages(`/api/tournament/stage-placement/`, {
      params: { destBracketId: stage.id },
    });

    await Promise.all(
      existingData.map(row => axios.delete(`/api/tournament/stage-placement/${row.id}/`)),
    );

    const newStagePlacements = placements.map((place, idx) => ({
      destBracket: stage.id,
      destPlace: idx + 1,
      team: place.placeType === PlaceType.Team ? place.placeId : null,
      srcBracket: place.placeType === PlaceType.PrevStage ? place.srcBracket : null,
      srcPlace: place.placeType === PlaceType.PrevStage ? place.srcPlace : null,
    }));

    setPlacementsByStage(v => ({ ...v, [stage.id]: newStagePlacements }));

    await Promise.all(
      newStagePlacements.map(place => axios.post(`/api/tournament/stage-placement/`, place)),
    );

    await axios.put(`/api/tournament/bracket/${stage.id}/create-matches/`, {});

    setChanged(false);
    setSaving(false);
    toast.success('Placements saved.');
  };

  const ctx = {
    stage,
    setStage,
    placements,
    setPlacements,
    placementsByStage,
    standingsByStage,
    assignableTeams,
    changed,
    setChanged,
    save,
    saving,
  };

  if (placements === null) return <CircularProgress />;

  return (
    <TeamPlacementContext.Provider value={ctx}>
      <DragDropContext onDragEnd={onDragEnd}>{children}</DragDropContext>
    </TeamPlacementContext.Provider>
  );
}

export function useTeamPlacement() {
  return useContext(TeamPlacementContext);
}
