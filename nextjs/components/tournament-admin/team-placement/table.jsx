import React from 'react';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';

import styles from './team-placement.module.css';
import { useTeamPlacement } from './context';
import { TeamTable } from './team-table';
import { PreviousStages } from './previous-stages';
import { StageTable } from './stage-table';

export function TeamPlacementTable() {
  const { stage } = useTeamPlacement();
  if (!stage) return null;

  return (
    <div className={styles.container}>
      <div className={styles.column}>
        <TeamTable />
        <PreviousStages />
      </div>
      <div className={styles.column}>
        <StageTable />
      </div>
    </div>
  );
}
