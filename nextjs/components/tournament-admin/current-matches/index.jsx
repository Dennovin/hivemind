import React from 'react';
import {
  Breadcrumbs,
  Button,
  CircularProgress,
  Grid,
  IconButton,
  Link,
  Typography,
} from '@material-ui/core';
import SettingsIcon from '@material-ui/icons/Settings';

import TournamentAdminCabinet from '@/components/TournamentAdminCabinet';
import { useTournamentAdmin } from '../context';
import { Subtitle } from '../subtitle';

export function CurrentMatches() {
  const { tournament, cabinets, reloadTournament, hideInactiveCabs, setHideInactiveCabs } =
    useTournamentAdmin();
  if (tournament === null || cabinets === null) {
    return <CircularProgress />;
  }

  return (
    <>
      <Subtitle title="Manage Current Matches" />
      <div className="space-y-0 py-4 px-4 pb-2 bg-gray-100 rounded-lg">
        <div className="flex justify-between items-center">
          <Typography variant="h2" className="m-0">
            Cabinets
          </Typography>
          <Button variant="outlined" color="default" onClick={() => setHideInactiveCabs(i => !i)}>
            {hideInactiveCabs ? 'Show' : 'Hide'} Inactive Cabinets
          </Button>
        </div>
        <Grid container direction="row" spacing={4}>
          {cabinets.map(cabinet => (
            <TournamentAdminCabinet key={cabinet.id} cabinet={cabinet} tournament={tournament} />
          ))}
        </Grid>
      </div>
      <Grid container direction="row" spacing={4}>
        <Grid item xs={12}></Grid>
      </Grid>
    </>
  );
}
