import React, { createContext, useContext, useState, useEffect, useMemo } from 'react';
import { useRouter } from 'next/router';
import { toast } from 'react-toastify';

import { getAxios } from '@/util/axios';
import { DRAFT_ORDER } from './constants';
import { useTournamentAdmin } from '../context';

export const DraftContext = createContext({});

export function DraftProvider({ children }) {
  const axios = getAxios({ authenticated: true });
  const { tournament } = useTournamentAdmin();
  const [draftOrder, setDraftOrder] = useState(DRAFT_ORDER.STRAIGHT);
  const [teams, setTeams] = useState([]);
  const [freeAgents, setFreeAgents] = useState([]);
  const [currentIdx, setCurrentIdx] = useState(null);
  const [selectedRound, setSelectedRound] = useState(0);
  const router = useRouter();

  const finalizeDraft = () => {
    axios.put(`/api/tournament/tournament/${tournament.id}/finalize-draft/`, { teams }).then(() => {
      toast.success('Teams created.');
      router.push(`/tournament/${tournament.id}/admin/teams`);
    });
  };

  const clearPicks = () => {
    setTeams(v => v.map(t => ({ ...t, selections: [] })));
    setCurrentIdx(0);
    setSelectedRound(0);
  };

  const saveStatus = () => {
    axios.put(`/api/tournament/tournament/${tournament.id}/draft-status/`, {
      draftOrder,
      teams,
      currentIdx,
      selectedRound,
    });
  };

  useEffect(saveStatus, [draftOrder, teams, currentIdx, selectedRound]);

  const getRoundOrder = roundNum => {
    if (draftOrder === DRAFT_ORDER.STRAIGHT) return 1;
    if (draftOrder === DRAFT_ORDER.SERPENTINE) return roundNum === 2 || roundNum === 4 ? -1 : 1;
    if (draftOrder === DRAFT_ORDER.REVERSE3) return roundNum === 3 ? -1 : 1;
  };

  const currentTeam = useMemo(
    () => teams.toSorted((a, b) => a.order - b.order)[currentIdx],
    [teams, currentIdx],
  );

  const addTeam = () => {
    setTeams(v => [
      ...v,
      {
        name: `Team ${v.length + 1}`,
        selections: [],
        id: v.length,
        order: v.length,
      },
    ]);
  };

  const removeTeam = id => {
    setTeams(v => v.filter(t => t.id !== id));
  };

  const selectNext = () => {
    const nextIdx = currentIdx + getRoundOrder(selectedRound);
    if (nextIdx < 0 || nextIdx >= teams.length) {
      if (selectedRound < 4) {
        const firstIdxNextRound = getRoundOrder(selectedRound + 1) === 1 ? 0 : teams.length - 1;
        setCurrentIdx(firstIdxNextRound);
        setSelectedRound(selectedRound + 1);
      }

      return;
    }

    setCurrentIdx(nextIdx);
  };

  const selectPlayer = player => {
    setTeams(v =>
      v.map(team => {
        if (team.id !== currentTeam.id) return team;
        team.selections[selectedRound] = player?.id ? player : null;
        return { ...team };
      }),
    );

    if (player?.id) {
      selectNext();
    }
  };

  useEffect(() => {
    axios.get(`/api/tournament/tournament/${tournament.id}/draft-status/`).then(response => {
      setDraftOrder(response.data.draftOrder);
      setTeams(response.data.teams);
      setCurrentIdx(response.data.currentIdx);
      setSelectedRound(response.data.selectedRound);
    });

    axios
      .getAllPages(`/api/tournament/player/`, {
        params: {
          tournamentId: tournament.id,
        },
      })
      .then(response => {
        const players = response.filter(p => !p.team);
        setFreeAgents(players);
      });
  }, []);

  const ctx = {
    draftOrder,
    setDraftOrder,
    teams,
    setTeams,
    freeAgents,
    setFreeAgents,
    currentTeam,
    currentIdx,
    setCurrentIdx,
    selectPlayer,
    selectedRound,
    setSelectedRound,
    finalizeDraft,
    clearPicks,
    addTeam,
    removeTeam,
  };

  return <DraftContext.Provider value={ctx}>{children}</DraftContext.Provider>;
}

export function useDraft() {
  return useContext(DraftContext);
}
