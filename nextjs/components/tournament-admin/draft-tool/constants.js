export const DRAFT_ORDER = {
  STRAIGHT: 'straight',
  SERPENTINE: 'serpentine',
  REVERSE3: 'reverse3',
};
