import React from 'react';
import { Select, MenuItem } from '@material-ui/core';

import { useDraft } from './context';
import { DRAFT_ORDER } from './constants';
import styles from './draft-tool.module.css';

export function OrderSelector() {
  const { draftOrder, setDraftOrder } = useDraft();

  const onChange = evt => {
    setDraftOrder(evt.target.value);
  };

  return (
    <Select className={styles.draftOrderSelect} value={draftOrder} onChange={onChange}>
      <MenuItem value={DRAFT_ORDER.STRAIGHT}>Straight</MenuItem>
      <MenuItem value={DRAFT_ORDER.SERPENTINE}>Serpentine</MenuItem>
      <MenuItem value={DRAFT_ORDER.REVERSE3}>Reverse Round 3 (Camp KQ)</MenuItem>
    </Select>
  );
}
