import React from 'react';
import { Select, MenuItem } from '@material-ui/core';

import { useDraft } from './context';
import styles from './draft-tool.module.css';

export function RoundSelector() {
  const { selectedRound, setSelectedRound } = useDraft();

  const onChange = evt => {
    setSelectedRound(evt.target.value);
  };

  return (
    <Select className={styles.roundSelect} value={selectedRound} onChange={onChange}>
      <MenuItem value={0}>Captains</MenuItem>
      <MenuItem value={1}>Round 1</MenuItem>
      <MenuItem value={2}>Round 2</MenuItem>
      <MenuItem value={3}>Round 3</MenuItem>
      <MenuItem value={4}>Round 4</MenuItem>
    </Select>
  );
}
