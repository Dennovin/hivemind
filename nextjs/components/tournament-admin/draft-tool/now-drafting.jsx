import { useDraft } from './context';
import styles from './draft-tool.module.css';

export function NowDrafting() {
  const { currentTeam } = useDraft();

  return (
    <div className={styles.nowDrafting}>
      <div className={styles.caption}>Now Drafting</div>
      <div className={styles.teamName}>{currentTeam?.name ?? 'Unknown Team'}</div>
    </div>
  );
}
