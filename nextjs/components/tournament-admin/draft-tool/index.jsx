import React from 'react';
import { Button } from '@material-ui/core';

import { Subtitle } from '../subtitle';
import { DraftProvider } from './context';
import { OrderSelector } from './order-selector';
import { NowDrafting } from './now-drafting';
import { TeamList } from './team-list';
import { FreeAgentList } from './free-agent-list';
import { ClearPicksButton } from './clear-picks-button';
import { FinalizeDraftButton } from './finalize-draft-button';
import styles from './draft-tool.module.css';

export function DraftTool() {
  return (
    <DraftProvider>
      <div className={styles.container}>
        <Subtitle title="Draft Tool" />

        <div className={styles.header}>
          <div className={styles.draftOrderContainer}>
            Draft Order:
            <OrderSelector />
          </div>

          <div className={styles.buttons}>
            <ClearPicksButton />
            <FinalizeDraftButton />
          </div>
        </div>

        <div className={styles.body}>
          <div className={styles.left}>
            <NowDrafting />
            <TeamList />
          </div>

          <div className={styles.right}>
            <FreeAgentList />
          </div>
        </div>
      </div>
    </DraftProvider>
  );
}
