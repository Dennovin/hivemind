import React, { useMemo } from 'react';
import clsx from 'clsx';

import { useDraft } from './context';
import styles from './draft-tool.module.css';

export function FreeAgent({ player, className }) {
  const { selectPlayer } = useDraft();

  return (
    <div className={clsx(styles.player, className)} onClick={() => selectPlayer(player)}>
      {player.name}
    </div>
  );
}

export function FreeAgentList() {
  const { freeAgents, teams, currentTeam, selectedRound } = useDraft();
  const unselected = useMemo(() => {
    const selected = new Set();
    for (const team of teams) {
      for (const player of team.selections) {
        if (player?.id) {
          selected.add(player.id);
        }
      }
    }

    return freeAgents.filter(p => !selected.has(p.id));
  }, [freeAgents, teams]);

  return (
    <div className={styles.freeAgents}>
      {currentTeam?.selections?.[selectedRound] && (
        <FreeAgent className="clearSelection" player={{ name: 'Clear Selection', id: null }} />
      )}
      {unselected.map(player => (
        <FreeAgent player={player} key={player.id} />
      ))}
    </div>
  );
}
