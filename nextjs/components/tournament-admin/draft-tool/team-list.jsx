import React, { useState } from 'react';
import { IconButton, TextField } from '@material-ui/core';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import { mdiDragVertical, mdiDelete } from '@mdi/js';
import Icon from '@mdi/react';
import clsx from 'clsx';

import { useDraft } from './context';
import { RoundSelector } from './round-selector';
import { AddTeamButton } from './add-team-button';
import styles from './draft-tool.module.css';

export function TeamName({ team }) {
  const { setTeams } = useDraft();

  const onChange = evt => {
    setTeams(v =>
      v.map(t => {
        if (t.id === team.id) {
          return { ...t, name: evt.target.value };
        }

        return t;
      }),
    );
  };

  return <TextField value={team.name} onChange={onChange} />;
}

export function Team({ team, idx }) {
  const { selectedRound, currentTeam, setCurrentIdx, removeTeam } = useDraft();

  const roundSelection =
    team.selections.length > selectedRound ? team.selections[selectedRound] : null;

  return (
    <Draggable draggableId={`${team.id}`} index={idx}>
      {(provided, snapshot) => (
        <div
          ref={provided.innerRef}
          className={clsx(styles.team, { active: currentTeam?.id === team.id })}
          {...provided.draggableProps}
        >
          <div className={styles.dragHandle} {...provided.dragHandleProps}>
            <Icon path={mdiDragVertical} size={1} />
          </div>
          <div className={styles.name}>
            <TeamName team={team} />
          </div>
          <div className={styles.selection} onClick={() => setCurrentIdx(idx)}>
            {roundSelection?.name}
          </div>
          <div className={styles.deleteButton}>
            <Icon path={mdiDelete} size={0.8} onClick={() => removeTeam(team.id)} />
          </div>
        </div>
      )}
    </Draggable>
  );
}

export function TeamList() {
  const { teams, setTeams } = useDraft();

  const onDragEnd = result => {
    const team = teams.filter(t => t.id === parseInt(result.draggableId)).pop();

    setTeams(v =>
      v.map(t => {
        if (t.id === team.id) {
          return { ...t, order: result.destination.index };
        }

        if (t.order > team.order && t.order <= result.destination.index) {
          return { ...t, order: t.order - 1 };
        }

        if (t.order >= result.destination.index && t.order < team.order) {
          return { ...t, order: t.order + 1 };
        }

        return { ...t };
      }),
    );
  };

  return (
    <DragDropContext onDragEnd={onDragEnd}>
      <div className={styles.teamList}>
        <div className={styles.teamListHeader}>
          <RoundSelector />
          <AddTeamButton />
        </div>

        <Droppable droppableId="draft-teams-droppable">
          {(provided, snapshot) => (
            <div ref={provided.innerRef}>
              {teams
                .toSorted((a, b) => a.order - b.order)
                .map((team, idx) => (
                  <Team key={team.id} team={team} idx={idx} />
                ))}
              {provided.placeholder}
            </div>
          )}
        </Droppable>
      </div>
    </DragDropContext>
  );
}
