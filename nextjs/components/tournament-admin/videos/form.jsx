import React, { useEffect, useState } from 'react';
import { MenuItem, Typography } from '@material-ui/core';
import { parseISO, sub } from 'date-fns';

import Form from '@/components/forms/Form';
import Field, { TextField, Select } from '@/components/fields/Field';
import { isAdminOf } from '@/util/auth';
import { getAxios } from '@/util/axios';
import { useTournamentAdmin } from '../context';

export function VideoForm() {
  const axios = getAxios({ authenticated: true });
  const [cabinets, setCabinets] = useState(null);
  const [gameEndTime, setGameEndTime] = useState(null);
  const { tournament } = useTournamentAdmin();

  useEffect(() => {
    axios
      .getAllPages('/api/game/cabinet/', { params: { sceneId: tournament?.scene?.id } })
      .then(setCabinets);
  }, [tournament.scene]);

  const postdata = {
    game: '',
    tournament: tournament.id,
    videoId: '',
    gameEndTime: '',
    length: '',
    referencePoint: 'postgame',
  };

  const props = {
    buttonText: 'Add Video',
    buttonColor: 'default',
    dialogTitle: 'Adding Video',
    canEdit: () => isAdminOf(tournament.scene.id),
    object: postdata,
    method: 'POST',
    url: '/api/tournament/video/add/',
  };

  return (
    <Form {...props}>
      <Field name="videoId" label="YouTube Video ID" component={TextField} />
      <Field name="length" label="Video Length" component={TextField} />

      <Typography>
        Find any postgame screen from the video. Enter the Game ID and the timestamp in the video
        when that screen appeared.
      </Typography>

      <Field name="game" label="Game ID" component={TextField} />
      <Field name="gameEndTime" label="Video Time at Reference Point" component={TextField} />

      <Field name="referencePoint" label="Reference Point" component={Select}>
        <MenuItem value="start">Game Start (end of countdown)</MenuItem>
        <MenuItem value="end">Game End (start of victory screen)</MenuItem>
        <MenuItem value="postgame">Postgame Screen Appears</MenuItem>
      </Field>
    </Form>
  );
}
