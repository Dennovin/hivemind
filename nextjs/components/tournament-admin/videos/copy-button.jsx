import React, { useState } from 'react';
import { IconButton } from '@material-ui/core';
import Icon from '@mdi/react';
import { mdiContentCopy } from '@mdi/js';
import { toast } from 'react-toastify';

import { getAxios } from '@/util/axios';

export function CopyButton({ video }) {
  const axios = getAxios();
  const [loading, setLoading] = useState(false);
  const copyTimestamps = () => {
    setLoading(true);
    axios.get(`/api/tournament/video/${video.id}/timestamps/`).then(response => {
      navigator.clipboard.writeText(response.data.timestamps);
      setLoading(false);
      toast.success('Copied video timestamps to clipboard.', { autoClose: 1500 });
    });
  };

  return (
    <IconButton onClick={copyTimestamps} disabled={loading}>
      <Icon size={1} path={mdiContentCopy} />
    </IconButton>
  );
}
