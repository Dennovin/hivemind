import React, { useState, useEffect } from 'react';

import { getAxios } from '@/util/axios';
import { useTournamentAdmin } from '../context';
import { VideosTable } from './table';

export function Videos() {
  const { tournament } = useTournamentAdmin();
  const [videos, setVideos] = useState(null);
  const axios = getAxios({ authenticated: true });

  const sortFunction = (a, b) =>
    a.cabinetName.localeCompare(b.cabinetName) || new Date(a.startTime) - new Date(b.startTime);

  useEffect(() => {
    if (!tournament?.id || videos !== null) return;

    axios
      .getAllPages(`/api/tournament/video/`, { params: { tournamentId: tournament.id } })
      .then(response => {
        setVideos(response.sort(sortFunction));
      });
  }, [tournament?.id]);

  return <VideosTable {...{ videos, setVideos }} />;
}
