import React from 'react';
import { format } from 'date-fns';

import styles from './Videos.module.css';
import Table, { TableRow, TableCell } from '@/components/tables/Table';
import DeleteButton from '@/components/DeleteButton';
import { getAxios } from '@/util/axios';
import { VideoForm } from './form';
import { CopyButton } from './copy-button';

export function VideoRow({ video, setVideos }) {
  const axios = getAxios({ authenticated: true });

  const onConfirmDelete = () => {
    axios
      .delete(`/api/tournament/video/${video.id}/`)
      .then(setVideos(val => val.filter(v => v.id !== video.id)));
  };

  return (
    <TableRow>
      <TableCell>{video.videoId}</TableCell>
      <TableCell>{video.cabinetName}</TableCell>
      <TableCell>{format(new Date(video.startTime), 'MMM d - hh:mm aa')}</TableCell>
      <TableCell>{video.length}</TableCell>
      <TableCell>
        <CopyButton video={video} />
      </TableCell>
      <TableCell>
        <DeleteButton onConfirm={onConfirmDelete} confirmText="Delete this video?" />
      </TableCell>
    </TableRow>
  );
}

export function VideosTable({ videos, setVideos }) {
  const props = {
    title: 'Tournament Videos',
    columnHeaders: ['ID', 'Cabinet', 'Start Time', 'Length', 'Copy Timestamps', ''],
    cellClassNames: [null, null, null, null, null, styles.deleteCell],
    isLoading: videos === null,
    titleButton: <VideoForm />,
  };

  return (
    <Table {...props}>
      {videos?.map(video => (
        <VideoRow key={video.id} {...{ video, setVideos }} />
      ))}
    </Table>
  );
}
