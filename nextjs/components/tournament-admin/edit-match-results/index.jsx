import React from 'react';

import TournamentMatchListTable from '@/components/tables/TournamentMatchListTable';
import { useTournamentAdmin } from '../context';

export function EditMatchResults() {
  const { tournament } = useTournamentAdmin();

  return <TournamentMatchListTable title="Match Results" editable={true} tournament={tournament} />;
}
