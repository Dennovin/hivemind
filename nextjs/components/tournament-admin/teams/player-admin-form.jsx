import React, { useState } from 'react';
import { Button } from '@material-ui/core';
import * as Yup from 'yup';

import Field, {
  ImageUploadField,
  Switch,
  TextField,
  imageURLRegex,
} from '@/components/fields/Field';
import { SceneSelect } from '@/components/fields/SceneSelect';
import { getAxios } from '@/util/axios';
import { TEAM_TYPE } from 'util/constants';
import Form from '@/components/forms/Form';
import RegisterTeamForm from '@/components/forms/RegisterTeamForm';
import { useTournamentAdmin } from '../context';

const PlayerSchema = Yup.object().shape({});

export default function TournamentPlayerAdminForm({
  player,
  team,
  tournament: tournamentProp,
  ...props
}) {
  const axios = getAxios({ authenticated: true });
  const [changeTeam, setChangeTeam] = useState(false);
  const { tournament: tournamentContext } = useTournamentAdmin();
  const tournament = tournamentProp || tournamentContext;

  const formProps = {
    url: `/api/tournament/player/${player.id}/`,
    method: 'PATCH',
    object: player,
    validationSchema: PlayerSchema,

    getPostData: {
      image: async data => (imageURLRegex.test(data?.image) ? undefined : data?.image),
      team: async data => {
        let team = data?.team;
        if (data?.teamType === TEAM_TYPE.CREATE_TEAM) {
          const newTeam = { tournament: tournament.id, name: data?.teamName };
          const response = await axios.post('/api/tournament/team/', newTeam);
          team = response.data.id;
        }
        return data?.teamType === TEAM_TYPE.FREE_AGENT ? null : team;
      },
    },
    ...props,
  };

  return (
    <Form {...formProps}>
      <div className="grid md:grid-cols-2 gap-6">
        <div className="grid gap-2">
          <Field name="name" label="Player Name" component={TextField} noGrid />

          <SceneSelect name="scene" label="Player Home Scene" noGrid />
        </div>
        <div>
          <Field name="image" label="Picture" component={ImageUploadField} noGrid />
        </div>
      </div>
      <div className="flex gap-6 justify-between items-start">
        <Field
          name="pronouns"
          label="Pronouns"
          component={TextField}
          noGrid
          className="basis-1/4"
        />
        <Field
          name="tidbit"
          label="Interesting tidbit"
          component={TextField}
          className="flex-grow"
          noGrid
        />
      </div>

      {tournament && (
        <section id="team-info" className="mt-8">
          {!changeTeam && (
            <div className="flex justify-between items-center mb-6">
              <h3 className="my-0">Team</h3>
              <Button
                variant="outlined"
                color="secondary"
                onClick={() => setChangeTeam(true)}
                className="inline-block"
              >
                Change Team Selection
              </Button>
            </div>
          )}

          {player?.id && player?.team && (
            <div className="text-xl text-center font-bold p-12 rounded bg-gradient-to-br from-blue-light5 to-blue-light3 mb-4">
              {team?.name}
            </div>
          )}

          {!changeTeam ? (
            <div>
              {!player?.team && (
                <div className="text-lg text-center p-12 rounded bg-gray-200 mb-4">
                  You are registered as a free agent.
                </div>
              )}
            </div>
          ) : (
            <RegisterTeamForm tournament={tournament} />
          )}
        </section>
      )}

      <section>
        <h3>Payment</h3>
        <Field name="paid" label="Mark Player as Paid" component={Switch} noGrid />
      </section>
    </Form>
  );
}
