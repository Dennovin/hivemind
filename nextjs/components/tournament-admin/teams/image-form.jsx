import React, { useState } from 'react';
import Form from '@/components/forms/Form';
import Field, { ImageUploadField, imageURLRegex } from '@/components/fields/Field';

import styles from './image-form.module.css';
import { getAxios } from '@/util/axios';

export function ImageForm({ team, setTeams }) {
  const [image, setImage] = useState(team.image);

  const object = {
    image: team.image,
  };

  const formProps = {
    url: `/api/tournament/team/${team.id}/`,
    method: 'PATCH',
    object,
    buttonText: team.image ? 'Replace Team Image' : 'Add Team Image',
    reloadOnSave: false,
    onSave: result => {
      setImage(result.image);
      setTeams(v => v.map(t => (t.id === team.id ? { ...t, image: result.image } : t)));
    },
    image: async data => (imageURLRegex.test(data?.image) ? undefined : data?.image),
  };

  return (
    <div className={styles.imageFormRow}>
      {team.image && (
        <div className={styles.imageContainer}>
          <img src={image} className={styles.image} alt={`${team.name} team image`} />
        </div>
      )}

      <div className={styles.editButton}>
        <Form {...formProps}>
          <ImageUploadField
            name="image"
            label="Team Image"
            component={ImageUploadField}
            helperText="Team image/logo to be used on stream"
            noGrid
          />
        </Form>
      </div>
    </div>
  );
}
