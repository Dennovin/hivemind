import React, { useState, createContext, useContext } from 'react';
import Icon from '@mdi/react';
import { mdiCog, mdiDelete } from '@mdi/js';
import { Link, Menu as MuiMenu, MenuItem as MuiMenuItem, Divider } from '@material-ui/core';
import { bindMenu, bindTrigger, usePopupState } from 'material-ui-popup-state/hooks';
import { useRouter } from 'next/router';
import clsx from 'clsx';

import styles from './Menu.module.css';
import { useAuth } from '@/util/auth';
import { getAxios } from '@/util/axios';
import { DeleteConfirmDialog } from '@/components/DeleteButton';
import { PERMISSION_TYPES, TOURNAMENT_LINK_TYPES } from '@/util/constants';
import { useTournamentAdmin } from './context';

const MenuContext = createContext({});
const useMenuContext = () => useContext(MenuContext);

function MenuSubtitle({ children }) {
  return <MuiMenuItem className={styles.subtitle}>{children}</MuiMenuItem>;
}

function MenuItem({ href, icon, children, onClick, className }) {
  const router = useRouter();
  const { tournament } = useTournamentAdmin();
  const { menuState } = useMenuContext();

  const handleClick = evt => {
    evt.preventDefault();

    if (onClick) return onClick();
    router.push(`/tournament/${tournament.id}/admin/${href}`, null, { shallow: true });
    menuState.close();
  };

  return (
    <MuiMenuItem onClick={handleClick}>
      <Link
        className={clsx(styles.menuItem, className)}
        href={`/tournament/${tournament.id}/admin/${href}`}
      >
        {icon && <Icon className={styles.icon} path={icon} size={1} />}

        {children}
      </Link>
    </MuiMenuItem>
  );
}

export function Menu() {
  const router = useRouter();
  const axios = getAxios({ authenticated: true });
  const { hasPermission } = useAuth();
  const { tournament } = useTournamentAdmin();
  const [deleteConfirmOpen, setDeleteConfirmOpen] = useState(false);
  const { menuState } = useMenuContext();

  const openDeleteConfirm = () => {
    setDeleteConfirmOpen(true);
  };

  const handleClose = () => {
    setDeleteConfirmOpen(false);
  };

  const handleConfirm = () => {
    axios.delete(`/api/tournament/tournament/${tournament.id}/`).then(() => {
      router.push(`/scene/${tournament.scene.name}`);
    });
  };

  return (
    <>
      <MuiMenu
        className={styles.menu}
        getContentAnchorEl={null}
        anchorOrigin={{ vertical: 'top', horizontal: 'left' }}
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
        {...bindMenu(menuState)}
      >
        <MenuSubtitle>Officiate</MenuSubtitle>
        <MenuItem href="dashboard">Dashboard</MenuItem>
        <MenuItem href="current-matches">Current Matches</MenuItem>
        <MenuItem href="edit-match-results">Edit Match Results</MenuItem>
        <MenuItem href="match-queue">Match Queue</MenuItem>

        {hasPermission(tournament?.scene?.id, PERMISSION_TYPES.ADMIN) && [
          <MenuSubtitle>Organize</MenuSubtitle>,
          <MenuItem href="">Basic Settings</MenuItem>,
          <MenuItem href="templates">Templates</MenuItem>,
          <MenuItem href="configure-registration">Registration Form</MenuItem>,
          <MenuItem href="payment-processor">Payment Processor</MenuItem>,
          <MenuItem href="links">Links</MenuItem>,
          <MenuItem href="teams">Teams</MenuItem>,
          <MenuItem href="draft-tool">Draft Tool</MenuItem>,
          <MenuItem href="registered-list">Registered Players</MenuItem>,
          <MenuItem href="stages">Stages</MenuItem>,
          tournament.linkType == TOURNAMENT_LINK_TYPES.HIVEMIND && (
            <MenuItem href="team-placement">Team Placement</MenuItem>
          ),
          <MenuItem href="cabinets">Assigned Cabinets</MenuItem>,
          <MenuItem href="videos">Videos</MenuItem>,
          <Divider />,
          <MenuItem
            icon={mdiDelete}
            onClick={openDeleteConfirm}
            className={styles.deleteTournament}
          >
            Delete Tournament
          </MenuItem>,
        ]}
      </MuiMenu>
      <DeleteConfirmDialog
        confirmText="Are you sure you want to delete this tournament?"
        open={deleteConfirmOpen}
        {...{ handleClose, handleConfirm }}
      />
    </>
  );
}

export function MenuIcon() {
  const { hasAnyPermission } = useAuth();
  const { tournament } = useTournamentAdmin();
  const menuState = usePopupState({ popupId: 'tournament-admin-nav' });

  if (menuState === undefined) return null;
  if (
    !hasAnyPermission(tournament?.scene?.id, [PERMISSION_TYPES.ADMIN, PERMISSION_TYPES.TOURNAMENT])
  )
    return null;

  return (
    <MenuContext.Provider value={{ menuState }}>
      <Icon path={mdiCog} size={2} className={styles.menuIcon} {...bindTrigger(menuState)} />
      <Menu />
    </MenuContext.Provider>
  );
}
