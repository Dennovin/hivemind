import React, { useEffect, useState } from 'react';
import { Breadcrumbs, CircularProgress, FormControlLabel, Link, Switch } from '@material-ui/core';
import TournamentConfigureRegistrationForm, {
  AddRegistrationQuestionForm,
} from 'components/forms/TournamentConfigureRegistrationForm';
import { useRouter } from 'next/router';
import { DragDropContext, Droppable } from 'react-beautiful-dnd';
import { toast } from 'react-toastify';
import { getAxios } from 'util/axios';

import { Subtitle } from '../subtitle';
import { TournamentAdminProvider, useTournamentAdmin } from '../context';

const updateOrder = async (order, tournamentId) => {
  const axios = getAxios({ authenticated: true });

  const response = await axios.put(`/api/tournament/tournament/${tournamentId}/question-order/`, {
    order,
  });
};

const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

const RequirePhotoSwitch = ({ tournament, reloadTournament }) => {
  const axios = getAxios({ authenticated: true });
  const handlePhotoRequirementChange = async e => {
    const response = await axios.patch(`/api/tournament/tournament/${tournament.id}/`, {
      requirePlayerPhoto: e.target.checked,
    });
    if (response.status === 200) {
      reloadTournament();
      toast.success('Photo requirement updated', { autoClose: 1500 });
    }
    // I dont think this is properly handling errors
    if (response.error) toast.error(response.error);
  };
  return (
    <FormControlLabel
      label="Require Player Photo"
      control={
        <Switch
          name="requirePlayerPhoto"
          checked={tournament.requirePlayerPhoto}
          onChange={handlePhotoRequirementChange}
        />
      }
    />
  );
};

function ConfigureRegistration() {
  const { tournament, reloadTournament } = useTournamentAdmin();
  const axios = getAxios({ authenticated: true });
  const [questions, setQuestions] = useState(null);
  const [disableDrop, setDisableDrop] = useState(false);

  const router = useRouter();

  const loadQuestions = async () => {
    const response = await axios.get(
      `/api/tournament/player-info-field/?tournament_id=${tournament.id}`,
    );
    setQuestions(response.data.results);
  };

  useEffect(() => {
    loadQuestions();
  }, []);

  if (tournament === null || questions === null) {
    return <CircularProgress />;
  }

  const dragEnd = async ({ source, destination }) => {
    if (!destination) return;
    if (destination.index === source.index) return;
    setDisableDrop(true);
    const newOrder = reorder(questions, source.index, destination.index);
    setQuestions(newOrder);
    const orderIds = newOrder.map(q => q.id);
    await updateOrder(orderIds, tournament.id);
    await loadQuestions();
    setDisableDrop(false);
  };

  return (
    <>
      <Subtitle
        title="Registration Questions"
        buttons={
          <AddRegistrationQuestionForm
            key="new"
            title="Add new question"
            tournament={tournament}
            question={{ order: questions.length + 1 }}
            onSave={() => loadQuestions()}
            dialog={true}
            buttonText="Add New Question"
            buttonVariant="contained"
            dialogTitle="Add New Question"
          />
        }
      />
      <p>Forms by default will require a Name, email, and scene. </p>
      <div>
        <RequirePhotoSwitch tournament={tournament} reloadTournament={reloadTournament} />
      </div>
      <h2 className="text-2xl">Registration Questions</h2>
      <p>
        Add additional questions for the registration sign up. Form will always include name,
        pronouns, photo, team select, tidbit info, and optional payment. Use this form to add
        additional questions such as Covid agreements, T-Shirt size, volunteering, etc.
      </p>
      <DragDropContext onDragEnd={dragEnd}>
        <Droppable droppableId="list" isDropDisabled={disableDrop}>
          {(provided, snapsnot) => (
            <div
              className="grid grid-cols-1 gap-4"
              ref={provided.innerRef}
              {...provided.droppableProps}
            >
              {questions.map((question, idx) => (
                <TournamentConfigureRegistrationForm
                  key={question.id}
                  tournament={tournament}
                  question={question}
                  index={idx}
                  onSave={() => loadQuestions()}
                />
              ))}

              {provided.placeholder}
            </div>
          )}
        </Droppable>
      </DragDropContext>
    </>
  );
}

ConfigureRegistration.title = 'Configure Registration Form';
export { ConfigureRegistration };
