import React from 'react';
import { format } from 'date-fns';

import { useGet, useGetAllPages } from '@/util/api';
import { useTournamentAdmin } from '../context';
import { Subtitle } from '../subtitle';
import { StripeButton } from './stripe-button';
import { PreviousAccountSelector } from './previous-account-selector';
import styles from './PaymentProcessor.module.css';
import { mdiCheckCircle } from '@mdi/js';
import Icon from '@mdi/react';

export function PaymentProcessor() {
  const { tournament } = useTournamentAdmin();
  const { data: previousAccounts } = useGetAllPages(`/api/tournament/payment-account/`, {
    params: { scene: tournament?.scene?.id },
  });
  const { data: currentAccount } = useGet(
    `/api/tournament/tournament/${tournament.id}/payment-account/`,
  );

  return (
    <>
      <Subtitle title="Payment Processor" />

      {tournament?.stripeConfigured ? (
        <div>
          <div className={styles.successText}>
            <Icon path={mdiCheckCircle} className={styles.icon} size={1} />
            This tournament has been successfully linked to a Stripe account.
          </div>
          <div className={styles.successRow}>
            Account ID: <b>{currentAccount?.stripeAccountId}</b>
          </div>
          {currentAccount?.createdTimestamp && (
            <div className={styles.successRow}>
              Created:{' '}
              <b>{format(new Date(currentAccount?.createdTimestamp), 'MMM d, yyyy - hh:mm aa')}</b>
            </div>
          )}
        </div>
      ) : (
        <div>No payment processor has been configured for this tournament.</div>
      )}

      <div>
        To link a new account, click the "New Stripe Account" button. You will be redirected to
        Stripe to complete account setup.
      </div>

      {previousAccounts?.length > 0 && (
        <div>To use a previously linked Stripe account, select it from the menu below.</div>
      )}

      <div>
        <b>Note:</b> Beginning on January 1st, 2025, HiveMind will collect an application fee of 1%
        of all registration charges.
      </div>

      <StripeButton />

      {previousAccounts?.length > 0 && <PreviousAccountSelector accounts={previousAccounts} />}
    </>
  );
}
