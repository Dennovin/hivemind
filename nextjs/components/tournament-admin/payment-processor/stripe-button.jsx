import React, { useState } from 'react';
import { Button } from '@material-ui/core';
import { useRouter } from 'next/router';

import { getAxios } from '@/util/axios';
import { useTournamentAdmin } from '../context';

export function StripeButton() {
  const axios = getAxios({ authenticated: true });
  const [isLoading, setIsLoading] = useState(false);
  const router = useRouter();
  const { tournament } = useTournamentAdmin();

  const configureStripe = async () => {
    setIsLoading(true);
    axios.put(`/api/tournament/tournament/${tournament.id}/stripe/`).then(response => {
      setIsLoading(false);
      window.open(response.data.url, '_blank');
    });
  };

  return (
    <div className="">
      <Button variant="outlined" color="secondary" onClick={configureStripe} disabled={isLoading}>
        New Stripe Account
      </Button>
    </div>
  );
}
