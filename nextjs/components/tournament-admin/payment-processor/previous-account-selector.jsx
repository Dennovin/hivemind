import React, { useState } from 'react';
import { FormControl, InputLabel, Select, MenuItem, Button } from '@material-ui/core';
import { toast } from 'react-toastify';
import { format } from 'date-fns';

import { getAxios } from '@/util/axios';
import { useTournamentAdmin } from '../context';
import styles from './PaymentProcessor.module.css';

export function PreviousAccountSelector({ accounts }) {
  const [account, setAccount] = useState('');
  const { tournament, reloadTournament } = useTournamentAdmin();
  const axios = getAxios({ authenticated: true });

  const onChange = evt => setAccount(evt.target.value);
  const onClick = () => {
    if (!account) return;

    axios
      .put(`/api/tournament/tournament/${tournament.id}/update-stripe/`, {
        paymentAccountId: account,
      })
      .then(() => {
        reloadTournament();
        toast.success('Payment account updated', { autoClose: 1500 });
      });
  };

  return (
    <div className={styles.previousAccountSelector}>
      <FormControl>
        <InputLabel>Select a previously used account...</InputLabel>
        <Select className={styles.accountSelect} value={account} onChange={onChange}>
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          {accounts.map(acc => (
            <MenuItem key={acc.stripeAccountId} value={acc.id}>
              Stripe account {acc.stripeAccountId} - created{' '}
              {format(new Date(acc.createdTimestamp), 'MMM d, yyyy')}
            </MenuItem>
          ))}
        </Select>
      </FormControl>

      {account && (
        <Button variant="outlined" color="primary" onClick={onClick}>
          Use This Account
        </Button>
      )}
    </div>
  );
}
