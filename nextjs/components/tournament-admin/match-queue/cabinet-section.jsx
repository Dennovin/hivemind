import React, { useState, useMemo } from 'react';
import {
  Card,
  Grid,
  Menu,
  MenuItem,
  Switch,
  Tooltip,
  Typography,
  IconButton,
} from '@material-ui/core';
import { Droppable } from 'react-beautiful-dnd';
import { bindMenu, bindTrigger, usePopupState } from 'material-ui-popup-state/hooks';
import MenuIcon from '@material-ui/icons/Menu';
import clsx from 'clsx';

import styles from './MatchQueue.module.css';
import { getAxios } from '@/util/axios';
import TournamentRefereeButton from '@/components/TournamentRefereeButton';
import { useTournamentAdmin } from '../context';
import { TournamentQueueMatch } from './match';

export function ClearQueueMenu({ queue, menuState }) {
  const axios = getAxios({ authenticated: true });
  const { setQueue } = useTournamentAdmin();

  const clearQueue = () => {
    axios
      .put(`/api/tournament/queue/${queue.id}/set-queue/`, {
        matches: [],
      })
      .then(response => setQueue(response.data))
      .catch(err => toast.error('Could not empty queue.'));

    menuState.close();
  };

  return (
    <Menu
      getContentAnchorEl={null}
      anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
      transformOrigin={{ vertical: 'top', horizontal: 'center' }}
      className={styles.menu}
      {...bindMenu(menuState)}
    >
      <MenuItem className={styles.menuItem} onClick={() => clearQueue()}>
        Remove All from Queue
      </MenuItem>
    </Menu>
  );
}

export function TournamentQueueCabinetSection({ queue }) {
  const axios = getAxios({ authenticated: true });
  const { tournament, queues, cabinets, matches } = useTournamentAdmin();
  const [isLoading, setIsLoading] = useState(false);
  const menuState = usePopupState({ popupId: `clear-queue-menu-${queue.id}` });

  const cabinetId = queue.cabinets.length === 1 && queue.cabinets[0];
  const cabinet = cabinets.filter(c => c.id === cabinetId).shift();
  const activeMatch = cabinetId && matches?.filter(m => m.activeCabinet === cabinetId).shift();

  const visible = useMemo(() => {
    if (activeMatch || queue?.enabled || queue?.matchList?.length > 0) {
      return true;
    }

    if (tournament?.assignedCabinets?.length > 0 && cabinetId) {
      return tournament.assignedCabinets.includes(cabinetId);
    }

    return true;
  }, [
    tournament?.assignedCabinets,
    cabinetId,
    activeMatch,
    queue?.enabled,
    queue?.matchList?.length,
  ]);

  if (queue === undefined) return <></>;

  const toggleEnabled = () => {
    setIsLoading(true);
    axios.patch(`/api/tournament/queue/${queue.id}/`, { enabled: !queue.enabled }).then(() => {
      setIsLoading(false);
    });
  };

  if (!visible) return null;

  return (
    <Grid item>
      <Card className={styles.card}>
        <Grid container direction="column" className={styles.container}>
          <Grid item className={clsx(styles.row, styles.cabinetNameRow)}>
            <Typography className={styles.cabinetName}>
              {(queue.autoCreated && cabinet?.displayName) || queue.name}
            </Typography>
            <Grid item className={styles.queueIcons}>
              {cabinet && <TournamentRefereeButton cabinet={cabinet} />}

              <Tooltip title="Enable/Disable Queue" placement="top" arrow>
                <Switch
                  className={styles.queueSwitch}
                  disabled={isLoading}
                  checked={queue?.enabled ?? false}
                  onChange={toggleEnabled}
                />
              </Tooltip>

              <MenuIcon className={styles.menuIcon} {...bindTrigger(menuState)} />
              <ClearQueueMenu {...{ queue, menuState }} />
            </Grid>
          </Grid>

          {cabinet && (
            <Droppable
              droppableId={`cabinet-${cabinet.id}`}
              isDropDisabled={Boolean(activeMatch?.id)}
            >
              {(provided, snapshot) => (
                <Grid
                  item
                  container
                  direction="column"
                  className={styles.activeMatchContainer}
                  ref={provided.innerRef}
                >
                  <TournamentQueueMatch
                    match={activeMatch}
                    cabinet={cabinet}
                    index={0}
                    queueEnabled={queue?.enabled}
                    className={clsx(styles.row, styles.activeMatch, { active: activeMatch })}
                  />
                  {provided.placeholder}
                </Grid>
              )}
            </Droppable>
          )}

          <Droppable droppableId={`queue-${queue.id}`}>
            {(provided, snapshot) => (
              <Grid item className={styles.queuedMatches} ref={provided.innerRef}>
                {queue?.matchList?.map((match, idx) => (
                  <TournamentQueueMatch
                    key={match.id}
                    match={match}
                    index={idx}
                    className={clsx(styles.row, styles.queuedMatch)}
                  />
                ))}
                {provided.placeholder}
              </Grid>
            )}
          </Droppable>
        </Grid>
      </Card>
    </Grid>
  );
}
