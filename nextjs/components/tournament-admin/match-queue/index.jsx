import { useEffect } from 'react';
import { Breadcrumbs, Button, CircularProgress, Link, Grid, IconButton } from '@material-ui/core';
import SettingsIcon from '@material-ui/icons/Settings';

import { TournamentQueueContainer } from './container';
import { TournamentQueueCabinetSection } from './cabinet-section';
import { TournamentQueueMatchList } from './match-list';
import { useTournamentAdmin } from '../context';
import { Subtitle } from '../subtitle';

export function MatchQueue() {
  const { tournament, visibleQueues, loadAvailableMatches } = useTournamentAdmin();

  useEffect(() => {
    loadAvailableMatches();
  }, [tournament?.id]);

  if (tournament === null || visibleQueues === null) {
    return <CircularProgress />;
  }

  return (
    <>
      <Subtitle title="Match Queue" />

      <TournamentQueueContainer>
        <Grid container direction="row" spacing={2}>
          <Grid container item direction="column" spacing={2} xs={12} md={6}>
            {visibleQueues.map(queue => (
              <TournamentQueueCabinetSection key={queue.id} queue={queue} tournament={tournament} />
            ))}
          </Grid>

          <Grid container item direction="column" spacing={2} xs={12} md={6}>
            <TournamentQueueMatchList />
          </Grid>
        </Grid>
      </TournamentQueueContainer>
    </>
  );
}
