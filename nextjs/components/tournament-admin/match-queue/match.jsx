import { Grid, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Icon from '@mdi/react';
import { mdiSkipNext } from '@mdi/js';
import { Draggable } from 'react-beautiful-dnd';
import clsx from 'clsx';

import styles from './MatchQueue.module.css';
import { getAxios } from '@/util/axios';
import { useTournamentAdmin } from '../context';

export function TournamentQueueMatch({ match, cabinet, className, index, queueEnabled }) {
  const axios = getAxios({ authenticated: true });
  const { tournament, teamsById } = useTournamentAdmin();

  const selectNext = () => {
    axios.put(`/api/tournament/tournament/${tournament.id}/select-next/`, { cabinet: cabinet.id });
  };

  const getTeamName = teamId => (teamId ? teamsById[teamId].name : '(unknown team)');

  if (!match?.id) {
    return (
      <Grid
        item
        container
        direction="row"
        justifyContent="space-between"
        className={clsx(styles.row, styles.noMatch, className)}
      >
        No current match
        <Button
          onClick={selectNext}
          size="small"
          endIcon={<Icon path={mdiSkipNext} size={0.8} />}
          variant="outlined"
          disabled={!queueEnabled}
        >
          Next
        </Button>
      </Grid>
    );
  }

  return (
    <Draggable draggableId={`${match.id}`} index={index}>
      {(provided, snapshot) => (
        <Grid
          item
          ref={provided.innerRef}
          className={clsx(styles.row, className)}
          {...provided.draggableProps}
          {...provided.dragHandleProps}
        >
          {tournament?.brackets?.length > 1 && match.stageName} {match.roundName}:{' '}
          {getTeamName(match.blueTeam)} vs. {getTeamName(match.goldTeam)}
          {match.activeCabinet && (
            <>
              {' '}
              ({match.blueScore}-{match.goldScore})
            </>
          )}
        </Grid>
      )}
    </Draggable>
  );
}
