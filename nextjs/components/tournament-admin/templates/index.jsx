import React, { useEffect, useState } from 'react';
import { Button, CircularProgress, TextField, Select, MenuItem } from '@material-ui/core';
import { toast } from 'react-toastify';

import { getAxios } from '@/util/axios';
import { useTournamentAdmin } from '../context';
import { Subtitle } from '../subtitle';
import styles from './templates.module.css';

const fetchBase64 = async url => {
  const response = await fetch(url);
  const arraybuffer = await response.arrayBuffer();
  const data = Buffer.from(arraybuffer).toString('base64');
  return data;
};

function Templates() {
  const { tournament, reloadTournament } = useTournamentAdmin();
  const [templates, setTemplates] = useState(null);
  const [templateName, setTemplateName] = useState('');
  const [selectedTemplate, setSelectedTemplate] = useState('');
  const axios = getAxios({ authenticated: true });

  useEffect(() => {
    if (tournament?.scene) {
      axios
        .getAllPages('/api/tournament/template/', { params: { sceneId: tournament.scene.id } })
        .then(setTemplates);
    }
  }, [tournament?.scene]);

  const saveTemplate = async () => {
    const registrationQuestions = (
      await axios.getAllPages('/api/tournament/player-info-field/', {
        params: { tournamentId: tournament.id },
      })
    ).map(v => ({
      choices: v.choices,
      fieldDescription: v.fieldDescription,
      fieldName: v.fieldName,
      fieldSlug: v.fieldSlug,
      fieldType: v.fieldType,
      isRequired: v.isRequired,
      order: v.order,
    }));

    const links = (
      await axios.getAllPages('/api/tournament/links/', {
        params: { tournament: tournament.id },
      })
    ).map(v => ({
      image: v.image,
      order: v.order,
      title: v.title,
      url: v.url,
    }));

    const paymentAccount = await axios.get(
      `/api/tournament/tournament/${tournament.id}/payment-account/`,
    );

    const brackets = tournament.brackets.map(v => ({
      addTiebreaker: v.addTiebreaker,
      autoWarmup: v.autoWarmup,
      matchesPerOpponent: v.matchesPerOpponent,
      name: v.name,
      queueTimer: v.queueTimer,
      randomizeSides: v.randomizeSides,
      roundsPerSet: v.roundsPerSet,
      stageTiebreakers: v.stageTiebreakers,
      stageType: v.stageType,
      thirdPlaceMatch: v.thirdPlaceMatch,
      winsPerMatch: v.winsPerMatch,
    }));

    const templateData = {
      description: tournament.description,
      allowRegistration: tournament.allowRegistration,
      registrationBanner: tournament.registrationBanner,
      registrationMessage: tournament.registrationMessage,
      requirePlayerPhoto: tournament.requirePlayerPhoto,
      registrationQuestions,
      links,
      paymentAccount: paymentAccount?.data?.id,
      brackets,
      assignedCabinets: tournament.assignedCabinets,
    };

    const existing = templates.filter(t => t.name === templateName);
    if (existing?.[0]) {
      axios
        .patch(`/api/tournament/template/${existing[0].id}/`, { data: templateData })
        .then(response => {
          toast.success(`Template "${templateName}" saved.`);
        });
    } else {
      axios
        .post('/api/tournament/template/', {
          scene: tournament.scene.id,
          name: templateName,
          data: templateData,
        })
        .then(response => {
          toast.success(`Template "${templateName}" saved.`);
        });
    }
  };

  const loadTemplate = async () => {
    const template = templates.filter(v => v.id === selectedTemplate).shift();
    if (!template) return;
    const data = template.data;

    await axios.patch(`/api/tournament/tournament/${tournament.id}/`, {
      assignedCabinets: data.assignedCabinets,
      description: data.description,
      allowRegistration: data.allowRegistration,
      registrationMessage: data.registrationMessage,
      requirePlayerPhoto: data.requirePlayerPhoto,
    });

    if (data.registrationBanner) {
      const bannerImage = await fetchBase64(data.registrationBanner);
      await axios.patch(`/api/tournament/tournament/${tournament.id}/`, {
        registrationBanner: bannerImage,
      });
    }

    // registration questions
    if (data.registrationQuestions) {
      const existingQuestions = await axios.getAllPages(`/api/tournament/player-info-field/`, {
        params: { tournamentId: tournament.id },
      });

      for (const question of existingQuestions) {
        await axios.delete(`/api/tournament/player-info-field/${question.id}/`);
      }

      for (const question of data.registrationQuestions) {
        await axios.post(`/api/tournament/player-info-field/`, {
          tournament: tournament.id,
          choices: question.choices,
          fieldDescription: question.fieldDescription,
          fieldName: question.fieldName,
          fieldSlug: question.fieldSlug,
          fieldType: question.fieldType,
          isRequired: question.isRequired,
          order: question.order,
        });
      }
    }

    // links
    if (data.links) {
      const existingLinks = await axios.getAllPages(`/api/tournament/links/`, {
        params: { tournament: tournament.id },
      });

      for (const link of existingLinks) {
        await axios.delete(`/api/tournament/links/${link.id}/`);
      }

      for (const link of data.links) {
        const postdata = {
          tournament: tournament.id,
          order: link.order,
          title: link.title,
          url: link.url,
        };

        if (link.image) {
          postdata.image = await fetchBase64(link.image);
        }

        await axios.post(`/api/tournament/links/`, postdata);
      }
    }

    // stages
    for (const bracket of data.brackets) {
      await axios.post(`/api/tournament/bracket/`, {
        tournament: tournament.id,
        addTiebreaker: bracket.addTiebreaker,
        autoWarmup: bracket.autoWarmup,
        matchesPerOpponent: bracket.matchesPerOpponent,
        name: bracket.name,
        queueTimer: bracket.queueTimer,
        randomizeSides: bracket.randomizeSides,
        roundsPerSet: bracket.roundsPerSet,
        stageTiebreakers: bracket.stageTiebreakers,
        stageType: bracket.stageType,
        thirdPlaceMatch: bracket.thirdPlaceMatch,
        winsPerMatch: bracket.winsPerMatch,
      });
    }

    // payment account
    if (data.paymentAccount) {
      await axios.put(`/api/tournament/tournament/${tournament.id}/update-stripe/`, {
        paymentAccountId: data.paymentAccount,
      });
    }

    toast.success('Loaded tournament settings from template.');
    reloadTournament();
  };

  if (templates === null) {
    return <CircularProgress />;
  }

  return (
    <>
      <Subtitle title="Tournament Templates" />

      <div>Tournament templates are currently experimental and may cause unexpected results.</div>

      <div>
        Templates will save the following settings to be used in future tournaments:
        <ul>
          <li>Description</li>
          <li>Enable/Disable Player Registration</li>
          <li>Registration Banner and Message</li>
          <li>Registration Questions</li>
          <li>Links</li>
          <li>Payment Processor Selection</li>
          <li>Stages (for tournaments using HiveMind brackets)</li>
          <li>Assigned Cabinets</li>
        </ul>
      </div>

      <h3>Load from Template</h3>

      {templates ? (
        <div className={styles.selectContainer}>
          <Select
            className={styles.select}
            value={selectedTemplate}
            onChange={evt => setSelectedTemplate(evt.target.value)}
          >
            {templates.map(template => (
              <MenuItem key={template.id} value={template.id}>
                {template.name}
              </MenuItem>
            ))}
          </Select>

          <Button
            variant="outlined"
            color="secondary"
            className={styles.button}
            onClick={loadTemplate}
          >
            Load Template
          </Button>
        </div>
      ) : (
        <div>This scene does not have any saved templates.</div>
      )}

      <h3>Save Template</h3>

      <div className={styles.saveContainer}>
        <TextField
          label="Template Name"
          placeholder="Enter template name..."
          value={templateName}
          onChange={evt => setTemplateName(evt.target.value)}
          className={styles.textField}
        />

        <Button variant="outlined" color="primary" onClick={saveTemplate}>
          Save Template
        </Button>
      </div>
    </>
  );
}

Templates.title = 'Templates';
export { Templates };
