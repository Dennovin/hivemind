import React, { useState, useEffect } from 'react';
import { Button, CircularProgress, List, ListItem, Switch, Typography } from '@material-ui/core';
import { toast } from 'react-toastify';

import DeleteButton from '@/components/DeleteButton';
import { getAxios } from '@/util/axios';
import { useTournamentAdmin } from '../context';
import { Subtitle } from '../subtitle';
import { CabinetGroupForm } from './cab-group-form';
import styles from './cabinets.module.css';

function CabinetSwitch({ cabinet }) {
  const { tournament, setTournament } = useTournamentAdmin();
  const axios = getAxios({ authenticated: true });

  const checked = !!tournament?.assignedCabinets?.includes(cabinet.id);
  const onChange = evt => {
    const assignedCabinets = [...tournament.assignedCabinets.filter(i => i !== cabinet.id)];
    if (!checked) {
      assignedCabinets.push(cabinet.id);
    }

    axios
      .patch(`/api/tournament/tournament/${tournament.id}/`, { assignedCabinets })
      .then(response => {
        setTournament(v => ({ ...v, assignedCabinets: response.data.assignedCabinets }));
      });
  };

  return (
    <div>
      <Switch {...{ checked, onChange }} />
      {cabinet.displayName}
    </div>
  );
}

export function Cabinets() {
  const { tournament, queues, setQueues } = useTournamentAdmin();
  const axios = getAxios({ authenticated: true });
  const [allCabinets, setAllCabinets] = useState(null);

  const blankQueue = {
    name: '',
    tournament: tournament.id,
    cabinets: [],
  };

  useEffect(() => {
    if (!tournament?.scene?.id) return;

    axios
      .getAllPages('/api/game/cabinet/', { params: { sceneId: tournament.scene.id } })
      .then(response => {
        setAllCabinets(response.sort((a, b) => a.displayName.localeCompare(b.displayName)));
      });
  }, [tournament?.scene?.id]);

  if (allCabinets === null) return <CircularProgress />;

  const deleteQueue = id => {
    axios.delete(`/api/tournament/queue/${id}/`).then(() => {
      toast.success('Cabinet group deleted.');
      setQueues(v => v.filter(q => q.id !== id));
    });
  };

  return (
    <>
      <Subtitle title="Assigned Cabinets" />

      <div>Selected cabinets will appear on the tournament management and match queue pages.</div>

      {allCabinets.map(cabinet => (
        <CabinetSwitch key={cabinet.id} cabinet={cabinet} />
      ))}

      <Subtitle title="Cabinet Groups" buttons={<CabinetGroupForm queue={blankQueue} />} />

      {queues && (
        <List>
          {queues?
           .filter(q => !q.autoCreated)
           .map(queue => (
             <ListItem className={styles.cabGroupListItem}>
               <div className={styles.name}>{queue.name}</div>
               <div className={styles.button}>
                 <CabinetGroupForm queue={queue} />
               </div>
               <div className={styles.button}>
                 {queue.matchList.length === 0 && (
                   <DeleteButton onConfirm={() => deleteQueue(queue.id)} confirmText="Delete this cabinet group?" />
                 )}
               </div>
             </ListItem>
           ))}
        </List>
      )}
    </>
  );
}
