import React, { useEffect, useState } from 'react';

import { getAxios } from '@/util/axios';
import { useTournamentAdmin } from '../context';
import { LinksTable } from './table';

function Links({ ...props }) {
  const { tournament } = useTournamentAdmin();
  const axios = getAxios({ authenticated: true });
  const [links, setLinks] = useState(null);

  useEffect(() => {
    if (links === null) {
      axios
        .getAllPages(`/api/tournament/links/`, { params: { tournament: tournament.id } })
        .then(setLinks);
    }
  }, []);

  return <LinksTable {...{ links, setLinks }} />;
}

Links.title = 'Links';
export { Links };
