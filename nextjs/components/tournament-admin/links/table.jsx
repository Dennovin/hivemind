import React from 'react';
import { format } from 'date-fns';

import styles from './Links.module.css';
import Table, { TableRow, TableCell } from '@/components/tables/Table';
import DeleteButton from '@/components/DeleteButton';
import { getAxios } from '@/util/axios';
import { useTournamentAdmin } from '../context';
import { LinkForm } from './form';

export function LinkRow({ link, setLinks }) {
  const axios = getAxios({ authenticated: true });

  const onConfirmDelete = () => {
    axios
      .delete(`/api/tournament/links/${link.id}/`)
      .then(setLinks(val => val.filter(v => v.id !== link.id)));
  };

  return (
    <TableRow>
      <TableCell>{link?.url}</TableCell>
      <TableCell>{link?.title}</TableCell>
      <TableCell>
        <DeleteButton onConfirm={onConfirmDelete} confirmText="Delete this link?" />
      </TableCell>
    </TableRow>
  );
}

export function LinksTable({ links, setLinks }) {
  const { tournament } = useTournamentAdmin();

  const emptyLink = { url: '', title: '', tournament: tournament.id };
  const props = {
    title: 'Related Links',
    columnHeaders: ['URL', 'Title', ''],
    cellClassNames: [null, null, styles.deleteCell],
    isLoading: links === null,
    titleButton: <LinkForm link={emptyLink} />,
  };

  return (
    <Table {...props}>
      {links?.map(link => (
        <LinkRow key={link.id} {...{ link, setLinks }} />
      ))}
    </Table>
  );
}
