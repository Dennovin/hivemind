import React, { useEffect, useState } from 'react';
import { useDebounce } from 'use-debounce';

import Form, { useForm } from '@/components/forms/Form';
import Field, { TextField, ImageUploadField, imageURLRegex } from '@/components/fields/Field';
import { isAdminOf } from '@/util/auth';
import { getAxios } from '@/util/axios';
import { useTournamentAdmin } from '../context';

function URLField({ name, ...props }) {
  const form = useForm();
  const [loading, setLoading] = useState(false);
  const [debouncedValue] = useDebounce(form?.values?.[name], 500);
  const axios = getAxios({ authenticated: true });

  const updateInfo = async url => {
    setLoading(true);

    try {
      const response = await axios.post(`/api/tournament/links/opengraph/`, {
        url: form.values[name],
      });

      if (response?.data?.ogTitle) {
        form.setFieldValue('title', response.data.ogTitle);
      }

      if (response?.data?.ogImage) {
        form.setFieldValue('image', response.data.ogImage);
      }
    } catch {
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    if (debouncedValue) {
      updateInfo(debouncedValue);
    }
  }, [debouncedValue]);

  return <TextField name={name} {...props} />;
}

export function LinkForm({ link }) {
  const axios = getAxios({ authenticated: true });
  const { tournament } = useTournamentAdmin();

  const props = {
    buttonText: link.id ? 'Edit Link' : 'Add Link',
    buttonColor: 'default',
    dialogTitle: link.id ? 'Editing Link' : 'Adding Link',
    canEdit: () => isAdminOf(tournament.scene.id),
    object: link,
    method: link.id ? 'PATCH' : 'POST',
    url: link.id ? `/api/tournament/links/${link.id}/` : '/api/tournament/links/',
    getPostData: {
      image: async data => {
        if (data.image === null) return undefined;
        return imageURLRegex.test(data?.image)
          ? undefined
          : data?.image?.startsWith('data:')
          ? data.image.split(',')[1]
          : data?.image;
      },
    },
  };

  return (
    <Form {...props}>
      <Field name="url" label="URL" component={URLField} />
      <Field name="title" label="Title" component={TextField} />
      <Field name="image" label="Image" component={ImageUploadField} required={false} />
    </Form>
  );
}
