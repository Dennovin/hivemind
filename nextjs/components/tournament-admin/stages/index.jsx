import React from 'react';
import { Grid, Link, Typography, Button } from '@material-ui/core';

import TournamentBracketTable from '@/components/tables/TournamentBracketTable';
import { TOURNAMENT_LINK_TYPES } from '@/util/constants';
import { useTournamentAdmin } from '../context';

export function Stages() {
  const { tournament, reloadTournament } = useTournamentAdmin();

  return <TournamentBracketTable tournament={tournament} onSave={reloadTournament} />;
}
