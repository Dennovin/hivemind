import React, { useEffect, useState } from 'react';
import { CircularProgress, Typography } from '@material-ui/core';
import clsx from 'clsx';

import { getAxios } from 'util/axios';
import styles from './UserStatsByMonth.module.css';
import UserStatsByMonthChart from 'components/charts/UserStatsByMonthChart';

export default function UserStatsByMonth({ user }) {
  const axios = getAxios({ authenticated: true });
  const [stats, setStats] = useState(null);
  const [selectedStat, setSelectedStat] = useState(null);

  useEffect(() => {
    if (stats === null) {
      axios.get(`/api/user/user/${user.id}/stats-by-month/`).then(response => {
        console.log(response);
        setStats(response.data.stats);
      });
    }
  }, []);

  if (!stats) {
    return <CircularProgress />;
  }

  return (
    <div className={styles.container}>
      <div className={styles.statTypes}>
        {stats.map(stat => (
          <div
            key={stat.name}
            className={clsx(styles.statType, {
              [styles.selected]: selectedStat?.name === stat.name,
            })}
            onClick={() => setSelectedStat(stat)}
          >
            {stat.label}
          </div>
        ))}
      </div>

      <div className={styles.selectedStat}>
        {selectedStat && (
          <>
            <Typography className={styles.title} variant="h3">
              {selectedStat.label}
            </Typography>

            <div className={styles.chart}>
              <UserStatsByMonthChart
                title={selectedStat.label}
                formatting={selectedStat.formatting}
                values={selectedStat.values}
              />
            </div>
          </>
        )}
      </div>
    </div>
  );
}
