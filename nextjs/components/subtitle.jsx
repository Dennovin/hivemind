import React from 'react';

import styles from './subtitle.module.css';

export function Subtitle({ title, buttons }) {
  return (
    <div className={styles.subtitle}>
      <div className={styles.subtitleText}>{title}</div>
      <div className={styles.buttons}>{buttons}</div>
    </div>
  );
}
