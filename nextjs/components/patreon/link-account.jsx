import React from 'react';
import getConfig from 'next/config';
import Icon from '@mdi/react';
import { mdiPatreon } from '@mdi/js';

import { useAuth } from '@/util/auth';
import styles from './link-account.module.css';

export function LinkPatreonAccount() {
  const { user } = useAuth();
  const { publicRuntimeConfig } = getConfig();

  if (!publicRuntimeConfig?.PATREON_CLIENT_ID) {
    return null;
  }

  const baseUrl = 'https://www.patreon.com/oauth2/authorize';
  const params = new URLSearchParams({
    response_type: 'code',
    client_id: publicRuntimeConfig?.PATREON_CLIENT_ID,
    redirect_uri: `${window.location.origin}/auth/patreon`,
    scope: 'identity identity[email] identity.memberships campaigns',
  });

  return (
    <div className={styles.row}>
      <div className={styles.icon}>
        <Icon path={mdiPatreon} size={2} />
      </div>

      <div className={styles.link}>
        {user?.linkedAccounts?.includes('patreon') ? (
          <>Patreon account linked.</>
        ) : (
          <a href={`${baseUrl}?${params.toString()}`}>Link your Patreon account</a>
        )}
      </div>
    </div>
  );
}
