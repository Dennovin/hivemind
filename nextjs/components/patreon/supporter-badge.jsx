import React from 'react';
import Icon from '@mdi/react';
import { mdiBee } from '@mdi/js';
import { Chip } from '@material-ui/core';

import styles from './supporter-badge.module.css';

export function SupporterBadge() {
  return (
    <Chip
      className={styles.chip}
      variant="outlined"
      icon={<Icon className={styles.icon} path={mdiBee} size={2} />}
      label="HiveMind Supporter"
      href="https://patreon.com/kqhivemind"
      clickable
      component="a"
    />
  );
}
