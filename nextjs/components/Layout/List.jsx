import { makeStyles } from '@material-ui/core/styles';
import { List as MuiList, ListItem as MuiListItem } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  list: {
    listStyleType: 'disc',
    listStylePosition: 'inside',
  },
  listItem: {
    display: 'list-item',
    fontSize: '17px',
  },
}));

export function List({ children, ...props }) {
  const classes = useStyles();
  return <MuiList className={classes.list}>{children}</MuiList>;
}

export function ListItem({ children, ...props }) {
  const classes = useStyles();
  return <MuiListItem className={classes.listItem}>{children}</MuiListItem>;
}
