import { makeStyles } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  paragraph: {
    fontSize: '17px',
    marginTop: '1em',
  },
  h2: {
    fontSize: '21px',
  },
}));

export function P({ children, ...props }) {
  const classes = useStyles();
  return (
    <Typography paragraph variant="body2" className={classes.paragraph} {...props}>
      {children}
    </Typography>
  );
}

export function H2({ children, ...props }) {
  const classes = useStyles();
  return (
    <Typography variant="h2" className={classes.h2} {...props}>
      {children}
    </Typography>
  );
}
