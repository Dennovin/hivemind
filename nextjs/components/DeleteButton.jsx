import { Button, Dialog, DialogActions, DialogContent, DialogContentText } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import PropTypes from 'prop-types';
import { useState } from 'react';

export function DeleteConfirmDialog({ open, confirmText, handleClose, handleConfirm }) {
  return (
    <Dialog open={open} onClose={handleClose}>
      <DialogContent>
        <DialogContentText>{confirmText}</DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button variant="outlined" color="default" onClick={handleClose}>
          Cancel
        </Button>
        <Button
          variant="contained"
          color="secondary"
          onClick={handleConfirm}
          startIcon={<DeleteIcon />}
        >
          Delete
        </Button>
      </DialogActions>
    </Dialog>
  );
}

export default function DeleteButton({
  onConfirm,
  buttonText,
  confirmText,
  className,
  size = 'small',
}) {
  const [open, setOpen] = useState(false);

  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const handleConfirm = () => {
    onConfirm();
    setOpen(false);
  };

  return (
    <>
      <Button
        variant="outlined"
        onClick={handleOpen}
        startIcon={<DeleteIcon className="text-lg" />}
        className={`text-gray-500 border-gray-500 ${className}`}
        size={size}
      >
        {buttonText}
      </Button>

      <DeleteConfirmDialog {...{ open, confirmText, handleClose, handleConfirm }} />
    </>
  );
}

DeleteButton.propTypes = {
  onConfirm: PropTypes.func.isRequired,
  buttonText: PropTypes.string,
  confirmText: PropTypes.string,
};

DeleteButton.defaultProps = {
  buttonText: 'Delete',
  confirmText: 'Delete this?',
};
