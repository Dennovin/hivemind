import React, { useState, useEffect } from 'react';
import { MenuItem, Dialog, DialogTitle, List, ListItem, CircularProgress } from '@material-ui/core';
import clsx from 'clsx';

import { getAxios } from 'util/axios';
import { useForm } from '../forms/Form';
import SceneColorBox from 'components/SceneColorBox';
import Select from './Select';

import styles from './CabinetSelect.module.css';

export default function CabinetSelect({
  name,
  label,
  sceneFilter,
  cabinetFilter,
  className,
  ...props
}) {
  const axios = getAxios();
  const form = useForm();
  const [isSceneOpen, setIsSceneOpen] = useState(false);
  const [isCabinetOpen, setIsCabinetOpen] = useState(false);
  const [scene, setScene] = useState(null);
  const [allScenes, setAllScenes] = useState(null);
  const [cabinet, setCabinet] = useState(null);
  const [cabinets, setCabinets] = useState(null);

  const filterScenes = s => (sceneFilter ? s.filter(sceneFilter) : s);
  const filterCabinets = c => (cabinetFilter ? c.filter(cabinetFilter) : c);

  useEffect(() => {
    axios.getAllPages('/api/game/scene/').then(scenes => {
      setAllScenes(filterScenes(scenes).sort((a, b) => a.displayName.localeCompare(b.displayName)));
    });
  }, []);

  useEffect(() => {
    setCabinet(null);
    if (form?.values[name]) {
      axios.get(`/api/game/cabinet/${form.values[name]}/`).then(response => {
        setCabinet(response.data);
      });
    }
  }, [form?.values[name]]);

  useEffect(() => {
    setCabinets(null);
    if (scene?.id) {
      axios.getAllPages('/api/game/cabinet/', { params: { sceneId: scene.id } }).then(rows => {
        setCabinets(
          filterCabinets(rows).sort((a, b) => a.displayName.localeCompare(b.displayName)),
        );
      });
    }
  }, [scene]);

  const selectScene = scene => {
    setScene(scene);
    setIsSceneOpen(false);
    setIsCabinetOpen(true);
  };

  const selectCabinet = cabinet => {
    setCabinet(cabinet);
    form.setFieldValue(name, cabinet.id);
    setIsCabinetOpen(false);
  };

  return (
    <>
      <div
        className={clsx(styles.cabinetSelectContainer, className)}
        onClick={() => setIsSceneOpen(true)}
      >
        <div className={styles.label}>{label}</div>
        {cabinet && <div className={styles.value}>{cabinet.displayName}</div>}
        {!cabinet && <div className={styles.nullValue}>None selected.</div>}
      </div>

      <Dialog
        className={styles.sceneDialog}
        onClose={() => setIsSceneOpen(false)}
        open={isSceneOpen}
        aria-labelledby="scene-dialog-title"
      >
        <DialogTitle id="scene-dialog-title">Select scene</DialogTitle>

        <List className={styles.sceneList}>
          {allScenes === null && <CircularProgress />}

          {allScenes?.map(scene => (
            <ListItem
              key={scene.id}
              className={styles.scene}
              button
              onClick={() => selectScene(scene)}
            >
              <SceneColorBox color={scene.backgroundColor} />
              <span className={styles.sceneName}>{scene.displayName}</span>
            </ListItem>
          ))}
        </List>
      </Dialog>

      <Dialog
        className={styles.cabinetDialog}
        onClose={() => setIsCabinetOpen(false)}
        open={isCabinetOpen}
        aria-labelledby="cabinet-dialog-title"
      >
        <DialogTitle id="cabinet-dialog-title">Select cabinet</DialogTitle>

        <List>
          {cabinets === null && <CircularProgress />}

          {cabinets?.map(cabinet => (
            <ListItem key={cabinet.id} button onClick={() => selectCabinet(cabinet)}>
              {cabinet.displayName}
            </ListItem>
          ))}
        </List>
      </Dialog>
    </>
  );
}
