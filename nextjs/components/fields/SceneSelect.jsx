import { MenuItem } from '@material-ui/core';
import SceneBadge from 'components/SceneBadge';
import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';
import { getAxios } from 'util/axios';
import Field, { Select } from './Field';
export function SceneSelect({ name = 'scene', label = 'Your Home Scene', noGrid = true, scenes }) {
  const [allScenes, setAllScenes] = useState(scenes);
  const axios = getAxios({ authenticated: true });
  useEffect(() => {
    if (!Boolean(allScenes)) {
      axios.getAllPages(`/api/tournament/scene/`).then(setAllScenes);
    }
  }, []);
  return (
    <Field name={name} label={label} component={Select} noGrid={noGrid}>
      {allScenes?.map(
        scene =>
          scene && (
            <MenuItem key={scene.name} value={scene.name}>
              <SceneBadge scene={scene} />
            </MenuItem>
          ),
      )}
    </Field>
  );
}

SceneSelect.propTypes = {
  name: PropTypes.string,
  label: PropTypes.string,
  onGrid: PropTypes.bool,
  scenes: PropTypes.array,
};

SceneSelect.defaultProps = {
  onGrid: true,
  name: 'scene',
  label: 'Your Home Scene',
  scenes: null,
};
