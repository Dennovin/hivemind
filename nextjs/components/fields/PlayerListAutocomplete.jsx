import React, { useEffect, useState } from 'react';
import { Box, Typography, FormControl, InputLabel, CircularProgress } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import MuiAutocomplete from '@material-ui/lab/Autocomplete';
import { createFilterOptions } from '@material-ui/lab/Autocomplete';
import PropTypes from 'prop-types';

import Field from './Field';
import TextField from './TextField';
import { useForm } from '../forms/Form';
import { getAxios } from 'util/axios';

const filter = createFilterOptions();

const useStyles = makeStyles(theme => ({
  label: {
    transform: 'translate(12px, 10px) scale(0.75)',
  },
}));

export function Autocomplete({ label, ...props }) {
  const classes = useStyles();
  const formik = useForm();

  if (!props.renderInput) {
    props.renderInput = params => (
      <TextField
        {...params}
        name="newItem"
        placeholder={props.placeholder}
        helperText={props.helperText}
      />
    );
  }

  return (
    <FormControl fullWidth>
      <InputLabel variant="standard" className={classes.label}>
        {label}
      </InputLabel>
      <MuiAutocomplete
        {...props}
        onChange={(evt, value, ...fnProps) => {
          formik.setFieldValue(props.name, value);
        }}
      />
    </FormControl>
  );
}

export default function PlayerListAutocomplete({ name, scene, ...props }) {
  const axios = getAxios({ authenticated: true });
  const formik = useForm();
  const [allPlayers, setAllPlayers] = useState(null);
  const [availablePlayers, setAvailablePlayers] = useState(null);

  const filterOptions = (options, params) => {
    const filtered = filter(options, params);
    const { inputValue } = params;

    const isExisting = options.some(option => inputValue === option.name);
    if (inputValue !== '' && !isExisting) {
      filtered.push({ name: `New Player: "${inputValue}"`, value: inputValue });
    }

    return filtered;
  };

  const loadPlayerList = () => {
    axios.getAllPages('/api/league/player/', { params: { sceneId: scene.id } }).then(setAllPlayers);
  };

  const updateAvailablePlayers = () => {
    if (allPlayers !== null) {
      const selected = new Set(formik.values[name].map(i => i.id));
      setAvailablePlayers(allPlayers.filter(p => !(p.id in selected)));
    }
  };

  useEffect(updateAvailablePlayers, [allPlayers]);

  if (allPlayers === null) {
    loadPlayerList();
  }

  if (availablePlayers === null) {
    return <CircularProgress />;
  }

  return (
    <Box>
      <Typography variant="h4">
        Player Count: <b>{formik?.values[name]?.length ?? 0}</b>
      </Typography>

      <Field
        {...props}
        component={Autocomplete}
        name={name}
        multiple
        freeSolo
        filterSelectedOptions
        options={availablePlayers}
        filterOptions={filterOptions}
        handleHomeEndKeys
        getOptionLabel={opt => opt.name}
        renderOption={opt => opt.name}
        onChange={updateAvailablePlayers}
        clearOnBlur
      />
    </Box>
  );
}

PlayerListAutocomplete.propTypes = {
  name: PropTypes.string.isRequired,
  scene: PropTypes.object.isRequired,
};
