import React from 'react';
import { InputAdornment, IconButton, TextField as MuiTextField } from '@material-ui/core';
import Icon from '@mdi/react';
import { mdiContentCopy } from '@mdi/js';

import styles from './TextField.module.css';
import { useForm } from '@/components/forms/Form';

export default function TextField({ name, withCopyButton, InputProps, ...props }) {
  const { values } = useForm();
  const fieldInputProps = InputProps ?? {};

  const copyValue = () => {
    navigator.clipboard.writeText(values[name]);
  };

  if (withCopyButton) {
    fieldInputProps.endAdornment = (
      <InputAdornment position="end">
        <IconButton onClick={copyValue}>
          <Icon size={1} path={mdiContentCopy} className={styles.copyButton} />
        </IconButton>
      </InputAdornment>
    );
  }

  return <MuiTextField variant="outlined" name={name} InputProps={fieldInputProps} {...props} />;
}
