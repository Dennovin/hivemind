import React, { useEffect, useMemo } from 'react';
import slugify from 'slugify';

import { useForm } from '@/components/forms/Form';
import TextField from './TextField';

const slugifyOpts = {
  lower: true,
};

export default function SlugField({ name, basedOn, baseUrl, helperText, ...props }) {
  const { values, initialValues, setFieldValue } = useForm();

  useEffect(() => {
    if (!initialValues?.[name]) {
      setFieldValue(name, slugify(values?.[basedOn], slugifyOpts));
    }
  }, [initialValues?.[name], values?.[basedOn]]);

  const fullHelperText = useMemo(
    () =>
      baseUrl && values?.[name] ? (
        <>
          {helperText}{' '}
          <b>
            {baseUrl}
            {values?.[name]}
          </b>
        </>
      ) : (
        helperText
      ),
    [baseUrl, helperText, values?.[name]],
  );

  return <TextField name={name} helperText={fullHelperText} {...props} />;
}
