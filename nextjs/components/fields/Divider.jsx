import { Divider as MuiDivider } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  divider: {
    margin: theme.spacing(2, 0),
  },
}));

export default function Divider({ ...props }) {
  const classes = useStyles();
  return (
    <MuiDivider className={classes.divider} {...props} />
  );
}

