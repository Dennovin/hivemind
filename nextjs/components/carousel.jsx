import React, { Children } from 'react';
import { Carousel } from 'react-responsive-carousel';
import Icon from '@mdi/react';
import { mdiHexagon, mdiChevronLeft, mdiChevronRight } from '@mdi/js';
import clsx from 'clsx';

import styles from './Carousel.module.css';

export const renderIndicator = (clickHandler, isSelected, index, label) => {
  return (
    <Icon
      className={clsx(styles.indicator, { [styles.selected]: isSelected })}
      path={mdiHexagon}
      onClick={clickHandler}
    />
  );
};

export const renderArrowPrev = (clickHandler, hasPrev, labelPrev) => {
  if (!hasPrev) return null;

  return (
    <div className={clsx(styles.arrow, styles.arrowPrev)} onClick={clickHandler}>
      <Icon className={styles.arrowIcon} path={mdiChevronLeft} />
    </div>
  );
};

export const renderArrowNext = (clickHandler, hasNext, labelNext) => {
  if (!hasNext) return null;

  return (
    <div
      aria-label={labelNext}
      className={clsx(styles.arrow, styles.arrowNext)}
      onClick={clickHandler}
    >
      <Icon className={styles.arrowIcon} path={mdiChevronRight} />
    </div>
  );
};

export default function CustomCarousel({ children, className, ...props }) {
  const slides = Children.toArray(children).filter(v => v !== null && v !== undefined);
  return (
    <Carousel
      className={clsx(styles.carousel, className)}
      showThumbs={false}
      {...{ renderIndicator, renderArrowPrev, renderArrowNext }}
      {...props}
    >
      {slides}
    </Carousel>
  );
}
