function SceneBadge({ scene }) {
  return (
    <div className="flex gap-2 justify-start items-center w-full">
      <span
        className="text-base font-mono font-bold px-2 py-0 w-14 text-center rounded-sm inline-block -my-1"
        style={{
          color: scene.foregroundColor,
          backgroundColor: scene.backgroundColor,
        }}
      >
        {scene.name}
      </span>
      <span className="text-sm font-mono uppercase tracking-wider">{scene.displayName}</span>
    </div>
  );
}

export default SceneBadge;
