import React, { useState } from 'react';

import { useAuth } from '@/util/auth';
import { LinkGoogle } from './google';
import { LinkDiscord } from './discord';
import { LinkTwitch } from './twitch';
import { LinkPatreon } from './patreon';
import styles from './linked-accounts.module.css';

export function LinkedAccounts() {
  const { user } = useAuth();

  if (!user) return null;

  return (
    <div className={styles.list}>
      <LinkGoogle />
      <LinkDiscord />
      <LinkTwitch />
      <LinkPatreon />
    </div>
  );
}
