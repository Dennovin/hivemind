import React from 'react';

import { LoginButton } from './login-button';
import { LinkAccount } from './link-account';
import Icon from './patreon.svg';

const params = {
  provider: 'patreon',
  url: 'https://www.patreon.com/oauth2/authorize',
  scope: 'identity identity[email] identity.memberships campaigns',
  Icon,
  iconColor: '#000000',
  clientIdConfig: 'PATREON_CLIENT_ID',
};

export function PatreonLogin() {
  return <LoginButton title="Log in with Patreon" {...params} />;
}

export function LinkPatreon() {
  return (
    <LinkAccount
      title="Connect your Patreon account"
      connectedText="Patreon account connected."
      {...params}
    />
  );
}
