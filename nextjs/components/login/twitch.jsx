import React from 'react';

import { LoginButton } from './login-button';
import { LinkAccount } from './link-account';
import Icon from './twitch.svg';

const params = {
  provider: 'twitch',
  url: 'https://id.twitch.tv/oauth2/authorize',
  scope: 'user:read:email',
  Icon,
  iconColor: '#9146FF',
  clientIdConfig: 'TWITCH_CLIENT_ID',
};

export function TwitchLogin() {
  return <LoginButton title="Log in with Twitch" {...params} />;
}

export function LinkTwitch() {
  return (
    <LinkAccount
      title="Connect your Twitch account"
      connectedText="Twitch account connected."
      {...params}
    />
  );
}
