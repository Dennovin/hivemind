import React from 'react';
import getConfig from 'next/config';
import { useRouter } from 'next/router';
import Icon from '@mdi/react';
import Image from 'next/image';

import styles from './login-button.module.css';

export function LoginButton({ provider, url, scope, Icon, iconColor, title, clientIdConfig }) {
  const { publicRuntimeConfig } = getConfig();
  const router = useRouter();

  if (!publicRuntimeConfig?.[clientIdConfig]) {
    return null;
  }

  const onClick = evt => {
    evt.preventDefault();
    evt.stopPropagation();

    const params = new URLSearchParams({
      response_type: 'code',
      client_id: publicRuntimeConfig?.[clientIdConfig],
      redirect_uri: `${window.location.origin}/auth/${provider}`,
      scope,
      state: window.location.pathname,
    });

    router.push(`${url}?${params.toString()}`);
  };

  return (
    <button className={styles.button} onClick={onClick}>
      {Icon && <Icon className={styles.icon} fill={iconColor} />}
      <span className={styles.title}>{title}</span>
    </button>
  );
}
