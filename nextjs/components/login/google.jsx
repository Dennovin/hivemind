import React from 'react';

import { LoginButton } from './login-button';
import { LinkAccount } from './link-account';
import Icon from './google.svg';

const params = {
  provider: 'google-oauth2',
  url: 'https://accounts.google.com/o/oauth2/v2/auth',
  scope: 'openid email',
  Icon,
  iconColor: '#4285F4',
  clientIdConfig: 'OAUTH_CLIENT_ID',
};

export function GoogleLogin() {
  return <LoginButton title="Log in with Google" {...params} />;
}

export function LinkGoogle() {
  return (
    <LinkAccount
      title="Connect your Google account"
      connectedText="Google account connected."
      {...params}
    />
  );
}
