import React from 'react';
import getConfig from 'next/config';
import { useRouter } from 'next/router';
import Icon from '@mdi/react';
import Image from 'next/image';

import { useAuth } from '@/util/auth';
import styles from './link-account.module.css';

export function LinkAccount({
  provider,
  url,
  scope,
  Icon,
  iconColor,
  title,
  connectedText,
  clientIdConfig,
}) {
  const { user } = useAuth();
  const { publicRuntimeConfig } = getConfig();
  const router = useRouter();

  if (!publicRuntimeConfig?.[clientIdConfig]) {
    return null;
  }

  const onClick = evt => {
    evt.preventDefault();
    evt.stopPropagation();

    const params = new URLSearchParams({
      response_type: 'code',
      client_id: publicRuntimeConfig?.[clientIdConfig],
      redirect_uri: `${window.location.origin}/auth/${provider}`,
      scope,
      state: window.location.pathname,
    });

    router.push(`${url}?${params.toString()}`);
  };

  if (user?.linkedAccounts?.includes(provider)) {
    return (
      <div className={`${styles.row} ${styles.connected}`}>
        {Icon && <Icon className={styles.icon} fill={iconColor} />}
        <span className={styles.title}>{connectedText ?? 'Account connected.'}</span>
      </div>
    );
  }

  return (
    <div className={styles.row} onClick={onClick}>
      {Icon && <Icon className={styles.icon} fill={iconColor} />}
      <span className={styles.title}>{title}</span>
    </div>
  );
}
