import { marked } from 'marked';
import DOMPurify from 'isomorphic-dompurify';
import PropTypes from 'prop-types';

export default function Markdown({ contents, className, options }) {
  const html = DOMPurify.sanitize(marked(contents, options));

  return <div className={className} dangerouslySetInnerHTML={{ __html: html }} />;
}

Markdown.propTypes = {
  contents: PropTypes.string.isRequired,
  className: PropTypes.string,
};

Markdown.defaultProps = {
  className: null,
};
