import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import { Box } from '@material-ui/core';
import PropTypes from 'prop-types';

const useStyles = makeStyles(theme => ({
  codeBlock: {
    fontFamily: '"Roboto Mono", monospace',
    border: '1px solid #ddd',
    background: '#eee',
    padding: theme.spacing(0, 0.5),
    margin: theme.spacing(0, 0.5),
    fontSize: '90%',
  },
}));

export default function Code({ children, className }) {
  const classes = useStyles();

  return (
    <Box className={clsx(classes.codeBlock, className)}>{children}</Box>
  );
}

Code.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.element),
    PropTypes.element,
  ]).isRequired,
  className: PropTypes.string,
};

Code.defaultProps = {
  className: null,
};

