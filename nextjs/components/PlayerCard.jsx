import React from 'react';
import { Card, CardHeader, CardContent, CardMedia, CardActionArea, Grid, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  container: {
    margin: theme.spacing(2, 0),
  },
  image: {
    width: '30%',
    aspectRatio: '1',
    objectFit: 'cover',
    float: 'left',
    margin: theme.spacing(0, 2, 2, 0),
  },
  text: {
    margin: theme.spacing(0, 2),
  },
}));

export default function PlayerCard({ player, href }) {
  const classes = useStyles();
  const cardTitle = player.scene ? `${player.name} [${player.scene}]` : player.name;

  return (
    <Grid item xs={12} md={6} className={classes.container}>
      <Card className={classes.card}>
        <CardActionArea href={href}>
          <CardHeader title={cardTitle} subheader={player.pronouns} />
          <CardContent className={classes.cardContent}>
            <Typography className={classes.text} variant="body1">
              {player.image && (
                <img className={classes.image} src={player.image} alt="user-provided image" />
              )}
              {player.tidbit}
            </Typography>
          </CardContent>
        </CardActionArea>
      </Card>
    </Grid>
  );
}


