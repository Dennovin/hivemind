import { createContext, useContext, useState } from 'react';

import { useOverlaySettings } from 'overlay/Settings';
import { getAxios } from 'util/axios';
import { useWebSocket } from 'util/websocket';
import { MatchStatsProvider } from './MatchStats';

export const GameStatsContext = createContext({});
const ALL_MAPS = {};

export function GameStatsProvider({ children }) {
  const axios = getAxios();
  const settings = useOverlaySettings();
  const [stats, setStats] = useState(null);

  const ws = useWebSocket(`/ws/ingame_stats/${settings?.cabinet?.id}`);

  ws.onJsonMessage(message => {
    if (!(message.mapName in ALL_MAPS)) {
      axios.get('/api/game/map/', { params: { name: message.mapName } }).then(response => {
        if (response?.data?.results?.length > 0) {
          ALL_MAPS[message.mapName] = response.data.results[0];
        } else {
          ALL_MAPS[message.mapName] = null;
        }
      });
    }

    setStats(message);
  });

  if (stats === null) {
    return (
      <MatchStatsProvider>
        <GameStatsContext.Provider value={null}>{children}</GameStatsContext.Provider>
      </MatchStatsProvider>
    );
  }

  const value = { ...stats };
  if (stats?.mapName in ALL_MAPS) {
    value.gameMap = ALL_MAPS[stats.mapName];
  }

  return <GameStatsContext.Provider value={value}>{children}</GameStatsContext.Provider>;
}

export function useGameStats() {
  return useContext(GameStatsContext);
}
