import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { BLUE_TEAM, GOLD_TEAM, POSITIONS_BY_TEAM } from 'util/constants';
import PlayerIcon from 'overlay/components/PlayerIcon';
import PlayerName from 'overlay/components/PlayerName';
import PlayerNamesBlock from 'overlay/components/PlayerNamesBlock';

const useStyles = makeStyles(theme => ({
  block: {
    display: 'flex',
    flexDirection: 'row',
  },
  position: {
    flexBasis: 0,
    flexGrow: 1,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  icon: {
    height: '40px',
    width: '40px',
    '&.active': {
      opacity: 1,
    },
  },
  name: {
    fontSize: '24px',
    color: '#efefef',
    width: '150%',
    margin: '10px -25%',
    textAlign: 'center',
    '.position:nth-child(even) &': {
      justifySelf: 'flex-end',
      marginTop: '54px',
    },
  },
}));

export default function PlayerNames({ team, className }) {
  const classes = useStyles({ team });

  return (
    <PlayerNamesBlock className={clsx(className, classes.block, 'player-names')}>

      {[...POSITIONS_BY_TEAM[team]].reverse().map(pos => (
        <div className={clsx(classes.position, 'position')} key={pos.ID}>
          <PlayerIcon className={clsx(classes.icon, 'icon')} position={pos} />
          <PlayerName className={clsx(classes.name, 'name')} position={pos} />
        </div>
      ))}

    </PlayerNamesBlock>
  );
}
