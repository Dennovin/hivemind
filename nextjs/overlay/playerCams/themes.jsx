import React from 'react';

import Default from './Default';

const allPlayerCamsThemes = [Default];

export function getPlayerCamsTheme(name) {
  for (const theme of allPlayerCamsThemes) {
    if (theme.themeProps.name == name) {
      return theme;
    }
  }
}

export { allPlayerCamsThemes };

