import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { useOverlaySettings } from 'overlay/Settings';
import CabinetName from 'overlay/components/CabinetName';
import CurrentTime from 'overlay/components/CurrentTime';
import EventName from 'overlay/components/EventName';
import WarmUpNotice from 'overlay/components/WarmUpNotice';
import { BLUE_TEAM, GOLD_TEAM } from 'util/constants';
import BerriesRemainingBox from './default/BerriesRemainingBox';
import GameTimeBox from './default/GameTimeBox';
import HiveMindLogo from './default/HiveMindLogo';
import KillFeed from './default/KillFeed';
import PlayerNames from './default/PlayerNames';
import PlayerStats from './default/PlayerStats';
import PredictionChartBox from './default/PredictionChartBox';
import TeamNameBox from './default/TeamNameBox';

const useStyles = makeStyles(theme => ({
  sidebar: {
    position: 'absolute',
    backgroundColor: 'black',
    width: '160px',
    height: '900px',
    top: '0px',
  },
  sidebarLeft: {
    left: '0px',
  },
  sidebarRight: {
    left: '1760px',
  },
  infoBox: {
    position: 'absolute',
    top: '0px',
    left: '0px',
    width: '160px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  bottombar: {
    position: 'absolute',
    backgroundColor: 'black',
    top: '900px',
    height: '180px',
    left: '320px',
    width: '1280px',
  },
  teamName: {
    position: 'absolute',
    color: 'black',
    width: '625px',
    top: '10px',
  },
  teamNameLeft: {
    left: '10px',
  },
  teamNameRight: {
    right: '10px',
  },
  bottomInfoBox: {
    position: 'absolute',
    top: '70px',
    left: '10px',
    width: '1260px',
    overflow: 'hidden',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  bottomInfo: {
    fontFamily: 'Roboto',
    fontSize: '22px',
    color: 'white',
    fontWeight: 'bold',
  },
  predictionChartBox: {
    position: 'absolute',
    top: '10px',
    left: '10px',
    width: '140px',
    height: '70px',
  },
}));

export default function DefaultOverlay({}) {
  const classes = useStyles();
  const settings = useOverlaySettings();

  const leftTeam = settings?.goldOnLeft ? GOLD_TEAM : BLUE_TEAM;
  const rightTeam = settings?.goldOnLeft ? BLUE_TEAM : GOLD_TEAM;

  return (
    <div className="ingame-overlay">
      <div className={clsx(classes.sidebar, classes.sidebarLeft, 'sidebar', 'sidebar-left')}>
        <PredictionChartBox className={classes.predictionChart} />
        <PlayerNames team={leftTeam} />
        <PlayerStats team={leftTeam} />
      </div>

      <div className={clsx(classes.sidebar, classes.sidebarRight, 'sidebar', 'sidebar-right')}>
        <div className={clsx(classes.infoBox, 'info-box')}>
          <HiveMindLogo />
          <GameTimeBox />
          <BerriesRemainingBox />
          <KillFeed />
        </div>

        <PlayerNames team={rightTeam} />
        <PlayerStats team={rightTeam} />
      </div>

      <div className={clsx(classes.bottombar, 'bottom-bar')}>
        <TeamNameBox className={clsx(classes.teamName, classes.teamNameLeft)} team={leftTeam} />
        <TeamNameBox className={clsx(classes.teamName, classes.teamNameRight)} team={rightTeam} />

        <div className={clsx(classes.bottomInfoBox, 'bottom-info-box')}>
          <EventName className={classes.bottomInfo} />
          <CabinetName className={classes.bottomInfo} />
          <CurrentTime className={classes.bottomInfo} />
          <WarmUpNotice className="text-white -mt-2" />
        </div>
      </div>
    </div>
  );
}

DefaultOverlay.themeProps = {
  name: 'default',
  displayName: 'Clover (Default)',
  description: `HiveMind's default theme.`,
  size: { w: 1920, h: 1080 },
  overlayWindow: { w: 1920, h: 1080, x: 0, y: 0 },
  gameCaptureWindow: { w: 1600, h: 900, x: 160, y: 0 },
  blueCameraWindow: { w: 320, h: 180, x: 0, y: 900 },
  goldCameraWindow: { w: 320, h: 180, x: 1600, y: 900 },
};
