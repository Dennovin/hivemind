import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { useMatch } from '@/overlay/Match';
import PlayerIconsRow from './PlayerIconsRow';
import SnailPercentageRow from './SnailPercentageRow';
import StatsRow from './StatsRow';

const useStyles = makeStyles(theme => ({
  row: {
    display: 'flex',
    flexDirection: 'row',
  },
  container: {
    background:
      "url('/static/racing/flag-bg-middle-stats.png') padding-box, linear-gradient(to right, #292323 0%, #464040 36%, #CEC6C6 49%, #464040 62%, #292323 100%) border-box",
    borderRadius: '8px',
    border: '4px solid transparent',
    color: '#FFFFFF',
    textShadow: '#1A1A1A 1px 1px',
    fontSize: '22px',
    fontWeight: 'bold',
  },
  warmup: {
    fontSize: '96px',
    textAlign: 'center',
  },
  queenKillIcon: {
    color: '#0BFF06',
  },
}));

export default function StatsSection({ className }) {
  const classes = useStyles();
  const match = useMatch();

  if (match?.isWarmUp) {
    return (
      <div className={clsx(classes.container, className)}>
        <PlayerIconsRow className={clsx(classes.row, 'header-row')} />
        <div className={clsx(classes.warmup, className)}>
          <img src={`/static/racing/warmup-3-cropped.svg`} />
        </div>
      </div>
    );
  }

  const queenKillIcon = <span className={classes.queenKillIcon}>&#8224;</span>;

  return (
    <div className={clsx(classes.container, className)}>
      <PlayerIconsRow className={clsx(classes.row, 'header-row')} />
      <StatsRow
        title="Kills"
        statKey="kills"
        colorOnChange={{ higher: '#0BFF06', duration: 1200 }}
        queenKillIcon={queenKillIcon}
      />
      <StatsRow
        title="Deaths"
        statKey="deaths"
        colorOnChange={{ higher: '#FF0606', duration: 1200 }}
      />
      <StatsRow
        title="Berries"
        statKey="totalBerries"
        colorOnChange={{ higher: '#E00CD7', duration: 1200 }}
      />
      <SnailPercentageRow colorOnChange={{ higher: '#DB96D9', duration: 1200 }} />
      <SnailPercentageRow
        useEstimatedDistance={true}
        colorOnChange={{ higher: '#DB96D9', duration: 2400 }}
      />
    </div>
  );
}
