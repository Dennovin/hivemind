import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import clsx from 'clsx';

import {BLUE_TEAM, GOLD_TEAM} from 'util/constants';
import TeamName from 'overlay/components/TeamName';
import TeamScoreCounter from 'overlay/components/TeamScoreCounter';
import {useMatch} from 'overlay/Match';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexDirection: ({team}) => team === GOLD_TEAM ? 'row-reverse' : 'row',
    marginRight: ({team}) => team === BLUE_TEAM && '1px',
    marginLeft: ({team}) => team === GOLD_TEAM && '1px',

    background: ({team}) => team === GOLD_TEAM ?
      "url('/static/racing/gold-in-game-team-banner.svg') no-repeat right padding-box, linear-gradient(to right, #211708, #211708) padding-box, linear-gradient(to right, #292323 0%, #464040 36%, #CEC6C6 49%, #464040 62%, #292323 100%) border-box" :
      "url('/static/racing/blue-in-game-team-banner.svg') no-repeat left padding-box, linear-gradient(to right, #030846, #030846) padding-box, linear-gradient(to right, #292323 0%, #464040 36%, #CEC6C6 49%, #464040 62%, #292323 100%) border-box",
    borderRadius: "8px",
    border: "4px solid transparent",
    color: "#FFFFFF",
    textShadow: "#1A1A1A 1px 1px",

    width: "468px",
    height: "54px",
    alignSelf: "end",
  },
  scoreCounter: {
    display: 'flex',
    flexDirection: ({team}) => team === GOLD_TEAM ? 'row-reverse' : 'row',
    padding: "2px 2px",
    minWidth: "154px",
  },
  marker: {
    width: "36px",
    margin: 0,
    padding: 0,
    marginTop: "3px",
    "&.gold:first-child": {
      marginRight: "4px",
    },
    "&.blue:first-child": {
      marginLeft: "4px",
    }
  },
  markerEmpty: {},
  markerFilled: {},
  teamName: {
    // These four are from google fonts recommendation
    fontFamily: 'Orbitron, sans-serif',
    fontOpticalSizing: "auto",
    fontWeight: "bold",
    fontStyle: "normal",

    flexGrow: 1,
    flexBasis: 0,
    textTransform: 'uppercase',
    color: 'FFFFFF',
    textShadow: "#1A1A1A 1px 1px",
    fontSize: '24px',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    padding: "6px 4px",
    textOverflow: 'ellipsis',
    letterSpacing: "1px",
    textAlign: ({team}) => team === GOLD_TEAM ? 'left' : 'right',
  },
}));

export default function TeamSection({team, className}) {
  const classes = useStyles({team});
  const match = useMatch();

  const MarkerFilled = () => (
    <div
      className={clsx(classes.marker, classes.markerFilled, team)}
    ><img
      src={`/static/racing/${team}-flag.svg`}
    /></div>
  );

  const MarkerEmpty = () => (
    <div
      className={clsx(classes.marker, classes.markerEmpty, team)}
    ><img
      src={`/static/racing/empty-${team}-flag-win-marker.svg`}
    /></div>
  );

  return (
    <div className={clsx(classes.container, className)}>
      <div className={clsx(classes.scoreCounter, "score-counter")}>
        <TeamScoreCounter team={team} markerFilled={MarkerFilled} markerEmpty={MarkerEmpty} />
      </div>

      <div className={clsx(classes.teamName, "team-name")}>
        <TeamName className={classes.teamName} team={team} />
      </div>
    </div>
  );
}
