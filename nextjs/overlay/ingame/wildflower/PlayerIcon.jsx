import { makeStyles } from '@material-ui/core/styles';

import { useOverlaySettings } from 'overlay/Settings';
import PlayerStatusSprite from 'overlay/components/PlayerStatusSprite';

const useStyles = makeStyles(theme => ({
  icon: {
    height: '79px',
    width: '58px',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center calc(100% - 4px)',
    backgroundImage: ({ position }) =>
      `url(/static/gdc/sprites/${position.TEAM}_${position.POSITION}@4x.png)`,
    backgroundSize: ({ position }) => (position.POSITION == 'queen' ? '55px 57px' : '30px 39px'),
    transform: ({ position, settings }) =>
      settings.isTeamOnLeft(position.TEAM) ? null : 'scaleX(-1)',
    '&.warrior': {
      backgroundImage: ({ position }) =>
        `url(/static/gdc/sprites/${position.TEAM}_${position.POSITION}_warrior@4x.png)`,
      backgroundSize: '43.5px 51px',
    },
    '&.speed': {
      backgroundImage: ({ position }) =>
        `url(/static/chicago/sprites/${position.TEAM}_${position.POSITION}_speed@4x.png)`,
      backgroundSize: '39px 51px',
      backgroundPositionX: 'calc(50% - 4.5px)',
      position: 'relative',
    },
    '&.speed.warrior': {
      backgroundImage: ({ position }) =>
        `url(/static/chicago/sprites/${position.TEAM}_${position.POSITION}_speed_warrior@4x.png)`,
      backgroundSize: '52.5px 63px',
      backgroundPositionX: 'calc(50% - 4.5px)',
    },
  },
}));

export default function PlayerIcon({ position }) {
  const settings = useOverlaySettings();
  const classes = useStyles({ position, settings });

  return <PlayerStatusSprite className={classes.icon} position={position} />;
}
