import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { useOverlaySettings } from 'overlay/Settings';
import { BLUE_TEAM, GOLD_TEAM } from 'util/constants';
import StatsSection from './ffvii/StatsSection';
import TeamNameBox from './ffvii/TeamNameBox';
import TeamSection from './ffvii/TeamSection';

const useStyles = makeStyles(theme => ({
  // DRW: Ticker and/or CabinetName should probably be a standalone component.
  // CabinetName currently only supports 5 characters (e.g. "Cab 1")
  cabinetName: {
    backgroundImage: 'linear-gradient(#cc3399, #993399)',
    border: '4px groove white',
    borderRadius: '8px',
    boxShadow: 'inset 0 0 4px #000000aa',
    display: 'flex',
    flexBasis: '50px',
    flexGrow: '0',
    justifyContent: 'center',
    margin: '0 1px',
    overflow: 'hidden',
  },
  headerBar: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'stretch',
    justifyContent: 'center',
    height: '207px',
  },
  teamSection: {
    flexGrow: 1,
    flexBasis: 0,
    overflow: 'hidden',
    textShadow: '2px 2px #212421, 1px 1px #212021',
  },
  ticker: {
    visibility: 'hidden',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'stretch',
    justifyContent: 'center',
    height: '28px',
    width: '375px',
    color: 'white',
    textShadow: '1px 1px #000000',
    fontWeight: 'bold',
    fontFamily: 'Reactor7, sans-serif',
  },
  statsSection: {
    flexGrow: 0,
  },
}));

export default function FFVIIOverlay({}) {
  const classes = useStyles();
  const settings = useOverlaySettings();

  const leftTeam = settings?.goldOnLeft ? GOLD_TEAM : BLUE_TEAM;
  const rightTeam = settings?.goldOnLeft ? BLUE_TEAM : GOLD_TEAM;

  return (
    <>
      <div className={clsx(classes.headerBar, 'header-bar')}>
        <TeamSection
          className={clsx(classes.teamSection, classes.blue, 'team-section', 'team-section-blue')}
          team={BLUE_TEAM}
        />
        <StatsSection className={clsx(classes.statsSection, 'statsSection')} />
        <TeamSection
          className={clsx(classes.teamSection, classes.gold, 'team-section', 'team-section-gold')}
          team={GOLD_TEAM}
        />
      </div>
      <div className={clsx(classes.ticker, 'ticker')}>
        <TeamNameBox className={clsx('team-left')} team={leftTeam} />
        <div className={clsx(classes.cabinetName, 'cabinet-name')}>
          <span>{settings?.cabinet?.displayName}</span>
        </div>
        <TeamNameBox className={clsx('team-right')} team={rightTeam} />
      </div>
    </>
  );
}

FFVIIOverlay.themeProps = {
  name: 'ffvii',
  displayName: 'FF VII',
  description: `Overlay inspired by Final Fantasy VII.`,
  size: { w: 1920, h: 1080 },
  overlayWindow: { w: 1536, h: 216, x: 0, y: 0 },
  gameCaptureWindow: { w: 1536, h: 864, x: 0, y: 216 },
  blueCameraWindow: { w: 320, h: 180, x: 0, y: 0 },
  goldCameraWindow: { w: 320, h: 180, x: 1216, y: 0 },
};
