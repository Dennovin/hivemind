import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Icon from '@mdi/react';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    width: '100%',
    alignItems: 'center',
  },
  icon: {
    verticalAlign: 'middle',
    height: '32px',
    color: 'white',
    margin: '0 8px',
  },
  text: {
    fontFamily: 'Roboto',
    fontSize: '24px',
    color: 'white',
    verticalAlign: 'middle',
  },
}));

export default function TextWithIcon({ path, className, children }) {
  const classes = useStyles();

  return (
    <div className={clsx(classes.container, className)}>
      <Icon className={clsx(classes.icon, 'icon')} path={path} />
      <div className={clsx(classes.text, 'text')}>{children}</div>
    </div>
  );
}
