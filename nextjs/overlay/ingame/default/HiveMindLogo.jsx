import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import StatValue from 'overlay/components/StatValue';

const useStyles = makeStyles(theme => ({
  container: {
    verticalAlign: 'middle',
    marginBottom: '20px',
  },
  image: {
    height: '32px',
    verticalAlign: 'middle',
  },
  text: {
    fontFamily: 'Roboto Slab',
    fontSize: '22px',
    fontWeight: 'bold',
    color: theme.palette.gold.main,
    verticalAlign: 'middle',
    marginLeft: '5px',
  },
}));

export default function HiveMindLogo({ className }) {
  const classes = useStyles();

  return (
    <div className={clsx(classes.container, className, 'hivemind-logo')}>
      <img className={classes.image} src="/static/hivemind.png" />
      <span className={classes.text}>HiveMind</span>
    </div>
  );
}
