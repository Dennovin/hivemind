import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';

import PredictionChart from 'overlay/components/PredictionChart';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
  },
  title: {
    color: 'white',
    textAlign: 'center',
    fontFamily: 'Roboto',
    fontSize: '13px',
  },
}));

export default function PredictionChartBox({ className, ...props }) {
  const classes = useStyles();

  return (
    <div className={clsx(classes.container, className, 'prediction-chart-box')}>
      <div className={clsx(classes.title, 'prediction-chart-title')}>KQuity Win Probability</div>
      <PredictionChart className={clsx(classes.chart, 'prediction-chart')} />
    </div>
  );
}
