import clsx from 'clsx';
import classes from './Chicago-League.module.css';
import Icon from '@mdi/react';
import { mdiCrown, mdiSword } from '@mdi/js';
import { useEffect } from 'react';

export default function MilitaryVictoryAnimation({ winner, finalKill,  flip = false }) {
  let isQueen = false;
  useEffect(() => {
    const finalKiller = finalKill?.killer;
    isQueen = finalKiller == 1 || finalKiller == 2;
  }, [finalKill]);

  const videoSrc = winner === 'blue' ? 'anime-slash.webm' : 'gold-win-anime.webm';

  const defaultAnimation = (<div className={classes.militaryVictory}>
    <div className={classes.milWinCrowns}>
      {[...Array(3)].map((v, i) => (
        <div className={classes.milWinCrown} key={`crown${i}`}>
          <Icon className={clsx(classes.milWinCrownIcon)} path={mdiCrown} />
          <div className={classes.milWinSwordWrap}><div className={classes.milWinSword}><Icon className={clsx(classes.milWinSwordIcon)} path={mdiSword} /></div></div>
        </div>
      ))}
    </div>
  </div>);

  const anime = (<div className={classes.militaryVictoryAnime}>
    <video className={classes.videoPlayer} src={`/static/chicago/videos/${videoSrc}`} autoPlay style={{transform: flip ? 'scaleX(-1)' : 'none'}} />
  </div>)

  return isQueen ? anime : defaultAnimation;
}