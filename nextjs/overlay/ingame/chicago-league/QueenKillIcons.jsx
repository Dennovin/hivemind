import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Icon from '@mdi/react';
import { mdiCrown } from '@mdi/js';
import clsx from 'clsx';

import { BLUE_TEAM, GOLD_TEAM } from 'util/constants';
import QueenKillIconContainer from './QueenKillIconContainer';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    height: '20px',
    width: '68px',
    position: 'absolute',
    bottom: '57px',
    left: '50%',
    transform: 'translateX(-50%)',
    alignItems: 'center',
    justifyContent: 'center',
    // overflow: 'hidden',
    zIndex: 3,

  },
  icon: {
    color: ({ position }) => position.TEAM == GOLD_TEAM ? 'var(--blue)' : 'var(--gold)',
    height: '16px',
    margin: '0 -2px',
    animation: '$queenKillFx 400ms 1 forwards  cubic-bezier(.68,-0.55,.27,1.55)'
  },
  '@keyframes queenKillFx': {
    '0%': {
      opacity: 0,
      transform: "scale(5) rotate(-8deg)"
    },
    '100%': {
      opacity: 1,
      transform: "scale(1) translateY(0) rotate(0)"
    },
  }
}));

export default function QueenKillIcons({ position }) {
  const classes = useStyles({ position });

  return (
    <QueenKillIconContainer
      className={clsx(classes.container, 'queen-kill')}
      position={position}
      element={(<Icon className={classes.icon} path={mdiCrown} />)}
    />
  );
}
