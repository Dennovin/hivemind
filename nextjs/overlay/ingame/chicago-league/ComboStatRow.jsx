import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import StatValue from 'overlay/components/StatValue';
import QueenKillIconContainer from 'overlay/components/QueenKillIconContainer';
import { POSITIONS_BY_TEAM, BLUE_TEAM, GOLD_TEAM, CABINET_POSITIONS } from 'util/constants';
import { redirects } from 'next.config';

const useStyles = makeStyles(theme => ({
  row: {
    display: 'flex',
    flexDirection: 'row',
    borderBottom: '2px solid #ffffff19',
    '&:first-child': {
      borderTop: '2px solid #ffffff19',
    }
  },
  cell: {
    textAlign: 'center',
    padding: '3px 0 1px 0',
    display: 'flex',
    flexDirection: 'row',
    width: '58px',
    justifyContent: 'center',

    position: 'relative',
    // borderWidth: '0 1px 0 0',
    // borderColor: 'gray',
    // borderStyle: 'solid',
    // '&:last-child':{
    //   borderWidth: 0
    // }

  },
  value: {
    gridTemplateColumns: '1fr 1fr',
    gap: '1px',
    color: '#ffffff33',
    fontSize: '18px',
    // '&::before': {
    //   content: '""',
    //   position: 'absolute',
    //   transform: 'translateX(-50%)',
    //   left: '50%',
    //   top: 0,
    //   bottom: 0,
    //   width: 1,
    //   backgroundColor: 'red',
    // }
  },
  valueText: {
    color: 'white',
    fontSize: '18px',
    fontFamily: 'monospace',
    '&.zero': {
      color: '#ffffff3f',
    },
  },
  divider: {
    color: '#888',
    display: 'inline-block',
    padding: '0 3px'
  },
  title: {
    gridTemplateColumns: '1fr',
    width: '120px',
    textTransform: 'uppercase',
    color: '#ffffffaa',
    fontSize: '20px',
    fontFamily: 'Roboto Condensed'
  },
  queenKillContainer: {
    color: 'white',
    marginLeft: '3px',
  },
}));


export default function StatsRow({ className, title, queenKillIcon, statKeys, divider = '/', queenMerge = false, queenDelete = false, colorOnChange, ...props }) {
  const classes = useStyles();
  const isQueen = (pos) => pos === CABINET_POSITIONS.GOLD_QUEEN || pos === CABINET_POSITIONS.BLUE_QUEEN;
  const keysToUse = (pos) => queenMerge && isQueen(pos) ? [statKeys[0]] : statKeys;

  const StatCell = (pos) => {
    const keys = keysToUse(pos);
    return (
      <div key={pos.ID} className={clsx(classes.cell, classes.value, )}>
        {isQueen(pos) && queenDelete ? '--' : (<>
          {keys.map((key, index) =>
            <React.Fragment key={key}>
              <StatValue key={key + pos.ID} statKey={key} position={pos} className={classes.valueText} {...props} colorOnChange={Array.isArray(colorOnChange) ? colorOnChange[index] : colorOnChange} />
              {(index < keys.length - 1 && keys.length > 1) && <span className={classes.divider}>{divider}</span>}
            </React.Fragment>
          )}
        </>)}
      </div>
    )
  }

  return (
    <div className={clsx(classes.row, className)}>

      {[...POSITIONS_BY_TEAM[BLUE_TEAM]].reverse().map(pos => StatCell(pos))}

      <div className={clsx(classes.cell, classes.title)}>{title}</div>

      {[...POSITIONS_BY_TEAM[GOLD_TEAM]].reverse().map(pos => StatCell(pos))}

    </div>
  );
}
