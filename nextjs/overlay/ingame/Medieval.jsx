import clsx from 'clsx';
import Head from 'next/head';

import { useOverlaySettings } from 'overlay/Settings';
import { BLUE_TEAM, GOLD_TEAM, LEFT_TEAM, RIGHT_TEAM } from 'util/constants';
import classes from './manuka/Chicago-League.module.css';

import CurrentTime from 'overlay/components/CurrentTime';
import EventName from 'overlay/components/EventName';
import WarmUpNotice from 'overlay/components/WarmUpNotice';
import Jumbotron from './medieval/Jumbotron';
import JumbotronPredictionMeter from './medieval/JumbotronPredictionMeter';
import PlayersLoggedIn from './medieval/PlayersLoggedIn';
import TeamImages from './medieval/TeamImages';
import TeamSection from './medieval/TeamSection';

export default function Medieval({}) {
  const settings = useOverlaySettings();
  const flipTeams = settings?.isTeamOnLeft(GOLD_TEAM);
  const leftTeam = flipTeams ? GOLD_TEAM : BLUE_TEAM;
  const rightTeam = flipTeams ? BLUE_TEAM : GOLD_TEAM;

  return (
    <>
      <Head>
        {settings?.additionalFonts && <link rel="stylesheet" href={settings.additionalFonts} />}
        <link
          href="https://fonts.googleapis.com/css2?family=MedievalSharp&display=swap"
          rel="stylesheet"
        ></link>
        <style type="text/css">
          {`
           .font-medieval { font-family: 'MedievalSharp', sans-serif; letter-spacing: 2px; }
           .carved-text { color: #898989; text-shadow: 1px 1px 3px black, 1px 1px 0px black, -1px -1px 0px white, -2px -2px 1px rgba(255,255,255,.5); }
           .engraved-text { color: #444; text-shadow: -1px -1px 3px rgba(0,0,0,.8), -1px -1px 0px black, 1px 1px 0px white, 2px 2px 3px rgba(255,255,255,0.5); }
           .shadow-text { text-shadow: 1px 1px 2px black; }
          `}
        </style>
      </Head>

      <div
        className={clsx(
          classes.overlay,
          'ingame-overlay  custom-color',
          flipTeams && classes.flipTeams,
        )}
      >
        <div className={clsx('w-full h-full relative')}>
          <Jumbotron flipTeams={flipTeams} />
          <div id="clock-time-container" className="absolute top-1.5 right-14 ">
            <CurrentTime
              className={clsx(
                'relative text-right flex gap-2',
                'carved-text  font-medieval text-2xl',
              )}
              dateClass=""
              timeFormat="h:mm a"
              dateFormat="MMM dd, yyyy"
              hideDivider
            />
          </div>
          <span
            id="cab-name"
            className="absolute top-1.5 left-12 text-2xl font-medieval carved-text"
          >
            Cab: {settings?.cabinet?.displayName}
          </span>
          <div
            id="event-cab-title"
            className="absolute p-2 left-1 top-10 w-[216px] h-[130px] overflow-clip text-ellipsis  z-50 text-3xl  font-bold font-medieval engraved-text"
          >
            <EventName id="event-title" className="text-left [&>span]:block" split={true} />
          </div>
          <div id="customFrameContainer" className="">
            <div
              id="customFrameVideoWindow"
              className={clsx(
                'absolute z-30 w-[1440px] h-[810px] top-[56px] left-[240px] rounded mix-blend-overlay',
                " before:content-['']",
                'before:rounded-sm before:shadow-[0_0_15px_5px_rgba(25,25,25,.75)] before:mix-blend-overlay before:inset-0 before:absolute before:z-10 before:block',
              )}
            ></div>
          </div>

          <PlayersLoggedIn flipTeams={flipTeams} />
        </div>

        <div
          className={clsx(
            'bottom-bar',
            'absolute bottom-1 w-[1920px] h-[213px] !left-1/2 -translate-x-1/2 z-40',
            'flex justify-center',
          )}
        >
          <TeamSection
            className={clsx('team-section-left', 'absolute left-0 bottom-0 w-1/2 pb-5')}
            team={leftTeam}
            side={LEFT_TEAM}
            key="leftteamsection"
          />

          <TeamSection
            className={clsx(
              'team-section-right',
              'absolute right-0 bottom-0 w-1/2 text-right pb-5',
            )}
            team={rightTeam}
            side={RIGHT_TEAM}
            key="rightteamsection"
          />
        </div>

        <WarmUpNotice
          className={clsx(
            'absolute top-[0px] left-1/2 -translate-x-1/2 w-fit bg-black border-y-4 border-y-current border-solid text-red-600',
            "after:content-[''] after:absolute after:-top-1 after:right-full after:translate-x-1/2 after:w-12 after:bg-black after:block after:-z-10 after:h-[calc(100%+8px)] after:skew-x-[30deg]",
            "before:content-[''] before:absolute before:-top-1 before:left-full before:-translate-x-1/2 before:w-12 before:bg-black before:block before:-z-10 before:h-[calc(100%+8px)] before:skew-x-[-30deg]",
            'before:border-4 before:border-current before:border-solid before:border-l-0',
            'after:border-4 after:border-current after:border-solid after:border-r-0 before:rounded',
            '![border-style:groove] after:![border-style:groove] before:![border-style:groove]',
          )}
        />
        <img
          src="/static/medieval/kq40logo.gif"
          className="absolute bottom-[77px] left-1/2 z-10 -translate-x-1/2"
        ></img>
        <img src="/static/medieval/medieval-ui.webp" className="absolute top-0 left-0 z-[-1]"></img>

        <svg
          className="absolute left-1/2 bottom-[52px] w-[119px] h-[154px] z-[0] mix-blend-multiply -translate-x-1/2  custom-color"
          id="shieldGem"
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 118.57 153.08"
        >
          <path
            d="M0,71.07c.43,11.69,3.82,23.29,10.09,34.45,5.06,9,11.98,17.75,20.58,26.01,12.55,12.05,25.09,19.54,28.61,21.54,3.52-2,16.06-9.5,28.61-21.54,8.6-8.26,15.53-17.01,20.58-26.01,6.27-11.17,9.67-22.76,10.09-34.45V0H0v71.07Z"
            fill="currentColor"
            strokeWidth="0"
          />
        </svg>
        <JumbotronPredictionMeter />
        <TeamImages />
      </div>
    </>
  );
}

Medieval.themeProps = {
  name: 'medieval',
  displayName: 'Medieval',
  description: `Medieval / CRPG inventory inspired overlay`,
  size: { w: 1920, h: 1080 },
  overlayWindow: { w: 1920, h: 1080, x: 0, y: 0 },
  gameCaptureWindow: { w: 1440, h: 810, x: 240, y: 59 },
  blueCameraWindow: { w: 466, h: 155, x: 220, y: 921 },
  goldCameraWindow: { w: 466, h: 155, x: 1234, y: 921 },
};
