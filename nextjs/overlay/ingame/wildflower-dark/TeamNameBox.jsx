import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { BLUE_TEAM, GOLD_TEAM } from 'util/constants';
import { useOverlaySettings } from 'overlay/Settings';
import TeamName from 'overlay/components/TeamName';
import TeamScore from 'overlay/components/TeamScore';
import { useMatch } from 'overlay/Match';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexDirection: 'row',
    borderWidth: '3px',
    borderStyle: 'solid',
    borderColor: ({ team }) => theme.palette[team].dark3,
    borderRadius: '5px',
    boxShadow: ({ team }) => `0 0 15px 3px ${theme.palette[team].light3}7f`,
  },
  section: {
    padding: '0 5px',
    textAlign: 'center',
    fontFamily: 'Oswald',
    fontSize: '28px',
    color: 'white',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    '&:first-of-type': {
      borderLeftWidth: '3px',
    },
  },
  teamName: {
    flexBasis: 0,
    flexGrow: 1,
  },
  score: {
    width: '80px',
    flexBasis: '80px',
    flexGrow: 0,
  },
}));

export default function TeamNameBox({ team, className }) {
  const settings = useOverlaySettings();
  const classes = useStyles({ team, settings });
  const match = useMatch();
  const colorOnChange = { higher: '#aa0', duration: 1200 };
  const score = match?.score[team];

  return (
    <div className={clsx(classes.container, className, 'team-name-container')}>
      <div className={clsx(classes.teamName, classes.section, 'team-name')}>
        <TeamName team={team} />
      </div>
      {match.showScore && (
        <div className={clsx(classes.score, classes.section, 'score', `score-${score}`)}>
          <TeamScore team={team} colorOnChange={colorOnChange} />
        </div>
      )}
    </div>
  );
}
