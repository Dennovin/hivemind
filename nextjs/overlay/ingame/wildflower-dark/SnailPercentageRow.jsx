import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import SnailPercentageValue from 'overlay/components/SnailPercentageValue';
import { POSITIONS_BY_TEAM, BLUE_TEAM, GOLD_TEAM } from 'util/constants';

const useStyles = makeStyles(theme => ({
  row: {
    display: 'flex',
    flexDirection: 'row',
    borderBottom: '2px solid #ffffff19',
    fontFamily: 'Oswald',
    color: '#dfdfdf',
  },
  cell: {
    textAlign: 'center',
    padding: '3px 0 1px 0',
  },
  value: {
    width: '58px',
  },
  valueText: {
    fontSize: '18px',
    '&.zero': {
      color: '#dfdfdf9f',
      fontWeight: '300',
    },
  },
  title: {
    width: '120px',
    textTransform: 'uppercase',
    color: '#dfdfdf',
    fontSize: '20px',
  },
  accurateSnailDistance: {},
  estimatedSnailDistance: {
    display: 'none'
  }
}));

export default function SnailPercentageRow({ className, useEstimatedDistance=false, ...props }) {
  const classes = useStyles();

  let estimatedOrAccurateClassNames = ["accurate-snail-distance", classes.accurateSnailDistance];
  if (useEstimatedDistance) {
    estimatedOrAccurateClassNames = ["estimated-snail-distance", classes.estimatedSnailDistance];
  }

  return (
    <div className={clsx(classes.row, className, estimatedOrAccurateClassNames)}>

      {[...POSITIONS_BY_TEAM[BLUE_TEAM]].reverse().map(pos => (
        <div key={pos.ID} className={clsx(classes.cell, classes.value)}>
          <SnailPercentageValue key={pos.ID} position={pos} className={classes.valueText} useEstimatedDistance={useEstimatedDistance} {...props} />
        </div>
      ))}

      <div className={clsx(classes.cell, classes.title)}>Snail</div>

      {[...POSITIONS_BY_TEAM[GOLD_TEAM]].reverse().map(pos => (
        <div key={pos.ID} className={clsx(classes.cell, classes.value)}>
          <SnailPercentageValue position={pos} className={classes.valueText} useEstimatedDistance={useEstimatedDistance} {...props} />
        </div>
      ))}

    </div>
  );
}
