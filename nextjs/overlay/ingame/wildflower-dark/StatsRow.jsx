import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { POSITIONS_BY_TEAM, BLUE_TEAM, GOLD_TEAM } from 'util/constants';
import StatValue from 'overlay/components/StatValue';
import QueenKillIconContainer from 'overlay/components/QueenKillIconContainer';
import { useOverlaySettings } from 'overlay/Settings';

const useStyles = makeStyles(theme => ({
  row: {
    display: 'flex',
    flexDirection: 'row',
    borderBottom: '2px solid #00000019',
    fontFamily: 'Oswald',
    color: '#dfdfdf',
  },
  cell: {
    textAlign: 'center',
    padding: '3px 0 1px 0',
    display: 'flex',
    flexDirection: 'row',
    width: '58px',
    justifyContent: 'center',
  },
  value: {},
  valueText: {
    fontSize: '18px',
    '&.zero': {
      color: '#dfdfdf9f',
      fontWeight: '300',
    },
  },
  title: {
    width: '120px',
    textTransform: 'uppercase',
    fontSize: '20px',
  },
  queenKillContainer: {
    color: 'white',
    '&:not(:empty)': {
      marginLeft: '3px',
    },
  },
}));

export default function StatsRow({ className, title, queenKillIcon, ...props }) {
  const classes = useStyles();
  const settings = useOverlaySettings();
  const leftTeam = settings?.goldOnLeft ? GOLD_TEAM : BLUE_TEAM;
  const rightTeam = settings?.goldOnLeft ? BLUE_TEAM : GOLD_TEAM;

  return (
    <div className={clsx(classes.row, className)}>
      {[...POSITIONS_BY_TEAM[leftTeam]].reverse().map(pos => (
        <div key={pos.ID} className={clsx(classes.cell, classes.value)}>
          <StatValue position={pos} className={classes.valueText} {...props} />
          {queenKillIcon && (
            <QueenKillIconContainer
              position={pos}
              className={classes.queenKillContainer}
              element={queenKillIcon}
            />
          )}
        </div>
      ))}

      <div className={clsx(classes.cell, classes.title)}>{title}</div>

      {[...POSITIONS_BY_TEAM[rightTeam]].reverse().map(pos => (
        <div key={pos.ID} className={clsx(classes.cell, classes.value)}>
          <StatValue position={pos} className={classes.valueText} {...props} />
          {queenKillIcon && (
            <QueenKillIconContainer
              position={pos}
              className={classes.queenKillContainer}
              element={queenKillIcon}
            />
          )}
        </div>
      ))}
    </div>
  );
}
