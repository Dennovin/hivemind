import { Grid } from '@material-ui/core';
import clsx from 'clsx';
import Head from 'next/head';

import { useOverlaySettings } from 'overlay/Settings';
import { BLUE_TEAM, GOLD_TEAM, LEFT_TEAM, RIGHT_TEAM } from 'util/constants';
import { useWebSocket } from 'util/websocket';
import classes from './chicago-league/Chicago-League.module.css';

import CabinetName from 'overlay/components/CabinetName';
import CurrentTime from 'overlay/components/CurrentTime';
import EventName from 'overlay/components/EventName';
import { useGameStats } from 'overlay/GameStats';
import KillFeed from './chicago-league/KillFeed';
import PlayersLoggedIn from './chicago-league/PlayersLoggedIn';
import StatsSection from './chicago-league/StatsSection';
import TeamSection from './chicago-league/TeamSection';
import HiveMindLogo from './default/HiveMindLogo';

let gameEventClass = '';
let winCondition = '';
let winningTeam = '';
let finalKill = null;

export default function ChicagoLeague({}) {
  const settings = useOverlaySettings();
  const flipTeams = settings?.isTeamOnLeft(GOLD_TEAM);
  const leftTeam = flipTeams ? GOLD_TEAM : BLUE_TEAM;
  const rightTeam = flipTeams ? BLUE_TEAM : GOLD_TEAM;
  const ws = useWebSocket('/ws/gamestate');
  const stats = useGameStats();

  ws.onJsonMessage(message => {
    if (message.cabinetId != settings?.cabinet?.id) {
      return;
    }
    gameEventClass = message.type;
    winCondition = message.winCondition;
    winningTeam = message.winningTeam;
    finalKill = stats?.finalKill;
  });

  return (
    <>
      <Head>
        <link rel="stylesheet" href="https://use.typekit.net/tue0thi.css" />
      </Head>

      <div
        className={clsx(
          classes.overlay,
          'ingame-overlay',
          flipTeams && classes.flipTeams,
          classes[gameEventClass],
          classes['win-' + winCondition],
          `win-${winCondition}`,
          `winner-${winningTeam}`,
          classes['winner-' + winningTeam],
        )}
        data-win={`${winCondition}`}
      >
        <div className={clsx(classes.sidebar, 'sidebar')}>
          <Grid container>
            <Grid item xs={5}>
              {/* <img className={classes.logan} src="/static/chicago/logan-arcade-logo.png" /> */}
            </Grid>
            <Grid item xs={7}>
              <CurrentTime className={classes.dateTime} />
              <HiveMindLogo className={classes.hivemindLogo} />
            </Grid>
          </Grid>

          <div className={clsx(classes.bottomInfoBox, 'bottom-info-box')}>
            <PlayersLoggedIn flipTeams={flipTeams} />
            <div className={classes.bottomInfo}>
              <EventName className={classes.eventName} />
              <CabinetName className={classes.cabName} />
            </div>
          </div>
        </div>

        <div className={clsx(classes.bottombar, 'bottom-bar')}>
          <KillFeed flipTeams={flipTeams} />
          <TeamSection
            className={clsx(classes.teamSection, classes.leftTeam)}
            team={leftTeam}
            side={LEFT_TEAM}
          />

          <StatsSection className={classes.statsSection} />

          <TeamSection
            className={clsx(classes.teamSection, classes.rightTeam)}
            team={rightTeam}
            side={RIGHT_TEAM}
          />
        </div>
      </div>
    </>
  );
}

ChicagoLeague.themeProps = {
  name: 'kqchi',
  displayName: 'Chicago League',
  description: `Chicago League Night default theme.`,
  size: { w: 1920, h: 1080 },
  overlayWindow: { w: 1920, h: 1080, x: 0, y: 0 },
  gameCaptureWindow: { w: 1552, h: 873, x: 0, y: 0 },
  blueCameraWindow: { w: 420, h: 172, x: 0, y: 903 },
  goldCameraWindow: { w: 420, h: 172, x: 1137, y: 903 },
};
