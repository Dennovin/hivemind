import React, { useState } from 'react';
import { format } from 'date-fns';
import { BLUE_TEAM, GOLD_TEAM } from 'util/constants';
import { formatUTC } from 'util/dates';
import { useOverlaySettings } from 'overlay/Settings';
import { useSignIn } from 'overlay/SignIn';
import { useMatch } from 'overlay/Match';
import { useGameStats } from 'overlay/GameStats';

const CABINET_POSITIONS = {
  GOLD_QUEEN: { id: 1, side: 'gold', position: 'queen' },
  BLUE_QUEEN: { id: 2, side: 'blue', position: 'queen' },
  GOLD_STRIPES: { id: 3, side: 'gold', position: 'stripes' },
  BLUE_STRIPES: { id: 4, side: 'blue', position: 'stripes' },
  GOLD_ABS: { id: 5, side: 'gold', position: 'abs' },
  BLUE_ABS: { id: 6, side: 'blue', position: 'abs' },
  GOLD_SKULLS: { id: 7, side: 'gold', position: 'skulls' },
  BLUE_SKULLS: { id: 8, side: 'blue', position: 'skulls' },
  GOLD_CHECKS: { id: 9, side: 'gold', position: 'checks' },
  BLUE_CHECKS: { id: 10, side: 'blue', position: 'checks' },
};

export default function CssOverride({}) {
  const settings = useOverlaySettings();
  const signIn = useSignIn();
  const match = useMatch();
  const stats = useGameStats();

  const [currentTime, setCurrentTime] = useState(null);

  if (currentTime === null) {
    setCurrentTime(new Date());
    setInterval(() => setCurrentTime(new Date()), 1000);
  }

  const leftTeam = settings?.goldOnLeft ? GOLD_TEAM : BLUE_TEAM;
  const rightTeam = settings?.goldOnLeft ? BLUE_TEAM : GOLD_TEAM;

  const matchStats = {
    teamNameLeft: match.teamName[leftTeam] ?? '',
    teamScoreLeft: match.score[leftTeam] ?? '',
    teamNameRight: match.teamName[rightTeam] ?? '',
    teamScoreRight: match.score[rightTeam] ?? '',
    gameTime: formatUTC((stats?.gameTime ?? 0) * 1000, 'm:ss'),
    berriesRemaining: stats?.berriesRemaining ?? 0,
    roundName: match?.roundName ?? '',
    cabinetName: settings?.cabinet?.displayName ?? '',
    sceneName: settings?.cabinet?.scene?.displayName ?? '',
  };

  const positionStats = Object.values(CABINET_POSITIONS).map(position => {
    let signedInName = signIn[position.id];
    if (signedInName !== null && signedInName == '') {
      signedInName = '(no name set)';
    }

    return {
      id: position.id,
      classNameIdentifier: `${position.side}-${position.position}`,
      signedInName,
      hasSpeed: stats?.speed.some(pos => parseInt(pos) == position.id) ?? false,
      hasWarrior: stats?.warriors.some(pos => parseInt(pos) == position.id) ?? false,
      queenKills: stats?.queenKills[position.id] ?? 0,
      kills: stats?.kills[position.id] ?? 0,
      deaths: stats?.deaths[position.id] ?? 0,
      snailDistance: stats?.snailDistance[position.id] ?? 0,
      snailDistanceInMeters: Math.floor((stats?.snailDistance[position.id] ?? 0) / 21),
      berries: stats?.totalBerries[position.id] ?? 0,
    };
  });

  return (
    <>
      <div className="team-name team-name-left">{matchStats.teamNameLeft}</div>
      <div
        className={`team-score-numerical team-score-numerical-left score-${matchStats.teamScoreLeft}`}
      >
        {matchStats.teamScoreLeft}
      </div>
      <div className={`team-score team-score-left score-${matchStats.teamScoreLeft}`}>
        {[1, 2, 3, 4, 5].map(
          teamScore =>
            matchStats.teamScoreLeft >= teamScore && (
              <div
                className={`team-score-graphic team-score-graphic-left team-score-graphic-left-${teamScore}`}
              />
            ),
        )}
      </div>

      <div className="team-name team-name-right">{matchStats.teamNameRight}</div>
      <div
        className={`team-score-numerical team-score-numerical-right score-${matchStats.teamScoreRight}`}
      >
        {matchStats.teamScoreRight}
      </div>
      <div className={`team-score team-score-right score-${matchStats.teamScoreRight}`}>
        {[1, 2, 3, 4, 5].map(
          teamScore =>
            matchStats.teamScoreRight >= teamScore && (
              <div
                className={`team-score-graphic team-score-graphic-right team-score-graphic-left-${teamScore}`}
              />
            ),
        )}
      </div>

      <div className="panel panel-left" />
      <div className="panel panel-right" />

      <div className="hivemind-logo" />
      <div className="game-duration">{matchStats.gameTime}</div>
      <div className="game-berries-remaining">{matchStats.berriesRemaining}</div>

      {positionStats.map(positionStat => (
        <div className={`player ${positionStat.classNameIdentifier}`}>
          <div className={`graphic ${positionStat.classNameIdentifier}-graphic`} />
          {positionStat.hasSpeed && (
            <div className={`speed-graphic ${positionStat.classNameIdentifier}-speed-graphic`} />
          )}
          {positionStat.hasWarrior && (
            <div
              className={`warrior-graphic ${positionStat.classNameIdentifier}-warrior-graphic`}
            />
          )}
          <div
            className={`queen-kill-container ${positionStat.classNameIdentifier}-queen-kill-container`}
          >
            {[1, 2, 3, 4, 5].map(
              queenKillCount =>
                positionStat.queenKills >= queenKillCount && (
                  <div
                    className={`queen-kill-graphic ${positionStat.classNameIdentifier}-queen-kill-graphic-${queenKillCount}`}
                  />
                ),
            )}
          </div>
          <div className={`kills stat-container ${positionStat.classNameIdentifier}-kills`}>
            {positionStat.kills}
          </div>
          <div className={`deaths stat-container ${positionStat.classNameIdentifier}-deaths`}>
            {positionStat.deaths}
          </div>
          <div
            className={`snail-distance stat-container ${positionStat.classNameIdentifier}-snail-distance`}
          >
            {positionStat.snailDistance}
          </div>
          <div
            className={`snail-distance-meters stat-container ${positionStat.classNameIdentifier}-snail-distance-meters`}
          >
            {positionStat.snailDistanceInMeters}
          </div>
          <div className={`berries stat-container ${positionStat.classNameIdentifier}-berries`}>
            {positionStat.berries}
          </div>
          <div className={`signed-in-name ${positionStat.classNameIdentifier}-signed-in-name`}>
            {positionStat.signedInName}
          </div>
        </div>
      ))}

      <div className="panel panel-bottom" />
      <div className="round-name">{matchStats.roundName}</div>
      <div className="cabinet-name">{matchStats.cabinetName}</div>
      <div className="scene-name">{matchStats.sceneName}</div>
      {currentTime !== null && (
        <div className="current-date">{format(currentTime, 'd MMM yyyy')}</div>
      )}
      {currentTime !== null && <div className="current-time">{format(currentTime, 'h:mm a')}</div>}
      <div className="prediction" data-prediction={matchStats.prediction} />
    </>
  );
}

CssOverride.themeProps = {
  name: 'CSS Override',
  displayName: 'HiveMind CSS Override',
  description: `HiveMind's CSS Override theme.`,
  size: { w: 1920, h: 1080 },
  overlayWindow: { w: 1920, h: 1080, x: 0, y: 0 },
  gameCaptureWindow: { w: 1600, h: 900, x: 160, y: 0 },
  blueCameraWindow: { w: 320, h: 180, x: 0, y: 900 },
  goldCameraWindow: { w: 320, h: 180, x: 1600, y: 900 },
};
