import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { useOverlaySettings } from 'overlay/Settings';
import { BLUE_TEAM, GOLD_TEAM } from 'util/constants';
import CurrentTime from '../components/CurrentTime';
import EventName from '../components/EventName';
import Bb5StyleHeader from './bb5/Bb5StyleHeader';
import StatsSection from './bb5/StatsSection';
import TeamSection from './bb5/TeamSection';

const useStyles = makeStyles(theme => ({
  // DRW: Ticker and/or CabinetName should probably be a standalone component.
  // CabinetName currently only supports 5 characters (e.g. "Cab 1")
  cabinetName: {
    backgroundImage: 'linear-gradient(#cc3399, #993399)',
    border: '4px groove white',
    borderRadius: '8px',
    boxShadow: 'inset 0 0 4px #000000aa',
    display: 'flex',
    flexBasis: '50px',
    flexGrow: '0',
    justifyContent: 'center',
    margin: '0 1px',
    overflow: 'hidden',
  },
  headerBar: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'stretch',
    justifyContent: 'center',
    height: '207px',
  },
  teamSection: {
    flexGrow: 1,
    flexBasis: 0,
    overflow: 'hidden',
  },
  // TODO: Update the ticker if DRay is gonna use it
  ticker: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'stretch',
    justifyContent: 'center',
    height: '28px',
    width: '375px',
    color: 'white',
    textShadow: '1px 1px #000000',
    fontWeight: 'bold',
    fontFamily: 'Reactor7, sans-serif',
  },
  statsSection: {
    flexGrow: 0,
  },
}));

export default function Bb5({}) {
  const classes = useStyles();
  const settings = useOverlaySettings();

  const leftTeam = settings?.goldOnLeft ? GOLD_TEAM : BLUE_TEAM;
  const rightTeam = settings?.goldOnLeft ? BLUE_TEAM : GOLD_TEAM;
  return (
    <>
      <Bb5StyleHeader />

      <div class="absolute w-[1920px] aspect-video grid grid-cols-[1544px,376px]">
        <div className="gameside max-w-[1544px]">
          <div
            className={clsx(
              classes.headerBar,
              'header-bar grid grid-cols-[492px,558px,492px] gap-px',
            )}
          >
            <TeamSection
              className={clsx(
                classes.teamSection,
                classes.blue,
                'team-section',
                'team-section-blue',
              )}
              team={BLUE_TEAM}
            />
            <StatsSection className={clsx(classes.statsSection, 'statsSection')} />
            <TeamSection
              className={clsx(
                classes.teamSection,
                classes.gold,
                'team-section',
                'team-section-gold',
              )}
              team={GOLD_TEAM}
            />
          </div>
          <div className="border-4 border-solid border-transparent rounded-lg aspect-video">
            <img src="/static/maps/day.png" class="aspect-video block w-full opacity-0" />
          </div>
        </div>
        <div className="sidebar flex flex-col gap-4 p-3 pt-0">
          <div className="branding grid place-content-center relative">
            <img src="/static/bb5/bb5-logo.svg" class="w-[375px] h-[191px] mt-3 -mb-3" />
            <div
              id="clock-time-container"
              className="bb-sans absolute -bottom-1 -ml-1 text-center left-1/2 -translate-x-1/2 "
            >
              <CurrentTime
                className={clsx(
                  'relative text-center flex gap-2',
                  ' text-md w-full whitespace-nowrap',
                )}
                dateClass=""
                timeFormat="h:mm a"
                dateFormat="MMM dd, yyyy"
                hideDivider
              />
            </div>
          </div>

          <div className="commentator-box opacity-0 aspect-video bb5-border rounded-2xl bg-black"></div>

          <div className="rounded-2xl flex-grow flex flex-col justify-end p-4"></div>
          <div className="flex-grow grid place-content-center"></div>
          <div className="cab-info relative flex-shrink-0 min-h-16 flex items-center justify-end gap-4">
            <div id="event-cab-title" className="overflow-clip text-ellipsis z-50 text-right">
              <EventName
                id="event-title"
                className="text-right bb5-stat-font text-xl font-semibold leading-none"
                split={false}
              />
            </div>
            <img
              class="w-[54px] h-16 "
              src={`/static/bb5/cab${settings?.cabinet.name?.slice(-1)}.svg`}
            />
          </div>
        </div>
      </div>
    </>
  );
}

Bb5.themeProps = {
  name: 'bb5',
  displayName: 'BB5 Theme',
  description: `Colorful, cake themed overlay.`,
  size: { w: 1920, h: 1080 },
  overlayWindow: { w: 1536, h: 216, x: 0, y: 0 },
  gameCaptureWindow: { w: 1536, h: 864, x: 0, y: 216 },
  blueCameraWindow: { w: 320, h: 180, x: 0, y: 0 },
  goldCameraWindow: { w: 320, h: 180, x: 1216, y: 0 },
};
