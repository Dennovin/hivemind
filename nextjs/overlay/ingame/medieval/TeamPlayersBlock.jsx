import clsx from 'clsx';
import React from 'react';

import PlayerName from 'overlay/components/PlayerName';
import { BLUE_TEAM, POSITIONS_BY_TEAM } from 'util/constants';
import QueenKillIcons from './QueenKillIcons';
import ComboStatsRow from './ComboStatRow';
export default function TeamBlock({ team, side, className, queenKillIcon }) {
  return (
    <div
      id={`team-players-block-${team}`}
      className={clsx(
        'team-players-block space-y-px max-w-[218px] overflow-hidden pt-3 ',
        className,
      )}
    >
      {POSITIONS_BY_TEAM[team].map(pos => (
        <React.Fragment key={'teamplayers-block-' + team + pos.NAME}>
          <div
            className={clsx('player-block relative w-[216px] flex')}
            style={{
              backgroundImage: 'url(/static/medieval/player-block-ui.webp)',
              backgroundRepeat: 'no-repeat',
            }}
          >
            <div
              className={clsx(
                'player-block-sprite',
                'padding-1 relative',
                'size-[56px] min-w-[56px] max-h-[56px] basis-[56px] flex-shrink-0 flex-grow-0 aspect-square',
              )}
            >
              {/* <PlayerSprite position={pos} /> */}
              <div
                className={clsx(
                  'w-[47px] h-[48px] relative grid place-content-center  mt-1 ml-1 ',
                  team === BLUE_TEAM ? 'bg-blue-dark4' : 'bg-gold-dark5/25',
                )}
              >
                <img
                  src={`/static/medieval/${pos.POSITION}.webp`}
                  className="object-contain h-[60%] w-[60%] absolute left-1/2 top-1/2 -translate-x-1/2 -translate-y-1/2"
                />
              </div>
            </div>
            <div className="flex-grow">
              <div
                className={clsx(
                  'player-block-name-row h-[28px] max-h-[28px] flex items-center px-1.5',
                  'py-0.5',
                )}
              >
                <div className="queen-kills-container relative">
                  <QueenKillIcons position={pos} side={side} />
                </div>

                <PlayerName
                  className={clsx(
                    'player-name  leading-7 font-medieval font-bold shadow-text text-white ',
                  )}
                  position={pos}
                />
              </div>
              <div
                className={clsx(
                  'player-block-stats',
                  'border-2 border-transparent border-solid',
                  'grid grid-cols-3 gap-1 h-[28px]',
                )}
              >
                <ComboStatsRow pos={pos} team={team}></ComboStatsRow>
              </div>
            </div>
          </div>
        </React.Fragment>
      ))}
    </div>
  );
}
