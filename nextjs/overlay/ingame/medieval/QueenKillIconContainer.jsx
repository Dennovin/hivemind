import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { useGameStats } from 'overlay/GameStats';

const useStyles = makeStyles(theme => ({
  hattrick: {
    height: '41px',
    width: '50px',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    backgroundImage: `url(/static/medieval/jester-hat-trick.gif)`,
    animation: '$spinin 1s forwards 1 ease-in-out, huerotatefull 1s infinite',
    position: 'relative',
    margin: '0 auto',

    left: '-65px',
    top: '2px',
  },
  crown: {
    margin: '0 12px 0 0',
    position: 'relative',
    left: '-62px',
    top: '19px',
  },
  '@keyframes spinin': {
    '0%': {
      opacity: 0,
      transform: 'rotate(-420deg) scale(0)',
    },
    '97%': {
      opacity: 1,
      transform: ' rotate(12deg) scale(1.1)',
    },
    '100%': {
      opacity: 1,
      transform: 'rotate(0deg) scale(1)',
    },
  },
}));

export default function QueenKillIconContainer({ className, position, element, side }) {
  const stats = useGameStats();
  const numKills = parseInt(stats?.queenKills[position.ID]) ?? 0;
  const styles = useStyles({ position, side });
  return (
    <div className={className}>
      {numKills > 0 &&
        numKills < 3 &&
        [...Array(numKills)].map((i, idx) => (
          <div className={clsx(styles.crown, 'queen-kill-crown')} key={idx}>
            {element}
          </div>
        ))}
      {numKills === 3 && <div className={clsx(styles.hattrick, 'hat-trick ')}></div>}
    </div>
  );
}
