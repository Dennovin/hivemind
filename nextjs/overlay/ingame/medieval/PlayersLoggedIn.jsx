import clsx from 'clsx';
import { BLUE_TEAM, GOLD_TEAM, LEFT_TEAM, RIGHT_TEAM } from 'util/constants';
import classes from '../manuka/Chicago-League.module.css';
import TeamPlayersBlock from './TeamPlayersBlock';

export default function PlayersLoggedIn({ flipTeams, className }) {
  return (
    <div
      id="stats-container"
      className={clsx(classes.players, 'players absolute bottom-1  inset-x-1 ')}
    >
      <div
        id="team-players-container"
        className={clsx('flex justify-between ', flipTeams ? 'flex-row-reverse' : 'flex-row')}
      >
        <TeamPlayersBlock team={BLUE_TEAM} side={flipTeams ? RIGHT_TEAM : LEFT_TEAM} />

        <TeamPlayersBlock team={GOLD_TEAM} side={flipTeams ? LEFT_TEAM : RIGHT_TEAM} />
      </div>
    </div>
  );
}
