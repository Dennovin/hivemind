import clsx from 'clsx';
import { useMatch } from 'overlay/Match';
import TeamName from 'overlay/components/TeamName';
import TeamScoreCounter from 'overlay/components/TeamScoreCounter';
import WebcamPlayerIcons from 'overlay/components/WebcamPlayerIcons';
import { BLUE_TEAM, LEFT_TEAM } from 'util/constants';
import classes from './medieval.module.css';

const markerClass = 'peer mx-[0px]  w-[48px] h-[66px]';
export const MarkerFilled = ({ type, team }) => {
  const wintype = type ? `${type}` : `military`;
  return (
    <div className={clsx(`${team}-score-marker-filled bg-contain `, markerClass)}>
      <img src={`/static/medieval/score-${team}-${wintype}.webp`} />
    </div>
  );
};
export const MarkerEmpty = ({ team }) => (
  <div className={clsx(`${team}-score-marker-empty `, markerClass)}>
    <img src={`/static/medieval/score-empty.webp`} />
  </div>
);

export default function TeamSection({ team, side, className }) {
  const match = useMatch();

  return (
    <div
      className={clsx(
        'team-info-container',
        'team-info-container-' + team,
        'h-[213px] relative flex items-end',
        className,
      )}
    >
      <div
        className={clsx(
          `team-score-container team-score-container-${team}`,
          'absolute top-3 z-10 flex justify-start min-h-[60px]',
          '',

          side === LEFT_TEAM
            ? 'pr-[70px] pl-[2px] right-0  flex-row-reverse '
            : 'pl-[70px] pr-[2px] left-0 flex-row ',
        )}
      >
        <TeamScoreCounter team={team} markerFilled={MarkerFilled} markerEmpty={MarkerEmpty} />
      </div>

      <div
        className={clsx(
          'team-name-container',
          'team-name-container-' + team,
          'absolute top-3 px-3 py-1.5',
          'flex items-center',
          'w-[460px] z-10',
          'text-3xl  font-medieval',
          side === LEFT_TEAM ? 'left-[223px]' : 'right-[223px] flex-row-reverse',
          // classes.ridgeBorder,
        )}
      >
        <TeamName
          team={team}
          className={clsx(
            `team-name team-name-${team}`,
            'relative z-10 block overflow-clip text-ellipsis whitespace-nowrap',
            classes.carvedText,
            team === BLUE_TEAM ? 'text-blue-main' : 'text-gold-dark3',
          )}
        />
      </div>
      <div
        className={clsx(
          'absolute bottom-1  w-[460px] h-[148px]  flex items-end',
          side === LEFT_TEAM ? 'left-[223px]' : 'right-[223px]',
        )}
      >
        <WebcamPlayerIcons
          team={team}
          className={clsx('w-full', side === LEFT_TEAM ? 'left-0' : 'right-0')}
        />
      </div>
    </div>
  );
}
