import GameTime from 'overlay/components/GameTime';
import QueenEggTracker from '../manuka/QueenEggTracker';

export default function Jumbotron({ flipTeams }) {
  return (
    <div
      id="jumbotron"
      className="general-game-stats absolute z-50 top-3 left-1/2 -translate-x-1/2 flex flex-col justify-center items-center"
    >
      <div>
        <div className="game-time tracking-tight pt-3 pb-4 px-1 flex h-6 w-[206px] text-center items-center justify-between gap-1 opacity-75 mix-blend-screen text-white engraved-text ">
          <span className="skew-x-[18deg]">
            <QueenEggTracker queenID={flipTeams ? 2 : 1} />
          </span>
          <GameTime className="relative top-0.5 text-3xl font-medieval text-white engraved-text " />
          <span className="skew-x-[-18deg]">
            <QueenEggTracker queenID={flipTeams ? 1 : 2} className="-scale-x-100" />
          </span>
        </div>
      </div>
    </div>
  );
}
