import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { BLUE_TEAM, GOLD_TEAM, POSITIONS_BY_TEAM } from 'util/constants';
import PlayerIcon from '../wildflower/PlayerIcon';
import QueenKillIcons from './QueenKillIcons';
import { useOverlaySettings } from 'overlay/Settings';

const useStyles = makeStyles(theme => ({
  logo: {
    width: '65px',
    height: '55px',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center center',
    backgroundSize: '65px 55px',
    backgroundImage: 'url(/static/chicago/chicago-starhive@2x.png)',
    flexGrow: 1,
    marginTop: -6,
  },
  playerIcon: {
    position: 'relative',


  },
}));

export default function PlayerIconsRow({ className }) {
  const classes = useStyles();
  const settings = useOverlaySettings();
  const leftTeam = settings?.goldOnLeft ? GOLD_TEAM : BLUE_TEAM;
  const rightTeam = settings?.goldOnLeft ? BLUE_TEAM : GOLD_TEAM;

  return (
    <div className={clsx(classes.row, className)}>

      {[...POSITIONS_BY_TEAM[leftTeam]].reverse().map(pos => (
        <div key={pos.ID} className={clsx(classes.icon, classes.playerIcon, 'left-team')}>
          <QueenKillIcons position={pos} className={clsx(classes.queenKills)} />
          <PlayerIcon position={pos} />
        </div>
      ))}

      <div className={classes.logo} />

      {[...POSITIONS_BY_TEAM[rightTeam]].reverse().map(pos => (
        <div key={pos.ID} className={clsx(classes.icon, classes.playerIcon, 'right-team')}>
          <QueenKillIcons position={pos} className={clsx(classes.queenKills)} />
          <PlayerIcon position={pos} />
        </div>
      ))}

    </div>
  );
}
