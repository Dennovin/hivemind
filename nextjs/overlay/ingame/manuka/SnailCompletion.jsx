import Icon from '@mdi/react';
import clsx from 'clsx';
import SnailPercentageValue from 'overlay/components/SnailPercentageValue';
import { useState } from 'react';
import { STAT_ICON_PATHS } from 'util/constants';
export default function SnailCompletion({ ...props }) {
  const [snailDirection, setSnailDirection] = useState('right');
  return (
    <div className="flex items-center">
      <Icon
        path={STAT_ICON_PATHS['snailDistance']}
        className={clsx('h-5 text-gray-900 mr-px', { '-scale-x-100': snailDirection === 'left' })}
      />
      <SnailPercentageValue
        className="font-stat-numbers-basic text-lg font-medium whitespace-nowrap relative top-px"
        useEstimatedDistance={true}
        format={val => val || '—'}
        directionCb={val => setSnailDirection(val)}
      />
    </div>
  );
}
