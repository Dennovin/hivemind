import clsx from 'clsx';
import React from 'react';

import PlayerName from 'overlay/components/PlayerName';
import { BLUE_TEAM, GOLD_TEAM, LEFT_TEAM, POSITIONS_BY_TEAM, RIGHT_TEAM } from 'util/constants';
import ComboStatsRow from './ComboStatRow';
import PlayerSprite from './PlayerSprite';
import QueenKillIcons from './QueenKillIcons';
export default function TeamBlock({ team, side, className, queenKillIcon }) {
  return (
    <div
      id={`team-players-block-${team}`}
      className={clsx('team-players-block space-y-3 max-w-[240px] overflow-hidden pt-3', className)}
    >
      {POSITIONS_BY_TEAM[team].map(pos => (
        <React.Fragment key={'teamplayers-block-' + team + pos.NAME}>
          <div className={clsx('player-block relative w-[240px]')}>
            <div
              className={clsx(
                'player-block-name-row',
                side === LEFT_TEAM ? 'pl-16 pr-8 bg-gradient-to-r' : 'pr-10 pl-8 bg-gradient-to-l',
                team === 'blue'
                  ? ' from-blue-dark4 to-blue-main'
                  : ' from-yellow-900 to-yellow-600',
              )}
            >
              <div className="queen-kills-container relative">
                <QueenKillIcons position={pos} side={side} />
              </div>

              <PlayerName
                className={clsx('player-name brightness-2 leading-7 font-player-names uppercase')}
                position={pos}
              />
            </div>
            <div
              className={clsx(
                'player-block-stats',
                'grid grid-cols-3 gap-1 py-2',
                side === LEFT_TEAM ? 'pl-16 pr-5 bg-gradient-to-r' : 'pr-10 pl-8 bg-gradient-to-l',
                team === 'blue' ? ' from-blue-dark5 to-blue-dark1' : ' from-amber-950 to-amber-700',
              )}
            >
              <ComboStatsRow pos={pos} team={team}></ComboStatsRow>
            </div>
            <div
              className={clsx(
                'player-block-sprite',
                'absolute z-10  top-1/3 -translate-y-1/2',
                side === LEFT_TEAM ? 'left-[-32px]' : 'right-[-32px]',
                {
                  '-scale-x-100':
                    (team === BLUE_TEAM && side === RIGHT_TEAM) ||
                    (team === GOLD_TEAM && side === LEFT_TEAM),
                },
              )}
            >
              <PlayerSprite position={pos} />
            </div>
          </div>
        </React.Fragment>
      ))}
    </div>
  );
}
