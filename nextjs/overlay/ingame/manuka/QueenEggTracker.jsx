import clsx from 'clsx';
import { useGameStats } from 'overlay/GameStats';

export default function QueenEggTracker({ queenID, className }) {
  const stats = useGameStats();
  const deaths = stats?.deaths[queenID] || 0;
  return (
    <div className={clsx('egg-tracker flex items-center', className)}>
      <svg
        id="Layer_2"
        data-name="Layer 2"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 11.58 17"
        height="28"
        width="24"
      >
        <g stroke="#000" strokeWidth="1" fill="transparent">
          <path
            d="m5.84,4.33c0,1.92-1.13,2.63-2.52,2.63s-2.52-.71-2.52-2.63S1.92,0,3.32,0s2.52,2.41,2.52,4.33Z"
            fill={deaths === 0 ? 'currentColor' : 'transparent'}
          />
          <path
            d="m5.05,13.68c0,1.92-1.13,2.63-2.52,2.63s-2.52-.71-2.52-2.63,1.13-4.33,2.52-4.33,2.52,2.41,2.52,4.33Z"
            fill={deaths <= 1 ? 'currentColor' : 'transparent'}
          />
          <path
            d="m11.58,9.56c0,1.92-1.13,2.63-2.52,2.63s-2.52-.71-2.52-2.63,1.13-4.33,2.52-4.33,2.52,2.41,2.52,4.33Z"
            fill={deaths <= 2 ? 'currentColor' : 'transparent'}
          />
        </g>
      </svg>
    </div>
  );
}
