import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { BLUE_TEAM, GOLD_TEAM, POSITIONS_BY_TEAM } from 'util/constants';
import QueenKillIcons from '../default/QueenKillIcons';
import StatusIcons from '../default/StatusIcons';
import KillsDeaths from '../default/KillsDeaths';

const useStyles = makeStyles(theme => ({
  block: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-end',
    // position: 'absolute',
    // top: '0',
    left: '0',
    width: '160px',
    // height: '880px',
    backgroundColor: 'green',
    marginTop: 'auto'
  },
  position: {
    display: 'flex',
    flexDirection: ({ team }) => (team == GOLD_TEAM ? 'row-reverse' : 'row'),
    margin: '6px 10px',
    alignItems: 'center',
    position: 'relative',
  },
  icon: {
    height: '30px',
    width: '30px',
    margin: '0 5px',
  },
}));

export default function PlayerStats({ team }) {
  const classes = useStyles({ team });

  return (
    <div className={clsx(classes.block, 'player-stats')}>
      {POSITIONS_BY_TEAM[team].map(pos => (
        <div className={clsx(classes.position, 'position', `player-stats-${pos.POSITION}`)} key={pos.ID}>
          <img className={classes.icon} src={pos.ICON} />
          <QueenKillIcons position={pos} />
          <StatusIcons position={pos} />
          <KillsDeaths position={pos} />
        </div>
      ))}
    </div>
  );
}
