import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { useMatch } from '@/overlay/Match';
import GameTime from 'overlay/components/GameTime';
import PlayerIconsRow from './PlayerIconsRow';
import SnailPercentageRow from './SnailPercentageRow';
import StatsRow from './StatsRow';

const useStyles = makeStyles(theme => ({
  row: {
    display: 'flex',
    flexDirection: 'row',
  },
  container: {
    background: ' var(--bb5-stats-bg) padding-box, var(--bb5-border-gradient)',
    borderRadius: '8px',
    border: '4px solid transparent',
    color: '#000',
    fontSize: '22px',
    fontWeight: 'bold',
  },
  warmup: {
    fontSize: '96px',
    textAlign: 'center',
  },
  queenKillIcon: {
    color: '#0BFF06',
  },
}));

export default function StatsSection({ className }) {
  const classes = useStyles();
  const match = useMatch();

  if (match?.isWarmUp) {
    return (
      <div className={clsx(classes.container, className)}>
        <PlayerIconsRow className={clsx(classes.row, 'header-row')} />
        <div className={clsx(classes.warmup, 'relative', className)}>
          <img src={`/static/bb5/warmup-3-cropped.svg`} class="absolute inset-x-0" />
        </div>
      </div>
    );
  }

  const queenKillIcon = <span className={classes.queenKillIcon}>&#8224;</span>;

  return (
    <div className={clsx(classes.container, 'relative', className)}>
      <GameTime className="absolute left-1/2 -translate-x-1/2 top-[48px] text-white bb5-stat-font tracking-wider text-lg" />
      <PlayerIconsRow className={clsx(classes.row, 'header-row')} />
      <StatsRow
        title="Kills"
        statKey="kills"
        colorOnChange={{ higher: '#0BFF06', duration: 1200 }}
        queenKillIcon={queenKillIcon}
      />
      <StatsRow
        title="Deaths"
        statKey="deaths"
        colorOnChange={{ higher: '#FF0606', duration: 1200 }}
      />
      <StatsRow
        title="Berries"
        statKey="totalBerries"
        colorOnChange={{ higher: '#E00CD7', duration: 1200 }}
      />
      <SnailPercentageRow colorOnChange={{ higher: '#DB96D9', duration: 1200 }} />
      <SnailPercentageRow
        useEstimatedDistance={true}
        colorOnChange={{ higher: '#DB96D9', duration: 2400 }}
      />
    </div>
  );
}
