import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import TeamName from 'overlay/components/TeamName';
import TeamScoreCounter from 'overlay/components/TeamScoreCounter';
import { useMatch } from 'overlay/Match';
import { BLUE_TEAM, GOLD_TEAM } from 'util/constants';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexDirection: ({ team }) => (team === GOLD_TEAM ? 'row-reverse' : 'row'),
    marginRight: ({ team }) => team === BLUE_TEAM && '1px',
    marginLeft: ({ team }) => team === GOLD_TEAM && '1px',

    background: ({ team }) =>
      team === GOLD_TEAM
        ? "url('/static/bb5/gold-in-game-team-banner.svg') no-repeat right padding-box,  var(--bb5-gold-box-gradient) padding-box, var(--bb5-border-gradient)"
        : "url('/static/bb5/blue-in-game-team-banner.svg') no-repeat left padding-box,  var(--bb5-blue-box-gradient) padding-box, var(--bb5-border-gradient)",
    borderRadius: '8px',
    border: '4px solid transparent',
    color: '#000',
    width: '100%',
    height: '54px',
    alignSelf: 'end',
    flexGrow: 0,
    flexShrink: 0,
    flexBasis: '54px',
    maxHeight: '54px',
  },
  scoreCounter: {
    display: 'flex',
    flexDirection: ({ team }) => (team === GOLD_TEAM ? 'row-reverse' : 'row'),
    padding: '2px 2px',
    minWidth: '154px',
  },
  marker: {
    width: '36px',
    margin: 0,
    padding: 0,
    marginTop: '3px',
    '&.gold:first-child': {
      marginRight: '4px',
    },
    '&.blue:first-child': {
      marginLeft: '4px',
    },
  },
  markerEmpty: {},
  markerFilled: {},
  teamName: {
    // These four are from google fonts recommendation
    fontFamily: 'Kanit, sans-serif',
    fontOpticalSizing: 'auto',
    fontWeight: 'black',
    fontStyle: 'italic',

    flexGrow: 1,
    flexBasis: 0,
    textTransform: 'uppercase',
    color: ({ team }) =>
      team === GOLD_TEAM ? 'var(--bb5-gold-team-text-color)' : 'var(--bb5-blue-team-text-color)',

    fontSize: '24px',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    padding: '6px 4px',
    textOverflow: 'ellipsis',
    letterSpacing: '1px',
    textAlign: ({ team }) => (team === GOLD_TEAM ? 'left' : 'right'),
  },
}));

export default function TeamSection({ team, className }) {
  const classes = useStyles({ team });
  const match = useMatch();

  const MarkerFilled = () => (
    <div className={clsx(classes.marker, classes.markerFilled, team)}>
      <img src={`/static/bb5/${team}-flag.svg`} />
    </div>
  );

  const MarkerEmpty = () => (
    <div className={clsx(classes.marker, classes.markerEmpty, team)}>
      <img src={`/static/bb5/empty-${team}-flag-win-marker.svg`} />
    </div>
  );

  return (
    <div className="flex flex-col w-full">
      <div className="flex-grow "></div>
      <div className={clsx(classes.container, className)}>
        <div className={clsx(classes.scoreCounter, 'score-counter')}>
          <TeamScoreCounter team={team} markerFilled={MarkerFilled} markerEmpty={MarkerEmpty} />
        </div>

        <div className={clsx(classes.teamName, 'team-name')}>
          <TeamName className={classes.teamName} team={team} />
        </div>
      </div>
    </div>
  );
}
