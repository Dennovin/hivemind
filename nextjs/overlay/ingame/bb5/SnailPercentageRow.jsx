import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import SnailPercentageValue from 'overlay/components/SnailPercentageValue';
import { BLUE_TEAM, GOLD_TEAM, isQueen, POSITIONS_BY_TEAM } from 'util/constants';

const useStyles = makeStyles(theme => ({
  row: {
    display: 'flex',
    flexDirection: 'row',
    fontFamily: 'Kanit',
  },
  cell: {
    textAlign: 'center',
    padding: '0',
  },
  value: {
    width: '48px',
  },
  valueText: {
    color: 'var(--bb5-stat-text-color)',
  },
  title: {
    width: '70px',
    textTransform: 'uppercase',
    color: 'var(--bb5-stat-text-color)',
  },
  accurateSnailDistance: {
    display: 'none',
  },
  estimatedSnailDistance: {},
  hideQueen: {
    display: 'none',
  },
}));

export default function SnailPercentageRow({ className, useEstimatedDistance = false, ...props }) {
  const classes = useStyles();

  let estimatedOrAccurateClassNames = ['accurate-snail-distance', classes.accurateSnailDistance];
  if (useEstimatedDistance) {
    estimatedOrAccurateClassNames = ['estimated-snail-distance', classes.estimatedSnailDistance];
  }

  return (
    <div
      className={clsx(
        classes.row,
        className,
        estimatedOrAccurateClassNames,
        'snailDistance-row items-center',
      )}
    >
      {[...POSITIONS_BY_TEAM[BLUE_TEAM]].reverse().map(pos => (
        <div key={pos.ID} className={clsx(classes.cell, classes.value)}>
          <SnailPercentageValue
            position={pos}
            className={clsx(classes.valueText, isQueen(pos) ? classes.hideQueen : '')}
            useEstimatedDistance={useEstimatedDistance}
            {...props}
          />
        </div>
      ))}

      <div
        className={clsx(
          classes.cell,
          classes.title,
          'stat-title text-lg',
          'stat-title-snailDistance',
        )}
      >
        Snail
      </div>

      {[...POSITIONS_BY_TEAM[GOLD_TEAM]].reverse().map(pos => (
        <div key={pos.ID} className={clsx(classes.cell, classes.value)}>
          <SnailPercentageValue
            position={pos}
            className={clsx(classes.valueText, isQueen(pos) ? classes.hideQueen : '')}
            useEstimatedDistance={useEstimatedDistance}
            {...props}
          />
        </div>
      ))}
    </div>
  );
}
