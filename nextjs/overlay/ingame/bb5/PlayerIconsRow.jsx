import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import KQLogo from '@/components/KqLogo';
import { BLUE_TEAM, GOLD_TEAM, POSITIONS_BY_TEAM } from 'util/constants';
import PlayerIcon from './PlayerIcon';

const useStyles = makeStyles(theme => ({
  logo: {
    width: '70px',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center center',
    backgroundSize: '120px 50px',
  },
}));

export default function PlayerIconsRow({ className }) {
  const classes = useStyles();

  return (
    <div className={clsx(classes.row, className)}>
      {[...POSITIONS_BY_TEAM[BLUE_TEAM]].reverse().map(pos => (
        <div
          key={pos.ID}
          className={clsx(classes.icon, 'player-sprite-holder', 'player-sprite-holder-blue')}
        >
          <PlayerIcon position={pos} />
        </div>
      ))}

      <div className={clsx(classes.logo, 'logo text-white grid place-content-center')}>
        <KQLogo className="w-12 block relative -top-2" />
      </div>

      {[...POSITIONS_BY_TEAM[GOLD_TEAM]].reverse().map(pos => (
        <div
          key={pos.ID}
          className={clsx(classes.icon, 'player-sprite-holder', 'player-sprite-holder-gold')}
        >
          <PlayerIcon position={pos} />
        </div>
      ))}
    </div>
  );
}
