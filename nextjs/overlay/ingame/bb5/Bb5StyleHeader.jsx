import Head from 'next/head';

export default function Bb5StyleHeader({ title = 'BB5' }) {
  return (
    <Head>
      <title>{title}</title>
      <link
        href="https://fonts.googleapis.com/css2?family=Kanit:ital,wght@0,400;0,500;1,900&display=swap"
        rel="stylesheet"
      />
      <style type="text/css">
        {`
            :root {

              /* Main background image (tilesbut doesnt have to) */
              --bb5-bg: url(/static/bb5/back.gif);


              /* Default box color. Repeat the same color for solid color */
              --bb5-box-bg: linear-gradient(to right, black, black);

              /* Team box backgrounds */
              --bb5-gold-box-gradient: linear-gradient(to left, #a32300 0%, #4f0503 100%);
              --bb5-blue-box-gradient: linear-gradient(to right, #0017c1 0%, #000b89 100%);

              /* Border gradient for all boxes. Use same color for solid border */
              --bb5-base-border-color: #efefe6;
              --bb5-inner-sheen-border-color: #efefe6;
              --bb5-specular-border-color: #efefe6;

              /* Stats background gradient */
              --bb5-stats-bg: url('/static/bb5/flag-bg-middle-stats.webp'); /* must be image or gradient */
              --bb5-gold-team-text-color: white;
              --bb5-blue-team-text-color: white;
              --bb5-stat-text-color: white;


              /* Do not edit below this line */
              --bb5-border-gradient: linear-gradient(to right, var(--bb5-base-border-color) 0%, var(--bb5-inner-sheen-border-color) 36%, var(--bb5-specular-border-color) 49%, var(--bb5-inner-sheen-border-color) 62%, var(--bb5-base-border-color) 100%) border-box;
            }
            .bb5-border {
              background: var(--bb5-box-bg) padding-box, var(--bb5-border-gradient);
              border: 4px solid transparent;
              border-radius: 8px;
            }
            .bb5-font {
              font-family: 'Kanit', sans-serif;
              text-transform: uppercase;
              font-style: italic;
              font-weight: black;
            }
            .bb5-stat-font {
              font-family: 'Kanit', sans-serif;
              font-weight: medium;
              text-transform: uppercase;
            }
            @font-face {
              font-family: 'bumblebashsans';
              src: url('/static/bb5/bbash-sans.woff2') format('woff2'),
                  url('/static/bb5/bbash-sans.woff') format('woff');
              font-weight: normal;
              font-style: normal;
            }
            .bb-sans {
              font-family: 'bumblebashsans', sans-serif;
            }
          `}
      </style>
    </Head>
  );
}
