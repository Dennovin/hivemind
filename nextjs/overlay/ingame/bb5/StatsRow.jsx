import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import QueenKillIconContainer from 'overlay/components/QueenKillIconContainer';
import StatValue from 'overlay/components/StatValue';
import { BLUE_TEAM, GOLD_TEAM, POSITIONS_BY_TEAM } from 'util/constants';

const useStyles = makeStyles(theme => ({
  row: {
    display: 'flex',
    flexDirection: 'row',
    fontFamily: 'Kanit',
  },
  cell: {
    textAlign: 'center',
    padding: '0',
    display: 'flex',
    flexDirection: 'row',
    width: '48px',
    justifyContent: 'center',
  },
  value: {},
  valueText: {
    color: 'var(--bb5-stat-text-color)',
  },
  title: {
    width: '70px',
    textTransform: 'uppercase',
    color: 'var(--bb5-stat-text-color)',
  },
  queenKillContainer: {
    color: '#000',
  },
}));

export default function StatsRow({ className, title, queenKillIcon, statKey, ...props }) {
  const classes = useStyles();

  return (
    <div className={clsx(classes.row, 'items-center', className, statKey + '-row')}>
      {[...POSITIONS_BY_TEAM[BLUE_TEAM]].reverse().map(pos => (
        <div key={pos.ID} className={clsx(classes.cell, classes.value)}>
          <StatValue position={pos} className={classes.valueText} statKey={statKey} {...props} />
          {queenKillIcon && (
            <QueenKillIconContainer
              position={pos}
              className={classes.queenKillContainer}
              element={queenKillIcon}
            />
          )}
        </div>
      ))}

      <div
        className={clsx(classes.cell, classes.title, 'stat-title text-lg', 'stat-title-' + statKey)}
      >
        {title}
      </div>

      {[...POSITIONS_BY_TEAM[GOLD_TEAM]].reverse().map(pos => (
        <div key={pos.ID} className={clsx(classes.cell, classes.value)}>
          <StatValue position={pos} className={classes.valueText} statKey={statKey} {...props} />
          {queenKillIcon && (
            <QueenKillIconContainer
              position={pos}
              className={classes.queenKillContainer}
              element={queenKillIcon}
            />
          )}
        </div>
      ))}
    </div>
  );
}
