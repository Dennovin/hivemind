import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Head from 'next/head';

import { useMatch } from 'overlay/Match';
import { useSceneController } from 'overlay/SceneController';
import { useOverlaySettings } from 'overlay/Settings';
import { BLUE_TEAM, GOLD_TEAM, OVERLAY_SCENES } from 'util/constants';
import EventNameBox from './racing/EventNameBox';
import TeamNameBox from './racing/TeamNameBox';

const useStyles = makeStyles(theme => ({
  container: {
    position: 'relative',
    width: 1540,
    height: 868,
    top: 0,
    left: 0,
    overflow: 'hidden',
  },
  eventName: {
    width: '100%',
    transform: 'translateY(-150%)',
    textAlign: 'center',
    fontSize: '54px',
    fontWeight: 'bold',
    fontFamily: 'Orbitron, sans-serif',
    // TODO: Font should be RO-shell
    // TODO:font needs rainbow border
    '.active &': {
      transform: 'translateY( 0%)',
    },

    borderRadius: "4px",
    border: "4px solid transparent",
    background: "linear-gradient(to right, #040404, #040404) content-box, linear-gradient(to right, #292323 0%, #464040 36%, #CEC6C6 49%, #464040 62%, #292323 100%) border-box",

    color: 'white',
  },
  teamName: {
    width: '100%',
    transition: 'transform 1s ease-in-out',
    flexGrow: 1,
    height: '100%',
  },
  teamNameLeft: {
    transform: 'translateX(-100%)',
    '.active &': {
      transform: 'translateX(0%)',
    },
  },
  teamNameRight: {
    transform: 'translateX(100%)',
    '.active &': {
      transform: 'translateX(0%)',
    },
  },
  vsText: {
    position: "absolute",
    top: "135px",
    height: "61px",
    width: "156px",
  },
}));

export default function Racing({}) {
  const classes = useStyles();
  const settings = useOverlaySettings();
  const scene = useSceneController();
  const match = useMatch();

  const active = (scene === OVERLAY_SCENES.MATCH_PREVIEW || scene === null) && match;

  const leftTeam = settings?.goldOnLeft ? GOLD_TEAM : BLUE_TEAM;
  const rightTeam = settings?.goldOnLeft ? BLUE_TEAM : GOLD_TEAM;

  return (
    <>
      <Head>
        {settings?.additionalFonts && <link rel="stylesheet" href={settings.additionalFonts}/>}
        <link href="https://fonts.googleapis.com/css2?family=Orbitron:wght@400..900&display=swap" rel="stylesheet"/>
        <link rel="stylesheet" type="text/css" href={`/static/racing/fonts/ds-digital/style.css`}/>
      </Head>
      <div
        id="matchPreviewOverlay"
        className={clsx(
          classes.container,
          ' flex flex-col gap-px items-center justify-evenly pt-px',
          {
            active,
          },
        )}
      >
        <EventNameBox className={clsx(classes.eventName, 'event-name')}/>
        <div className="grid grid-cols-2 gap-px w-full h-full flex-grow">
          <TeamNameBox
            className={clsx(classes.teamName, classes.teamNameLeft, 'team', 'left-team')}
            team={leftTeam}
            teamColor={'blue'}
          />
          <TeamNameBox
            className={clsx(classes.teamName, classes.teamNameRight, 'team', 'right-team')}
            team={rightTeam}
            teamColor={'gold'}
          />
        </div>
        <div className={clsx(classes.vsText, 'vs-text')}>
          {
            active ?
              <img src={'/static/racing/vs.svg'}/> :
              null
          }
        </div>
      </div>
    </>
  );
}

Racing.themeProps = {
  name: 'racing',
  displayName: 'Racing (GDC VIII)',
  description: `Colorful, racing themed overlay.`,
};
