import React from 'react';
import clsx from 'clsx';
import Icon from '@mdi/react';

import { BLUE_TEAM, GOLD_TEAM } from 'util/constants';
import { roleIcon } from '@/components/whiteboard';

import styles from '../Wildflower.module.css';

export default function PlayerName({ player, team, className }) {
  return (
    <div className={clsx(styles.player, 'player')}>
      <div className={clsx(styles.image, 'player-image')}>
        {player.image && !player.doNotDisplay && <img src={player.image} />}
      </div>
      {player?.role && (
        <div className={clsx(styles.role, 'role')}>
          <Icon path={roleIcon[player.role]} size={2} />
        </div>
      )}
      <div className={clsx(styles.textColumn, 'text')}>
        <div className={clsx(styles.nameRow, 'name-row')}>
          <div className={clsx(styles.playerName, 'player-name')}>
            {player.scene ? `${player.name} [${player.scene}]` : player.name}
          </div>
          {player.pronouns && (
            <div className={clsx(styles.pronouns, 'pronouns')}>{player.pronouns}</div>
          )}
        </div>

        {player.tidbit && <div className={clsx(styles.tidbit, 'tidbit')}>{player.tidbit}</div>}
      </div>
    </div>
  );
}
