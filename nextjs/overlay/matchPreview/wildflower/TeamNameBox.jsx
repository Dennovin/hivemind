import React, { useMemo } from 'react';
import clsx from 'clsx';
import ordinal from 'ordinal';

import { STAGE_TYPE } from 'util/constants';
import PlayerName from './PlayerName';
import TeamName from 'overlay/components/TeamName';
import { useMatch } from 'overlay/Match';

import styles from '../Wildflower.module.css';

export default function TeamNameBox({ team, className }) {
  const match = useMatch();
  const players = match?.players[team];
  const pics = players?.some(player => player.image);

  const [teamPlace, teamStandingsEntry] = useMemo(() => {
    if (match.groupStandings) {
      const teamEntries = match.groupStandings.filter(t => t.id === match.tournamentTeamId[team]);
      if (teamEntries.length === 0) return [null, null];

      const teamEntry = teamEntries[0];
      if (teamEntry.gameWins + teamEntry.gameLosses === 0) return [null, null];

      const teamIds = match.groupStandings.map(t => t.id);
      const place = teamIds.indexOf(teamEntry.id) + 1;

      return [place, teamEntry];
    }

    return [null, null];
  }, [match.groupStandings]);

  const teamSeeded = useMemo(() => {
    if ([STAGE_TYPE.SINGLE_ELIM, STAGE_TYPE.DOUBLE_ELIM].includes(match?.bracket?.stageType)) {
      return match?.bracket?.teams?.map(t => t.id).indexOf(match.tournamentTeamId[team]) + 1;
    }

    return null;
  });

  if (players?.length > 0) {
    return (
      <div className={clsx(className, 'team-container', 'large')}>
        <div className={clsx(styles.teamName, 'team-name')}>
          <TeamName team={team} />
          {teamPlace && teamStandingsEntry && (
            <div className={clsx(styles.teamStandings, 'team-standings')}>
              {ordinal(teamPlace)} place - matches {teamStandingsEntry.matchWins}-
              {teamStandingsEntry.matchLosses}, games {teamStandingsEntry.gameWins}-
              {teamStandingsEntry.gameLosses}
            </div>
          )}
          {teamSeeded && (
            <div className={clsx(styles.teamStandings, 'team-standings')}>
              {ordinal(teamSeeded)} seed
            </div>
          )}
        </div>

        <div className={clsx(styles.playerNames, 'player-names', { pics })}>
          {players.map(player => (
            <PlayerName
              key={player.id}
              className={clsx(styles.playerName, 'player-name')}
              team={team}
              player={player}
            />
          ))}
        </div>
      </div>
    );
  }

  return (
    <div className={clsx(className, 'small')}>
      <div className={clsx(styles.teamName, 'team-name')}>
        <TeamName team={team} />
        {teamPlace && teamStandingsEntry && (
          <div className={clsx(styles.teamStandings, 'team-standings')}>
            {ordinal(teamPlace)} place ({teamStandingsEntry.matchWins}-
            {teamStandingsEntry.matchLosses} in matches, {teamStandingsEntry.gameWins}-
            {teamStandingsEntry.gameLosses} in games)
          </div>
        )}
        {teamSeeded && (
          <div className={clsx(styles.teamStandings, 'team-standings')}>
            {ordinal(teamSeeded)} seed
          </div>
        )}
      </div>
    </div>
  );
}
