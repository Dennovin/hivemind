import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { useMatch } from 'overlay/Match';
import { useSceneController } from 'overlay/SceneController';
import { useOverlaySettings } from 'overlay/Settings';
import { BLUE_TEAM, GOLD_TEAM, OVERLAY_SCENES } from 'util/constants';
import Bb5StyleHeader from '../ingame/bb5/Bb5StyleHeader';
import EventNameBox from './bb5/EventNameBox';
import TeamNameBox from './bb5/TeamNameBox';

const useStyles = makeStyles(theme => ({
  eventName: {
    width: '100%',
    transform: 'translateY(-150%)',
    textAlign: 'center',
    fontSize: '54px',
    fontWeight: 'bold',
    lineHeight: '1',

    '.active &': {
      transform: 'translateY( 0%)',
    },
    background: 'transparent',
    color: 'black',
  },
  teamName: {
    width: '100%',
    transition: 'transform 1s ease-in-out',
    flexGrow: 1,
    height: '100%',
  },
  teamNameLeft: {
    transform: 'translateX(-100%)',
    '.active &': {
      transform: 'translateX(0%)',
    },
  },
  teamNameRight: {
    transform: 'translateX(100%)',
    '.active &': {
      transform: 'translateX(0%)',
    },
  },
  vsText: {
    position: 'absolute',
    top: '125px',
    height: '61px',
    width: '156px',
  },
}));

export default function Bb5({}) {
  const classes = useStyles();
  const settings = useOverlaySettings();
  const scene = useSceneController();
  const match = useMatch();

  const active = (scene === OVERLAY_SCENES.MATCH_PREVIEW || scene === null) && match;

  const leftTeam = settings?.goldOnLeft ? GOLD_TEAM : BLUE_TEAM;
  const rightTeam = settings?.goldOnLeft ? BLUE_TEAM : GOLD_TEAM;

  return (
    <>
      <Bb5StyleHeader />
      <div
        id="matchPreviewOverlay"
        className={clsx(
          'absolute h-[868px] w-[1540px] top-[212px] pt-px  overflow-hidden bg-black/0',
          {
            active,
          },
        )}
      >
        <div className="flex flex-col gap-px items-center justify-evenly animate-slab-down absolute inset-0 pb-8">
          <EventNameBox className={clsx(classes.eventName, 'event-name bb5-font')} />
          <div className="grid grid-cols-2 gap-px w-full h-full flex-grow">
            <TeamNameBox
              className={clsx(classes.teamName, classes.teamNameLeft, 'team', 'left-team')}
              team={leftTeam}
              teamColor={'blue'}
            />
            <TeamNameBox
              className={clsx(classes.teamName, classes.teamNameRight, 'team', 'right-team')}
              team={rightTeam}
              teamColor={'gold'}
            />
          </div>
        </div>
      </div>
    </>
  );
}

Bb5.themeProps = {
  name: 'bb5',
  displayName: 'Bb5 Theme',
  description: `Colorful, bb5 themed overlay.`,
};
