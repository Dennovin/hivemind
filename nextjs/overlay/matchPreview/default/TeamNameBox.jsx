import React, { useMemo } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import ordinal from 'ordinal';

import { STAGE_TYPE } from 'util/constants';
import PlayerName from './PlayerName';
import TeamName from 'overlay/components/TeamName';
import { useMatch } from 'overlay/Match';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    opacity: '0.9',
    background: ({ team }) => theme.gradients[team].light,
  },
  containerLarge: {
    height: '818px',
    overflow: 'hidden',
    alignItems: 'center',
  },
  containerSmall: {},
  teamName: {
    padding: '10px 0',
    minWidth: '100%',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: '42px',
    overflow: 'hidden',
    whiteSpace: 'nowrap',
    '.team-container &': {
      borderBottom: '1px solid #444',
    },
  },
  teamStandings: {
    minWidth: '100%',
    textAlign: 'center',
    fontSize: '24px',
    fontWeight: 'normal',
    fontStyle: 'italic',
    whiteSpace: 'nowrap',
  },
  playerNames: {
    margin: '0 0 10px 0',
    flexBasis: 0,
    flexGrow: 1,
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-around',
  },
}));

export default function TeamNameBox({ team, className }) {
  const classes = useStyles({ team });
  const match = useMatch();
  const players = match?.players[team];
  const pics = players?.some(player => player.image);

  const [teamPlace, teamStandingsEntry] = useMemo(() => {
    if (match.groupStandings) {
      const teamEntries = match.groupStandings.filter(t => t.id === match.tournamentTeamId[team]);
      if (teamEntries.length === 0) return [null, null];

      const teamEntry = teamEntries[0];
      if (teamEntry.gameWins + teamEntry.gameLosses === 0) return [null, null];

      const teamIds = match.groupStandings.map(t => t.id);
      const place = teamIds.indexOf(teamEntry.id) + 1;

      return [place, teamEntry];
    }

    return [null, null];
  }, [match.groupStandings]);

  const teamSeeded = useMemo(() => {
    if ([STAGE_TYPE.SINGLE_ELIM, STAGE_TYPE.DOUBLE_ELIM].includes(match?.bracket?.stageType)) {
      return match?.bracket?.teams?.map(t => t.id).indexOf(match.tournamentTeamId[team]) + 1;
    }

    return null;
  });

  if (players?.length > 0) {
    return (
      <div className={clsx(classes.container, classes.containerLarge, className, 'team-container')}>
        <div className={clsx(classes.teamName, 'team-name')}>
          <TeamName team={team} />
          {teamPlace && teamStandingsEntry && (
            <div className={clsx(classes.teamStandings, 'team-standings')}>
              {ordinal(teamPlace)} place - matches {teamStandingsEntry.matchWins}-
              {teamStandingsEntry.matchLosses}, games {teamStandingsEntry.gameWins}-
              {teamStandingsEntry.gameLosses}
            </div>
          )}
          {teamSeeded && (
            <div className={clsx(classes.teamStandings, 'team-standings')}>
              {ordinal(teamSeeded)} seed
            </div>
          )}
        </div>

        <div className={clsx(classes.playerNames, 'player-names', { pics })}>
          {players.map(player => (
            <PlayerName
              key={player.id}
              className={clsx(classes.playerName, 'player-name')}
              team={team}
              player={player}
            />
          ))}
        </div>
      </div>
    );
  }

  return (
    <div
      className={clsx(classes.container, classes.containerSmall, className, 'team-container-small')}
    >
      <div className={clsx(classes.teamName, 'team-name')}>
        <TeamName team={team} />
        {teamPlace && teamStandingsEntry && (
          <div className={clsx(classes.teamStandings, 'team-standings')}>
            {ordinal(teamPlace)} place ({teamStandingsEntry.matchWins}-
            {teamStandingsEntry.matchLosses} in matches, {teamStandingsEntry.gameWins}-
            {teamStandingsEntry.gameLosses} in games)
          </div>
        )}
        {teamSeeded && (
          <div className={clsx(classes.teamStandings, 'team-standings')}>
            {ordinal(teamSeeded)} seed
          </div>
        )}
      </div>
    </div>
  );
}
