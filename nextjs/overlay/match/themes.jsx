import Bb5 from './Bb5MatchSummary';
import Default from './Default';
import FFVII from './FFVII';
import Medieval from './Medieval';
import Racing from './Racing';
import Wildflower from './Wildflower';
import WildflowerDark from './WildflowerDark';

const allMatchThemes = [Default, FFVII, Medieval, Racing, Wildflower, WildflowerDark, Bb5];

export function getMatchTheme(name) {
  for (const theme of allMatchThemes) {
    if (theme.themeProps.name == name) {
      return theme;
    }
  }
}

export { allMatchThemes };
