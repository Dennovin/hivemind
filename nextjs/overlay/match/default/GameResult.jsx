import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import { format, parseISO } from 'date-fns';

import { BLUE_TEAM, GOLD_TEAM, WIN_CONDITIONS } from 'util/constants';

const useStyles = makeStyles(theme => ({
  row: {
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
    padding: '4px 100px',
    '&:first-of-type': {
      borderTopLeftRadius: '5px',
      borderTopRightRadius: '5px',
    },
    '&:last-of-type': {
      borderBottomLeftRadius: '5px',
      borderBottomRightRadius: '5px',
    },
  },

  blueWin: {
    background: theme.palette.blue.main,
    color: 'white',
  },
  goldWin: {
    background: theme.palette.gold.main,
  },
}));

export default function GameResult({ className, game, idx, ...props }) {
  const classes = useStyles();
  const rowClasses = clsx(classes.row, className, {
    [classes.blueWin]: game?.winningTeam == BLUE_TEAM,
    [classes.goldWin]: game?.winningTeam == GOLD_TEAM,
  });

  if (!game) {
    return <></>;
  }
  const cell = 'basis-0 px-4 text-2xl flex-grow font-bold font-stat-numbers-clean uppercase';
  return (
    <div className={rowClasses}>
      <div className={clsx(cell, '')}>Game {idx + 1}</div>
      <div className={clsx(cell)}>{game.mapName}</div>
      <div className={clsx(cell)}>{WIN_CONDITIONS[game.winCondition]}</div>
      <div className={clsx(cell, 'text-right')}>
        {format(parseISO(game.endTime) - parseISO(game.startTime), 'm:ss')}
      </div>
    </div>
  );
}
