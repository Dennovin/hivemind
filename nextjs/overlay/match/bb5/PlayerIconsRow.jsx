import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { useOverlaySettings } from 'overlay/Settings';
import { BLUE_TEAM, GOLD_TEAM, POSITIONS_BY_TEAM, isQueen } from 'util/constants';
import { useMatchStats } from "../../MatchStats";

const useStyles = makeStyles(theme => ({
  row: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around',
    minHeight: '50px',
    marginBottom: '10px',
  },
  spacer: {
    width: '300px',
    flexBasis: '300px',
    flexGrow: 0,
  },
  flipped: {
    transform: "scaleX(-1)",
  },
}));

const iconStyles = makeStyles(theme => ({
  icon: {
    flexGrow: 1,
    flexBasis: 0,
    height: '70px',
    width: '48px',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center calc(100% - 4px)',
    backgroundImage: ({ position }) =>
      `url(/static/gdc/sprites/${position.TEAM}_${position.POSITION}@4x.png)`,
    backgroundSize: ({ position }) => (isQueen(position) ? '55px 57px' : '30px 39px'),
    transform: ({ position }) => position.TEAM === GOLD_TEAM && 'scaleX(-1)',
    '&.warrior': {
      backgroundImage: ({ position }) =>
        `url(/static/gdc/sprites/${position.TEAM}_${position.POSITION}_warrior@4x.png)`,
      backgroundSize: '43.5px 51px',
    },
  },
}));

export default function PlayerIconsRow({ className }) {
  const classes = useStyles();
  const settings = useOverlaySettings();
  const match = useMatchStats();

  let blueWon = true;
  let goldWon = true;

  const blueScore = match?.blueScore;
  const goldScore = match?.goldScore;

  // Remember that ties can happen in groups in some tournaments, so can double true (since they're all winners)
  if (blueScore < goldScore) {
    blueWon = false;
  } else if (blueScore > goldScore) {
    goldWon = false;
  }

  return (
    <div className={clsx(classes.row, className)}>
      {[...POSITIONS_BY_TEAM[settings?.goldOnLeft ? GOLD_TEAM : BLUE_TEAM]].reverse().map(position => {
        const iconClasses = iconStyles({ position });
        return (
          <div
            key={position.ID}
            className={clsx(iconClasses.icon, { 'warrior': blueWon && !isQueen(position) })}
          />
        );
      })}

      <div className={classes.spacer} />

      {[...POSITIONS_BY_TEAM[settings?.goldOnLeft ? BLUE_TEAM : GOLD_TEAM]].reverse().map(position => {
        const iconClasses = iconStyles({ position });
        return (
          <div
            key={position.ID}
            className={clsx(iconClasses.icon, classes.flipped, { 'warrior': goldWon && !isQueen(position) })}
          />
        );
      })}
    </div>
  );
}
