import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import { useMatchStats } from 'overlay/MatchStats';
import { useOverlaySettings } from 'overlay/Settings';
import Bb5StyleHeader from '../ingame/bb5/Bb5StyleHeader';
import GameListTable from './bb5/GameListTable';
import MatchStatsTable from './bb5/MatchStatsTable';

const useStyles = makeStyles(theme => ({
  container: {
    opacity: 0,

    width: '1540px',
    height: '868px',
    position: 'absolute',
    left: 0,
    top: '212px',
    padding: '20px 0',
    transition: 'opacity 1s',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    gap: '12px',
    color: 'transparent',
    '&.active': {
      opacity: 1,
    },
  },
  blue: {
    background: theme.gradients.blue.light,
  },
  gold: {
    background: theme.gradients.gold.light,
  },
}));

export default function Bb5({}) {
  const classes = useStyles();
  const settings = useOverlaySettings();
  const stats = useMatchStats();

  // TODO: All the fonts need text shadow
  // TODO: Table might grow too big in 5 game sets.  Maybe cap it?

  return (
    <>
      <Bb5StyleHeader />
      <div id="matchSummaryOverlay" className={clsx(classes.container, { active: stats?.visible })}>
        <GameListTable className={clsx(classes.gameListTable)} />
        <MatchStatsTable className={clsx(classes.matchStatsTable)} />
      </div>
    </>
  );
}

Bb5.themeProps = {
  name: 'bb5',
  displayName: 'Bb5 Theme',
  description: `Colorful, bb5 themed overlay.`,
};
