import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { useMatchStats } from 'overlay/MatchStats';
import { formatTime } from 'util/dates';
import PlayerIconsRow from './PlayerIconsRow';
import StatsRow from './StatsRow';

const useStyles = makeStyles(theme => ({
  table: {
    width: '90%',
    margin: '1px auto',
    padding: '12px 24px',
    display: 'flex',
    flexDirection: 'column',
    flexGrow: 1,

    color: '#FFFFFF',
    textShadow: "#1A1A1A 1px 1px",

    borderRadius: "8px",
    border: "4px solid transparent",
    background: "linear-gradient(to right, #040404, #040404) padding-box, linear-gradient(to right, #292323 0%, #464040 36%, #CEC6C6 49%, #464040 62%, #292323 100%) border-box",
  },
}));

export default function MatchSummary({ className }) {
  const classes = useStyles();
  const match = useMatchStats();

  if (match === null) {
    return <></>;
  }

  return (
    <div className={clsx(classes.table, className)}>
        <PlayerIconsRow className={'flex'} />
        <StatsRow title="K/D" getValue={stat => `${stat.kills}-${stat.deaths}`} />
        <StatsRow
          title="Military K/D"
          getValue={stat => `${stat.militaryKills}-${stat.militaryDeaths}`}
        />
        <StatsRow title="Queen Kills" statKey="queenKills" />
        <StatsRow title="Bump Assists" statKey="bumpAssists" />
        <StatsRow
          title="Warrior Uptime"
          getValue={stat => formatTime(stat.warriorUptime)}
          defaultText=""
        />
        <StatsRow title="Berries Deposited" statKey="berries" />
        <StatsRow
          title="Snail Distance"
          getValue={stat => `${stat.snailDistancePct}%`}
          showForQueen={false}
        />
    </div>
  );
}
