import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import { format, parseISO } from 'date-fns';

import { BLUE_TEAM, GOLD_TEAM, WIN_CONDITIONS } from 'util/constants';

const useStyles = makeStyles(theme => ({
  row: {
    display: 'flex',
    flexDirection: 'row',
    width: '630px',
    margin: '8px auto 8px',
    padding: '0px 32px',

    border: 0,

    fontWeight: 'bold',
    fontFamily: 'Orbitron, sans-serif',
    textShadow: "#756F74 1px 1px",
    color: '#FFFFFF',
    textTransform: "uppercase",

    backgroundRepeat: "no-repeat",
    backgroundPosition: "center center",
  },

  blueWin: {
    backgroundImage: "url('/static/racing/lrg-stats-blue-banner.svg')",
  },
  goldWin: {
    backgroundImage: "url('/static/racing/lrg-stats-gold-banner.svg')",
  },

  gameCell: {
    minWidth: "150px",
  },

  mapCell: {
    minWidth: "175px",
  },

  winConCell: {
    maxWidth: "150px",
  },

  timeCell: {
    maxWidth: "150px",
  },
}));

export default function GameResult({ className, game, idx, ...props }) {
  const classes = useStyles();
  const rowClasses = clsx(classes.row, className, {
    [classes.blueWin]: game?.winningTeam === BLUE_TEAM,
    [classes.goldWin]: game?.winningTeam === GOLD_TEAM,
  });

  if (!game) {
    return <></>;
  }
  const cell = 'basis-0 px-2 text-2xl flex-grow';
  return (
    <div className={rowClasses}>
      <div className={clsx(classes.gameCell, cell, '')}>Game {idx + 1}</div>
      <div className={clsx(classes.mapCell, cell, 'text-left')}>
        {/*Twilight*/}
        {game.mapName}
      </div>
      <div className={clsx(classes.winConCell, cell, 'text-left')}>{WIN_CONDITIONS[game.winCondition]}</div>
      <div className={clsx(classes.timeCell, cell, 'text-right')}>
        {format(parseISO(game.endTime) - parseISO(game.startTime), 'm:ss')}
      </div>
    </div>
  );
}
