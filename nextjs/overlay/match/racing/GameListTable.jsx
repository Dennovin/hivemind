import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { useMatchStats } from 'overlay/MatchStats';
import GameResult from './GameResult';

const useStyles = makeStyles(theme => ({
  table: {
    margin: '0',
    display: 'flex',
    flexDirection: 'column',
    padding: 0,
    gap: '1px',
    flexGrow: 1,
  },
  row: {
    flexGrow: 1,
    alignItems: 'center',
  },
}));

export default function GameListTable({ className }) {
  const classes = useStyles();
  const match = useMatchStats();

  if (match === null) {
    return <></>;
  }

  return (
    <div id="matchSummaryGameList" className={clsx(classes.table, className)}>
      {match?.games &&
        match.games
          .sort((a, b) => new Date(a.endTime) - new Date(b.endTime))
          .map((game, idx) => (
            <>
              <GameResult className={classes.row} key={game.id} idx={idx} game={game} />
            </>
          ))}
    </div>
  );
}
