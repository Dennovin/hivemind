import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import { format, parseISO } from 'date-fns';

import { BLUE_TEAM, GOLD_TEAM, WIN_CONDITIONS } from 'util/constants';

const useStyles = makeStyles(theme => ({
  row: {
    display: 'flex',
    flexDirection: 'row',
    width: '50%',
    margin: '0 auto',
    padding: '4px 50px',

    // Standard border config
    border: '4px groove white',
    boxShadow: 'inset 0 0 4px #000000aa',
    borderRadius: '8px',
    color: 'white',
    textShadow: '1.5px 1.5px #000000',
    background: 'rgba(0,0,0,0.8)',
    // End standard border config

    fontWeight: 'bold',
    fontFamily: 'Reactor7, sans-serif',
  },

  blueWin: {
    backgroundImage: 'linear-gradient(#000099, #000033)',
    color: 'white',
  },
  goldWin: {
    backgroundImage: 'linear-gradient(#cc9933, #996633)',
  },
}));

export default function GameResult({ className, game, idx, ...props }) {
  const classes = useStyles();
  const rowClasses = clsx(classes.row, className, {
    [classes.blueWin]: game?.winningTeam == BLUE_TEAM,
    [classes.goldWin]: game?.winningTeam == GOLD_TEAM,
  });

  if (!game) {
    return <></>;
  }
  const cell = 'basis-0 px-2 text-3xl flex-grow';
  return (
    <div className={rowClasses}>
      <div className={clsx(cell, '')}>Game {idx + 1}</div>
      <div className={clsx(cell, 'text-right')}>{game.mapName}</div>
      <div className={clsx(cell, 'text-right')}>{WIN_CONDITIONS[game.winCondition]}</div>
      <div className={clsx(cell, 'text-right')}>
        {format(parseISO(game.endTime) - parseISO(game.startTime), 'm:ss')}
      </div>
    </div>
  );
}
