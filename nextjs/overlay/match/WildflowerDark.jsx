import Wildflower from './Wildflower';

export default function WildflowerDark() {
  return <Wildflower dark={true} />;
}

WildflowerDark.themeProps = {
  name: 'wildflower-dark',
  displayName: 'Wildflower (Dark)',
  description: `Wildflower match stats theme`,
};
