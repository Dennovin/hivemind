import clsx from 'clsx';
import Head from 'next/head';
import { useMatchStats } from 'overlay/MatchStats';
import { useOverlaySettings } from 'overlay/Settings';
import medievalClasses from 'overlay/ingame/medieval/medieval.module.css';
import TeamImages from '../ingame/medieval/TeamImages';
import MatchStatsTable from './ffvii/MatchStatsTable';
import MatchSummary from './medieval/MatchSummary';
export default function Medieval({}) {
  const settings = useOverlaySettings();
  const stats = useMatchStats();

  return (
    <>
      <Head>
        {settings?.additionalFonts && <link rel="stylesheet" href={settings.additionalFonts} />}
        <link
          href="https://fonts.googleapis.com/css2?family=MedievalSharp&display=swap"
          rel="stylesheet"
        ></link>
      </Head>
      <div className="match-summary-container aspect-video w-screen absolute top-0 left-0">
        <div
          id="matchSummaryOverlay"
          className={clsx(
            'aspect-video w-[75vw] top-[3.125vw] left-[12.5vw] absolute overflow-hidden flex flex-col justify-end',
            'transition-opacity ease-out [&.active]:opacity-100 opacity-0',
            { active: stats?.visible },
          )}
        >
          <MatchSummary className={clsx('match-summary ')} />

          <MatchStatsTable
            className={clsx(
              'match-stats mb-2 border-gray-600 h-[22.8125vw]',
              medievalClasses.ridgeBorder,
            )}
          />
          <img
            src="/static/medieval/match-topper.webp"
            className="absolute -z-10 inset-0 top-0  w-full  "
          />
          <TeamImages classNameOverride="-z-[11] absolute inset-x-[215px] scale-125 top-[90px] flex justify-between" />
          <img
            src="/static/medieval/slab.webp"
            className="absolute -z-20 inset-0 aspect-video w-full h-full "
          />
        </div>
      </div>
    </>
  );
}

Medieval.themeProps = {
  name: 'medieval',
  displayName: 'Medieval',
  description: `Match Summary Medieval style`,
};
