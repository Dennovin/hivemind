import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Head from 'next/head';
import { useMatchStats } from 'overlay/MatchStats';
import { useOverlaySettings } from 'overlay/Settings';
import GameListTable from './racing/GameListTable';
import MatchStatsTable from './racing/MatchStatsTable';

const useStyles = makeStyles(theme => ({
  container: {
    opacity: 0,

    width: '1540px',
    height: '868px',
    position: 'absolute',
    left: 0,
    top: 0,
    padding: '1px 0 0',
    transition: 'opacity 1s',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    gap: '1px',
    color: 'transparent',
    '&.active': {
      opacity: 1,
    },
  },
  blue: {
    background: theme.gradients.blue.light,
  },
  gold: {
    background: theme.gradients.gold.light,
  },
}));

export default function Racing({}) {
  const classes = useStyles();
  const settings = useOverlaySettings();
  const stats = useMatchStats();

  // TODO: All the fonts need text shadow
  // TODO: Table might grow too big in 5 game sets.  Maybe cap it?


  return (
    <>
      <Head>
        {settings?.additionalFonts && <link rel="stylesheet" href={settings.additionalFonts}/>}
        <link href="https://fonts.googleapis.com/css2?family=Orbitron:wght@400..900&display=swap" rel="stylesheet"/>
        <link rel="stylesheet" type="text/css" href={`/static/racing/fonts/ds-digital/style.css`}/>
      </Head>
      <div id="matchSummaryOverlay" className={clsx(classes.container, { active: stats?.visible })}>
        <GameListTable className={clsx(classes.gameListTable)} />
        <MatchStatsTable className={clsx(classes.matchStatsTable)} />
      </div>
    </>
  );
}

Racing.themeProps = {
  name: 'racing',
  displayName: 'Racing (GDC VIII)',
  description: `Colorful, racing themed overlay.`,
};
