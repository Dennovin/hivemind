import clsx from 'clsx';

import { useMatchStats } from 'overlay/MatchStats';
import { useOverlaySettings } from 'overlay/Settings';
import medievalClasses from 'overlay/ingame/medieval/medieval.module.css';
import { BLUE_TEAM, GOLD_TEAM } from 'util/constants';
import GameListTable from './GameListTable';
export default function MatchSummary({ className }) {
  const match = useMatchStats();
  const settings = useOverlaySettings();

  if (match === null) {
    return <></>;
  }

  const blueTeam = {
    slug: BLUE_TEAM,
    name: match?.blueTeam?.name,
    score: match?.blueScore,
  };

  const goldTeam = {
    slug: GOLD_TEAM,
    name: match?.goldTeam?.name,
    score: match?.goldScore,
  };

  const [leftTeam, rightTeam] = settings?.goldOnLeft ? [goldTeam, blueTeam] : [blueTeam, goldTeam];

  return (
    <div
      id="matchSummaryNames"
      className={clsx(
        'flex-grow grid grid-cols-[1fr_3fr_1fr] items-end justify-between p-8 absolute inset-8 h-[19vw]',
        className,
      )}
    >
      <TeamName team={leftTeam} />
      <GameListTable className={clsx('game-list-table')} />
      <TeamName team={rightTeam} />
    </div>
  );
}

const TeamName = ({ team }) => (
  <div key={team.slug} className={clsx('', team.slug, 'team-container last:text-right')}>
    <div
      className={clsx(
        'text-5xl font-bold relative z-50 text-white',
        medievalClasses.fontMedieval,

        team.slug,
        'team-name',
      )}
    >
      {team.name}
    </div>
    {/* <div className={clsx('', team.slug, 'score-counter')}>
  <TeamScoreCounter
    team={team.slug}
    markerFilled={MarkerFilled}
    markerEmpty={MarkerEmpty}
  />
</div> */}
  </div>
);
