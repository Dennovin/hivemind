import { MarkerFilled } from '@/overlay/ingame/medieval/TeamSection';
import clsx from 'clsx';
import { format, parseISO } from 'date-fns';
import medievalClasses from 'overlay/ingame/medieval/medieval.module.css';
import { BLUE_TEAM, GOLD_TEAM } from 'util/constants';

export default function GameResult({ className, game, idx, ...props }) {
  const rowClasses = clsx('', className, {
    'blue-win': game?.winningTeam == BLUE_TEAM,
    'gold-win': game?.winningTeam == GOLD_TEAM,
  });

  if (!game) {
    return <></>;
  }
  const cell = 'flex-grow justify-center text-2xl font-bold engraved-text text-white';
  return (
    <div className="flex items-center flex-col justify-center pt-0 relative w-[112px]">
      <div
        className={clsx(
          'bg-[#292826] inset-0 absolute',
          medievalClasses.ridgeBorder,
          'border-gray-500',
          'mix-blend-soft-ligh -z-[1]',
        )}
      >
        <img
          src="/static/medieval/gothic-top.webp"
          className="absolute block -translate-y-full top-1 -inset-1  h-auto z-0a "
        />
      </div>
      <div className="border-t-4 [border-top-style:ridge] border-t-gray-300 mt-2 w-[88%] flex items-center">
        <div className="relative -top-[7px]  origin-top scale-125 mb-1 mx-auto">
          <MarkerFilled team={game.winningTeam} type={game.winCondition} />
        </div>
      </div>
      <div
        className={clsx(
          'text-3xl font-bold text-gray-200 absolute text-center pt-0.5 left-1/2 -translate-x-1/2 -top-[70px] rounded-full bg-[#2b2b2a] border-gray-600 [border-style:groove] border-4 aspect-square block w-12',
          medievalClasses.fontMedieval,
        )}
      >
        {idx + 1}
      </div>
      <div className={clsx(cell, 'text-center mt-2', medievalClasses.fontMedieval)}>
        {game.mapName}
      </div>
      {/* <div className={clsx(cell, 'text-right')}>{WIN_CONDITIONS[game.winCondition]}</div> */}
      <div className={clsx(cell, 'text-center mb-2', medievalClasses.fontMedieval)}>
        {format(parseISO(game.endTime) - parseISO(game.startTime), 'm:ss')}
      </div>
    </div>
  );
}
