import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Head from 'next/head';
import { useMatchStats } from 'overlay/MatchStats';
import { useOverlaySettings } from 'overlay/Settings';
import GameListTable from './ffvii/GameListTable';
import MatchStatsTable from './ffvii/MatchStatsTable';
import MatchSummary from './ffvii/MatchSummary';

const useStyles = makeStyles(theme => ({
  container: {
    opacity: 0,

    width: '1544px',
    height: '872px',
    position: 'absolute',
    left: 0,
    top: 0,
    padding: '1px 0 0',
    transition: 'opacity 1s',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    gap: '1px',
    color: 'transparent',
    '&.active': {
      opacity: 1,
    },
  },
  boxes: {
    padding: '15px',
    borderRadius: '15px',
    boxShadow: '3px 3px 2px rgba(0, 0, 0, 0.5)',
    // margin: '50px 10px',
  },
  blue: {
    background: theme.gradients.blue.light,
  },
  gold: {
    background: theme.gradients.gold.light,
  },
}));

export default function FFVIIOverlay({}) {
  const classes = useStyles();
  const settings = useOverlaySettings();
  const stats = useMatchStats();

  return (
    <>
      <Head>
        {settings?.additionalFonts && <link rel="stylesheet" href={settings.additionalFonts} />}
        <link
          rel="stylesheet"
          type="text/css"
          href={`/static/ffvii/fonts/final_fantasy/stylesheet.css`}
        />
        <link
          rel="stylesheet"
          type="text/css"
          href={`/static/ffvii/fonts/enge-etienne/stylesheet.css`}
        />
        <link rel="stylesheet" type="text/css" href={`/static/ffvii/fonts/reactor7/webfont.css`} />
      </Head>
      <div id="matchSummaryOverlay" className={clsx(classes.container, { active: stats?.visible })}>
        <MatchSummary className={clsx(classes.matchSummary)} />
        <GameListTable className={clsx(classes.gameListTable)} />
        <MatchStatsTable className={clsx(classes.matchStatsTable)} />
        <div className="custom-color absolute -z-10  shadow-[inset_0_0_5vw_5vw_currentColor] after:content-[''] after:absolute after:inset-0 after:bg-current after:opacity-50  inset-0"></div>
      </div>
    </>
  );
}

FFVIIOverlay.themeProps = {
  name: 'ffvii',
  displayName: 'FF VII',
  description: `Match stats theme inspired by Final Fantasy VII.`,
};
