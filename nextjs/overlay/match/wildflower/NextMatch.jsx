import React from 'react';
import clsx from 'clsx';

import styles from '../Wildflower.module.css';

export default function NextMatch({ team, match }) {
  return (
    <>
      {match?.nextMatch && match.nextMatch[team.slug] && (
        <div className={clsx(classes.teamResult, 'font-custom')}>
          Advances to {match.nextMatch[team.slug]}
        </div>
      )}
      {team.isWinner && match?.winnerPlace && (
        <div className={clsx(classes.teamResult, 'font-custom')}>
          Finishes in {ordinal(match.winnerPlace)} place
        </div>
      )}
      {team.isLoser && match?.loserPlace && (
        <div className={clsx(classes.teamResult, 'font-custom')}>
          Finishes in {ordinal(match.loserPlace)} place
        </div>
      )}
    </>
  );
}
