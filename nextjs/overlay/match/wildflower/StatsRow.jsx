import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import PropTypes from 'prop-types';

import { useMatchStats } from 'overlay/MatchStats';
import { useOverlaySettings } from 'overlay/Settings';
import { BLUE_TEAM, CABINET_POSITIONS, GOLD_TEAM, POSITIONS_BY_TEAM } from 'util/constants';
import styles from '../Wildflower.module.css';

export default function StatsRow({ className, title, statKey, getValue, defaultText, ...props }) {
  const match = useMatchStats();
  const settings = useOverlaySettings();

  if (!match?.stats?.byPlayer) {
    return <></>;
  }

  const statsByPlayer = {};
  for (const pos of Object.values(CABINET_POSITIONS)) {
    statsByPlayer[pos.ID] = {};
    for (const [key, values] of Object.entries(match.stats.byPlayer)) {
      statsByPlayer[pos.ID][key] = values[pos.ID] || defaultText;
    }
  }

  const getStatValue = pos => {
    if (getValue) {
      return getValue(statsByPlayer[pos.ID]);
    } else {
      return statsByPlayer[pos.ID][statKey];
    }
  };

  return (
    <div className={clsx(styles.row, className)}>
      {[...POSITIONS_BY_TEAM[settings?.goldOnLeft ? GOLD_TEAM : BLUE_TEAM]].reverse().map(pos => (
        <div key={pos.ID} className={clsx(styles.cell, 'cell')}>
          {getStatValue(pos)}
        </div>
      ))}

      <div className={clsx(styles.statNameCell, 'stat-name-cell')}>{title}</div>

      {[...POSITIONS_BY_TEAM[settings?.goldOnLeft ? BLUE_TEAM : GOLD_TEAM]].reverse().map(pos => (
        <div key={pos.ID} className={clsx(styles.cell, 'cell')}>
          {getStatValue(pos)}
        </div>
      ))}
    </div>
  );
}

StatsRow.propTypes = {
  className: PropTypes.string,
  title: PropTypes.string.isRequired,
  statKey: PropTypes.string,
  getValue: PropTypes.func,
  defaultText: PropTypes.string,
};

StatsRow.defaultProps = {
  defaultText: '0',
};
