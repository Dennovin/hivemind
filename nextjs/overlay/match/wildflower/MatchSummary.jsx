import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import ordinal from 'ordinal';

import { getAxios } from 'util/axios';
import { useMatchStats } from 'overlay/MatchStats';
import { useOverlaySettings } from 'overlay/Settings';
import { BLUE_TEAM, GOLD_TEAM } from 'util/constants';

import styles from '../Wildflower.module.css';

function nextMatchText(match, team) {
  if (match?.nextMatch && match.nextMatch[team.slug]) {
    return `Advances to ${match.nextMatch[team.slug]}`;
  }

  if (team.isWinner && match?.winnerPlace) {
    return `Finishes in ${ordinal(match.winnerPlace)} place`;
  }

  if (team.isLoser && match?.loserPlace) {
    return `Finishes in ${ordinal(match.winnerPlace)} place`;
  }
}

export default function MatchSummary({ className }) {
  const match = useMatchStats();
  const settings = useOverlaySettings();
  const axios = getAxios();

  const blueTeam = {
    slug: BLUE_TEAM,
    name: match?.blueTeam?.name,
    score: match?.blueScore,
    isWinner: match?.blueScore > match?.goldScore,
  };

  const goldTeam = {
    slug: GOLD_TEAM,
    name: match?.goldTeam?.name,
    score: match?.goldScore,
    isWinner: match?.goldScore > match?.blueScore,
  };

  const teams = settings?.goldOnLeft ? [goldTeam, blueTeam] : [blueTeam, goldTeam];
  const [leftTeam, rightTeam] = teams;
  const winner = blueTeam.score < goldTeam.score ? GOLD_TEAM : BLUE_TEAM;

  return (
    <div className={clsx(styles.matchInfo, 'match-info', className)}>
      <div className={clsx(styles.roundName, 'round-name')}>{match?.roundName}</div>

      <div className={clsx(styles.teamContainer, 'team-container', 'left', leftTeam.slug)}>
        <div
          className={clsx(styles.teamNameContainer, 'team-name-container', 'left', leftTeam.slug)}
        >
          <div className={clsx(styles.teamName, 'team-name', 'left', leftTeam.slug)}>
            {leftTeam.name}
          </div>
          <div className={clsx(styles.nextMatch, 'next-match', 'left', leftTeam.slug)}>
            {nextMatchText(match, leftTeam)}
          </div>
        </div>
        <div className={clsx(styles.teamScore, 'team-score', 'left', leftTeam.slug)}>
          {leftTeam.score}
        </div>
      </div>

      <div className={clsx(styles.teamContainer, 'team-container', 'right', rightTeam.slug)}>
        <div
          className={clsx(styles.teamNameContainer, 'team-name-container', 'left', leftTeam.slug)}
        >
          <div className={clsx(styles.teamName, 'team-name', 'right', rightTeam.slug)}>
            {rightTeam.name}
          </div>
          <div className={clsx(styles.nextMatch, 'next-match', 'right', rightTeam.slug)}>
            {nextMatchText(match, rightTeam)}
          </div>
        </div>
        <div className={clsx(styles.teamScore, 'team-score', 'right', rightTeam.slug)}>
          {rightTeam.score}
        </div>
      </div>
    </div>
  );
}
