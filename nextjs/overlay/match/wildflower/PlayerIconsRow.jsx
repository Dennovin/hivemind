import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { useOverlaySettings } from 'overlay/Settings';
import { BLUE_TEAM, GOLD_TEAM, POSITIONS_BY_TEAM } from 'util/constants';

const useStyles = makeStyles(theme => ({
  row: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around',
    minHeight: '50px',
    marginBottom: '10px',
  },
  icon: {
    flexGrow: 1,
    flexBasis: 0,
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'contain',
  },
  spacer: {
    width: '300px',
    flexBasis: '300px',
    flexGrow: 0,
  },
}));

export default function PlayerIconsRow({ className }) {
  const classes = useStyles();
  const settings = useOverlaySettings();

  return (
    <div className={clsx(classes.row, className)}>
      {[...POSITIONS_BY_TEAM[settings?.goldOnLeft ? GOLD_TEAM : BLUE_TEAM]].reverse().map(pos => (
        <div
          key={pos.ID}
          className={classes.icon}
          style={{ backgroundImage: `url(${pos.ICON})` }}
        />
      ))}

      <div className={classes.spacer} />

      {[...POSITIONS_BY_TEAM[settings?.goldOnLeft ? BLUE_TEAM : GOLD_TEAM]].reverse().map(pos => (
        <div
          key={pos.ID}
          className={classes.icon}
          style={{ backgroundImage: `url(${pos.ICON})` }}
        />
      ))}
    </div>
  );
}
