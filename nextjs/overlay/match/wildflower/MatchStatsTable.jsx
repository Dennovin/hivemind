import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { useMatchStats } from 'overlay/MatchStats';
import { formatTime } from 'util/dates';
import PlayerIconsRow from './PlayerIconsRow';
import StatsRow from './StatsRow';
import styles from '../Wildflower.module.css';

export default function MatchSummary({ className }) {
  const match = useMatchStats();

  if (match === null) {
    return <></>;
  }

  return (
    <div className={clsx(styles.matchStatsTable, 'match-stats-table', className)}>
      <div className={clsx(styles.headerRow, 'header-row')}>
        <PlayerIconsRow />
        <StatsRow title="K/D" getValue={stat => `${stat.kills}-${stat.deaths}`} />
        <StatsRow
          title="Military K/D"
          getValue={stat => `${stat.militaryKills}-${stat.militaryDeaths}`}
        />
        <StatsRow title="Queen Kills" statKey="queenKills" />
        <StatsRow title="Bump Assists" statKey="bumpAssists" />
        <StatsRow
          title="Warrior Uptime"
          getValue={stat => formatTime(stat.warriorUptime)}
          defaultText=""
        />
        <StatsRow title="Berries Deposited" statKey="berries" />
        <StatsRow title="Snail Distance" statKey="snailMeters" />
      </div>
    </div>
  );
}
