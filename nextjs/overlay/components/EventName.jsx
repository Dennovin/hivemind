import clsx from 'clsx';
import { useMatch } from 'overlay/Match';

export default function EventName({ id, className, split = false }) {
  const match = useMatch();

  return match?.roundName ? (
    <span id={id} className={clsx('event-name', className)}>
      {split
        ? match?.roundName.split(' - ').map(name => <span key={name}>{name}</span>)
        : match?.roundName}
    </span>
  ) : null;
}
