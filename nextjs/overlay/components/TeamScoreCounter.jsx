import PropTypes from 'prop-types';
import { useState } from 'react';

import { useMatch } from 'overlay/Match';
import { useOverlaySettings } from 'overlay/Settings';

export default function TeamScoreCounter({ team, markerEmpty, markerFilled, className }) {
  const settings = useOverlaySettings();
  const match = useMatch();
  const [score, setScore] = useState(0);

  const gamesScoreData = match?.completedGames?.filter(game => game.winningTeam === team);

  if (match?.score?.[team] != score) {
    setScore(match.score?.[team]);
  }

  const numMarkers = Math.max(score, match.winsPerMatch);

  if (!settings.showScore) {
    return <></>;
  }

  let scoreMarkers = [];
  for (let m = 0; m < numMarkers; m++) {
    scoreMarkers.push(m < score ? markerFilled : markerEmpty);
  }

  return scoreMarkers
    .slice(0, 4)
    .map((Marker, index) => (
      <Marker
        key={'marker' + index}
        index={index}
        total={match.winsPerMatch}
        type={gamesScoreData?.[index]?.winCondition}
        team={team}
      />
    ));
}

TeamScoreCounter.propTypes = {
  team: PropTypes.string.isRequired,
  markerEmpty: PropTypes.elementType.isRequired,
  markerFilled: PropTypes.elementType.isRequired,
  className: PropTypes.string,
};

TeamScoreCounter.defaultProps = {
  className: undefined,
};
