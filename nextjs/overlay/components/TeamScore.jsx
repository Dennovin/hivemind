import React, { useState } from 'react';
import PropTypes from 'prop-types';
import useColorChange from 'use-color-change';

import { useMatch } from 'overlay/Match';

export default function TeamScore({ team, className, colorOnChange }) {
  const match = useMatch();
  const [score, setScore] = useState(0);
  const colorStyle = useColorChange(score, colorOnChange);

  if (match.score[team] != score) {
    setScore(match.score[team]);
  }

  return (
    <span style={colorStyle} className={className}>{score ?? ''}</span>
  );
}

TeamScore.propTypes = {
  colorOnChange: PropTypes.object,
};

TeamScore.defaultProps = {
  colorOnChange: { duration: 0 },
};
