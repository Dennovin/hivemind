import clsx from 'clsx';
import PropTypes from 'prop-types';
import { useState } from 'react';
import useColorChange from 'use-color-change';

import { useGameStats } from 'overlay/GameStats';

export default function StatValue({ className, statKey, position, colorOnChange, format }) {
  const stats = useGameStats();
  const [value, setValue] = useState(0);
  const colorStyle = useColorChange(value, colorOnChange);

  if (stats === null) {
    return <span className={clsx(className, 'zero', 'missing', 'loading')}>0</span>;
  }

  let statValue = stats[statKey];

  if (statValue === undefined) {
    return <span className={clsx(className, 'zero', 'missing')}>0</span>;
  }

  if (position) {
    if (statKey === 'snailDistance') {
      statValue = Math.floor((statValue[position.ID] ?? 0) / 21);
    } else {
      statValue = statValue[position.ID] ?? 0;
    }
  }

  if (value != statValue) {
    setValue(statValue);
  }

  return (
    <span style={colorStyle} className={clsx(className, { 'opacity-[.35]': value === 0 })}>
      {format(value)}
    </span>
  );
}

StatValue.propTypes = {
  colorOnChange: PropTypes.object,
  format: PropTypes.func,
};

StatValue.defaultProps = {
  colorOnChange: { duration: 0 },
  format: v => v,
};
