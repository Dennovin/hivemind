import { STAT_KEYS } from 'util/constants';
import StatIcon from './StatIcons';

export const STAT_LEGEND = {
  militaryKills: 'military',
  militaryDeaths: 'military',
  totalBerries: 'berries',
  workerKills: 'drone',
  workerDeaths: 'drone',
  snailDistance: 'dist',
};

export default function StatIconLegend() {
  return (
    <div
      id="stats-legend"
      className="max-w-[230px] px-2 grid grid-cols-3 gap-x-1 gap-y-1 opacity-75 text-white bg-black/40 pt-1 pb-2"
    >
      <div className="pl-px text-xs tracking-wider font-bold">KILLS</div>
      <div className="pl-px text-xs tracking-wider font-bold">DEATHS</div>
      <div className="pl-px text-xs tracking-wider font-bold">OBJ</div>
      {STAT_KEYS.map(statKey => (
        <div key={'legend-' + statKey} className="text-white  flex items-center  gap-1 relative">
          <StatIcon statKey={statKey} />
          <span className="text-xs">{STAT_LEGEND[statKey]}</span>
        </div>
      ))}
    </div>
  );
}
