import React, { useRef } from 'react';
import { Doughnut } from 'react-chartjs-2';
import { useGameStats } from 'overlay/GameStats';
import { useTheme } from '@material-ui/core/styles';

export default function PredictionChart({ className, ...props }) {
  const stats = useGameStats();
  const chartRef = useRef(null);
  const theme = useTheme();

  const options = {
    title: {
      display: false,
    },
    animation: { duration: 0 },
    maintainAspectRatio: false,
    plugins: {
      legend: {
        display: false,
      },
    },
  };

  const data = {
    labels: ['Win Probability'],
    datasets: [
      {
        label: 'Win Probability',
        data: [stats?.prediction, 1 - stats?.prediction],
        backgroundColor: [theme.palette.blue.dark2, theme.palette.gold.dark2],
        borderWidth: 1,
        borderColor: 'rgba(255, 255, 255, 0.8)',
        rotation: 180,
        cutout: '20%',
      },
    ],
  };

  return (
    <div className={className} {...props}>
      <Doughnut ref={chartRef} options={options} data={data} />
    </div>
  );
}
