import { useOverlaySettings } from 'overlay/Settings';
import { createContext, useCallback, useContext, useEffect, useState } from 'react';
import { useImmerReducer } from 'use-immer';
import { OVERLAY_SCENES } from 'util/constants';
import { useWebSocket } from 'util/websocket';
const GAME_SCENE = 'HMAuto-game';
const POSTGAME_SCENE = 'HMAuto-post';

const POSTGAME_SCENES_MAP = Object.freeze({
  gold: {
    economic: 'HMAuto-post-gold-econ',
    military: 'HMAuto-post-gold-mil',
    snail: 'HMAuto-post-gold-snail',
  },
  blue: {
    economic: 'HMAuto-post-blue-econ',
    military: 'HMAuto-post-blue-mil',
    snail: 'HMAuto-post-blue-snail',
  },
});
const POSTGAME_SCENE_NAMES = [
  GAME_SCENE,
  POSTGAME_SCENE,
  ...Object.entries(POSTGAME_SCENES_MAP.blue).map(([key, value]) => value),
  ...Object.entries(POSTGAME_SCENES_MAP.gold).map(([key, value]) => value),
];

export const SceneControllerContext = createContext(null);

export function SceneController({ children }) {
  const settings = useOverlaySettings();
  const [obsScene, setObsScene] = useState(null);
  const [showDebug, setShowDebug] = useState(false);
  const [debugLog, setDebugLog] = useImmerReducer((draft, action) => {
    draft.splice(0, 0, action);
  }, []);

  const formatLogEntry = (key, value) => (
    <>
      {new Date().toLocaleTimeString()} -- <span className="uppercase">{key}</span>:{' '}
      <span>{value}</span>
    </>
  );

  const initialState = {
    scene: OVERLAY_SCENES.HIDDEN,
    gameEndTime: null,
    sceneChanges: [],
    gameRunning: true,
  };
  const handleObsScene = newObsScene => {
    if (newObsScene && window.obsstudio?.setCurrentScene && settings?.enableAutoscene) {
      setObsScene('loading');
      window.obsstudio.getCurrentScene(currentObsScene => {
        setObsScene({ last: currentObsScene.name, new: newObsScene });
        if (
          (settings?.enableAutosceneAlways && newObsScene === GAME_SCENE) ||
          currentObsScene.name === GAME_SCENE ||
          POSTGAME_SCENE_NAMES.includes(currentObsScene.name)
        )
          window.obsstudio.setCurrentScene(newObsScene);
      });
      setDebugLog(formatLogEntry('scene change', `${newObsScene}`));
    }
  };
  const reducer = useCallback(
    (draft, action) => {
      const postgameDelayDurationMs = settings?.postgameDelayDurationMs || 6000;
      const postgameSceneDurationMs = settings?.postgameSceneDurationMs || 15000;
      const matchSummaryDurationMs = settings?.matchSummaryDurationMs || 30000;

      switch (action?.type) {
        case 'tick': {
          if (!draft || draft.sceneChanges.length == 0) {
            return draft;
          }

          const currentTime = Date.now();
          const keepSceneChanges = [];

          for (const change of draft.sceneChanges) {
            if (change.actionTime <= currentTime) {
              if (change?.scene) draft.scene = change.scene;
              if (change?.obsScene) handleObsScene(change.obsScene);
            } else {
              keepSceneChanges.push(change);
            }
          }
          draft.sceneChanges = keepSceneChanges;

          return draft;
        }
        case 'clear':
          draft.scene = OVERLAY_SCENES.HIDDEN;
          draft.sceneChanges = [];
          draft.gameRunning = true;
          setDebugLog(
            formatLogEntry(`${action.message.type} clear`, 'Switching to [HMAutoGame] scene'),
          );
          handleObsScene(GAME_SCENE);
          return draft;

        case 'gameend': {
          draft.gameEndTime = Date.now();
          const startPostGameTime = draft.gameEndTime + postgameDelayDurationMs;
          const endPostGameTime =
            draft.gameEndTime + (postgameDelayDurationMs + postgameSceneDurationMs);

          /*
            Schedule the post game
            - Set gameRunning to false
            - Clear the queue
            - Schedule the post game scenes
            - Schedule the win condition scenes
            - Schedule the clear if using autoscene and duration is enabled
          */
          draft.gameRunning = false;
          draft.sceneChanges = [];

          draft.sceneChanges.push({
            actionTime: startPostGameTime,
            scene: OVERLAY_SCENES.POSTGAME,
            obsScene: settings?.enableAutoscene ? POSTGAME_SCENE : undefined,
          });

          // If no autoscene, do default behavior and exit
          if (!settings?.enableAutoscene) {
            draft.sceneChanges.push({
              actionTime: endPostGameTime,
              scene: OVERLAY_SCENES.HIDDEN,
            });
            return draft;
          }

          if (action?.winCondition && action?.winningTeam) {
            draft.sceneChanges.push({
              actionTime: startPostGameTime,
              obsScene: POSTGAME_SCENES_MAP[action.winningTeam][action.winCondition],
            });
          }

          if (settings?.enablePostgameSceneDuration) {
            draft.sceneChanges.push({
              actionTime: endPostGameTime,
              scene: OVERLAY_SCENES.HIDDEN,
              obsScene: GAME_SCENE,
            });
          }

          setDebugLog(formatLogEntry('gameend', 'Scheduled scene changes'));
          return draft;
        }
        case 'match': {
          const currentMatch = action.message.currentMatch;
          setDebugLog(formatLogEntry('match', 'Entered Match'));
          if (
            draft.gameRunning ||
            currentMatch === null ||
            currentMatch?.blueScore > 0 ||
            currentMatch?.goldScore > 0
          ) {
            return draft;
          }

          let changeTime = Date.now() + 1000;
          if (draft.sceneChanges.length > 0) {
            // This loops through the current queue, finds the next CLEAR scene change.
            // If that exists, it adds a one second delay before the match preview shows.
            for (const change of draft.sceneChanges) {
              if (change.scene == OVERLAY_SCENES.HIDDEN) {
                changeTime = change.actionTime + 1000;
              }
            }
          }

          draft.sceneChanges.push({ actionTime: changeTime, scene: OVERLAY_SCENES.MATCH_PREVIEW });
          setDebugLog(formatLogEntry('match', 'Scheduled match preview'));
          return draft;
        }
        case 'matchend': {
          /*
            Schedule the match summary
            - get end time of post game
            - remove any clears
            - add match summary
            - exit early if autoscene is on and duration is off
            - add clear if autoscene is off or duration is enabled
          */

          const endOfPostGameTime =
            draft.sceneChanges.find(c => c.scene == OVERLAY_SCENES.HIDDEN)?.actionTime ??
            draft.sceneChanges.find(c => c.scene == OVERLAY_SCENES.POSTGAME)?.actionTime +
              postgameSceneDurationMs;

          draft.sceneChanges = draft.sceneChanges.filter(c => c.scene !== OVERLAY_SCENES.HIDDEN);

          draft.sceneChanges.push({
            actionTime: endOfPostGameTime,
            scene: OVERLAY_SCENES.MATCH_SUMMARY,
          });

          setDebugLog(formatLogEntry('matchend', 'Scheduled match summary'));
          if (settings?.enableAutoscene && !settings?.enablePostgameSceneDuration) return draft;

          draft.sceneChanges.push({
            actionTime: endOfPostGameTime + matchSummaryDurationMs,
            scene: OVERLAY_SCENES.HIDDEN,
            obsScene: GAME_SCENE,
          });
          setDebugLog(formatLogEntry('matchend', 'Scheduled clear'));
          return draft;
        }
        default:
          throw new Error();
      }
    },
    [
      settings?.postgameSceneDurationMs,
      settings?.postgameDelayDurationMs,
      settings?.enableAutoscene,
      settings?.enablePostgameSceneDuration,
      settings?.enableMatchSummaryDuration,
      settings?.matchSummaryDurationMs,
    ],
  );

  const [state, dispatch] = useImmerReducer(reducer, initialState);

  useEffect(() => {
    const tick = setInterval(() => {
      dispatch({ type: 'tick' });
    }, 250);

    return () => clearInterval(tick);
  }, []);

  const ws = useWebSocket('/ws/gamestate');

  ws.onJsonMessage(
    message => {
      if (message.cabinetId != settings?.cabinet?.id) {
        return;
      }
      // leaving in for debugging on prod
      console.log(message.type, Date());
      switch (message.type) {
        case 'gameend':
          dispatch({
            type: 'gameend',
            winCondition: message.winCondition,
            winningTeam: message.winningTeam,
          });
          break;
        case 'matchend':
          dispatch({ type: 'matchend', message });
          break;
        case 'match':
          dispatch({ type: 'match', message });
          break;
        case 'playernames':
        case 'gamestart':
          dispatch({ type: 'clear', message });
          break;
        default:
          break;
      }
    },
    [settings?.cabinet?.id],
  );

  return (
    <SceneControllerContext.Provider value={state.scene}>
      <button
        className="absolute z-50 inset-0 p-6 bg-indigo-500 text-white opacity-0"
        onClick={() => setShowDebug(v => !v)}
      >
        Debug
      </button>
      {showDebug && (
        <>
          <div className="rounded bg-black/70 text-white absolute p-6 z-50 top-6 left-6 text-lg capitalize max-w-[40%]">
            <div className="flex gap-2 mb-4">
              <button
                onClick={() =>
                  dispatch({
                    type: 'clear',
                    message: {
                      type: 'gamestart',
                      cabinetId: '99',
                      cabinetName: 'test1',
                      sceneName: 'test',
                      gameId: '861560',
                    },
                  })
                }
              >
                START GAME
              </button>
              <button
                onClick={() =>
                  dispatch({
                    type: 'gameend',
                    winCondition:
                      Math.random() > 0.5
                        ? Math.random() > 0.5
                          ? 'military'
                          : 'snail'
                        : 'economic',
                    winningTeam: Math.random() > 0.5 ? 'blue' : 'gold',
                  })
                }
              >
                END GAME
              </button>
              <button
                onClick={() =>
                  dispatch({
                    type: 'match',
                    message: {
                      type: 'match',
                      matchType: 'tournament',
                      sceneName: 'test',
                      cabinetName: 'test1',
                      cabinetId: 99,
                      currentMatch: {
                        id: 3578,
                        blueTeam: 'Checks yoself, Wrecks yoself',
                        blueScore: 0,
                        goldTeam: 'Prince Pinche',
                        goldScore: 0,
                        isWarmup: true,
                        roundsPerMatch: 0,
                        winsPerMatch: 1,
                        roundName: 'Stage 1 - Octofinals',
                      },
                      onDeck: null,
                    },
                  })
                }
              >
                START MATCH
              </button>
              <button
                onClick={() =>
                  dispatch({
                    type: 'matchend',
                    message: {
                      type: 'matchend',
                      matchId: 3578,
                      cabinetId: 99,
                      blueTeam: 'Checks yoself, Wrecks yoself',
                      blueScore: 0,
                      goldTeam: 'Prince Pinche',
                      goldScore: 1,
                    },
                  })
                }
              >
                END MATCH
              </button>
            </div>
            <h2 className="mt-0 text-xl">Auto Scene Debug Info:</h2>
            <div>
              <span className="font-bold">AutoScene Enabled:</span>{' '}
              {settings?.enableAutoscene.toString()}
            </div>
            <div>
              <span className="font-bold">Enable Duration:</span>{' '}
              {settings?.enablePostgameSceneDuration.toString()}
            </div>
            <div>
              <span className="font-bold">Enable Always Switch:</span>{' '}
              {settings?.enableAutosceneAlways.toString()}
            </div>
            <div>
              <span className="font-bold">Delay:</span> {settings?.postgameDelayDurationMs}ms
            </div>
            <div>
              <span className="font-bold">Duration:</span> {settings?.postgameSceneDurationMs}ms
            </div>
            {Object.entries(state).map(([key, value]) => {
              if (typeof value === 'object') {
                Object.entries(state).map(([key, value]) => (
                  <div key={key}>
                    <span className="font-bold">{key}:</span> {value?.toString()}
                  </div>
                ));
              } else {
                return (
                  <div key={key}>
                    <span className="font-bold">{key}:</span> {value?.toString()}
                  </div>
                );
              }
            })}

            <div>
              <span className="font-bold">Last OBS Scene:</span> {obsScene?.last}
            </div>
            <div>
              <span className="font-bold">New OBS Scene:</span> {obsScene?.new}
            </div>
            <h4 className="my-0">Change Queue</h4>
            <table className=" w-full text-sm">
              <thead>
                <tr>
                  <th className="text-xs text-left bg-white/10">Time</th>
                  <th className="text-xs text-left bg-white/10">Overlay</th>
                  <th className="text-xs text-left bg-white/10">OBS Scene</th>
                </tr>
              </thead>
              <tbody>
                {state.sceneChanges.map((change, i) => (
                  <tr key={i}>
                    <td className="px-1">{change.actionTime}</td>
                    <td className="px-1">{change.scene}</td>
                    <td className="px-1">{change.obsScene}</td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </>
      )}
      {showDebug && (
        <div className="absolute z-50 right-6 top-6 bottom-6 overflow-scroll max-w-[60%] bg-black/70 text-white p-6 rounded ">
          <h2 className="mt-0 text-xl text-white">Event Log:</h2>
          <ul className="flex flex-col gap-2">
            {debugLog.map((log, i) => (
              <li className="first:opacity-100 opacity-80" key={i}>
                {log}
              </li>
            ))}
          </ul>
        </div>
      )}
      {children}
    </SceneControllerContext.Provider>
  );
}

export function useSceneController() {
  return useContext(SceneControllerContext);
}
