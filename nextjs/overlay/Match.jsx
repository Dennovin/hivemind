import { createContext, useContext, useEffect, useRef, useState } from 'react';

import { WhiteboardProviderByCabinet, useWhiteboard } from '@/components/whiteboard';
import { useOverlaySettings } from 'overlay/Settings';
import { getAxios } from 'util/axios';
import { BLUE_TEAM, GOLD_TEAM, STAGE_TYPE, TEAMS } from 'util/constants';
import { useWebSocket } from 'util/websocket';

export const MatchContext = createContext({});
export const POST_MATCH_DELAY = 21000;

export function MatchProviderInner({ cabinetId, children }) {
  const axios = getAxios();
  const settings = useOverlaySettings();
  const whiteboard = useWhiteboard();
  const [initialized, setInitialized] = useState(false);
  const [showScore, setShowScore] = useState(false);
  const [roundName, setRoundName] = useState(null);
  const [eventInProgress, setEventInProgress] = useState(false);
  const [winsPerMatch, setWinsPerMatch] = useState(null);
  const [roundsPerMatch, setRoundsPerMatch] = useState(null);
  const [matchId, setMatchId] = useState(null);
  const [isWarmUp, setIsWarmUp] = useState(false);
  const [players, setPlayers] = useState({});
  const [matchInfo, setMatchInfo] = useState(null);
  const [bracket, setBracket] = useState(null);
  const [groupStandings, setGroupStandings] = useState(null);
  const [completedGames, setCompletedGames] = useState(null);

  const resetNamesTimeoutRef = useRef(null);

  const tournamentTeamsInfo = {};
  const teamName = {};
  const setTeamName = {};
  const tournamentTeamId = {};
  const setTournamentTeamId = {};
  const score = {};
  const setScore = {};

  useEffect(() => {
    setFromOverlaySettings();
  }, [settings]);

  for (const team of TEAMS) {
    [teamName[team], setTeamName[team]] = useState(null);
    [score[team], setScore[team]] = useState(null);
    [tournamentTeamId[team], setTournamentTeamId[team]] = useState(null);
  }
  const [teamImages, setTeamImages] = useState({});

  const setResetNamesTimeout = timeout => {
    // Make sure we have one timer running at most
    if (resetNamesTimeoutRef.current) {
      clearTimeout(resetNamesTimeoutRef.current);
    }

    resetNamesTimeoutRef.current = timeout;
  };

  const ws = useWebSocket('/ws/gamestate');

  ws.onJsonMessage(
    message => {
      if (message.type == 'match' && message.cabinetId == settings?.cabinet?.id) {
        if (message.currentMatch === null) {
          setMatchId(null);
          setIsWarmUp(false);

          // If a match ends, don't reset the names immediately
          const timeout = setTimeout(() => {
            setCompletedGames([]);
            setFromOverlaySettings({ clearEvent: true });
          }, POST_MATCH_DELAY);
          setResetNamesTimeout(timeout);
        } else {
          setEventInProgress(true);

          if (message.matchType == 'tournament') {
            axios.get(`/api/tournament/match/${message.currentMatch.id}/`).then(response => {
              setMatchInfo(response.data);

              axios.get(`/api/tournament/bracket/${response.data.bracket}/`).then(response => {
                setBracket(response.data);
                axios.get(`/api/tournament/bracket/${response.data.id}/teams/`).then(response => {
                  setBracket(v => ({ ...v, teams: response.data }));
                });

                if (response.data.stageType === STAGE_TYPE.ROUND_ROBIN) {
                  axios
                    .get(`/api/tournament/bracket/${response.data.id}/standings/`)
                    .then(response => {
                      setGroupStandings(response.data.standings);
                    });
                }
              });

              if (
                tournamentTeamId[BLUE_TEAM] !== response.data.blueTeam ||
                tournamentTeamId[GOLD_TEAM] !== response.data.goldTeam
              ) {
                setTournamentTeamId[BLUE_TEAM](response.data.blueTeam);
                setTournamentTeamId[GOLD_TEAM](response.data.goldTeam);
                getTournamentPlayers(BLUE_TEAM, response.data.blueTeam);
                getTournamentPlayers(GOLD_TEAM, response.data.goldTeam);
              }
            });
          }

          if (message.matchType == 'league' && message.currentMatch.id !== matchId) {
            setPlayers({});
            const matchType = message.currentMatch.isQuickplay ? 'qpmatch' : 'match';
            axios.get(`/api/league/${matchType}/${message.currentMatch.id}/`).then(response => {
              getLeaguePlayers(response.data);
            });
          }

          const update = () => {
            setTeamName[BLUE_TEAM](message.currentMatch.blueTeam);
            setTeamName[GOLD_TEAM](message.currentMatch.goldTeam);
            setTeamImages({
              [BLUE_TEAM]: message.currentMatch.blueTeamImage,
              [GOLD_TEAM]: message.currentMatch.goldTeamImage,
            });
            setScore[BLUE_TEAM](message.currentMatch.blueScore);
            setScore[GOLD_TEAM](message.currentMatch.goldScore);
            setRoundName(message.currentMatch.roundName);
            setWinsPerMatch(message.currentMatch.winsPerMatch);
            setRoundsPerMatch(message.currentMatch.roundsPerMatch);
            setMatchId(message.currentMatch.id);
            setIsWarmUp(message.currentMatch.isWarmup || false);
            setShowScore(true);
          };

          if (!matchId || message.currentMatch.id === matchId) {
            update();
          } else {
            const timeout = setTimeout(update, POST_MATCH_DELAY);
            setResetNamesTimeout(timeout);
          }

          // If a new match gets selected before the names are reset,
          // clear the timeout so the new match stays up
          if (resetNamesTimeoutRef.current) {
            clearTimeout(resetNamesTimeoutRef.current);
            resetNamesTimeoutRef.current = null;
          }

          return;
        }
      }

      if (
        message.type === 'gameend' &&
        parseInt(message.cabinetId) === parseInt(settings?.cabinet?.id)
      ) {
        setCompletedGames(val => {
          return val ? [...val, { ...message }] : [{ ...message }];
        });
      }
    },
    [settings?.cabinet?.id],
  );

  const getTournamentTeamInfo = async id => {
    if (!(id in tournamentTeamsInfo)) {
      let response = await axios.get(`/api/tournament/team/${id}/`);
      tournamentTeamsInfo[id] = response.data;
    }

    return tournamentTeamsInfo[id];
  };

  const getTournamentPlayers = (teamColor, team) => {
    setPlayers(players => ({ ...players, [teamColor]: undefined }));

    if (team) {
      axios.get(`/api/tournament/player/`, { params: { teamId: team } }).then(response => {
        setPlayers(players => ({ ...players, [teamColor]: response.data.results }));
      });
    }
  };

  const getLeaguePlayers = matchData => {
    setPlayers({});

    const blueTeamId = matchData.blueTeam?.id ?? matchData.blueTeamId;
    const goldTeamId = matchData.goldTeam?.id ?? matchData.goldTeamId;

    if (blueTeamId && goldTeamId) {
      axios.get(`/api/league/player/`, { params: { teamId: blueTeamId } }).then(response => {
        setPlayers(players => ({ ...players, blue: response.data.results }));
      });
      axios.get(`/api/league/player/`, { params: { teamId: goldTeamId } }).then(response => {
        setPlayers(players => ({ ...players, gold: response.data.results }));
      });
    } else {
      axios.get(`/api/league/qpmatch/${matchData.id}/players/`).then(response => {
        setPlayers(response.data);
      });
    }
  };

  const getWhiteboardPlayers = () => {
    if (whiteboard?.currentMatch) {
      setPlayers({
        blue: whiteboard.currentMatch.bluePlayers,
        gold: whiteboard.currentMatch.goldPlayers,
      });
    }
  };

  const setFromOverlaySettings = options => {
    if (settings && (!eventInProgress || options?.clearEvent)) {
      setEventInProgress(false);
      setTeamName[BLUE_TEAM](settings.blueTeam);
      setTeamName[GOLD_TEAM](settings.goldTeam);
      setTeamImages({});
      setScore[BLUE_TEAM](settings.blueScore);
      setScore[GOLD_TEAM](settings.goldScore);
      setShowScore(settings.showScore);
      setWinsPerMatch(settings.matchWinMax);
      setRoundsPerMatch(null);
      setRoundName('');
      setPlayers({});
      setMatchInfo(null);
      setBracket(null);
      setGroupStandings(null);
    }
  };

  const loadMatchData = async () => {
    // Check for tournament match
    let response = await axios.get(`/api/tournament/match/`, {
      params: { activeCabinetId: settings.cabinet.id },
    });
    if (response.data.count > 0) {
      setEventInProgress(true);
      const matchData = response.data.results[0];

      response = await axios.get(`/api/tournament/bracket/${matchData.bracket}/`);
      const bracketData = response.data;

      const blueTeamInfo = await getTournamentTeamInfo(matchData.blueTeam);
      const goldTeamInfo = await getTournamentTeamInfo(matchData.goldTeam);
      console.log('bgi', blueTeamInfo);
      setTeamName[BLUE_TEAM](blueTeamInfo.name);
      setTeamName[GOLD_TEAM](goldTeamInfo.name);
      setTeamImages({
        [BLUE_TEAM]: blueTeamInfo.image,
        [GOLD_TEAM]: goldTeamInfo.image,
      });
      setScore[BLUE_TEAM](matchData.blueScore);
      setScore[GOLD_TEAM](matchData.goldScore);
      if (matchData.roundName) {
        setRoundName(`${bracketData.name} - ${matchData.roundName}`);
      } else {
        setRoundName(bracketData.name);
      }

      setShowScore(true);
      setTournamentTeamId[BLUE_TEAM](matchData.blueTeam);
      setTournamentTeamId[GOLD_TEAM](matchData.goldTeam);

      getTournamentPlayers(BLUE_TEAM, matchData.blueTeam);
      getTournamentPlayers(GOLD_TEAM, matchData.goldTeam);

      response = await axios.getAllPages(`/api/game/game/`, {
        params: { tournamentMatchId: matchData.id },
      });
      setCompletedGames(response);

      return;
    }

    // Check for event in progress
    response = await axios.get(`/api/league/event/`, {
      params: { cabinetId: settings.cabinet.id, isActive: true },
    });
    if (response.data.count > 0) {
      const eventData = response.data.results[0];
      const params = { eventId: eventData.id, isComplete: false };
      const matchType = eventData.isQuickplay ? 'qpmatch' : 'match';
      const url = `/api/league/${matchType}/`;

      response = await axios.get(url, { params });
      if (response.data.count > 0) {
        const matchData = response.data.results[0];
        setEventInProgress(true);

        setTeamName[BLUE_TEAM](matchData.blueTeam?.name || 'Blue Team');
        setTeamName[GOLD_TEAM](matchData.goldTeam?.name || 'Gold Team');
        setTeamImages({
          [BLUE_TEAM]: matchData.blueTeam?.image,
          [GOLD_TEAM]: matchData.goldTeam?.image,
        });
        setScore[BLUE_TEAM](matchData.blueScore);
        setScore[GOLD_TEAM](matchData.goldScore);
        setWinsPerMatch(matchData.winsPerMatch);
        setRoundsPerMatch(matchData.roundsPerMatch);
        setRoundName(eventData.name);
        setShowScore(true);
        getLeaguePlayers(matchData);

        return;
      }
    }

    // Use overlay settings
    setFromOverlaySettings({ clearEvent: true });
  };

  if (settings?.cabinet && !initialized) {
    setInitialized(true);
    loadMatchData();
  }

  // Set whiteboard player names if there's an active match
  useEffect(() => {
    if (whiteboard?.currentMatch && whiteboard?.status?.cabinet === settings?.cabinet?.id) {
      setShowScore(true);
      setScore[BLUE_TEAM](whiteboard.currentMatch.blueScore);
      setScore[GOLD_TEAM](whiteboard.currentMatch.goldScore);
      getWhiteboardPlayers();
    }
  }, [whiteboard?.currentMatch, settings?.cabinet?.id]);

  const context = {
    teamName,
    score,
    showScore,
    roundName,
    winsPerMatch,
    roundsPerMatch,
    players,
    isWarmUp,
    matchInfo,
    bracket,
    groupStandings,
    tournamentTeamId,
    completedGames,
    teamImages,
  };

  return <MatchContext.Provider value={context}>{children}</MatchContext.Provider>;
}

export function MatchProvider({ cabinetId, children }) {
  const settings = useOverlaySettings();
  if (!settings?.cabinet?.scene) return null;

  return (
    <WhiteboardProviderByCabinet cabinetId={cabinetId}>
      <MatchProviderInner cabinetId={cabinetId}>{children}</MatchProviderInner>
    </WhiteboardProviderByCabinet>
  );
}

export function useMatch() {
  return useContext(MatchContext);
}
