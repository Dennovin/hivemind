import Bb5 from './Bb5PostGame';
import Default from './Default';
import FFVII from './FFVII';
import Medieval from './Medieval';
import Racing from './Racing';
import Wildflower from './Wildflower';
const allPostgameThemes = [Default, Wildflower, FFVII, Medieval, Racing, Bb5];

export function getPostgameTheme(name) {
  for (const theme of allPostgameThemes) {
    if (theme.themeProps.name == name) {
      return theme;
    }
  }
}

export { allPostgameThemes };
