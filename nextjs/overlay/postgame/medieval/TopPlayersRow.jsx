import clsx from 'clsx';

import { POSITIONS_BY_ID } from 'util/constants';

// const useStyles = makeStyles(theme => ({
//   row: {
//     display: 'flex',
//     flexDirection: 'row',
//     margin: '0.5vw 0',
//   },
//   label: {
//     flexGrow: 3,
//     flexBasis: 0,
//     fontSize: '1.5vw',
//     whiteSpace: 'nowrap',
//     margin: '0 1vw',
//     fontWeight: 'bold',
//   },
//   value: {
//     fontSize: '1.5vw',
//     textAlign: 'right',
//     flexGrow: 1,
//     flexBasis: 0,
//     margin: '0 1vw',
//     padding: '0 0.5vw',
//     fontWeight: 'bold',
//   },
//   icons: {
//     flexGrow: 2,
//     flexBasis: 0,
//     textAlign: 'right',
//   },
//   icon: {
//     height: '2vw',
//     marginLeft: '5px',
//   },
// }));

export default function TopPlayersRow({ label, value, from, format, className }) {
  if (!value?.count && !from) {
    return <></>;
  }

  let count = value?.count;
  let players = value?.players;

  if (from) {
    count = Math.max(...Object.values(from));
    if (count <= 0) {
      return <></>;
    }

    players = Object.keys(from).filter(p => from[p] == count);
  }

  if (format) {
    count = format(count);
  }

  return (
    <div className={clsx('flex p-[0.25vw] justify-between engraved-light-text', className)}>
      <div className={'text-[1.25vw] font-bold flex-grow'}>{label}</div>
      <div className={'text-[1.25vw] font-bold pr-[1vw]'}>{count}</div>
      <div className={'text-[1.25vw] font-bold'}>
        {players &&
          players.map(id => (
            <img key={id} src={POSITIONS_BY_ID[id].ICON} className={'w-[2vw] aspect-square'} />
          ))}
      </div>
    </div>
  );
}
