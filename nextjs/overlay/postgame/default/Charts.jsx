import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import BerryChart from './BerryChart';
import SnailChart from './SnailChart';
import WarriorChart from './WarriorChart';
import WinProbabilityChart from './WinProbabilityChart';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    gap: '0.25vw',
    padding: '0.25vw',
  },
  chart: {
    flexGrow: 1,
    flexBasis: 0,
  },
}));

export default function Charts({ className }) {
  const classes = useStyles();

  return (
    <div className={clsx(classes.container, className)}>
      <SnailChart className={classes.chart} />
      <BerryChart className={classes.chart} />
      <WarriorChart className={classes.chart} />
      <WinProbabilityChart className={classes.chart} />
    </div>
  );
}
