import PropTypes from 'prop-types';
import QRCode from 'qrcode';
import { useMemo, useState } from 'react';

import { usePostgameStats } from 'overlay/PostgameStats';

export default function GameLinkQrCode({ className, lightColor, darkColor }) {
  const stats = usePostgameStats();
  const [src, setSrc] = useState(null);

  const opts = {
    color: {
      dark: darkColor,
      light: lightColor,
    },
    output: 'svg',
  };

  useMemo(() => {
    if (stats?.gameId) {
      QRCode.toDataURL(`${window.location.origin}/game/${stats.gameId}`, opts).then(setSrc);
      // QRCode.toString(`${window.location.origin}/game/${stats.gameId}`, opts).then(setSrc);
    }
  }, [stats]);

  return <img src={src} className={className} />;

  // return <div className="w-32 h-32" dangerouslySetInnerHTML={{ __html: src }}></div>;
}

GameLinkQrCode.propTypes = {
  lightColor: PropTypes.string,
  darkColor: PropTypes.string,
};

GameLinkQrCode.defaultProps = {
  lightColor: '#00000000',
  darkColor: undefined,
};
