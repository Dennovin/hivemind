import { makeStyles, useTheme } from '@material-ui/core/styles';
import clsx from 'clsx';
import Head from 'next/head';
import { defaults } from 'chart.js';

import { usePostgameStats } from 'overlay/PostgameStats';
import { useOverlaySettings } from 'overlay/Settings';
import { BLUE_TEAM, GOLD_TEAM } from 'util/constants';
import Awards from './racing/Awards';
import Charts from './racing/Charts';
import GameSummary from './racing/GameSummary';
import TeamStats from './racing/TeamStats';
import TopPlayers from './racing/TopPlayers';

const useStyles = makeStyles(theme => ({
  background: {
    opacity: 0,
    position: 'absolute',
    left: 0,
    top: 0,
    transition: 'opacity 1s',
    '&.active': {
      opacity: 1,
    },
  },
  container: {
    fontFamily: 'Orbitron, sans-serif',
    fontOpticalSizing: "auto",
    fontWeight: 400,
    fontStyle: "normal",
    fontSize: '1.5vw',

    color: "#FFFFFF",
    textShadow: "#1A1A1A 1px 1px",
    opacity: 0,
    width: '100vw',
    height: '100vh',
    position: 'absolute',
    left: 0,
    top: 0,
    transition: 'opacity 1s',
    display: 'flex',
    flexDirection: 'row',
    maxWidth: 'unset',
    '&.active': {
      opacity: 1,
    },
  },
  blueWin: {
    background: "linear-gradient(to right, #030846, #030846) padding-box, linear-gradient(to right, #0000FF, #0000FF) border-box",
    borderRadius: "8px",
    border: "4px solid transparent",
    color: "#FFFFFF",
    textShadow: "#1A1A1A 1px 1px",
  },
  goldWin: {
    background: "linear-gradient(to right, #211708, #211708) padding-box, linear-gradient(to right, #DB5C09, #DB5C09) border-box",
    borderRadius: "8px",
    border: "4px solid transparent",
    color: "#FFFFFF",
    textShadow: "#1A1A1A 1px 1px",
  },
  gameSummary: {
    flexGrow: 0,
  },
  charts: {
    borderRadius: "4px",
    padding: "0px",
    border: "4px solid transparent",
    background: "linear-gradient(to right, #040404, #040404) content-box, linear-gradient(to right, #292323 0%, #464040 36%, #CEC6C6 49%, #464040 62%, #292323 100%) border-box",

    color: "#FFFFFF",
    textShadow: "#1A1A1A 1px 1px",
    marginRight: '1px',
    width: '50vw',
    flexBasis: '50vw',

    fontFamily: 'Orbitron, sans-serif',
    fontOpticalSizing: "auto",
    fontWeight: 400,
    fontStyle: "normal",
  },
  rightSide: {
    flexBasis: 0,
    flexGrow: 1,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
}));

export default function Racing({}) {
  const classes = useStyles();
  const settings = useOverlaySettings();
  const stats = usePostgameStats();
  const theme = useTheme();

  const boxClasses = clsx('box');

  const winClasses = clsx({
    [classes.blueWin]: stats?.winningTeam === BLUE_TEAM,
    [classes.goldWin]: stats?.winningTeam === GOLD_TEAM,
  });

  // Chart.js defaults
  defaults.borderColor = "#FFFFFF";
  defaults.color = "#FFFFFF";
  defaults.font = {
    family: 'DS-Digital Italic, sans-serif',
    size: 14,
    style: "normal",
  }

  return (
    <>
      <Head>
        {settings?.additionalFonts && <link rel="stylesheet" href={settings.additionalFonts}/>}
        <link href="https://fonts.googleapis.com/css2?family=Orbitron:wght@400..900&display=swap" rel="stylesheet"/>
        <link rel="stylesheet" type="text/css" href={`/static/racing/fonts/ds-digital/style.css`}/>
      </Head>

      <div
        id="postGameOverlay"
        className={clsx(classes.background, 'background relative font-sans', {
          active: stats?.visible,
        })}
      >
        <div
          className={clsx(classes.container, classes[stats?.map], 'container', {
            active: stats?.visible,
          })}
        >
          <Charts className={clsx(classes.charts, 'charts', 'box', boxClasses)} />

          <div className={clsx(classes.rightSide, 'right-side justify-start')}>
            <GameSummary className={clsx(classes.gameSummary, 'game-summary', boxClasses, winClasses)} />
            <Awards
              className={clsx(
                classes.statsBox,
                'awards text-white',
                classes.boxes,
              )}
            />
            <TeamStats
              className={clsx(
                classes.statsBox,
                'team-stats flex-grow flex flex-col justify-around',
                boxClasses,
              )}
            />
            <TopPlayers
              className={clsx(
                classes.statsBox,
                'top-players flex-grow flex flex-col justify-around',
                boxClasses,
              )}
            />
          </div>
        </div>
      </div>
    </>
  );
}

Racing.themeProps = {
  name: 'racing',
  displayName: 'Racing (GDC VIII)',
  description: `Colorful, racing themed overlay.`,
};
