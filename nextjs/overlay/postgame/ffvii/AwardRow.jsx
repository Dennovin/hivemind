import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import { sprite } from './constants';

import { POSITIONS_BY_ID } from 'util/constants';

const useStyles = makeStyles(theme => ({
  row: {
    display: 'flex',
    flexDirection: 'row',
  },
  label: {
    flexGrow: 3,
    flexBasis: 0,
    lineHeight: '2vw',
    whiteSpace: 'nowrap',
    margin: '0 1vw',
  },
  icons: {
    flexGrow: 2,
    flexBasis: 0,
    textAlign: 'right',
  },
  icon: {
    height: '3vw',
    marginLeft: '5px',
    marginTop: '-0.5vw',
  },
  iconHolder: {
    height: '3vw',
    width: '3vw',
    display: 'inline',
  },
  teamIcon: {
    height: '3vw',
    width: '3vw',
    border: '3px solid #ffffff99',
    marginLeft: '5px',
    marginTop: '-0.5vw',
    display: 'inline-block',
    '&.blue': {
      backgroundColor: theme.palette.blue.dark2,
    },
    '&.gold': {
      backgroundColor: theme.palette.gold.dark2,
    },
  },
}));

export default function AwardRow({ label, players, teams, className }) {
  const classes = useStyles();

  return (
    <div className={clsx(classes.row, className)}>
      <div className={classes.label}>{label}</div>
      <div className={classes.icons}>
        {players &&
          players.map(id => (
            <div className={clsx(classes.iconHolder, 'icon-holder')}>
              <img key={id} src={sprite(POSITIONS_BY_ID[id])} className={classes.icon} />
            </div>
          ))}
        {teams && teams.map(id => <div key={id} className={clsx(classes.teamIcon, id)}></div>)}
      </div>
    </div>
  );
}
