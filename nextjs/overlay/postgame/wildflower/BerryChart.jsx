import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { usePostgameStats } from 'overlay/PostgameStats';
import ChartContainer from './ChartContainer';
import BaseBerryChart from 'components/charts/BaseBerryChart';

const useStyles = makeStyles(theme => ({
}));

export default function BerryChart({ className }) {
  const classes = useStyles();
  const stats = usePostgameStats();

  return (
    <ChartContainer title="Berries" className={className}>
      {stats?.berryData && (
        <BaseBerryChart game={stats} datasetProps={{ pointRadius: 0 }} />
      )}
    </ChartContainer>
  );
}
