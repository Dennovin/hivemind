import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import {sprite} from "./constants";

import { POSITIONS_BY_ID } from 'util/constants';

const useStyles = makeStyles(theme => ({
  row: {
    display: 'flex',
    flexDirection: 'row',
  },
  label: {
    flexGrow: 3,
    flexBasis: 0,
    whiteSpace: 'nowrap',
    margin: 'auto 1vw',
  },
  value: {
    textAlign: 'right',
    flexGrow: 1,
    flexBasis: 0,
    margin: '0 1vw',
    padding: '0 0.5vw',

    fontFamily: 'DS-Digital Italic',
    fontSize: "2vw",
  },
  icons: {
    flexGrow: 2,
    flexBasis: 0,
    textAlign: 'right',
    display: 'flex',
    justifyContent: 'right',
    alignItems: 'center',
  },
  icon: {
    height: '3vw',
    marginLeft: '5px',
  },
  iconHolder: {
    width: '3vw',
    alignItems: 'center',
    textAlign: 'center',
    height: '3vw',
  }
}));

export default function TopPlayersRow({ label, value, from, format, className, showWarrior }) {
  const classes = useStyles();

  if (!value?.count && !from) {
    return <></>;
  }

  let count = value?.count;
  let players = value?.players;

  if (from) {
    count = Math.max(...Object.values(from));
    if (count <= 0) {
      return <></>;
    }

    players = Object.keys(from).filter(p => from[p] == count);
  }

  if (format) {
    count = format(count);
  }

  return (
    <div className={clsx(classes.row, className)}>
      <div className={classes.label}>{label}</div>
      <div className={classes.icons}>
        {players &&
          players.map(id => (
            <div className={clsx(classes.iconHolder, 'icon-holder')}>
              <img key={id} src={sprite(POSITIONS_BY_ID[id], showWarrior)} className={classes.icon} />
            </div>
          ))}
      </div>
      <div className={classes.value}>{count}</div>
    </div>
  );
}
