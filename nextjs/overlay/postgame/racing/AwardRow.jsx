import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import { sprite } from './constants';

import { POSITIONS_BY_ID } from 'util/constants';

const useStyles = makeStyles(theme => ({
  row: {
    display: 'flex',
    flexDirection: 'row',
  },
  label: {
    flexGrow: 3,
    flexBasis: 0,
    lineHeight: '1.5vw',
    whiteSpace: 'nowrap',
    margin: '0 1vw',
  },
  icons: {
    flexGrow: 2,
    flexBasis: 0,
    textAlign: 'right',
  },
  icon: {
    height: '3vw',
    marginLeft: '5px',
    marginTop: '-0.5vw',
  },
  iconHolder: {
    height: '3vw',
    width: '3vw',
    display: 'inline',
  },
  teamIcon: {
    height: '3vw',
    width: '3vw',
    marginLeft: '5px',
    marginTop: '-0.5vw',
    display: 'inline-block',
    // TODO: Waiting on Steve design advice since the gold colors aren't great here
    // https://discord.com/channels/714546857363111977/1271968491766939712/1288666528207736842
    '&.blue': {
      background: "linear-gradient(to right, #030846, #030846) content-box, linear-gradient(to right, #0000FF, #0000FF) border-box",
      borderRadius: "8px",
      border: "4px solid transparent",
      color: "#FFFFFF",
      textShadow: "#1A1A1A 1px 1px",
    },
    '&.gold': {
      background: "linear-gradient(to right, #211708, #211708) content-box, linear-gradient(to right, #DB5C09, #DB5C09) border-box",
      borderRadius: "8px",
      border: "4px solid transparent",
      color: "#FFFFFF",
      textShadow: "#1A1A1A 1px 1px",
    },
  },
}));

export default function AwardRow({ label, players, teams, className }) {
  const classes = useStyles();

  return (
    <div className={clsx(classes.row, className)}>
      <div className={classes.label}>{label}</div>
      <div className={classes.icons}>
        {players &&
          players.map(id => (
            <div className={clsx(classes.iconHolder, 'icon-holder')}>
              <img key={id} src={sprite(POSITIONS_BY_ID[id])} className={classes.icon} />
            </div>
          ))}
        {teams && teams.map(id => <div key={id} className={clsx(classes.teamIcon, id)}></div>)}
      </div>
    </div>
  );
}
