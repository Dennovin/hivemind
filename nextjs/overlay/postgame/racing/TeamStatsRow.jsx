import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

const useStyles = makeStyles(theme => ({
  row: {
    display: 'flex',
    flexDirection: 'row',
    margin: '0.25vw 0',
  },
  label: {
    flexGrow: 4,
    flexBasis: 0,
    whiteSpace: 'nowrap',
    margin: 'auto 1vw',
  },
  value: {
    textAlign: 'right',
    flexGrow: 1,
    flexBasis: 0,
    marginRight: "16px",
    paddingRight: "32px",
    border: 0,
    backgroundRepeat: "no-repeat",
    backgroundPositionX: "center",

    width: "127px",
    height: "45px",

    fontFamily: 'DS-Digital Italic',
    fontSize: "2vw",
  },
  blue: {
    backgroundImage: "url('/static/racing/sml-stats-blue-banner.svg')",
  },
  gold: {
    backgroundImage: "url('/static/racing/sml-stats-gold-banner.svg')",
  },
}));

export default function TeamStatsRow({ label, value, className }) {
  const classes = useStyles();

  return (
    <div className={clsx(classes.row, 'row', className)}>
      <div className={clsx(classes.label, 'label')}>{label}</div>
      <div className={clsx(classes.value, classes.blue, 'value', 'blue')}>{value?.blue ?? ''}</div>
      <div className={clsx(classes.value, classes.gold, 'value', 'gold')}>{value?.gold ?? ''}</div>
    </div>
  );
}
