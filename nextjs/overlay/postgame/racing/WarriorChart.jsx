import { makeStyles } from '@material-ui/core/styles';

import BaseWarriorChart from 'components/charts/BaseWarriorChart';
import { usePostgameStats } from 'overlay/PostgameStats';
import ChartContainer from './ChartContainer';

const useStyles = makeStyles(theme => ({}));

export default function WarriorChart({ className }) {
  const classes = useStyles();
  const stats = usePostgameStats();

  return (
    <ChartContainer title="Wars Up Queens Dwn" className={className}>
      {stats?.warriorData && <BaseWarriorChart
        game={stats}
        datasetProps={{ pointRadius: 0, borderWidth: 4 }}
        blueBackgroundColor={"#0000FF"}
        blueLineColor={"#0000FF"}
        blueQueenDeathColor={"#0000FF"}
        goldBackgroundColor={"#DB5C09"}
        goldLineColor={"#DB5C09"}
        goldQueenDeathColor={"#DB5C09"}
      />}
    </ChartContainer>
  );
}
