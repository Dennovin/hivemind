import {isQueen} from "../../../util/constants";

export function sprite(position, showWarrior) {
  if (isQueen(position)) {
    return `/static/gdc/sprites/${position.TEAM}_queen@4x.png`;
  } else if (showWarrior) {
    return `/static/gdc/sprites/${position.TEAM}_${position.POSITION}_warrior@4x.png`;
  } else {
    return `/static/gdc/sprites/${position.TEAM}_${position.POSITION}@4x.png`;
  }
}
