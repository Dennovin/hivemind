import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexDirection: 'row',
  },
  title: {
    width: '1vw',
    writingMode: 'vertical-rl',
    transform: 'rotate(180deg)',
    textAlign: 'center',
    fontSize: '1.15vw',

    fontWeight: 'bold',
    marginRight: '0.25vw',
  },
  chart: {
    flexGrow: 1,
    flexBasis: 0,
  },
}));

export default function ChartContainer({ title, children, className }) {
  const classes = useStyles();

  return (
    <div className={clsx(classes.container, className)}>
      <div className={classes.title}>{title}</div>

      <div className={classes.chart}>{children}</div>
    </div>
  );
}
