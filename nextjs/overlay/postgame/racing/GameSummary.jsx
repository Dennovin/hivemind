import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { usePostgameStats } from 'overlay/PostgameStats';
import GameLinkQrCode from './GameLinkQrCode';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: '1px',
  },
  textSection: {
    flexBasis: 0,
    flexGrow: 1,
    padding: '0 2vw',
  },
  winDescription: {
    textAlign: 'center',
    margin: '0.5vw 0 0 0',
  },
  gameInfo: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    margin: '0 0 0.5vw 0',
  },
  summaryRows: {
    flexGrow: 1,
  },
  qrCodeStamp: {
    background: 'white',
    height: '12vh',
    marginRight: '1vw',
  },
  numericFont: {
    fontFamily: 'DS-Digital Italic',
    fontSize: "28px",
  },
}));

export default function GameSummary({ className }) {
  const classes = useStyles();
  const stats = usePostgameStats();

  if (stats === null) {
    return <></>;
  }

  return (
    <div className={clsx(classes.container, className)}>
      <div className={clsx(classes.textSection, 'text-section')}>
        <div
          className={clsx(
            classes.winDescription,
            'win-description cheee-lowgrav tracking-wider text-6xl bg-clip-text',
          )}
        >
          {stats?.winningTeamDisplay ?
            (<img src={`/static/racing/` + stats.winningTeamDisplay.toLowerCase() + `-victory-cropped.svg`} />) : "????"}
        </div>
        <div className={clsx(classes.gameInfo, 'game-info justify-between')}>
          <div className={clsx('game-info-entry')}>
            {stats.map}
            <span className={classes.numericFont}></span>
          </div>
          <div className={clsx('game-info-entry')}>
            {stats.winConditionDisplay}
            <span className={classes.numericFont}></span>
          </div>
          <div className={clsx('game-info-entry')}>
            <span className={classes.numericFont}>{stats.length}</span>
          </div>
          <div className={clsx('game-info-entry')}>
            Game ID: <span class={classes.numericFont}>{stats.gameId}</span>
          </div>
        </div>
      </div>
      <GameLinkQrCode className={clsx(classes.qrCode, classes.qrCodeStamp)} />
    </div>
  );
}
