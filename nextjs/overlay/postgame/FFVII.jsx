import { makeStyles, useTheme } from '@material-ui/core/styles';
import clsx from 'clsx';
import Head from 'next/head';
import { defaults } from 'chart.js';

import { usePostgameStats } from 'overlay/PostgameStats';
import { useOverlaySettings } from 'overlay/Settings';
import { BLUE_TEAM, GOLD_TEAM } from 'util/constants';
import Awards from './ffvii/Awards';
import Charts from './ffvii/Charts';
import GameSummary from './ffvii/GameSummary';
import TeamStats from './ffvii/TeamStats';
import TopPlayers from './ffvii/TopPlayers';

const useStyles = makeStyles(theme => ({
  background: {
    opacity: 0,
    position: 'absolute',
    left: 0,
    top: 0,
    transition: 'opacity 1s',
    '&.active': {
      opacity: 1,
    },
  },
  container: {
    color: "white",
    textShadow: "1.5px 1.5px #000000",
    fontFamily: 'Reactor7, sans-serif',
    fontSize: '2vw',
    opacity: 0,
    width: '100vw',
    height: '100vh',
    position: 'absolute',
    left: 0,
    top: 0,
    transition: 'opacity 1s',
    display: 'flex',
    flexDirection: 'row',
    maxWidth: 'unset',
    '&.active': {
      opacity: 1,
    },
  },
  blueWin: {
    background: "linear-gradient(#000099, #000033)",
  },
  goldWin: {
    background: "linear-gradient(#cc9933, #996633)",
  },
  gameSummary: {
    flexGrow: 0,
  },
  charts: {
    textShadow: "-1.5px 1.5px #000000",
    border: '4px groove white',
    borderRadius: '8px',
    boxShadow: "inset 0 0 4px #000000aa",
    marginRight: '1px',
    background: 'rgba(0,0,0,0.8)',
    width: '50vw',
    flexBasis: '50vw',
  },
  rightSide: {
    flexBasis: 0,
    flexGrow: 1,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
}));

export default function FFVIIOverlay({}) {
  const classes = useStyles();
  const settings = useOverlaySettings();
  const stats = usePostgameStats();
  const theme = useTheme();

  const boxClasses = clsx('box');

  const winClasses = clsx({
    [classes.blueWin]: stats?.winningTeam == BLUE_TEAM,
    [classes.goldWin]: stats?.winningTeam == GOLD_TEAM,
  });

  // Chart.js defaults
  defaults.borderColor = "#FFFFFF";
  defaults.color = "#FFFFFF";
  defaults.font = {
    // fontWeight: 'bold',
    family: 'Reactor7, sans-serif',
    size: 14,
    style: "normal",
  }

  return (
    <>
      <Head>
        {settings?.additionalFonts && <link rel="stylesheet" href={settings.additionalFonts} />}
        <link rel="stylesheet" type="text/css" href={`/static/ffvii/fonts/final_fantasy/stylesheet.css`} />
        <link rel="stylesheet" type="text/css" href={`/static/ffvii/fonts/enge-etienne/stylesheet.css`} />
        <link rel="stylesheet" type="text/css" href={`/static/ffvii/fonts/reactor7/webfont.css`} />
      </Head>

      <div
        id="postGameOverlay"
        className={clsx(classes.background, 'background relative font-sans', {
          active: stats?.visible,
        })}
      >
        <div
          className={clsx(classes.container, classes[stats?.map], 'container', {
            active: stats?.visible,
          })}
        >
          <Charts className={clsx(classes.charts, 'charts', 'box', boxClasses)} />

          <div className={clsx(classes.rightSide, 'right-side justify-start')}>
            <GameSummary className={clsx(classes.gameSummary, 'game-summary', boxClasses, winClasses)} />
            <Awards
              className={clsx(
                classes.statsBox,
                'awards m-0 text-white',
                classes.boxes,
              )}
            />
            <TeamStats
              className={clsx(
                classes.statsBox,
                'team-stats flex-grow flex flex-col justify-around',
                boxClasses,
              )}
            />
            <TopPlayers
              className={clsx(
                classes.statsBox,
                'top-players flex-grow flex flex-col justify-around',
                boxClasses,
              )}
            />
          </div>
        </div>
      </div>
    </>
  );
}

FFVIIOverlay.themeProps = {
  name: 'ffvii',
  displayName: 'FF VII',
  description: `Post-game overlay inspired by Final Fantasy VII.`,
};
