import { makeStyles, useTheme } from '@material-ui/core/styles';
import clsx from 'clsx';
import Head from 'next/head';

import { usePostgameStats } from 'overlay/PostgameStats';
import { useOverlaySettings } from 'overlay/Settings';
import { BLUE_TEAM, GOLD_TEAM } from 'util/constants';
import Awards from './default/Awards';
import Charts from './default/Charts';
import GameSummary from './default/GameSummary';
import TeamStats from './default/TeamStats';
import TopPlayers from './default/TopPlayers';

const useStyles = makeStyles(theme => ({
  background: {
    opacity: 0,
    width: '100vw',
    height: '56.25vw',
    position: 'absolute',
    left: 0,
    top: 0,
    transition: 'opacity 1s',
    '&.active': {
      opacity: 1,
    },
  },
  container: {
    opacity: 0,
    width: '98vw',
    height: '54.25vw',
    position: 'absolute',
    left: 0,
    top: 0,
    margin: '1vw',
    transition: 'opacity 1s',
    display: 'flex',
    flexDirection: 'row',
    maxWidth: 'unset',
    '&.active': {
      opacity: 1,
    },
  },
  boxes: {
    borderWidth: '0px',
    borderStyle: 'solid',
    borderRadius: '5px',
    mixBlendMode: 'hard-light',
  },
  gameSummary: {},
  blueWin: {
    backgroundColor: theme.palette.blue.light4,
  },
  goldWin: {
    backgroundColor: theme.palette.gold.light5,
  },
  gameSummary: {
    flexGrow: 0,
  },
  charts: {
    width: '50vw',
    flexBasis: '50vw',
    marginRight: '1vw',
  },
  rightSide: {
    flexBasis: 0,
    flexGrow: 1,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
}));

export default function DefaultOverlay({}) {
  const classes = useStyles();
  const settings = useOverlaySettings();
  const stats = usePostgameStats();
  const theme = useTheme();

  const boxClasses = clsx('box', classes.boxes, {
    [classes.blueWin]: stats?.winningTeam == BLUE_TEAM,
    [classes.goldWin]: stats?.winningTeam == GOLD_TEAM,
  });

  return (
    <>
      <Head>
        {settings?.additionalFonts && <link rel="stylesheet" href={settings.additionalFonts} />}
      </Head>

      <div
        id="postGameOverlay"
        className={clsx(classes.background, 'background relative font-sans', {
          active: stats?.visible,
        })}
      >
        <div
          className={clsx(classes.container, classes[stats?.map], 'container', {
            active: stats?.visible,
          })}
        >
          <Charts className={clsx(classes.charts, 'charts', 'box', boxClasses)} />

          <div className={clsx(classes.rightSide, 'right-side justify-start gap-4')}>
            <GameSummary className={clsx(classes.gameSummary, 'game-summary', boxClasses)} />
            <Awards
              className={clsx(
                classes.statsBox,
                'awards m-0 text-white',
                stats.winningTeam === 'blue'
                  ? 'bg-gradient-to-br from-slate-500 to-slate-600'
                  : 'bg-gold-main',
                classes.boxes,
              )}
            />
            <TeamStats
              className={clsx(
                classes.statsBox,
                'team-stats flex-grow flex flex-col justify-around',
                boxClasses,
              )}
            />
            <TopPlayers
              className={clsx(
                classes.statsBox,
                'top-players flex-grow flex flex-col justify-around',
                boxClasses,
              )}
            />
          </div>
        </div>
        <div className="custom-color absolute -z-10 shadow-[inset_0_0_5vw_5vw_currentColor] after:content-[''] after:absolute after:inset-0 after:bg-current after:opacity-50  inset-0"></div>
      </div>
    </>
  );
}

DefaultOverlay.themeProps = {
  name: 'default',
  displayName: 'Clover (Default)',
  description: `A responsive post-game overlay, designed to sit atop the game footage.`,
};
