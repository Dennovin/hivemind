import clsx from 'clsx';

import { POSITIONS_BY_ID } from 'util/constants';

export default function TopPlayersRow({ label, value, from, format, className }) {
  if (!value?.count && !from) {
    return <></>;
  }

  let count = value?.count;
  let players = value?.players;

  if (from) {
    count = Math.max(...Object.values(from));
    if (count <= 0) {
      return <></>;
    }

    players = Object.keys(from).filter(p => from[p] == count);
  }

  if (format) {
    count = format(count);
  }

  return (
    <div className={clsx('flex p-[0.25vw] justify-between bb5-font items-center', className)}>
      <div className={'text-[1.25vw] font-bold flex-grow'}>{label}</div>
      <div className={'text-[1.25vw] font-bold w-[4vw] flex justify-end'}>
        {players &&
          players.map(id => (
            <img key={id} src={POSITIONS_BY_ID[id].ICON} className={'w-[2vw] aspect-square'} />
          ))}
      </div>
      <div className={'text-[1.25vw] font-bold pl-[1vw] w-[4vw]'}>{count}</div>
    </div>
  );
}
