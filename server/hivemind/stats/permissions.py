from rest_framework.permissions import SAFE_METHODS, BasePermission

from ..constants import HTTPMethod
from ..game.models import Cabinet, ClientDevice


class NFCPermission(BasePermission):
    def has_permission(self, request, view):
        return request.user.is_authenticated

    def has_object_permission(self, request, view, obj):
        if request.user.is_authenticated and obj.user == request.user:
            return True

        if request.user.is_site_admin:
            return True


class RequireToken(BasePermission):
    def has_permission(self, request, view):
        token = None

        if "cabinet" in request.data:
            try:
                cabinet = Cabinet.objects.get(id=request["cabinet"])
                token = cabinet.token
            except Cabinet.DoesNotExist:
                return False

        elif request.data.get("cabinet_name") and request.data.get("scene_name"):
            try:
                cabinet = Cabinet.by_name(request.data["scene_name"], request.data["cabinet_name"])
                token = cabinet.token
            except Cabinet.DoesNotExist:
                return False

        elif request.data.get("device_id"):
            try:
                device = ClientDevice.objects.get(id=request.data["device_id"])
                token = device.token
            except ClientDevice.DoesNotExist:
                return False

        else:
            return False

        return token is not None and request.data.get("token") == token
