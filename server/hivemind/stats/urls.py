from django.urls import include, path
from rest_framework import routers, serializers, viewsets

from . import views

router = routers.DefaultRouter()
router.register(r"nfc", views.NFCViewSet)
router.register(r"signin", views.SignInViewSet)
router.register(r"recap", views.RecapViewSet, basename="recap")
router.register(r"user-recap", views.UserRecapViewSet)
router.register(r"", views.StatsViewSet, basename="stats")

urlpatterns = [
    path("", include(router.urls)),
]
