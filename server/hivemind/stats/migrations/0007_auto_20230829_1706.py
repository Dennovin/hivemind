# Generated by Django 3.2.7 on 2023-08-29 17:06

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('stats', '0006_rename_recap_userrecap'),
    ]

    operations = [
        migrations.AlterField(
            model_name='nfc',
            name='date_registered',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
        migrations.AlterField(
            model_name='signinlog',
            name='timestamp',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
    ]
