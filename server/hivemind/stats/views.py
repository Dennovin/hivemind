import base64
import itertools
import json
import secrets
from datetime import date, datetime, timedelta, timezone

from django.conf import settings
from django.core.files.base import ContentFile
from django.db import models
from django.db.models import Count, F
from django.db.models.functions import Trunc, TruncMonth
from django.http import HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404, render
from rest_framework import status, viewsets
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from hivemind.game.models import Game, GameMap, Scene

from ..client import redis_client, redis_get, redis_set
from ..constants import AggregateStatType, HTTPMethod, StatType
from ..game.models import Cabinet, ClientDevice, Game, Scene
from ..model import BaseViewSet, ReadOnly
from ..permissions import SiteAdminPermission
from ..tournament.models import TournamentPlayer
from ..user.models import User, UserGame
from .leaderboards import get_leaderboard
from .models import NFC, SignInLog, UserRecap
from .permissions import NFCPermission, RequireToken
from .serializers import NFCSerializer
from .stattype import StatType, stat_types
from .tasks import generate_recap, generate_site_recap


def recap_max_year():
    if settings.DEBUG:
        return datetime.utcnow().year + 1

    return datetime.utcnow().year


class StatsViewSet(viewsets.ViewSet):
    permission_classes = [ReadOnly]

    @action(detail=False, url_path="games-by-week")
    def games_by_week(self, request):
        WEEKS = 52

        min_date = date.today() - timedelta(weeks=WEEKS, days=date.today().weekday())
        query = Game.objects.filter(end_time__isnull=False, end_time__gte=min_date)
        query = query.annotate(week=Trunc("start_time", "week"))
        query = query.values("week", "cabinet__scene").annotate(Count("id"))

        games_by_week = {}
        scene_ids = set()
        for row in query:
            scene_id = row["cabinet__scene"]
            scene_ids.add(scene_id)
            week_start = row["week"]
            games_by_week[week_start.date()] = games_by_week.get(week_start.date(), {})
            games_by_week[week_start.date()][scene_id] = row["id__count"]

        scenes = Scene.objects.filter(id__in=scene_ids)

        labels = []
        datasets = {}
        for scene in scenes:
            datasets[scene.id] = {
                "stack": "all",
                "label": scene.display_name,
                "borderColor": "black",
                "backgroundColor": scene.background_color,
                "data": [],
            }

        week_start = date.today() - timedelta(days=date.today().weekday())
        for weeks_back in range(WEEKS):
            week = week_start - timedelta(weeks=(WEEKS-weeks_back-1))
            if week < min_date:
                continue

            labels.append(week.strftime("%Y-%m-%d"))
            for scene in scenes:
                datasets[scene.id]["data"].append(games_by_week.get(week, {}).get(scene.id, 0))

        data = {
            "labels": labels,
            "datasets": list(datasets.values()),
        }

        return JsonResponse(data)

    @action(detail=False, url_path="win-conditions")
    def win_conditions(self, request):
        map_names = [GameMap.DAY, GameMap.NIGHT, GameMap.DUSK, GameMap.TWILIGHT]
        query = Game.objects.filter(end_time__isnull=False, end_time__gt=datetime.now() - timedelta(weeks=8))
        query = query.filter(player_count__gte=8, map_name__in=map_names)
        query = query.values("map_name", "win_condition").annotate(Count("id"))

        win_cond_by_map = {i: {} for i in map_names}
        for row in query:
            win_cond_by_map[row["map_name"]][row["win_condition"]] = row["id__count"]

        data = {}
        for map_name in map_names:
            game_map = GameMap.objects.get(name=map_name)
            data[map_name] = {
                "map_name": game_map.display_name,
                "labels": ["Military", "Economic", "Snail"],
                "datasets": [{
                    "data": [win_cond_by_map[map_name].get(i, 0) for i in ["military", "economic", "snail"]],
                    "backgroundColor": ["#b00", "#0b0", "#00b"],
                    "label": game_map.display_name,
                }],
            }

        return JsonResponse(data)

    @action(detail=False)
    def leaderboards(self, request):
        leaderboards = []

        leaderboard_types = [
            (AggregateStatType.GAMES, "Most Games Played"),
            (StatType.BERRIES, "Most Berries Deposited"),
            (StatType.BERRIES_KICKED, "Most Berries Kicked In"),
            (StatType.BUMP_ASSISTS, "Most Bump Assists"),
            (StatType.SNAIL_METERS, "Most Snail Distance"),
            (StatType.KILLS, "Most Total Kills"),
        ]

        for stat_type, title in leaderboard_types:
            leaderboards.append({
                "title": title,
                "leaders": get_leaderboard(stat_type),
            })

        return JsonResponse({"results": leaderboards})


class NFCViewSet(BaseViewSet):
    queryset = NFC.objects.all()
    serializer_class = NFCSerializer
    permission_classes = [NFCPermission]

    def get_queryset(self):
        user = self.request.user
        return NFC.objects.filter(user=user).order_by("date_registered")

    @action(detail=False, methods=[HTTPMethod.POST])
    def relay(self, request, pk=None, permission_classes=[SiteAdminPermission]):
        data = {
            "type": "nfc_relay",
            **{k: v for k, v in request.data.items() if k in ["reader_id", "card_id", "user_id"]},
        }

        redis_client.publish("signin", json.dumps(data))
        return Response({"success": True})

    @action(detail=False, methods=[HTTPMethod.POST], permission_classes=[IsAuthenticated])
    def register(self, request, pk=None):
        data = {
            "type": "nfc_register",
            "reader_id": request.data.get("reader"),
            "device_id": request.data.get("device_id"),
            "user": request.user.id,
        }

        if "cabinet" in request.data:
            cabinet = get_object_or_404(Cabinet, id=request.data["cabinet"])
            if not cabinet.allow_nfc_signin:
                return Response({"success": False, "error": "This cabinet is not configured to allow NFC sign-in."})

            data["scene_name"] = cabinet.scene.name
            data["cabinet_name"] = cabinet.name

        redis_client.publish("signin", json.dumps(data))
        return Response({"success": True})

    @action(detail=False, permission_classes=[SiteAdminPermission], url_path="by-serial")
    def by_serial(self, request):
        try:
            card = NFC.objects.get(card_id=request.query_params.get("card_id").lower().strip())
            return Response({ "user": card.user.get_public_info() })
        except NFC.DoesNotExist:
            return Response({})


class SignInViewSet(viewsets.ViewSet):
    queryset = SignInLog.objects.filter(id__isnull=True)
    serializer_class = SignInLog.serializer()

    @action(detail=False, methods=[HTTPMethod.POST], url_path="qr", permission_classes=[IsAuthenticated])
    def qr(self, request, pk=None):
        try:
            if "cabinet_id" in request.data:
                cabinet = Cabinet.objects.get(id=request.data["cabinet_id"])
            elif "device_id" in request.data:
                device = ClientDevice.objects.get(id=request.data["device_id"])
                cabinet = device.cabinet
        except (Cabinet.DoesNotExist, ClientDevice.DoesNotExist):
            return Response({"success": False, "error": "Invalid cabinet"}, status.HTTP_400_BAD_REQUEST)

        if cabinet is None:
            return Response({"success": False, "error": "Invalid cabinet"}, status.HTTP_400_BAD_REQUEST)

        if not cabinet.allow_qr_signin:
            return Response({"success": False, "error": "Cabinet does not allow QR sign-in"},
                            status.HTTP_400_BAD_REQUEST)

        if request.data.get("action") == SignInLog.Action.SIGN_IN:
            for sign_in in SignInLog.objects.filter(user=request.user, action=SignInLog.Action.SIGN_IN,
                                                    is_current=True):
                sign_in.sign_out(method=SignInLog.SignInMethod.QR)

            sign_in = SignInLog(
                user=request.user,
                method=SignInLog.SignInMethod.QR,
                action=request.data.get("action"),
                cabinet=cabinet,
                player_id=request.data.get("player"),
            )

            sign_in.set_current()
            sign_in.save()
            sign_in.publish()

            return Response({"success": True, "id": sign_in.id})

        if request.data.get("action") == SignInLog.Action.SIGN_OUT:
            for sign_in in SignInLog.objects.filter(user=request.user, action=SignInLog.Action.SIGN_IN,
                                                    is_current=True):
                sign_in.sign_out(method=SignInLog.SignInMethod.QR)

            return Response({"success": True})

        return Response({"success": False, "error": "Unknown action."}, status.HTTP_400_BAD_REQUEST)


    @action(detail=False, methods=[HTTPMethod.POST], url_path="nfc", permission_classes=[RequireToken])
    def nfc(self, request, pk=None):
        try:
            if request.data.get("cabinet_name") and request.data.get("scene_name"):
                cabinet = Cabinet.by_name(request.data["scene_name"], request.data["cabinet_name"])
            elif request.data.get("device_id"):
                device = ClientDevice.objects.get(id=request.data["device_id"])
                cabinet = device.cabinet
        except (Cabinet.DoesNotExist, ClientDevice.DoesNotExist):
            return Response({"success": False, "error": "Invalid cabinet"})

        if cabinet and not cabinet.allow_nfc_signin:
            return Response({"success": False, "error": "Cabinet does not allow NFC sign-in"})

        if request.data.get("action") == SignInLog.Action.NFC_REGISTER_TAPPED:
            nfc, created = NFC.objects.get_or_create(
                card_id=request.data.get("card"),
                defaults={"user_id": request.data["user"]},
            )

            if nfc.user and not created:
                nfc.publish_error(f"Card {nfc.card_id} is already registered.",
                                  user_id=request.data["user"])
                return Response({"error": f"Card {nfc.card_id} is already registered."},
                                status=status.HTTP_400_BAD_REQUEST)

            if nfc.user is None:
                return Response({"error": "User not found"}, status=status.HTTP_400_BAD_REQUEST)

            nfc.save()
            nfc.publish_new_card()

            return Response({"success": True, "id": nfc.id})

        # Only registration works if the device isn't assigned to a cabinet
        if cabinet is None:
            return Response({"success": False, "error": "Client device is not assigned to a cabinet."})

        if request.data.get("action") == SignInLog.Action.SIGN_IN:
            nfc = get_object_or_404(NFC, card_id=request.data.get("card"))

            nfc.sign_out_all()

            if nfc.user is None:
                return Response({"error": f"Card {nfc.card_id} is not registered."},
                                status=status.HTTP_400_BAD_REQUEST)

            sign_in = SignInLog(
                user=nfc.user,
                method=SignInLog.SignInMethod.NFC,
                action=request.data.get("action"),
                cabinet=cabinet,
                player_id=request.data.get("player"),
            )

            sign_in.set_current()
            sign_in.save()
            sign_in.publish()

            return Response({"success": True, "id": sign_in.id})

        if request.data.get("action") == SignInLog.Action.SIGN_OUT:
            try:
                sign_in = SignInLog.objects.get(cabinet=cabinet, player_id=request.data.get("player"),
                                                is_current=True)
                sign_in.sign_out()
            except SignInLog.DoesNotExist:
                pass

            return Response({"success": True})

        return Response({"success": False, "error": "Unknown action."})


class RecapViewSet(viewsets.ViewSet):
    permission_classes = [ReadOnly]

    def retrieve(self, request, pk=None):
        year = int(pk)
        if not year:
            return Response({"success": False, "error": "invalid year"})

        if year >= recap_max_year():
            return Response({"success": False, "error": "flux capacitor not installed"})

        redis_key = f"recap.{year}"
        data = redis_get(redis_key)

        if data is not None:
            return Response({"success": True, "data": data})

        generate_site_recap.delay(year)
        return Response({"success": False})



class UserRecapViewSet(BaseViewSet):
    queryset = UserRecap.objects.all()
    serializer_class = UserRecap.serializer()
    permission_classes = [ReadOnly]
    filterset_fields = ["user_id", "year"]

    @action(detail=False, methods=[HTTPMethod.POST], permission_classes=[IsAuthenticated])
    def generate(self, request):
        if not request.user.is_authenticated:
            return Response({"success": False, "error": "not logged in"})
        try:
            year = int(request.data.get("year"))
        except ValueError:
            return Response({"success": False, "error": "invalid year"})

        if year >= recap_max_year():
            return Response({"success": False, "error": "flux capacitor not installed"})

        UserRecap.objects.get_or_create(user=request.user, year=year)
        generate_recap.delay(request.user.id, year)
        return Response({"success": True})

    @action(detail=False, methods=[HTTPMethod.POST], permission_classes=[IsAuthenticated])
    def image(self, request, pk=None):
        if not request.user.is_authenticated:
            return Response({"success": False, "error": "not logged in"})
        try:
            year = int(request.data.get("year"))
        except ValueError:
            return Response({"success": False, "error": "invalid year"})

        if year >= recap_max_year():
            return Response({"success": False, "error": "flux capacitor not installed"})

        recap = get_object_or_404(UserRecap, user=request.user, year=year)
        _, imgstr = request.data["image"].split(';base64,')
        recap.share_image.save(secrets.token_urlsafe(16) + ".png", ContentFile(base64.b64decode(imgstr)), save=True)
        recap.save()

        return Response({"success": True})
