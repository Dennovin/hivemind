from datetime import datetime, timedelta

from django.db import models

from ..client import redis_get, redis_set
from ..user.models import PlaySessionStat, User
from .stattype import stat_types


def get_leaderboard(stat_type):
    return redis_get("leaderboards.{}".format(stat_type))

def refresh_leaderboards(count=5, days=7):
    stats = PlaySessionStat.objects.filter(session__start_time__gt=datetime.now() - timedelta(days=days)) \
        .values(
            "session__user",
            "stat_type",
        ).annotate(models.Sum("value"))

    for stat_type in stat_types(include_in_leaderboards=True):
        leaders = []

        for row in stats.filter(stat_type=stat_type.name).order_by("-value__sum")[:count]:
            user = User.objects.get(id=row["session__user"])
            leaders.append({
                "user": user.id,
                "name": user.name,
                "scene": user.scene,
                "is_profile_public": user.is_profile_public,
                "image": user.image.url if user.image else None,
                "value": row["value__sum"],
            })

        redis_set("leaderboards.{}".format(stat_type.name), leaders)
