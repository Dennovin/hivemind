from django.db import models, transaction

from ..model import BaseModel
from .stattype import (StatType, add_game_to_aggregates, calculate_aggregates,
                       stat_types)


class AggregatedStatModel(BaseModel):
    stat_type = models.TextField(max_length=30, choices=StatType.choices)
    value = models.IntegerField(default=0)

    class Meta:
        abstract = True

    @classmethod
    def aggregate_on(cls):
        return [i.attname for i in cls._meta.get_fields() if i.name not in ["id", "stat_type", "value"]]

    @classmethod
    def get_games_in_aggregate(cls, **filters):
        """ Should accept a list of filters, which should match the columns returned by cls.aggregate_on()
            Should return a list of (game, position_id) tuples representing the games to aggregate
              for the provided filter list.
            e.g. for user totals, given a user, return a list of (game, position_id) that the specified
              user has played in. """

        raise NotImplementedError

    @classmethod
    @transaction.atomic
    def calculate_aggregates(cls, games, **filters):
        values = calculate_aggregates(games)

        for stat_type, value in values.items():
            if value is None:
                continue

            obj, _ = cls.objects.get_or_create(stat_type=stat_type, **filters)
            obj.value = value
            obj.save()

    @classmethod
    @transaction.atomic
    def add_game_to_aggregates(cls, game, position_id, **filters):
        current_values = {i.stat_type: i.value for i in cls.objects.filter(**filters)}
        updated_values = add_game_to_aggregates(current_values, game, position_id)

        for stat_type, value in updated_values.items():
            if value is None:
                continue

            obj, _ = cls.objects.get_or_create(stat_type=stat_type, **filters)
            obj.value = value
            obj.save()

    @classmethod
    def get_stats(cls, **filters):
        stats = []
        stat_values = {i.stat_type: i for i in cls.objects.filter(**filters)}

        for stat_type in stat_types(is_hidden=False):
            stats.append({
                "name": stat_type.name,
                "label": stat_type.display_name,
                "formatting": stat_type.formatting,
                "value": stat.value if (stat := stat_values.get(stat_type.name)) else "",
            })

        return stats
