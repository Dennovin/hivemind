from rest_framework import serializers

from .models import UserAchievement


class UserAchievementSerializer(serializers.ModelSerializer):
    title = serializers.CharField(read_only=True, source="get_achievement.title")
    description = serializers.CharField(read_only=True, source="get_achievement.description")
    icon_url = serializers.CharField(read_only=True, source="get_achievement.icon_url")

    class Meta:
        model = UserAchievement
        fields = "__all__"
