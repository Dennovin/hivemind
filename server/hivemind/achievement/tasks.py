from celery import shared_task
from . import models

@shared_task
def check_history(achievement_name):
    getattr(models, achievement_name).check_history()
