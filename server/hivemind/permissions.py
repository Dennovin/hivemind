import logging

from django.apps import apps
from rest_framework.exceptions import PermissionDenied
from rest_framework.permissions import SAFE_METHODS, BasePermission

from .constants import HTTPMethod, PermissionType


class RequirePermission(BasePermission):
    can_delete = False
    permissions = []

    def has_permission(self, request, view):
        if request.method in (*SAFE_METHODS, HTTPMethod.PUT, HTTPMethod.PATCH):
            return True

        if request.method == HTTPMethod.POST:
            if not request.user.is_authenticated:
                return False

            try:
                scene_id = self.get_scene_id_from_request(request)
            except:
                return False

            scene = self.get_scene(scene_id)

            return request.user.has_any_permission(scene, self.permissions)

        return False

    def has_object_permission(self, request, view, obj):
        if not request.user.is_authenticated:
            return False

        if request.method in (HTTPMethod.GET, HTTPMethod.PUT, HTTPMethod.PATCH):
            scene = self.get_scene_from_object(obj)
            return request.user.has_any_permission(scene, self.permissions)

        if request.method == HTTPMethod.DELETE and self.can_delete:
            scene = self.get_scene_from_object(obj)
            return request.user.has_any_permission(scene, self.permissions)

        return False

    def get_scene(self, scene_id):
        Scene = apps.get_model("game", "Scene")
        try:
            return Scene.objects.get(id=scene_id)
        except Scene.DoesNotExist:
            raise PermissionDenied()

    def get_scene_from_object(self, obj):
        return obj.scene

    def get_scene_id_from_request(self, request):
        return request.data["scene"]


class RequirePermissionOrReadOnly(BasePermission):
    can_delete = False
    write_permissions = []

    def has_permission(self, request, view):
        if request.method in (*SAFE_METHODS, HTTPMethod.PUT, HTTPMethod.PATCH):
            return True

        if request.method == HTTPMethod.DELETE:
            return self.can_delete

        if request.method == HTTPMethod.POST:
            if not request.user.is_authenticated:
                return False

            try:
                scene_id = self.get_scene_id_from_request(request)
            except:
                return False

            scene = self.get_scene(scene_id)

            return request.user.has_any_permission(scene, self.write_permissions)

        return False

    def has_object_permission(self, request, view, obj):
        if request.method in SAFE_METHODS:
            return True

        if not request.user.is_authenticated:
            return False

        if request.method in (HTTPMethod.PUT, HTTPMethod.PATCH):
            scene = self.get_scene_from_object(obj)
            return request.user.has_any_permission(scene, self.write_permissions)

        if request.method == HTTPMethod.DELETE and self.can_delete:
            scene = self.get_scene_from_object(obj)
            return request.user.has_any_permission(scene, self.write_permissions)

        return False

    def get_scene(self, scene_id):
        Scene = apps.get_model("game", "Scene")
        try:
            return Scene.objects.get(id=scene_id)
        except Scene.DoesNotExist:
            raise PermissionDenied()

    def get_scene_from_object(self, obj):
        return obj.scene

    def get_scene_id_from_request(self, request):
        return request.data["scene"]


class SceneAdminOnly(RequirePermission):
    permissions = [PermissionType.ADMIN]


class SceneAdminOrReadOnly(RequirePermissionOrReadOnly):
    write_permissions = [PermissionType.ADMIN]


class TournamentOfficialOrReadOnly(RequirePermissionOrReadOnly):
    write_permissions = [PermissionType.ADMIN, PermissionType.TOURNAMENT]


class SiteAdminPermission(BasePermission):
    def has_permission(self, request, view):
        return request.user.is_authenticated and request.user.is_site_admin

    def has_object_permission(self, request, view, obj):
        return request.user.is_authenticated and request.user.is_site_admin
