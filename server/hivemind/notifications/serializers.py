from rest_framework import serializers

from .models import NotificationConfig


class NotificationConfigSerializer(serializers.ModelSerializer):
    class Meta:
        model = NotificationConfig
        fields = ["id", "user", "phone_number", "is_validated", "notify_on_tournament_match",
                  "notify_on_whiteboard_match", "notify_on_message"]
        read_only_fields = ["is_validated"]
