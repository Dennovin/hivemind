from django.db import models

from ..model import BaseModel
from ..tournament.models import TournamentPlayer
from ..user.models import User


class NotificationConfig(BaseModel):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    phone_number = models.CharField(max_length=20)
    token = models.CharField(max_length=20)
    is_validated = models.BooleanField(default=False)
    notify_on_tournament_match = models.BooleanField(default=False)
    notify_on_whiteboard_match = models.BooleanField(default=False)
    notify_on_message = models.BooleanField(default=False)
