import logging
import secrets
import string
import time

import requests
from celery import shared_task
from django.conf import settings

from ..client import redis_get, redis_set
from ..tournament.models import TournamentMatch
from ..user.models import Message
from ..whiteboard.models import Match as WhiteboardMatch
from .models import NotificationConfig

logger = logging.getLogger(__name__)

@shared_task
def send_message(config_id, message_text):
    if settings.DEBUG:
        return

    config = NotificationConfig.objects.get(id=config_id)

    url = "https://api.twilio.com/2010-04-01/Accounts/{}/Messages.json".format(settings.TWILIO_ACCOUNT_SID)
    postdata = {
        "To": config.phone_number,
        "MessagingServiceSid": settings.TWILIO_MESSAGING_SID,
        "Body": message_text,
    }

    requests.post(url, data=postdata, auth=(settings.TWILIO_ACCOUNT_SID, settings.TWILIO_AUTH_TOKEN))

@shared_task
def notify_tournament_match_started(match_id):
    last_sent = redis_get("notifications.match_started.{}".format(match_id))
    if last_sent and last_sent > time.time() - 60:
        return

    redis_set("notifications.match_started.{}".format(match_id), time.time())

    match = TournamentMatch.objects.get(id=match_id)
    players = list(match.blue_team.tournamentplayer_set.all()) + list(match.gold_team.tournamentplayer_set.all())
    users = [p.user for p in players]
    configs = NotificationConfig.objects.filter(user__in=users)

    if match.active_cabinet is None:
        return

    message_text = "{} vs. {} is about to start on {}. Match info: {}".format(
        match.blue_team.name,
        match.gold_team.name,
        match.active_cabinet.display_name,
        settings.PUBLIC_URL(f"/tournament-match/{match.id}"),
    )

    for config in configs:
        if config.is_validated and config.notify_on_tournament_match:
            send_message.delay(config.id, message_text)

@shared_task
def notify_whiteboard_match_started(match_id):
    last_sent = redis_get("notifications.whiteboard_match_started.{}".format(match_id))
    if last_sent and last_sent > time.time() - 60:
        return

    redis_set("notifications.match_started.{}".format(match_id), time.time())
    match = WhiteboardMatch.objects.get(id=match_id)

    for player_match in match.playermatch_set.all():
        config = NotificationConfig.objects.filter(user_id=player_match.player.user_id).first()
        if config and config.is_validated and config.notify_on_whiteboard_match:
            message_text = "Whiteboard match is about to start on {}. You are playing {} {}.".format(
                match.session.cabinet.display_name,
                player_match.team.upper(),
                player_match.role.upper(),
            )

            send_message.delay(config.id, message_text)

@shared_task
def notify_message(message_id):
    message = Message.objects.get(id=message_id)
    config = NotificationConfig.objects.filter(user=message.user).first()

    if config and config.notify_on_message and not message.is_read:
        message_text = f"HiveMind: {message.message_text}"
        send_message.delay(config.id, message_text)

@shared_task
def send_opt_in_message(config_id):
    config = NotificationConfig.objects.get(id=config_id)
    if config.is_validated:
        return

    config.token = "".join(secrets.choice(string.digits) for i in range(6))
    config.save()

    message_text = f"Your HiveMind confirmation code is {config.token}. Reply STOP to opt out."
    send_message.delay(config.id, message_text)
