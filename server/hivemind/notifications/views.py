import secrets
import string

from django.shortcuts import get_object_or_404
from rest_framework.decorators import action
from rest_framework.exceptions import PermissionDenied
from rest_framework.response import Response

from ..constants import HTTPMethod
from ..model import BaseViewSet
from .models import NotificationConfig
from .permissions import NotificationConfigPermission
from .serializers import NotificationConfigSerializer
from .tasks import send_opt_in_message


class NotificationConfigViewSet(BaseViewSet):
    queryset = NotificationConfig.objects.all()
    serializer_class = NotificationConfigSerializer
    permission_classes = [NotificationConfigPermission]
    filterset_fields = ["user"]

    def get_queryset(self):
        user = self.request.user
        if not user.is_authenticated:
            raise PermissionDenied()

        return NotificationConfig.objects.filter(user=user).order_by("id")

    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, **kwargs)

        send_opt_in_message.delay(response.data.get("id"))

        return response

    @action(detail=True, methods=[HTTPMethod.PUT])
    def resend(self, request, pk=None):
        config = get_object_or_404(NotificationConfig, id=pk)
        send_opt_in_message.delay(config.id)

        return Response({ "success": True })

    @action(detail=True, methods=[HTTPMethod.PUT])
    def validate(self, request, pk=None):
        config = get_object_or_404(NotificationConfig, id=pk)

        if not request.data.get("token") == config.token:
            return Response({ "success": False, "error": "Invalid token" })

        config.is_validated = True
        config.save()

        return Response({ "success": True })
