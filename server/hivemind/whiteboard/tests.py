import http.client
from datetime import datetime

from faker import Faker

from ..constants import PlayerRole
from ..game.models import Scene
from ..tests import HiveMindTest
from ..user.models import User
from .models import Player, PlayerSignup, WhiteboardSession

fake = Faker()

class WhiteboardTestCase(HiveMindTest):
    def setUp(self):
        super().setUp()
        self.session = WhiteboardSession.objects.create(scene=self._scene, cabinet=self._cabinet)

        self.players = []
        players = []
        for player_name in self.get_player_names(15):
            user = User.objects.create(username=player_name, email=fake.email(), name=player_name)
            player = Player.objects.create(user=user)
            self.players.append(player)

    def set_available_players(self, roles):
        for ps in self.session.playersignup_set.all():
            ps.roles = []
            ps.save()

        for role, players in roles.items():
            for player_idx in players:
                ps, _ = PlayerSignup.objects.get_or_create(
                    player=self.players[player_idx],
                    session=self.session,
                    defaults={"roles": []},
                )

                ps.is_ready = True
                ps.roles.append(role)
                ps.save()


    def test_determine_team_comps(self):
        # Not enough players
        self.set_available_players({
            PlayerRole.QUEEN: range(0, 9),
            PlayerRole.WARRIOR: range(0, 9),
            PlayerRole.OBJECTIVE: range(0, 9),
        })
        self.assertIsNone(self.session.next_match_team_comp())

        # Simple 10 players available
        self.set_available_players({
            PlayerRole.QUEEN: range(0, 2),
            PlayerRole.WARRIOR: range(2, 8),
            PlayerRole.OBJECTIVE: range(8, 10),
        })
        self.assertEqual(self.session.next_match_team_comp(), (1, 3, 1))

        self.set_available_players({
            PlayerRole.QUEEN: range(0, 2),
            PlayerRole.WARRIOR: range(2, 6),
            PlayerRole.OBJECTIVE: range(6, 10),
        })
        self.assertEqual(self.session.next_match_team_comp(), (1, 2, 2))

        # Not enough queens
        self.set_available_players({
            PlayerRole.QUEEN: range(0, 1),
            PlayerRole.WARRIOR: range(0, 15),
            PlayerRole.OBJECTIVE: range(0, 15),
        })
        self.assertIsNone(self.session.next_match_team_comp())

        # Only 3 players for queen/obj
        self.set_available_players({
            PlayerRole.QUEEN: range(0, 3),
            PlayerRole.WARRIOR: range(0, 15),
            PlayerRole.OBJECTIVE: range(0, 3),
        })
        self.assertIsNone(self.session.next_match_team_comp())

    def test_next_match_players(self):
        # Not enough players
        self.set_available_players({
            PlayerRole.QUEEN: range(0, 9),
            PlayerRole.WARRIOR: range(0, 9),
            PlayerRole.OBJECTIVE: range(0, 9),
        })
        self.assertIsNone(self.session.next_match_players())

        # Simple 10 players available
        self.set_available_players({
            PlayerRole.QUEEN: range(0, 2),
            PlayerRole.WARRIOR: range(2, 8),
            PlayerRole.OBJECTIVE: range(8, 10),
        })

        players_by_role = self.session.next_match_players()
        self.assertEqual(len(players_by_role[PlayerRole.QUEEN]), 2)
        self.assertEqual(len(players_by_role[PlayerRole.WARRIOR]), 6)
        self.assertEqual(len(players_by_role[PlayerRole.OBJECTIVE]), 2)

    def test_generate_match(self):
        self.set_available_players({
            PlayerRole.QUEEN: list(range(0, 3)),
            PlayerRole.WARRIOR: list(range(3, 12)),
            PlayerRole.OBJECTIVE: list(range(12, 15)),
        })

        self.assertEqual(self.session.next_match_team_comp(), (1, 3, 1))

        match = self.session.generate_match()
        self.assertIsNotNone(match)

    def test_remove_and_regenerate(self):
        self.set_available_players({
            PlayerRole.QUEEN: list(range(0, 3)),
            PlayerRole.WARRIOR: list(range(3, 12)),
            PlayerRole.OBJECTIVE: list(range(12, 15)),
        })

        self.assertEqual(self.session.next_match_team_comp(), (1, 3, 1))

        match = self.session.generate_match()
        self.assertIsNotNone(match)

        original_ids = set([pm.player.id for pm in match.playermatch_set.all()])

        to_remove = match.playermatch_set.order_by("id").first()
        to_remove_id = to_remove.player.id

        to_remove_signup = to_remove.signup
        to_remove_signup.is_ready = False
        to_remove_signup.save()

        to_remove.delete()

        self.session.generate_match(match=match)
        new_ids = set([pm.player.id for pm in match.playermatch_set.all()])

        self.assertFalse(to_remove_id in new_ids)
        self.assertNotEqual(original_ids, new_ids)
        self.assertEqual(original_ids - new_ids, set([to_remove_id]))
        self.assertEqual(len(new_ids - original_ids), 1)


class WhiteboardAPITestCase(HiveMindTest):
    pass
