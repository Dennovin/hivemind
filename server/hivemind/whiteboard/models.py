import hashlib
import itertools
import json
import statistics

import trueskill
from django.contrib.postgres.fields import ArrayField
from django.db import models, transaction
from django.utils import timezone

from ..client import redis_client
from ..constants import CabinetTeam, PlayerRole
from ..game.models import Cabinet, Scene
from ..model import BaseModel
from ..user.models import User
from ..worker import app

trueskill_env = trueskill.TrueSkill(draw_probability=0.0)

class WhiteboardSession(BaseModel):
    scene = models.ForeignKey(Scene, on_delete=models.CASCADE)
    cabinet = models.ForeignKey(Cabinet, on_delete=models.CASCADE)
    is_active = models.BooleanField(default=False)
    is_paused = models.BooleanField(default=False)
    is_last_set = models.BooleanField(default=False)
    start_time = models.DateTimeField(default=timezone.now)
    rounds_per_match = models.IntegerField(default=0)
    wins_per_match = models.IntegerField(default=0)

    # (queen, warrior, obj) count for each team
    TEAM_COMPS = [
        (1, 3, 1),
        (1, 2, 2),
        (1, 1, 3),
    ]

    def next_match_team_comp(self):
        signups = self.playersignup_set.filter(is_ready=True, roles__len__gt=0)
        if len(signups) < 10:
            return None

        available = {role: signups.filter(roles__contains=[role]).count() for role in PlayerRole}

        comp_sd = {
            comp: statistics.stdev([available[role] / n for role, n in zip(PlayerRole, comp)])
            for comp in self.TEAM_COMPS
        }

        team_comps_sorted = sorted(self.TEAM_COMPS, key=lambda i: comp_sd[i])

        for comp in team_comps_sorted:
            available = {role: signups.filter(roles__contains=[role]).count()
                         for role in PlayerRole}
            needed = {role: n*2 for role, n in zip(PlayerRole, comp)}

            # First make sure there's enough for each role
            if any([available[role] < needed[role] for role in PlayerRole]):
                continue

            # Then make sure a valid comp can be created
            for signup in signups.order_by("roles__len"):
                selected_role = sorted(signup.roles, key=lambda i: available[i] - needed[i])[0]
                needed[selected_role] -= 1

                for role in signup.roles:
                    available[role] -= 1

                if not any([v > 0 for v in needed.values()]):
                    return comp

    def next_match_players(self, match=None):
        comp = self.next_match_team_comp()
        if comp is None:
            return None

        signups = list(self.playersignup_set.filter(is_ready=True))
        players_by_role = {role: [] for role in PlayerRole}

        # Find priority for each player
        priority = {}
        for signup in signups:
            priority[signup.id] = signup.priority()

        # If this is an existing match, remove all players and set them to highest priority
        if match is not None:
            existing_players = [pm.player for pm in match.playermatch_set.all()]
            match.playermatch_set.all().delete()

            for player in existing_players:
                signups.push(player)
                signup = PlayerSignup.objects.get(player=player, session=self)
                priority[signup.id] = (float("-inf"), )

        signups.sort(key=lambda i: priority.get(i.id))
        needed = {role: n*2 for role, n in zip(PlayerRole, comp)}

        while any([i > 0 for i in needed.values()]):
            available = {role: len(list(filter(lambda i: role in i.roles, signups))) for role in PlayerRole}
            selected_role = list(sorted(filter(lambda i: needed[i] > 0, needed.keys()),
                                        key=lambda i: available[i] - needed[i]))[0]

            try:
                selected_player = list(filter(lambda i: selected_role in i.roles, signups))[0]
            except IndexError:
                return None

            players_by_role[selected_role].append(selected_player)
            signups.remove(selected_player)
            needed[selected_role] -= 1

        return players_by_role

    def generate_match(self, match=None):
        if match is None and self.match_set.filter(is_active=True).count() > 0:
            return None

        players = self.next_match_players(match=match)
        if players is None:
            if match is not None:
                match.is_active = False
                match.save()

            return None

        # Get each player's rating in their selected role
        ratings = {player.player_id: player.player.get_rating(role)
                   for role in players.keys()
                   for player in players[role]}

        # Get list of all previous teams so we don't repeat
        previous_teams = set()
        for prev_match in self.match_set.all():
            for team in CabinetTeam:
                previous_teams.add(frozenset([i.player_id for i in prev_match.playermatch_set.filter(team=team)]))

        # All possible combinations
        combinations_by_role = [list(itertools.combinations(players[role], int(len(players[role]) / 2)))
                                for role in PlayerRole]
        combinations = itertools.product(*combinations_by_role)

        # Get match quality for each combination, and select the team with the best quality
        blue_team = None
        gold_team = None
        best_quality = 0

        for blue_players_by_role in combinations:
            gold_players_by_role = [[i for i in players[role] if i not in blue_players_by_role[role_idx]]
                                    for role_idx, role in enumerate(PlayerRole)]

            blue_players = [i for role_players in blue_players_by_role for i in role_players]
            gold_players = [i for role_players in gold_players_by_role for i in role_players]
            blue_ratings = [ratings[i.player_id] for i in blue_players]
            gold_ratings = [ratings[i.player_id] for i in gold_players]
            match_quality = trueskill_env.quality((blue_ratings, gold_ratings))

            if match_quality > best_quality:
                blue_team = blue_players_by_role
                gold_team = gold_players_by_role
                best_quality = match_quality

        # Create match and add players
        if match is None:
            match = Match(session=self)
            match.save()

            app.send_task("hivemind.notifications.tasks.notify_whiteboard_match_started",
                          countdown=5, args=[match.id])


        match.is_active = True
        match.save()

        for role, blue_players, gold_players in zip(PlayerRole, blue_team, gold_team):
            for player in blue_players:
                PlayerMatch.objects.create(player=player.player, match=match, team=CabinetTeam.BLUE, role=role)
            for player in gold_players:
                PlayerMatch.objects.create(player=player.player, match=match, team=CabinetTeam.GOLD, role=role)

        return match

    def replace_player(self, match, signup):
        try:
            player_match = match.playermatch_set.get(player=signup.player)
        except PlayerMatch.DoesNotExist:
            return None

        role = player_match.role
        rating = signup.player.get_rating(role)

        role_signups = match.session.playersignup_set \
                                    .filter(is_ready=True, roles__contains=[player_match.role]) \
                                    .exclude(id__in=[i.signup.id for i in match.playermatch_set.all()])

        # If there are no other signups, panic
        if role_signups.count() == 0:
            return None

        ratings = {player_signup.id: player_signup.player.get_rating(role)
                   for player_signup in role_signups}
        rating_diffs = {k: abs(v.mu - rating.mu) for k, v in ratings.items()}
        signups_sorted = sorted(role_signups, key=lambda i: rating_diffs[i.id])
        selected_signup = signups_sorted[0]

        player_match.player = selected_signup.player
        player_match.save()

    def process_game_end(self, game):
        if self.is_paused or not self.is_active:
            return

        match = self.match_set.filter(is_active=True).first()
        if match is None:
            return

        game.whiteboard_match = match
        game.save()

        if game.winning_team == CabinetTeam.BLUE:
            match.blue_score += 1
        if game.winning_team == CabinetTeam.GOLD:
            match.gold_score += 1

        if match.start_time is None:
            match.start_time = game.start_time

        match.save()

        if self.rounds_per_match > 0 and match.blue_score + match.gold_score >= self.rounds_per_match:
            match.set_complete()
        if self.wins_per_match > 0 and max(match.blue_score, match.gold_score) >= self.wins_per_match:
            match.set_complete()

        self.publish()

    def get_current_match(self):
        data = {}

        current_match = self.match_set.filter(is_active=True).first()
        if current_match:
            return {
                "id": current_match.id,
                "blue_team": "Blue Team",
                "blue_score": current_match.blue_score,
                "blue_players": [mp.get_public_info() for mp in current_match.blue_players()],
                "gold_team": "Gold Team",
                "gold_score": current_match.gold_score,
                "gold_players": [mp.get_public_info() for mp in current_match.gold_players()],
                "rounds_per_match": self.rounds_per_match,
                "wins_per_match": self.wins_per_match,
            }

        return None

    def get_status(self):
        return {
            "scene": self.scene.id,
            "cabinet": self.cabinet.id,
            "is_active": self.is_active,
            "is_paused": self.is_paused,
            "current_match": self.get_current_match(),
            "rounds_per_match": self.rounds_per_match,
            "wins_per_match": self.wins_per_match,
            "players_queued": self.playersignup_set.filter(is_ready=True).count(),
        }

    def publish(self):
        app.send_task("hivemind.whiteboard.tasks.publish", args=[self.id])


class Player(BaseModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def get_rating(self, role):
        try:
            return self.rating_set.get(role=role)
        except Rating.DoesNotExist:
            return trueskill_env.create_rating()


class Rating(BaseModel):
    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    role = models.CharField(max_length=30, choices=PlayerRole.choices)
    mu = models.FloatField(default=0)
    sigma = models.FloatField(default=0)

    @classmethod
    def create_initial_rating(cls, player, role, self_rating=0):
        initial_mu_value = trueskill_env.mu + trueskill_env.beta * (self_rating - 2)
        return cls.objects.create(player=player, role=role, mu=initial_mu_value, sigma=trueskill_env.sigma)

    class Meta:
        unique_together = ("player", "role")


class Match(BaseModel):
    session = models.ForeignKey(WhiteboardSession, on_delete=models.CASCADE)
    blue_score = models.IntegerField(default=0)
    gold_score = models.IntegerField(default=0)
    is_active = models.BooleanField(default=False)
    is_complete = models.BooleanField(default=False)
    created_time = models.DateTimeField(default=timezone.now)
    start_time = models.DateTimeField(null=True)

    def blue_players(self):
        return self.playermatch_set.filter(team=CabinetTeam.BLUE).order_by("id")

    def gold_players(self):
        return self.playermatch_set.filter(team=CabinetTeam.GOLD).order_by("id")

    @transaction.atomic
    def rate_players(self):
        blue_players = self.playermatch_set.filter(team=CabinetTeam.BLUE)
        gold_players = self.playermatch_set.filter(team=CabinetTeam.GOLD)
        blue_ratings = {pm: pm.player.get_rating(pm.role) for pm in blue_players}
        gold_ratings = {pm: pm.player.get_rating(pm.role) for pm in gold_players}

        if len(blue_players) == 0 or len(gold_players) == 0:
            return

        ranks = [0 if self.blue_score > self.gold_score else 1, 0 if self.gold_score > self.blue_score else 1]
        rating_groups = trueskill_env.rate([blue_ratings, gold_ratings], ranks=ranks)
        for group in rating_groups:
            for player_match, rating in group.items():
                rating_entry, _ = Rating.objects.get_or_create(
                    player=player_match.player,
                    role=player_match.role,
                )
                rating_entry.mu = rating.mu
                rating_entry.sigma = rating.sigma
                rating_entry.save()

                RatingHistory.objects.update_or_create(
                    player=player_match.player,
                    match=self,
                    defaults={
                        "role": player_match.role,
                        "mu": rating.mu,
                        "sigma": rating.sigma,
                    },
                )

    def set_complete(self):
        with transaction.atomic():
            self.is_active = False
            self.is_complete = True
            self.save()
            self.rate_players()

            for player_match in self.playermatch_set.all():
                signup = self.session.playersignup_set.get(player=player_match.player)
                if not signup.requeue:
                    signup.is_ready = False
                    signup.save()

        if self.session.is_last_set:
            self.session.is_active = False
            self.session.save()
        else:
            self.session.generate_match()


class RatingHistory(BaseModel):
    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    role = models.CharField(max_length=30, choices=PlayerRole.choices)
    match = models.ForeignKey(Match, on_delete=models.CASCADE)
    rating_date = models.DateTimeField(default=timezone.now)
    mu = models.FloatField(default=0)
    sigma = models.FloatField(default=0)

    class Meta:
        unique_together = ("player", "match")


class PlayerSignup(BaseModel):
    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    session = models.ForeignKey(WhiteboardSession, on_delete=models.CASCADE)
    roles = ArrayField(models.CharField(max_length=30, choices=PlayerRole.choices), default=list)
    is_ready = models.BooleanField(default=False)
    signup_time = models.DateTimeField(default=timezone.now)
    requeue = models.BooleanField(default=True)

    class Meta:
        unique_together = ("player", "session")

    def priority(self):
        last_match = PlayerMatch.objects.filter(
            player=self.player,
            match__session=self.session,
            match__start_time__isnull=False
        ) \
                                        .select_related("match") \
                                        .order_by("-match__start_time") \
                                        .first()

        match_count = PlayerMatch.objects.filter(
            player=self.player,
            match__session=self.session,
            match__is_complete=True,
        ).count()

        if last_match:
            count_since = max(last_match.match.created_time, self.signup_time)
        else:
            count_since = self.signup_time

        matches_since_last = self.session.match_set.filter(created_time__gt=count_since).count()

        return (
            -matches_since_last,
            match_count,
            # Last item in sort order is "random" but needs to be consistent
            hashlib.sha1(bytes(self.id)).hexdigest(),
        )

    def publish(self):
        data = {
            "id": self.id,
            "player_id": self.player_id,
            "user": {
                "id": self.player.user_id,
                **self.player.user.get_public_info(),
            },
            "is_ready": self.is_ready,
            "requeue": self.requeue,
            "roles": self.roles,
            "priority": self.priority(),
        }

        redis_client.publish("whiteboard.{}.player".format(self.session.id), json.dumps(data))


class PlayerMatch(BaseModel):
    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    match = models.ForeignKey(Match, on_delete=models.CASCADE)
    team = models.CharField(max_length=5, null=True, choices=CabinetTeam.choices)
    role = models.CharField(max_length=30, choices=PlayerRole.choices)

    @property
    def signup(self):
        return PlayerSignup.objects.get(player=self.player, session=self.match.session)

    def get_public_info(self):
        return {
            "role": self.role,
            "player_id": self.player.id,
            **self.player.user.get_public_info(),
        }
