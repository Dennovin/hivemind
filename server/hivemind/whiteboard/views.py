from django.db import IntegrityError
from django.shortcuts import get_object_or_404
from django.utils import timezone
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response

from ..constants import HTTPMethod, PlayerRole
from ..model import BaseViewSet
from . import permissions
from .models import (Player, PlayerMatch, PlayerSignup, Rating,
                     WhiteboardSession)
from .serializers import PlayerSignupSerializer


class WhiteboardSessionViewSet(BaseViewSet):
    queryset = WhiteboardSession.objects.order_by("start_time").all()
    serializer_class = WhiteboardSession.serializer()
    filterset_fields = ["is_active", "scene_id", "cabinet_id"]
    permission_classes = [permissions.WhiteboardSessionPermission]

    def update(self, request, *args, **kwargs):
        response = super().update(request, *args, **kwargs)
        self.get_object().publish()
        return response

    @action(detail=True, methods=[HTTPMethod.GET], permission_classes=[AllowAny])
    def status(self, request, pk=None):
        session = get_object_or_404(WhiteboardSession, id=pk)
        return Response(session.get_status())

    @action(detail=True, methods=[HTTPMethod.GET], permission_classes=[AllowAny])
    def players(self, request, pk=None):
        session = get_object_or_404(WhiteboardSession, id=pk)
        players = session.playersignup_set.all()
        return Response(PlayerSignupSerializer(players, many=True).data)

    @action(detail=True, methods=[HTTPMethod.GET, HTTPMethod.PUT])
    def signup(self, request, pk=None):
        if request.method == HTTPMethod.GET:
            return self.get_signup(request, pk)
        if request.method == HTTPMethod.PUT:
            return self.update_signup(request, pk)

    def get_signup(self, request, pk=None):
        if not request.user.is_authenticated:
            return Response({})

        session = get_object_or_404(WhiteboardSession, id=pk)

        try:
            player = PlayerSignup.objects.get(user_id=request.user.id, session=session)
        except PlayerSignup.DoesNotExist:
            return Response({})

        return Response(PlayerSignupSerializer(player).data)

    def update_signup(self, request, pk=None):
        if not request.user.is_authenticated:
            return Response({})

        session = get_object_or_404(WhiteboardSession, id=pk)

        player, _ = Player.objects.get_or_create(user=request.user)
        signup, _ = PlayerSignup.objects.get_or_create(player=player, session=session)

        if request.data.get("is_ready", False) and not signup.is_ready:
            signup.signup_time = timezone.now()

        signup.roles = request.data.get("roles", [])
        signup.is_ready = request.data.get("is_ready", False)
        signup.requeue = request.data.get("requeue", False)

        signup.save()
        signup.publish()

        session.generate_match()
        session.publish()

        return Response(PlayerSignup.serializer()(signup).data)

    @action(detail=True, methods=[HTTPMethod.PUT], url_path="add-player")
    def add_player(self, request, pk=None):
        session = get_object_or_404(WhiteboardSession, id=pk)
        signup = get_object_or_404(PlayerSignup, id=request.data.get("signup_id"))

        signup.is_ready = True
        signup.save()
        signup.publish()

        session.generate_match()
        session.publish()

        return Response({})

    @action(detail=True, methods=[HTTPMethod.PUT], url_path="remove-player")
    def remove_player(self, request, pk=None):
        session = get_object_or_404(WhiteboardSession, id=pk)
        signup = get_object_or_404(PlayerSignup, id=request.data.get("signup_id"))

        signup.is_ready = False
        signup.save()

        # Check if player is in the current match
        player_match = PlayerMatch.objects.filter(
            match__session=session,
            player=signup.player,
            match__is_active=True,
        ).first()
        if player_match is not None:
            match = player_match.match
            session.replace_player(match=match, signup=signup)

        session.publish()

        return Response({})

class PlayerViewSet(BaseViewSet):
    queryset = Player.objects.order_by("id").all()
    serializer_class = Player.serializer()
    permission_classes = [permissions.PlayerPermission]
    filterset_fields = ["user_id"]

    @action(detail=False, methods=[HTTPMethod.GET], permission_classes=[IsAuthenticated])
    def me(self, request):
        if not request.user.is_authenticated:
            return Response({})

        try:
            player = Player.objects.get(user=request.user)
            return Response({"id": player.id})
        except Player.DoesNotExist:
            return Response({})

    @action(detail=False, methods=[HTTPMethod.POST], permission_classes=[IsAuthenticated], url_path="create")
    def create_player(self, request):
        if not request.user.is_authenticated:
            return Response({})

        player, _ = Player.objects.get_or_create(user=request.user)

        for role in PlayerRole:
            try:
                Rating.create_initial_rating(player, role, request.data.get(role, 0))
            except IntegrityError:
                return Response({"success": False, "error": "Player rating already exists."})

        return Response({"success": True})
