# Generated by Django 3.2.7 on 2023-11-27 23:39

import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('whiteboard', '0003_rename_games_per_match_whiteboardsession_rounds_per_match'),
    ]

    operations = [
        migrations.AddField(
            model_name='whiteboardsession',
            name='is_paused',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='playersignup',
            name='roles',
            field=django.contrib.postgres.fields.ArrayField(base_field=models.CharField(choices=[('queen', 'Queen'), ('warrior', 'Warrior'), ('objective', 'Objective')], max_length=30), default=list, size=None),
        ),
    ]
