import random

from django.core.management import call_command
from django.core.management.base import BaseCommand

from hivemind.constants import PlayerRole
from hivemind.user.models import User
from hivemind.whiteboard.models import Player, PlayerSignup, WhiteboardSession


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument("--id", help="Session ID to add players to.")
        parser.add_argument("--count", help="Number of players/users to create.")

    def handle(self, *args, **kwargs):
        count = int(kwargs.get("count"))
        session = WhiteboardSession.objects.get(id=kwargs.get("id"))

        call_command("create_test_users", count=count)

        users = User.objects.order_by("-id")[:count]

        role_selections = [
            (PlayerRole.QUEEN,),
            (PlayerRole.WARRIOR,),
            (PlayerRole.OBJECTIVE,),
            (PlayerRole.QUEEN, PlayerRole.WARRIOR),
            (PlayerRole.WARRIOR, PlayerRole.OBJECTIVE),
            (PlayerRole.QUEEN, PlayerRole.WARRIOR, PlayerRole.OBJECTIVE),
        ]

        for user in users:
            player, _ = Player.objects.get_or_create(user=user)
            signup = PlayerSignup.objects.create(
                player=player,
                session=session,
                roles=random.choice(role_selections),
                is_ready=True,
            )

        session.publish()
