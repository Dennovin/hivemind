import json
import logging
from datetime import timedelta

from celery import shared_task
from django.utils import timezone

from ..client import redis_client
from .models import WhiteboardSession
from .serializers import PlayerSignupSerializer

logger = logging.getLogger(__name__)

@shared_task
def publish(session_id):
    session = WhiteboardSession.objects.get(id=session_id)
    current_match = session.get_current_match()

    redis_client.publish("matchstate", json.dumps({
        "type": "match",
        "match_type": "whiteboard",
        "cabinet_id": session.cabinet.id,
        "session_id": session.id,
        "current_match": current_match,
    }))

    redis_client.publish("whiteboard.{}.status".format(session.id), json.dumps(session.get_status()))

    players = session.playersignup_set.all()
    redis_client.publish(
        "whiteboard.{}.players".format(session.id),
        json.dumps(PlayerSignupSerializer(players, many=True).data),
    )

@shared_task
def deactivate_stale_sessions():
    for session in WhiteboardSession.objects.filter(is_active=True):
        last_activity_time = max(filter(lambda i: i is not None, [
            session.start_time,
            session.match_set.order_by("-start_time").first().start_time if session.match_set.count() > 0 else None,
        ]))

        if last_activity_time < timezone.now() - timedelta(hours=4):
            session.is_active = False
            session.save()
