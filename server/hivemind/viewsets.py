from datetime import datetime

from rest_framework import viewsets


class DeleteableMixin(viewsets.GenericViewSet):
    def get_queryset(self):
        return super().get_queryset().filter(deleted=False)

    def perform_destroy(self, instance):
        instance.deleted = True
        instance.deleted_by = self.request.user
        instance.deleted_at = datetime.utcnow()

        instance.save()
