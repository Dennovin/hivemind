import http.client

from ..tests import HiveMindTest


class WikiTestCase(HiveMindTest):
    def test_create_page(self):
        self.client.force_authenticate(self._user)

        response = self.client.post("/api/wiki/publish/", {
            "slug": "Test_Page",
            "title": "Test Page",
            "page_text": "This is a test page.",
        })

        self.assertEqual(response.status_code, http.client.CREATED)

        response = self.client.get("/api/wiki/page/", params={"slug": "Test_Page"})
        self.assertEqual(response.status_code, http.client.OK)
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(response.data["results"][0]["current"]["title"], "Test Page")

        response = self.client.post("/api/wiki/publish/", {
            "slug": "Test_Page",
            "title": "Test Page Edit",
            "page_text": "This page has been edited.",
        })

        self.assertEqual(response.status_code, http.client.CREATED)

        response = self.client.get("/api/wiki/page/", params={"slug": "Test_Page"})
        self.assertEqual(response.status_code, http.client.OK)
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(response.data["results"][0]["current"]["title"], "Test Page Edit")
