import itertools
import math

from .constants import MatchType


class MatchEntry:
    def __init__(self, blue_team=None, gold_team=None, winner_to=None, loser_to=None,
                 round_num=None, match_type=None, winner_place=None, loser_place=None):
        self.blue_team = blue_team
        self.gold_team = gold_team
        self.winner_to = winner_to
        self.loser_to = loser_to
        self.round_num = round_num
        self.match_type = match_type
        self.winner_place = winner_place
        self.loser_place = loser_place

    def swap_teams(self):
        (self.blue_team, self.gold_team) = (self.gold_team, self.blue_team)

    def __str__(self):
        return f"{self.blue_team}-{self.gold_team} W={self.winner_to} L={self.loser_to}"


def get_games_played(team, schedule):
    """
    Get the total number of games that this team has played
    """

    return len([i for i in schedule if team in (i.blue_team, i.gold_team)])

def get_games_since_team_played(team, schedule):
    """
    Get the number of games since this team has played
    """

    for i, match in enumerate(schedule[::-1]):
        if team in (match.blue_team, match.gold_team):
            return i

    return len(schedule)

def get_match_sort_value(match, schedule):
    """
    Get the sort value for when a match should be played in a given schedule
    Sort by total games played (ascending), then games since last played (descending)
    """

    return (
        sum([get_games_played(i, schedule) for i in match]),
        -sum([get_games_since_team_played(i, schedule) for i in match]),
        sum(match),
    )

def generate_round_robin_matches(num_teams):
    """
    Generate a list of team placements for a round robin between the provided number of teams
    Returns a generator, with each item being a MatchEntry object
    """

    midpoint = math.ceil(num_teams/2)
    num_teams_to_match = midpoint * 2
    num_rounds = num_teams_to_match - 1
    rounds = []
    schedule = []

    for round_num in range(num_rounds):
        all_teams = list(range(num_teams_to_match))
        # Circle method: keep first team in place and rotate the rest,
        # then match up first vs. last and so on
        rotated = all_teams[0:1] + all_teams[num_teams_to_match-round_num:] + \
            all_teams[1:num_teams_to_match-round_num]

        this_round = []
        for a, b in zip(rotated[:midpoint], rotated[midpoint:][::-1]):
            if (a < num_teams and b < num_teams):
                this_round.append((a, b))

        while len(this_round) > 0:
            this_round = sorted(this_round, key=lambda i: get_match_sort_value(i, schedule))
            teams = this_round.pop(0)
            match = MatchEntry(*sorted(teams), round_num=round_num+1)

            # Give each team an equal number of games on gold and blue (or as close as possible)
            # by reversing games where the sum of the team IDs is even
            if sum(teams) % 2 == 0:
                match.swap_teams()

            schedule.append(match)
            yield match

def generate_ladder_matches(num_teams):
    """
    Generate a list of team placements for a ladder stage between the provided number of teams
    Returns a list, with each item being a MatchEntry object
    """
    matches = [
        MatchEntry(blue_team=num_teams-2, gold_team=num_teams-1, winner_to=1, round_num=num_teams-1)
    ]

    for i in range(num_teams-3, -1, -1):
        matches.append(
            MatchEntry(blue_team=i, round_num=i+1, winner_to=(num_teams - i - 1 if i > 0 else None))
        )

    return matches

def snake_order(teams):
    """
    Orders a list in the way that the teams would appear in a single-elimination bracket.
    """

    left = []
    right = []

    left.append(teams.pop(0))

    while len(teams) > 0:
        try:
            right.append(teams.pop(0))
            right.append(teams.pop(0))
            left.append(teams.pop(0))
            left.append(teams.pop(0))
        except IndexError:
            pass

    if len(left) > 2:
        left = snake_order(left)
    if len(right) > 2:
        right = snake_order(right)

    return left + right

def remove_matches(games, to_remove):
    """
    Removes games from a list. Adjusts the target index of other games.
    """

    for idx in sorted(to_remove, reverse=True):
        for game in games:
            if game.winner_to is not None and game.winner_to > idx:
                game.winner_to -= 1
            if game.loser_to is not None and game.loser_to > idx:
                game.loser_to -= 1

        games.pop(idx)


def generate_single_elim_matches(num_teams):
    """
    Generate a list of team placements for a single elimination bracket between the provided number of teams
    Returns a list, with each item being a MatchEntry object
    """

    num_rounds = int(math.ceil(math.log(num_teams) / math.log(2)))
    num_byes = 2**num_rounds - num_teams

    games = []

    # First round
    first_round = snake_order(list(range(2**num_rounds)))

    for game in range(int(len(first_round) / 2)):
        blue = first_round.pop(0)
        gold = first_round.pop(0)
        games.append(MatchEntry(blue, gold, round_num=num_rounds))

    # All other rounds
    for i in range(2**(num_rounds-1) - 1):
        games.append(MatchEntry())

    # Fill in winner_to
    for idx, game in enumerate(games[:-1]):
        game.winner_to = int(len(games) - math.floor((len(games) - idx) / 2))

    # Set round number
    for game in games[::-1]:
        if game.winner_to is None:
            game.round_num = 1
            game.winner_place = 1
            game.loser_place = 2
        else:
            game.round_num = games[game.winner_to].round_num + 1
            game.loser_place = 2**games[game.winner_to].round_num + 1

    # Award byes
    to_remove = []
    for idx, game in enumerate(games[:2**(num_rounds-1)]):
        if game.round_num == num_rounds and game.blue_team < num_byes:
            if games[game.winner_to].blue_team is None:
                games[game.winner_to].blue_team = game.blue_team
            else:
                games[game.winner_to].gold_team = game.blue_team

            to_remove.append(idx)

    remove_matches(games, to_remove)

    return games

def generate_double_elim_matches(num_teams):
    """
    Generate a list of team placements for a double elimination bracket between the provided number of teams
    Returns a generator, with each item being a MatchEntry object
    """

    num_wb_rounds = math.ceil(math.log(num_teams) / math.log(2))
    num_lb_rounds = 2 * num_wb_rounds - 2
    full_bracket_size = 2 ** num_wb_rounds
    num_byes = full_bracket_size - num_teams
    total_matches = 2 * full_bracket_size - 1

    # Create MatchEntry objects
    finals = [
        MatchEntry(round_num=0, match_type=MatchType.FIRST_FINAL),
        MatchEntry(round_num=0, match_type=MatchType.SECOND_FINAL),
    ]

    wb_rounds = [
        [MatchEntry(round_num=rnd+1, match_type=MatchType.WINNERS) for i in range(2**rnd)]
        for rnd in range(num_wb_rounds)
    ]

    lb_rounds = [
        [MatchEntry(round_num=rnd+1, match_type=MatchType.LOSERS) for i in range(2**int(math.floor(rnd/2)))]
        for rnd in range(num_lb_rounds)
    ]

    # Fill in first round teams
    first_round = snake_order(list(range(full_bracket_size)))

    for idx in range(int(full_bracket_size / 2)):
        wb_rounds[-1][idx].blue_team = first_round.pop(0)
        wb_rounds[-1][idx].gold_team = first_round.pop(0)

    # Fill winner_to and loser_to for winners bracket
    for round_idx, wb_round in enumerate(wb_rounds):
        last_idx = 2**wb_round[0].round_num
        for idx, match in enumerate(wb_round[::-1]):
            match.winner_to = total_matches - int(last_idx + math.floor(idx / 2))

            # Flip the order every other round to make rematches less likely
            m = idx if round_idx % 2 == 0 else len(wb_round) - idx - 1
            match.loser_to = total_matches - int(last_idx + len(wb_round) + m)

    # First round fills in loser_to differently, fix those
    for idx, match in enumerate(wb_rounds[-1]):
        m = idx if num_wb_rounds % 2 > 0 else len(wb_rounds[-1]) - idx - 1
        match.loser_to = len(wb_rounds[-1]) + int(math.floor(m / 2))

    # Fill out winner_to for losers bracket
    for round_idx, lb_round in enumerate(lb_rounds):
        if round_idx % 2 == 0:
            start_idx = total_matches - int(3 * 2**int(round_idx/2) - 1)
            for idx, match in enumerate(lb_round):
                match.winner_to = start_idx + int(math.floor(idx/2))
                match.loser_place = 2**int(round_idx/2 + 1) + 1
        else:
            start_idx = total_matches - 2**int((round_idx+3)/2) + 1
            for idx, match in enumerate(lb_round):
                match.winner_to = start_idx + int(math.floor(idx))
                match.loser_place = 2**int(math.floor(round_idx/2)) * 3 + 1

    # Put rounds in order
    all_matches = []
    while len(wb_rounds) > 0:
        all_matches += wb_rounds.pop()

        while len(lb_rounds) > 0 and len(lb_rounds) >= len(wb_rounds) * 2:
            all_matches += lb_rounds.pop()

    all_matches += finals

    # Award byes
    to_remove = []
    for idx, game in enumerate(all_matches[:int(full_bracket_size/2)]):
        if game.blue_team < num_byes:
            if all_matches[game.winner_to].blue_team is None:
                all_matches[game.winner_to].blue_team = game.blue_team
            else:
                all_matches[game.winner_to].gold_team = game.blue_team

            for other_idx, other_game in enumerate(all_matches):
                if idx != other_idx and other_game.loser_to == game.loser_to:
                    other_game.loser_to = all_matches[other_game.loser_to].winner_to

            to_remove.append(idx)
            to_remove.append(game.loser_to)

    remove_matches(all_matches, to_remove)

    return all_matches
