import json
import os
from datetime import datetime

from django.db import models
from django.utils import timezone

from ..client import redis_client
from ..game.models import Cabinet, Scene
from ..model import BaseModel
from ..user.models import User


class OverlayType(models.TextChoices):
    INGAME = "ingame", "In-Game"
    POSTGAME = "postgame", "Postgame"
    AUTO = "auto", "Auto"

    @property
    def template_filename(self):
        FILENAMES = {
            self.INGAME: "ingame.html",
            self.POSTGAME: "postgame.html",
            self.AUTO: "auto.html",
        }

        return FILENAMES[self]


class Overlay(BaseModel):
    name = models.CharField(max_length=200, default="Default Overlay")
    cabinet = models.ForeignKey(Cabinet, on_delete=models.CASCADE, related_name="overlays")
    ingame_theme = models.CharField(max_length=50, default="default")
    postgame_theme = models.CharField(max_length=50, default="default")
    match_theme = models.CharField(max_length=50, default="default")
    match_preview_theme = models.CharField(max_length=50, default="default")
    player_cams_theme = models.CharField(max_length=50, default="default")
    is_default = models.BooleanField(default=False)
    show_players = models.BooleanField(default=False)
    show_chat = models.BooleanField(default=False)
    gold_on_left = models.BooleanField(default=False)
    blue_team = models.CharField(max_length=50, default="Blue Team", blank=True)
    blue_score = models.IntegerField(default=0)
    gold_team = models.CharField(max_length=50, default="Gold Team", blank=True)
    gold_score = models.IntegerField(default=0)
    match_win_max = models.IntegerField(default=0)
    show_score = models.BooleanField(default=False)
    update_score = models.BooleanField(default=False)
    last_updated = models.DateTimeField(default=timezone.now)
    last_updated_by = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
    additional_fonts = models.URLField(blank=True, default="")
    enable_autoscene = models.BooleanField(default=False)
    enable_postgame_scene_duration = models.BooleanField(default=True)
    enable_match_summary_duration = models.BooleanField(default=True)
    enable_autoscene_always = models.BooleanField(default=False)
    postgame_scene_duration_ms = models.IntegerField(default=10000)
    postgame_delay_duration_ms = models.IntegerField(default=6000)
    match_summary_duration_ms = models.IntegerField(default=30000)

    def publish_settings(self):
        redis_client.publish("overlaysettings", json.dumps({
            "type": "overlaysettings",
            "overlay_id": self.id,
            "ingame_theme": self.ingame_theme,
            "postgame_theme": self.postgame_theme,
            "match_theme": self.match_theme,
            "match_preview_theme": self.match_preview_theme,
            "player_cams_theme": self.player_cams_theme,
            "cabinet_name": self.cabinet.name,
            "show_players": self.show_players,
            "blue_team": self.blue_team,
            "blue_score": self.blue_score,
            "gold_team": self.gold_team,
            "gold_score": self.gold_score,
            "show_score": self.show_score,
            "match_win_max": self.match_win_max,
        }))
