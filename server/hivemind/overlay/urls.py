from django.urls import include, path
from rest_framework import routers

from .models import Overlay
from .views import OverlayViewSet

router = routers.DefaultRouter()
router.register(r"overlay", OverlayViewSet)

urlpatterns = [
    path("", include(router.urls)),
]
