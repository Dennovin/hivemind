from django.db.models import Q
from django_filters import rest_framework as filters

from .models import Match, Player


class MatchFilter(filters.FilterSet):
    team_id = filters.NumberFilter(method="team_id_filter")
    byoteam_id = filters.NumberFilter(method="byoteam_id_filter")

    def team_id_filter(self, queryset, name, value):
        return queryset.filter(Q(blue_team_id=value) | Q(gold_team_id=value))

    def byoteam_id_filter(self, queryset, name, value):
        return queryset.filter(Q(blue_team__byoteam_id=value) | Q(gold_team__byoteam_id=value))

    class Meta:
        model = Match
        fields = [
            "event_id",
            "team_id",
            "byoteam_id",
            "is_complete",
        ]


class PlayerFilter(filters.FilterSet):
    event_id = filters.NumberFilter(field_name="event__id")
    team_id = filters.NumberFilter(field_name="team__id")
    byoteam_id = filters.NumberFilter(field_name="byoteam__id")
    qp_team_id = filters.NumberFilter(field_name="qp_team__id")

    class Meta:
        model = Player
        fields = [
            "scene_id",
            "event_id",
            "team_id",
            "byoteam_id",
            "qp_team_id",
        ]
