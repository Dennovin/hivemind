from django.urls import include, path
from rest_framework import routers

from .models import (BYOTeam, Match, Player, PlayerRating, QPMatch, QPPlayer,
                     Team)
from .views import (BYOTeamViewSet, EventViewSet, MatchViewSet, PlayerViewSet,
                    QPMatchViewSet, SeasonPlaceViewSet, SeasonViewSet,
                    TeamViewSet)

router = routers.DefaultRouter()
router.register(r"player", PlayerViewSet)
router.register(r"season", SeasonViewSet)
router.register(r"event", EventViewSet)
router.register(r"byoteam", BYOTeamViewSet)
router.register(r"team", TeamViewSet)
router.register(r"match", MatchViewSet)
router.register(r"qpmatch", QPMatchViewSet)
router.register(r"qpplayer", QPPlayer.viewset())
router.register(r"player-rating", PlayerRating.viewset())
router.register(r"season-place", SeasonPlaceViewSet)

urlpatterns = [
    path("", include(router.urls)),
]
