from jinja2 import Environment

from hivemind.game.models import CabinetPosition


def environment(*args, **kwargs):
    env = Environment(*args, **kwargs)

    env.filters["positionname"] = lambda i: CabinetPosition(int(i)).label
    env.filters["positionimage"] = lambda i: CabinetPosition(int(i)).image
    env.filters["positionwarriorimage"]  = lambda i: CabinetPosition(int(i)).warrior_image

    return env
