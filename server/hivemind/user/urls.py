import hivemind.stats.views
from django.urls import include, path
from rest_framework import routers
from rest_framework_simplejwt.views import TokenRefreshView

from . import views
from .models import User, UserGame

router = routers.DefaultRouter()
router.register(r"user", views.UserViewSet)
router.register(r"permission", views.PermissionViewSet)
router.register(r"permission-invite", views.PermissionInviteViewSet, basename="permission-invite")
router.register(r"game", UserGame.viewset())
router.register(r"scene-request", views.SceneRequestViewSet)
router.register(r"message", views.MessageViewSet)
router.register(r"nfc", hivemind.stats.views.NFCViewSet)
router.register(r"signin", hivemind.stats.views.SignInViewSet)

urlpatterns = [
    path("me/", views.current_user),
    path("exchange-token/<backend>/", views.exchange_token),
    path("refresh-token/", TokenRefreshView.as_view(), name="token_refresh"),

    path("", include(router.urls)),
]
