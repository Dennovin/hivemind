import logging
from collections import OrderedDict
from datetime import timedelta

import cachecontrol
import google.auth.transport.requests
import jwt
import patreon
import requests
import social_core.pipeline.social_auth
from django.conf import settings
from django.contrib.auth import login
from django.db import IntegrityError, transaction
from django.shortcuts import get_object_or_404, render
from django.utils import timezone
from google.oauth2 import id_token
from requests.exceptions import HTTPError
from rest_framework import status, viewsets
from rest_framework.decorators import action, api_view, permission_classes
from rest_framework.exceptions import PermissionDenied
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework_simplejwt.tokens import RefreshToken
from social_django.utils import psa

from ..constants import HTTPMethod, PermissionType
from ..game.models import Cabinet, Scene
from ..game.serializers import SceneSerializer
from ..model import BaseViewSet, ReadOnly
from ..pagination import Pagination
from ..permissions import SiteAdminPermission
from ..stats.models import NFC, SignInLog
from ..stats.stattype import stat_types
from ..tournament.models import TournamentPlayer
from .models import Message, Permission, PermissionInvite, SceneRequest, User
from .permissions import (MessagePermission, PermissionPermission,
                          SceneRequestPermission, UserPermission)
from .serializers import (MessageSerializer, PermissionInviteSerializer,
                          PermissionSerializer, SceneRequestSerializer,
                          SocialSerializer, UserPrivateSerializer,
                          UserPublicSerializer, UserSerializer,
                          UserTournamentSerializer)

logger = logging.getLogger(__name__)

session = requests.session()
cached_session = cachecontrol.CacheControl(session)

@api_view(["POST"])
@permission_classes([AllowAny])
@psa()
def exchange_token(request, backend=None):
    if request.backend.name == "google-oauth2":
        return exchange_token_google(request, backend)
    if request.backend.name == "patreon":
        return exchange_token_patreon(request, backend)
    if request.backend.name == "discord":
        return exchange_token_discord(request, backend)
    if request.backend.name == "twitch":
        return exchange_token_twitch(request, backend)

def exchange_token_oauth2(request, token_url, user_info_url, auth,
                          headers=None, get_email_from_response=None, extra_params=None):
    data = {
        "grant_type": "authorization_code",
        "code": request.data.get("code"),
        "redirect_uri": settings.PUBLIC_URL(f"/auth/{request.backend.name}"),
    }

    if extra_params:
        data.update(extra_params)

    response = requests.post(token_url, data=data, auth=auth, headers=headers)
    try:
        response.raise_for_status()
    except:
        logger.error("Could not authenticate: {}".format(response.text))
        logger.error("Postdata: {}".format(data))
        logger.error("Auth: {}".format(auth))
        raise

    access_token = response.json().get("access_token")
    req_headers = {"Authorization": f"Bearer {access_token}"}
    if headers is not None:
        req_headers.update(headers)

    response = requests.get(user_info_url, headers=req_headers)

    if get_email_from_response is None:
        email = response.json().get("email")
    else:
        email = get_email_from_response(response.json())

    if email is None:
        raise Exception("No email in response: {}".format(response.json()))
    social = request.backend.strategy.storage.user.get_social_auth(request.backend.name, email)
    if social is None:
        if request.user.is_authenticated:
            user = request.user
        else:
            user = User.objects.create(email=email, username=email)

        social = request.backend.strategy.storage.user.create_social_auth(user, email, request.backend.name)

    logger.warning(request.backend)
    login(request, social.user, backend=request.backend.name)
    token = RefreshToken.for_user(social.user)

    return Response({
        "success": True,
        "user_id": social.user.id,
        "access_token": str(token.access_token),
        "refresh_token": str(token),
    })

def exchange_token_google(request, backend=None):
    return exchange_token_oauth2(
        request,
        "https://www.googleapis.com/oauth2/v4/token",
        "https://www.googleapis.com/oauth2/v2/userinfo",
        (settings.SOCIAL_AUTH_GOOGLE_OAUTH2_KEY, settings.SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET),
    )

def exchange_token_twitch(request, backend=None):
    return exchange_token_oauth2(
        request,
        "https://id.twitch.tv/oauth2/token",
        "https://api.twitch.tv/helix/users",
        (settings.SOCIAL_AUTH_TWITCH_KEY, settings.SOCIAL_AUTH_TWITCH_SECRET),
        extra_params={
            "client_id": settings.SOCIAL_AUTH_TWITCH_KEY,
            "client_secret": settings.SOCIAL_AUTH_TWITCH_SECRET,
        },
        headers={
            "Client-Id": settings.SOCIAL_AUTH_TWITCH_KEY,
        },
        get_email_from_response=lambda r: r["data"][0]["email"],
    )

def exchange_token_patreon(request, backend=None):
    oauth_client = patreon.OAuth(settings.SOCIAL_AUTH_PATREON_KEY, settings.SOCIAL_AUTH_PATREON_SECRET)
    tokens = oauth_client.get_tokens(request.data.get("code"), settings.PUBLIC_URL("/auth/patreon"))
    access_token = tokens.get("access_token")

    api_client = patreon.API(access_token)
    patreon_user = api_client.fetch_user()
    email = patreon_user.data().attribute("email")

    user_data = patreon_user.json_data
    patreon_amount = None

    for item in user_data.get("included", []):
        if item.get("attributes") and item.get("attributes").get("amount_cents"):
            patreon_amount = item["attributes"]["amount_cents"]

    social = request.backend.strategy.storage.user.get_social_auth(request.backend.name, email)
    if social is None:
        user = request.user
        social = request.backend.strategy.storage.user.create_social_auth(
            user, email, request.backend.name)

        if patreon_amount is not None:
            user.patreon_amount = patreon_amount
            user.save()

    return Response({
        "success": True,
        "user_id": social.user.id,
    })

def exchange_token_discord(request, backend=None):
    return exchange_token_oauth2(
        request,
        "https://discord.com/api/v10/oauth2/token",
        "https://discord.com/api/v10/users/@me",
        (settings.SOCIAL_AUTH_DISCORD_KEY, settings.SOCIAL_AUTH_DISCORD_SECRET),
    )

@api_view(["GET"])
@permission_classes([AllowAny])
def current_user(request):
    if not request.user.is_authenticated:
        return Response({})

    serializer = UserPrivateSerializer(request.user)
    return Response(serializer.data)


class UserViewSet(BaseViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [UserPermission]

    @action(detail=True, methods=[HTTPMethod.GET], permission_classes=[AllowAny], url_path="public-data")
    def public_data(self, request, pk=None):
        user = get_object_or_404(User, id=pk)
        serializer = UserPublicSerializer(user)
        return Response(serializer.data)

    @action(detail=True, methods=[HTTPMethod.GET], permission_classes=[SiteAdminPermission])
    def email(self, request, pk=None):
        user = get_object_or_404(User, id=pk)
        return Response({ "email": user.email })

    @action(detail=True, methods=[HTTPMethod.GET], permission_classes=[UserPermission])
    def tournaments(self, request, pk=None):
        user = get_object_or_404(User, id=pk)
        queryset = TournamentPlayer.objects.filter(user_id=user.id).order_by("-tournament__date")
        paginator = Pagination()
        page = paginator.paginate_queryset(queryset, request, view=self)

        if page is None:
            serializer = UserTournamentSerializer(queryset, many=True)
            return Response(serializer.data)

        serializer = UserTournamentSerializer(page, many=True)
        return paginator.get_paginated_response(serializer.data)

    @action(detail=True, methods=[HTTPMethod.GET], permission_classes=[UserPermission], url_path="stats-by-month")
    def stats_by_month(self, request, pk=None):
        user = get_object_or_404(User, id=pk)
        stats = {}

        for row in user.usermonthlystat_set.all().order_by("stat_type", "month"):
            if row.stat_type not in stats:
                stats[row.stat_type] = []

            stats[row.stat_type].append({
                "month": row.month,
                "value": row.value,
            })

        response = []
        for stat_type in stat_types(is_hidden=False):
            response.append({
                "name": stat_type.name,
                "label": stat_type.display_name,
                "formatting": stat_type.formatting,
                "values": stats.get(stat_type.name, []),
            })

        return Response({"stats": response})

class PermissionViewSet(BaseViewSet):
    queryset = Permission.objects.all().order_by("id")
    serializer_class = PermissionSerializer
    permission_classes = [PermissionPermission]
    filterset_fields = ["scene_id", "user_id", "permission"]

    def get_queryset(self):
        user = self.request.user
        admin_permissions = Permission.objects.filter(user=user, permission=PermissionType.ADMIN)
        scene_ids = [p.scene_id for p in admin_permissions]
        return Permission.objects.filter(scene_id__in=scene_ids).order_by("id")

    def create(self, request):
        try:
            scene = Scene.objects.get(id=request.data["scene"])
        except Scene.DoesNotExist:
            raise PermissionDenied()

        if not (request.user.is_site_admin or request.user.is_admin_of(scene)):
            raise PermissionDenied()

        return super().create(request)


class PermissionInviteViewSet(viewsets.ViewSet):
    permission_classes = [AllowAny]

    def create(self, request):
        scene = get_object_or_404(Scene, id=request.data.get("scene"))
        if not (request.user.is_authenticated and request.user.is_admin_of(scene)):
            raise PermissionDenied()

        invite = PermissionInvite.objects.create(
            scene=scene,
            permission=request.data.get("permission"),
            created_by=request.user,
        )

        serializer = PermissionInviteSerializer(invite)
        return Response(serializer.data)

    def destroy(self, request, pk=None):
        invite = get_object_or_404(PermissionInvite, id=pk)
        if not (request.user.is_authenticated and request.user.is_admin_of(invite.scene)):
            raise PermissionDenied()

        invite.delete()
        return Response({ "success": True })

    @action(detail=False, url_path="by-scene/(?P<scene_id>[0-9]+)")
    def by_scene(self, request, scene_id):
        scene = get_object_or_404(Scene, id=scene_id)
        if not (request.user.is_authenticated and request.user.is_admin_of(scene)):
            raise PermissionDenied()

        invites = PermissionInvite.objects.filter(
            scene=scene,
            accepted_by__isnull=True,
            created_timestamp__gt=timezone.now() - timedelta(hours=1),
        )

        serializer = PermissionInviteSerializer(invites, many=True)
        return Response(serializer.data)

    @action(detail=False, url_path="by-token/(?P<token>[0-9a-f]+)")
    def by_token(self, request, token):
        invite = get_object_or_404(
            PermissionInvite,
            token=token,
            accepted_by__isnull=True,
            created_timestamp__gt=timezone.now() - timedelta(hours=1),
        )

        serializer = PermissionInviteSerializer(invite)
        return Response(serializer.data)

    @action(detail=True, url_path="accept", methods=[HTTPMethod.POST])
    def accept(self, request, pk=None):
        if not request.user.is_authenticated:
            raise PermissionDenied()

        invite = get_object_or_404(
            PermissionInvite,
            id=pk,
            token=request.data.get("token"),
            accepted_by__isnull=True,
            created_timestamp__gt=timezone.now() - timedelta(hours=1),
        )

        invite.accept(request.user)
        return Response({ "success": True })


class MessageViewSet(BaseViewSet):
    queryset = Message.objects.all().order_by("-timestamp")
    serializer_class = MessageSerializer
    permission_classes = [MessagePermission]

    def get_queryset(self):
        user = self.request.user
        return Message.objects.filter(user=user).order_by("-timestamp")

    @action(detail=False, methods=[HTTPMethod.POST], permission_classes=[SiteAdminPermission])
    def send(self, request):
        super().create(request)


class SceneRequestViewSet(BaseViewSet):
    queryset = SceneRequest.objects.all().order_by("-submit_date")
    serializer_class = SceneRequestSerializer
    permission_classes = [SceneRequestPermission]

    def create(self, request):
        response = super().create(request)

        message_text = 'A new scene has been registered: "{}"'.format(request.data.get("display_name"))
        for admin_user in User.objects.filter(is_site_admin=True):
            Message.objects.create(user=admin_user, message_text=message_text, url='/admin/scene-requests')

        return response

    @action(detail=True, methods=[HTTPMethod.POST])
    @transaction.atomic
    def approve(self, request, pk=None):
        scene_request = get_object_or_404(SceneRequest, id=pk)

        # Create scene from postdata in case site admin changed anything
        serializer = SceneSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        scene = serializer.instance

        # Give admin to user
        Permission.objects.create(
            user=scene_request.user,
            scene=scene,
            permission=PermissionType.ADMIN,
        )

        # Send message
        Message.objects.create(
            user=scene_request.user,
            message_text="The {} scene has been created.".format(scene.display_name),
            url="/scene/{}".format(scene.name),
        )

        # Mark request completed
        scene_request.completed = True
        scene_request.save()

        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
