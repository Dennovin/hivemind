import logging
import time
from datetime import timedelta

import patreon
from celery import shared_task
from django.conf import settings
from django.utils import timezone
from hivemind.game.models import Game
from hivemind.stats.models import SignInLog
from hivemind.tournament.models import TournamentPlayerGame

from ..constants import CabinetPosition, CabinetTeam
from .models import User, UserGame

logger = logging.getLogger(__name__)


@shared_task
def reprocess_user_stats(user_id):
    user = User.objects.get(id=user_id)

    user.userstat_set.all().delete()
    user.userpositionstat_set.all().delete()
    user.usermonthlystat_set.all().delete()
    user.playsession_set.all().delete()

    user.collect_stats()

    for player in user.tournamentplayer_set.all():
        player.collect_stats()


@shared_task
def reprocess_user_games(user_id):
    user = User.objects.get(id=user_id)

    user.userstat_set.all().delete()
    user.userpositionstat_set.all().delete()
    user.playsession_set.all().delete()
    user.usergame_set.all().delete()
    user.usermonthlystat_set.all().delete()

    for sign_in in user.signinlog_set.filter(action=SignInLog.Action.SIGN_IN).order_by("timestamp"):
        sign_out = SignInLog.objects.filter(
            cabinet=sign_in.cabinet,
            timestamp__gt=sign_in.timestamp,
            player_id=sign_in.player_id,
        ).order_by("timestamp").first()

        games = Game.objects.filter(
            cabinet=sign_in.cabinet,
            end_time__isnull=False,
            end_time__gte=sign_in.timestamp,
            end_time__lte=sign_out.timestamp,
        )

        for game in games.order_by("end_time"):
            # Discard games with invalid end times
            if game.end_time < game.start_time or game.end_time > game.start_time + timedelta(hours=1):
                continue

            user_game, created = UserGame.objects.get_or_create(
                game=game,
                player_id=sign_out.player_id,
                defaults={"user": user},
            )

            if not created:
                user_game.user = user
                user_game.save()

    reprocess_user_tournament_games(user_id)
    reprocess_user_stats(user_id)

@shared_task
def reprocess_all_users():
    users = User.objects.order_by("id")
    for user in users:
        while Game.objects.filter(start_time__gt=timezone.now() - timedelta(minutes=30)).count() > 0:
            time.sleep(60)

        logger.info("Reprocessing user {}".format(user.id))
        reprocess_user_games(user.id)

@shared_task
def reprocess_user_tournament_games(user_id):
    user = User.objects.get(id=user_id)

    for user_game in user.usergame_set.all():
        game = user_game.game

        # If this is a tournament game, check if user has a registration for that tournament
        if game.tournament_match_id is not None:
            tournament = game.tournament_match.bracket.tournament
            player = tournament.tournamentplayer_set.filter(user=user).first()

            if player is None:
                continue

            is_on_team = (
                (CabinetPosition(user_game.player_id).team == CabinetTeam.BLUE and
                 player.team == game.tournament_match.blue_team)
                or
                (CabinetPosition(user_game.player_id).team == CabinetTeam.GOLD and
                 player.team == game.tournament_match.gold_team)
            )

            if is_on_team:
                tpg, created = TournamentPlayerGame.objects.get_or_create(
                    game=game,
                    player_id=user_game.player_id,
                    defaults={"tournament_player_id": player.id},
                )

                if not created and tpg.tournament_player != player:
                    tpg.tournament_player = player
                    tpg.save()

@shared_task
def update_patrons():
    api_client = patreon.API(settings.PATREON_CREATOR_TOKEN)
    campaign = api_client.fetch_campaign()
    campaign_id = campaign.data()[0].id()

    User.objects.all().update(patreon_amount=None)

    pledges = api_client.fetch_page_of_pledges(campaign_id, 250)
    for pledge in pledges.data():
        email = pledge.relationship("patron").attribute("email")
        try:
            user = User.objects.get(email=email)
        except User.DoesNotExist:
            continue

        if pledge.attribute("declined_since"):
            continue

        user.patreon_amount = pledge.attributes().get("pledge_cap_cents")
        user.save()
