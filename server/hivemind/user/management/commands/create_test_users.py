import random

import py_avataaars
from django.core.management.base import BaseCommand
from faker import Faker

from hivemind.tests import HiveMindTest
from hivemind.tournament.models import HomeScene
from hivemind.user.models import User

fake = Faker()

class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument("--count", help="Number of users to create.")

    def handle(self, *args, **kwargs):
        count = int(kwargs.get("count"))

        if count is None:
            return

        all_scenes = [s.name for s in HomeScene.objects.all()]

        for player_name in HiveMindTest().get_player_names(count):
            image = py_avataaars.PyAvataaar(
                skin_color=random.choice(list(py_avataaars.SkinColor)),
                hair_color=random.choice(list(py_avataaars.HairColor)),
                facial_hair_type=random.choice(list(py_avataaars.FacialHairType)),
                facial_hair_color=random.choice(list(py_avataaars.HairColor)),
                top_type=random.choice(list(py_avataaars.TopType)),
                hat_color=random.choice(list(py_avataaars.Color)),
                mouth_type=random.choice(list(py_avataaars.MouthType)),
                eye_type=random.choice(list(py_avataaars.EyesType)),
                eyebrow_type=random.choice(list(py_avataaars.EyebrowType)),
                nose_type=random.choice(list(py_avataaars.NoseType)),
                accessories_type=random.choice(list(py_avataaars.AccessoriesType)),
                clothe_type=random.choice(list(py_avataaars.ClotheType)),
                clothe_color=random.choice(list(py_avataaars.Color)),
                clothe_graphic_type=random.choice(list(py_avataaars.ClotheGraphicType)),
            )
            image.render_png_file("/tmp/avatar.png")

            email = fake.email()
            user = User.objects.create(
                username=email,
                email=email,
                name=player_name,
                scene=random.choice(all_scenes),
                pronouns=random.choice(["he/him", "she/her", "he/they", "she/they", "they/them"]),
                is_profile_public=random.choice([True, False]),
            )

            with open("/tmp/avatar.png", "rb") as fh:
                user.image.save("user_avatar.png", fh, save=True)
