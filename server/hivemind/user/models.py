import hashlib
import itertools
import json
import logging
import os
import secrets
from datetime import datetime, timedelta

import pytz
from django.contrib.auth.models import AbstractUser, UserManager
from django.db import models, transaction
from django.db.models.functions import TruncDate, TruncMonth
from django.db.models.signals import post_save
from django.utils import timezone
from rest_framework.authtoken.models import Token

from ..client import redis_client
from ..constants import PermissionType
from ..game.models import Cabinet, CabinetPosition, Game, GameStat, Scene
from ..model import BaseModel
from ..stats.statmodel import AggregatedStatModel
from ..stats.stattype import (StatType, calculate_aggregates,
                              calculate_game_stats, stat_types)
from ..worker import app

logger = logging.getLogger(__name__)

def generate_token():
    return hashlib.sha512(os.urandom(2048)).hexdigest()[:32]


class User(AbstractUser):
    name = models.CharField(max_length=200, default="")
    email = models.EmailField(unique=True)
    is_site_admin = models.BooleanField(default=False)
    is_profile_public = models.BooleanField(default=False)
    scene = models.CharField(max_length=10, null=True, blank=True)
    image = models.ImageField(null=True, blank=True, upload_to="user/user-images/")
    pronouns = models.CharField(max_length=30, null=True, blank=True)
    patreon_amount = models.IntegerField(null=True, blank=True)

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    objects = UserManager()

    @classmethod
    def by_email(cls, email):
        try:
            return cls.objects.get(email=email)
        except cls.DoesNotExist:
            return cls(email=email)

    def generate_token(self):
        token = Token.objects.create(user=self)
        token.save()

        return token

    def get_current_signin(self):
        return SignInLog.objects.filter(user=self, is_current=True, action=SignInLog.Action.SIGN_IN).first()

    def is_admin_of(self, scene):
        return self.has_permission(scene, PermissionType.ADMIN)

    def has_permission(self, scene, permission):
        return self.permissions.filter(scene=scene, permission=permission).count() > 0

    def has_any_permission(self, scene, permissions):
        return self.permissions.filter(scene=scene, permission__in=permissions).count() > 0

    @property
    def is_supporter(self):
        return self.patreon_amount and self.patreon_amount >= 100

    def get_public_info(self):
        return {
            "name": self.name,
            "scene": self.scene,
            "image": self.image.url if self.image else None,
            "pronouns": self.pronouns,
            "is_profile_public": self.is_profile_public,
            "is_supporter": self.is_supporter,
        }

    def get_user_scenes(self):
        scenes = {}

        for usergame in self.usergame_set.filter(game__start_time__gt=datetime.now() - timedelta(days=30)) \
                                         .order_by("-game__end_time"):
            scene = usergame.game.cabinet.scene
            if scene.name not in scenes:
                scenes[scene.name] = {
                    "name": scene.name,
                    "display_name": scene.display_name,
                    "background_color": scene.background_color,
                    "last_played": usergame.game.end_time,
                }

        for permission in self.permissions.filter(permission=PermissionType.ADMIN):
            scene = permission.scene
            if scene.name in scenes:
                scenes[scene.name]["is_admin"] = True
            else:
                scenes[scene.name] = {
                    "name": scene.name,
                    "display_name": scene.display_name,
                    "background_color": scene.background_color,
                    "is_admin": True,
                }

        return sorted(scenes.values(), key=lambda s: s["display_name"])

    def get_linked_accounts(self):
        return [row.provider for row in self.social_auth.all()]

    def get_last_session(self):
        return self.playsession_set.order_by("-end_time").first()

    @transaction.atomic
    def collect_stats(self):
        all_stat_types = stat_types()

        for user_game in self.usergame_set.filter(play_session__isnull=True) \
                                          .select_for_update(skip_locked=True) \
                                          .order_by("game__start_time"):
            # New session if they haven't played in the past 4 hours
            session, _ = PlaySession.objects.filter(
                user=self,
                start_time__lte=user_game.game.end_time,
                end_time__gt=user_game.game.end_time - timedelta(hours=4),
            ).get_or_create(
                user=self,
                defaults={
                    "cabinet": user_game.game.cabinet,
                    "start_time": user_game.game.start_time,
                    "end_time": user_game.game.end_time,
                },
            )

            game = user_game.game
            player_id = user_game.player_id

            UserStat.add_game_to_aggregates(game, player_id, user_id=self.id)
            UserMonthlyStat.add_game_to_aggregates(game, player_id, user_id=self.id,
                                                   month=game.start_time.strftime("%Y-%m"))
            UserPositionStat.add_game_to_aggregates(game, player_id, user_id=self.id, player_id=player_id)
            PlaySessionStat.add_game_to_aggregates(game, player_id, session_id=session.id)
            PlaySessionPositionStat.add_game_to_aggregates(game, player_id, session_id=session.id,
                                                           player_id=player_id)

            user_game.play_session = session
            user_game.save()

            session.end_time = max(session.end_time, game.end_time)
            session.save()

    def get_stats(self):
        return UserStat.get_stats(user_id=self.id)

    def get_stats_by_position(self):
        stats_dict = {(i.player_id, i.stat_type): i for i in self.userpositionstat_set.all()}
        stats = []

        for stat_type in stat_types(is_hidden=False):
            stats.append({
                "name": stat_type.name,
                "label": stat_type.display_name,
                "formatting": stat_type.formatting,
                "values": {
                    i.value: stat.value if (stat := stats_dict.get((i.value, stat_type.name))) else 0
                    for i in CabinetPosition
                },
            })

        return stats

    def get_last_session_stats(self):
        session = self.get_last_session()

        if session is None:
            return {}

        return PlaySessionStat.get_stats(session_id=session.id)


class Permission(BaseModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="permissions")
    scene = models.ForeignKey(Scene, on_delete=models.CASCADE, related_name="permissions")
    permission = models.TextField(max_length=20, choices=PermissionType.choices)

    PermissionType = PermissionType

    def __repr__(self):
        return "<Permission: {} / {} / {}>".format(
            self.user.email,
            self.permission,
            self.scene.name,
        )


class PermissionInvite(BaseModel):
    scene = models.ForeignKey(Scene, on_delete=models.CASCADE, related_name="+")
    permission = models.TextField(max_length=20, choices=PermissionType.choices)
    created_timestamp = models.DateTimeField(default=timezone.now)
    created_by = models.ForeignKey(User, null=True, on_delete=models.SET_NULL, related_name="+")
    token = models.CharField(max_length=50, default=generate_token)
    accepted_timestamp = models.DateTimeField(null=True)
    accepted_by = models.ForeignKey(User, null=True, on_delete=models.SET_NULL, related_name="+")

    @transaction.atomic
    def accept(self, user):
        self.accepted_by = user
        self.accepted_timestamp = timezone.now()
        self.save()

        Permission.objects.create(
            user=user,
            scene=self.scene,
            permission=self.permission,
        )


class UserGame(BaseModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    game = models.ForeignKey(Game, on_delete=models.CASCADE)
    player_id = models.IntegerField(choices=CabinetPosition.choices)
    play_session = models.ForeignKey("user.PlaySession", null=True, on_delete=models.SET_NULL)

    class Meta:
        unique_together = ("game", "player_id")


class SceneRequest(BaseModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    submit_date = models.DateTimeField(default=timezone.now)
    user_name = models.CharField(max_length=50)
    name = models.CharField(max_length=10)
    display_name = models.CharField(max_length=30)
    comments = models.TextField(null=True, blank=True)
    completed = models.BooleanField(default=False)

    def create_scene(self):
        if self.completed:
            return

        scene = Scene(name=self.name, display_name=self.display_name)
        scene.save()

        Permission.objects.create(user=user, scene=scene, permission=Permission.PermissionType.ADMIN)
        Message.objects.create(user=self.user, url="/scene/{}".format(scene.name),
                               message_text="Your scene, {}, has been created.".format(self.display_name))

        self.completed = True
        self.save()


class Message(BaseModel):
    timestamp = models.DateTimeField(default=timezone.now)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    message_text = models.TextField()
    is_read = models.BooleanField(default=False)
    url = models.TextField(null=True)

    def save(self, **kwargs):
        result = super().save(**kwargs)
        app.send_task("hivemind.notifications.tasks.notify_message", args=[self.id])
        return result


class UserStat(AggregatedStatModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    @classmethod
    def get_games_in_aggregate(cls, **filters):
        return [(ug.game, ug.player_id) for ug in UserGame.objects.filter(user_id=filters.get("user_id"))]

    class Meta:
        unique_together = ("user", "stat_type")


class UserMonthlyStat(AggregatedStatModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    month = models.TextField()

    @classmethod
    def get_games_in_aggregate(cls, **filters):
        return [
            (ug.game, ug.player_id)
            for ug in UserGame.objects.filter(user_id=filters.get("user_id")) \
              .annotate(month=TruncMonth("game__start_time")) \
              .filter(month="{}-01".format(filters.get("month")))
        ]

    class Meta:
        unique_together = ("user", "month", "stat_type")


class UserPositionStat(AggregatedStatModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    player_id = models.IntegerField(choices=CabinetPosition.choices)

    @classmethod
    def get_games_in_aggregate(cls, **filters):
        return [(ug.game, ug.player_id) for ug in UserGame.objects.filter(
            user_id=filters.get("user_id"),
            player_id=filters.get("player_id"),
        )]

    class Meta:
        unique_together = ("user", "player_id", "stat_type")


class PlaySession(BaseModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    cabinet = models.ForeignKey(Cabinet, null=True, on_delete=models.CASCADE)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()

    class Meta:
        unique_together = ("user", "start_time")


class PlaySessionStat(AggregatedStatModel):
    session = models.ForeignKey(PlaySession, on_delete=models.CASCADE)

    @classmethod
    def get_games_in_aggregate(cls, **filters):
        session = PlaySession.objects.get(id=filters.get("session_id"))
        return [(ug.game, ug.player_id) for ug in UserGame.objects.filter(
            user=session.user,
            game__start_time__gte=session.start_time,
            game__start_time__lte=session.end_time,
        )]

    class Meta:
        unique_together = ("session", "stat_type")


class PlaySessionPositionStat(AggregatedStatModel):
    session = models.ForeignKey(PlaySession, on_delete=models.CASCADE)
    player_id = models.IntegerField(choices=CabinetPosition.choices)

    @classmethod
    def get_games_in_aggregate(cls, **filters):
        session = PlaySession.objects.get(id=filters.get("session_id"))
        if session is None:
            return []

        return [(ug.game, ug.player_id) for ug in UserGame.objects.filter(
            user=session.user,
            player_id=filters.get("player_id"),
            game__start_time__gte=session.start_time,
            game__start_time__lte=session.end_time,
        )]

    class Meta:
        unique_together = ("session", "player_id", "stat_type")
