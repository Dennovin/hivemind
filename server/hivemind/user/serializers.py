from drf_extra_fields.fields import Base64ImageField
from rest_framework import serializers

from hivemind.game.serializers import SceneSerializer

from ..game.models import Scene
from ..game.serializers import SceneSerializer
from ..tournament.models import TournamentTeam
from .models import (Message, Permission, PermissionInvite, PlaySession,
                     SceneRequest, User)


class PermissionSerializer(serializers.ModelSerializer):
    user = serializers.CharField(source="user.email")
    scene = serializers.PrimaryKeyRelatedField(queryset=Scene.objects.all())
    permission = serializers.CharField()

    def validate_user(self, email):
        try:
            return User.objects.get(email=email)
        except User.DoesNotExist:
            raise serializers.ValidationError("User not found")

    def create(self, validated_data):
        user = User.objects.get(email=validated_data["user"]["email"])
        return Permission.objects.create(
            user=user,
            scene=validated_data["scene"],
            permission=validated_data["permission"],
        )

    class Meta:
        model = Permission
        fields = ["id", "user", "scene", "permission"]


class PermissionInviteSerializer(serializers.ModelSerializer):
    scene = SceneSerializer(read_only=True)

    class Meta:
        model = PermissionInvite
        fields = ["id", "permission", "created_timestamp", "token", "scene"]


class UserPublicSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["id", "name", "scene", "image", "pronouns"]


class UserSerializer(serializers.ModelSerializer):
    stats = serializers.ListField(source="get_stats", read_only=True)
    stats_by_position = serializers.ListField(source="get_stats_by_position", read_only=True)
    last_session = PlaySession.serializer()(source="get_last_session", read_only=True)
    last_session_stats = serializers.ListField(source="get_last_session_stats", read_only=True)
    user_scenes = serializers.ListField(source="get_user_scenes", read_only=True)
    image = Base64ImageField(required=False)
    is_supporter = serializers.BooleanField(read_only=True)

    class Meta:
        model = User
        fields = ["id", "name", "stats", "stats_by_position", "last_session", "last_session_stats",
                  "user_scenes", "is_profile_public", "scene", "image", "pronouns", "is_supporter"]


class UserPrivateSerializer(serializers.ModelSerializer):
    permissions = PermissionSerializer(read_only=True, many=True)
    linked_accounts = serializers.ListField(source="get_linked_accounts", read_only=True)
    is_supporter = serializers.BooleanField(read_only=True)

    class Meta:
        model = User
        fields = ["id", "name", "is_profile_public", "permissions", "last_login", "is_site_admin",
                  "linked_accounts", "scene", "image", "pronouns", "is_supporter"]


class UserTournamentSerializer(serializers.Serializer):
    name = serializers.CharField(source="tournament.name", read_only=True)
    player_id = serializers.IntegerField(source="id", read_only=True)
    tournament_id = serializers.IntegerField(source="tournament.id", read_only=True)
    date = serializers.DateField(source="tournament.date", read_only=True)
    scene = SceneSerializer(source="tournament.scene", read_only=True)
    team = TournamentTeam.serializer()(read_only=True)


class ScenePermissionSerializer(serializers.ModelSerializer):
    user = UserPublicSerializer()

    class Meta:
        model = Permission
        fields = ["id", "user", "permission"]


class SocialSerializer(serializers.Serializer):
    access_token = serializers.CharField(allow_blank=False, trim_whitespace=True)


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = ["id", "timestamp", "message_text", "is_read", "url"]
        read_only_fields = ["id", "timestamp", "message_text", "url"]


class SceneRequestSerializer(serializers.ModelSerializer):
    user = UserSerializer(default=serializers.CurrentUserDefault())

    class Meta:
        model = SceneRequest
        fields = "__all__"
        read_only_fields = ["user"]
