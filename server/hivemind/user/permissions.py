from rest_framework.permissions import SAFE_METHODS, BasePermission

from ..constants import HTTPMethod


class UserPermission(BasePermission):
    def has_object_permission(self, request, view, obj):
        if obj and request.user and obj == request.user:
            return True

        return obj.is_profile_public and request.method in SAFE_METHODS


class PermissionPermission(BasePermission):
    def has_permission(self, request, view):
        return request.user.is_authenticated

    def has_object_permission(self, request, view, obj):
        if not request.user.is_authenticated:
            return False

        if request.method == HTTPMethod.DELETE and request.user == obj.user:
            return True

        return request.user.is_site_admin or request.user.is_admin_of(obj.scene)


class MessagePermission(BasePermission):
    def has_permission(self, request, view):
        return request.user.is_authenticated

    def has_object_permission(self, request, view, obj):
        if request.method in SAFE_METHODS or request.method == HTTPMethod.PUT:
            if request.user.is_authenticated and obj.user == request.user:
                return True


class NFCPermission(BasePermission):
    def has_permission(self, request, view):
        return request.user.is_authenticated

    def has_object_permission(self, request, view, obj):
        if request.user.is_authenticated and obj.user == request.user:
            return True


class SceneRequestPermission(BasePermission):
    def has_permission(self, request, view):
        if not request.user.is_authenticated:
            return False

        if request.method == HTTPMethod.GET:
            return request.user.is_site_admin
        if request.method == HTTPMethod.POST:
            return True

    def has_object_permission(self, request, view, obj):
        return request.user.is_authenticated and request.user.is_site_admin
