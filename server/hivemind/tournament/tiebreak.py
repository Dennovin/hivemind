import hashlib
import math
from datetime import timedelta
from functools import reduce

from django.db import models

DEFAULT_TIEBREAKERS = ["game_win_pct", "h2h_games", "match_win_pct", "game_time_in_losses", "dice_roll"]

def standings_by_tiebreakers(bracket):
    teams = bracket.teams()
    matches = list(bracket.tournamentmatch_set.all())

    stage_tiebreakers = bracket.stage_tiebreakers or DEFAULT_TIEBREAKERS
    first_tiebreak = get_tiebreaker_class(stage_tiebreakers[0])

    standings = [
        {
            "team": team,
            "tiebreak": (first_tiebreak.value(bracket, team, matches), ),
            "tiebreak_desc": [],
        }
        for team in teams
    ]

    # If any teams haven't played any matches yet, don't bother trying to sort
    if any([v["tiebreak"][0] is None for v in standings]):
        return standings

    for tiebreak_name in stage_tiebreakers[1:]:
        tiebreaker = get_tiebreaker_class(tiebreak_name)

        # Set of all distinct standings values
        tb_values = {i["tiebreak"] for i in standings}

        # All teams with this same value are currently tied
        for tb_value in tb_values:
            tied_standings = list(filter(lambda row: row["tiebreak"] == tb_value, standings))
            if len(tied_standings) <= 1:
                continue

            group_teams = [i["team"] for i in tied_standings]
            tiebreak_by_team = {}

            for row in tied_standings:
                tiebreak_by_team[row["team"]] = tiebreaker.value_within_group(
                    bracket, row["team"], matches, group_teams)

            if any([v[0] is None for v in tiebreak_by_team.values()]):
                continue

            for row in tied_standings:
                next_value, description = tiebreak_by_team[row["team"]]
                row["tiebreak"] += (next_value, )
                row["tiebreak_desc"].append(description)

    standings.sort(key=lambda i: i["tiebreak"], reverse=True)
    return standings

def tiebreaker_types(**filters):
    subclasses = Tiebreaker.all_tiebreaker_types

    for k, v in filters.items():
        subclasses = list(filter(lambda c: getattr(c, k) == v, subclasses))

    return subclasses

def get_tiebreaker_class(name):
    return {tb.name: tb for tb in tiebreaker_types()}[name]


class Tiebreaker:
    name = None
    title = None
    description = None
    is_final = False     # If true, this tiebreaker cannot result in a tie

    all_tiebreaker_types = []

    def __init_subclass__(cls, **kwargs):
        super().__init_subclass__(**kwargs)

        if cls.name is not None:
            cls.all_tiebreaker_types.append(cls)

    @classmethod
    def value(cls, bracket, team, matches):
        raise NotImplementedError

    @classmethod
    def get_description(cls, value):
        if value is None:
            return None

        return "{}: {:5.03f}".format(cls.title, value)

    @classmethod
    def value_within_group(cls, bracket, team, matches, group_teams):
        value = cls.value(bracket, team, matches)

        return value, cls.get_description(value)


class GameWinPct(Tiebreaker):
    name = "game_win_pct"
    title = "Game Win%"
    description = "Total percentage of games/maps won."

    @classmethod
    def value(cls, bracket, team, matches):
        game_wins = 0
        game_total = 0

        for match in matches:
            if match.blue_team == team:
                game_wins += match.blue_score
                game_total += match.blue_score + match.gold_score
            if match.gold_team == team:
                game_wins += match.gold_score
                game_total += match.blue_score + match.gold_score

        if game_total == 0:
            return None

        return game_wins / game_total


class MatchWinPct(Tiebreaker):
    name = "match_win_pct"
    title = "Match Win%"
    description = "Total percentage of matches/sets won."

    @classmethod
    def value(cls, bracket, team, matches):
        match_wins = 0
        match_total = 0

        for match in matches:
            if match.blue_score + match.gold_score == 0:
                continue
            if match.team_side(team) is None:
                continue

            team_wins = match.team_wins(team)
            team_losses = match.team_losses(team)

            match_total += 1

            if team_wins > team_losses:
                match_wins += 1
            if team_wins == team_losses:
                match_wins += 0.5

        if match_total == 0:
            return None

        return match_wins / match_total


class HeadToHeadGames(Tiebreaker):
    name = "h2h_games"
    title = "Head-to-Head Game Win%"
    description = "Total percentage of games/maps won in head-to-head games between tied teams."

    @classmethod
    def value_within_group(cls, bracket, team, matches, group_teams):
        group_matches = [i for i in matches
                         if i.blue_team in group_teams and i.gold_team in group_teams]

        value = GameWinPct.value(bracket, team, group_matches)
        return value, cls.get_description(value)


class HeadToHeadMatches(Tiebreaker):
    name = "h2h_matches"
    title = "Head-to-Head Match Win%"
    description = "Total percentage of matches/sets won in head-to-head games between tied teams."

    @classmethod
    def value_within_group(cls, bracket, team, matches, group_teams):
        group_matches = [i for i in matches
                         if i.blue_team in group_teams and i.gold_team in group_teams]

        value = MatchWinPct.value(bracket, team, group_matches)
        return value, cls.get_description(value)


class GameTimeInLosses(Tiebreaker):
    name = "game_time_in_losses"
    title = "Game Time in Losses"
    description = "Average game time in lost maps. Only works if ALL games have stats associated (no scores adjusted manually)."

    @classmethod
    def value(cls, bracket, team, matches):
        total_time = timedelta(0)
        losses = 0
        losses_counted = 0

        for match in matches:
            if match.team_side(team) is None:
                continue

            losses += match.team_losses(team)

            for game in match.game_set.filter(end_time__isnull=False,
                                              winning_team=match.team_side(team).opponent):
                total_time += game.end_time - game.start_time
                losses_counted += 1

        if losses == losses_counted and losses > 0:
            return total_time.total_seconds() / losses

    @classmethod
    def get_description(cls, value):
        if value is None:
            return None

        return "Average game time in losses: {}:{:4.2f}".format(math.floor(value / 60), value % 60)


class InitialSeeding(Tiebreaker):
    name = "initial_seeding"
    title = "Initial Seeding"
    description = "Teams that are initially seeded closer to 1st place will win the tiebreaker."
    is_final = True

    @classmethod
    def value(cls, bracket, team, matches):
        placed_teams = bracket.get_placed_teams()

        try:
            return len(placed_teams) - placed_teams.index(team)
        except ValueError:
            return None


    @classmethod
    def get_description(cls, value):
        return "Tie broken by initial seeding"


class DiceRoll(Tiebreaker):
    name = "dice_roll"
    title = "Dice Roll"
    description = "Tie is broken in a random order."
    is_final = True

    @classmethod
    def value_within_group(cls, bracket, team, matches, group_teams):
        group_matches = [i for i in matches
                         if i.blue_team in group_teams and i.gold_team in group_teams]

        bytelist = reduce(
            lambda a, i: a + (i.id % 256, i.blue_score, i.gold_score),
            group_matches,
            (team.id % 256, ),
        )

        return hashlib.md5(bytes(bytelist)).hexdigest(), "Tie broken randomly"

StageTiebreaker = models.TextChoices("StageTiebreaker", {
    s.name.upper(): (s.name, s.title) for s in tiebreaker_types()
})
