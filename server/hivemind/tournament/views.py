import base64
import http.client
import json
import math
from datetime import datetime, timedelta

import opengraph_parse
import requests
import requests.exceptions
import stripe
from django.conf import settings
from django.core.exceptions import BadRequest, ValidationError
from django.db import transaction
from django.db.models import Count, Max, Q
from django.shortcuts import get_object_or_404, render
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from ..constants import FieldType, HTTPMethod, PermissionType
from ..game.models import Cabinet, Game, timedelta_parse
from ..model import BaseViewSet
from ..viewsets import DeleteableMixin
from .filters import PlayerInfoFilter, TournamentFilter, TournamentMatchFilter
from .models import (HomeScene, MatchQueue, PaymentAccount, PaymentOption,
                     PlayerInfo, PlayerInfoField, PlayerStat,
                     SelectedPaymentOption, StagePlacement, Tournament,
                     TournamentBracket, TournamentLink, TournamentMatch,
                     TournamentPlayer, TournamentTeam, TournamentTemplate,
                     Video)
from .permissions import (MatchQueuePermission, PaymentAccountPermission,
                          PaymentOptionPermission, PlayerInfoFieldPermission,
                          StagePlacementPermission,
                          TournamentBracketPermission,
                          TournamentLinkPermission, TournamentMatchPermission,
                          TournamentPermission, TournamentPlayerPermission,
                          TournamentTeamPermission,
                          TournamentTemplatePermission, VideoCreatePermission,
                          VideoPermission)
from .serializers import (MatchQueueSerializer, PlayerInfoFieldSerializer,
                          RegistrationSerializer, TournamentBracketSerializer,
                          TournamentLinkSerializer, TournamentMatchSerializer,
                          TournamentPlayerSerializer, TournamentSerializer,
                          TournamentTeamSerializer, VideoSerializer)
from .tasks import (get_available_matches, publish_match, publish_matches,
                    publish_queue, select_next_match)
from .tiebreak import tiebreaker_types


class TournamentViewSet(BaseViewSet, DeleteableMixin):
    queryset = Tournament.objects.order_by("-date", "-id").all()
    serializer_class = TournamentSerializer
    filterset_class = TournamentFilter
    permission_classes = [TournamentPermission]

    @action(detail=True, methods=[HTTPMethod.GET], url_path="available-matches")
    def available_matches(self, request, pk=None):
        tournament = get_object_or_404(Tournament, id=pk)
        matches = tournament.get_available_matches()

        get_available_matches.delay(pk)

        return Response(TournamentMatchSerializer(matches, many=True).data)

    @action(detail=True, methods=[HTTPMethod.PUT], url_path="select-next")
    def select_next(self, request, pk=None):
        tournament = get_object_or_404(Tournament, id=pk)
        cabinet = get_object_or_404(Cabinet, id=request.data.get("cabinet"))

        if tournament.scene.id != cabinet.scene.id:
            raise BadRequest("Invalid cabinet")

        select_next_match.delay(tournament.id, cabinet.id)
        return Response({})

    @action(detail=True, methods=[HTTPMethod.GET], url_path="registration")
    def get_registration(self, request, pk=None):
        if not request.user.is_authenticated:
            return Response({})

        tournament = get_object_or_404(Tournament, id=pk)

        try:
            player = TournamentPlayer.objects.get(tournament=tournament, user=request.user)
        except TournamentPlayer.DoesNotExist:
            return Response({})

        return Response(RegistrationSerializer(player).data)

    @action(detail=True, methods=[HTTPMethod.PUT])
    def register(self, request, pk=None):
        tournament = get_object_or_404(Tournament, id=pk)
        if not tournament.allow_registration or (
                tournament.registration_close_date and
                tournament.registration_close_date < datetime.utcnow().date()
        ):
            return Response({"error": "This tournament is not accepting registrations."})

        data = request.data
        data["tournament"] = tournament.id
        data["user"] = request.user.id

        missing_fields = []
        for field in tournament.playerinfofield_set.filter(is_required=True):
            if not data.get(field.field_name):
                missing_fields.append(field)

        if missing_fields:
            raise ValidationError({
                f.field_name: "This field is required" for f in missing_fields
            })

        if request.data.get("team_type") == "free-agent":
            data["team"] = None
        if request.data.get("team_type") == "create-team":
            data["team"] = TournamentTeam.objects.create(tournament=tournament, name=request.data["team_name"]).id

        serializer = RegistrationSerializer(data=data, context={"request": request})
        serializer.is_valid(raise_exception=True)

        player = serializer.save()

        if request.user.image and not player.image:
            player.image = request.user.image
            player.save()

        for field in tournament.playerinfofield_set.all():
            player_info, _ = PlayerInfo.objects.get_or_create(player=player, field=field)
            if request.data.get(field.field_name) and field.field_type == FieldType.MULTI_CHOICE:
                player_info.value = json.dumps(request.data.get(field.field_name))
            else:
                player_info.value = request.data.get(field.field_name)

            player_info.save()

        return Response(TournamentPlayerSerializer(player).data)

    @action(detail=True, methods=[HTTPMethod.PUT], url_path="create-queue")
    def create_queue(self, request, pk=None):
        tournament = get_object_or_404(Tournament, id=pk)
        queue = MatchQueue.objects.create(tournament=tournament, name=request.data.get("name"), auto_created=False)
        publish_queue.delay(queue.id)

        return Response(MatchQueueSerializer(queue).data)

    @action(detail=True, methods=[HTTPMethod.PUT], url_path="create-queues")
    def create_queues(self, request, pk=None):
        tournament = get_object_or_404(Tournament, id=pk)
        queues = []

        try:
            queue = tournament.matchqueue_set.annotate(Count("cabinets")).get(auto_created=True, cabinets__count=0)
            queues.append(MatchQueueSerializer(queue).data)
        except MatchQueue.DoesNotExist:
            queue = MatchQueue.objects.create(
                tournament=tournament,
                name="First Available",
                auto_created=True,
            )

        for queue in MatchQueue.objects.filter(tournament=tournament, auto_created=False):
            queues.append(MatchQueueSerializer(queue).data)

        for cabinet in tournament.scene.cabinet_set.order_by("id"):
            try:
                queue = cabinet.matchqueue_set.get(tournament=tournament, auto_created=True)
            except MatchQueue.DoesNotExist:
                queue = MatchQueue.objects.create(
                    tournament=tournament,
                    name="{} Queue".format(cabinet.display_name),
                    auto_created=True,
                )
                queue.cabinets.set([cabinet])

            queues.append(MatchQueueSerializer(queue).data)

        return Response({"queues": queues})

    @action(detail=True, methods=[HTTPMethod.PUT], permission_classes=[TournamentPermission])
    def stripe(self, request, pk=None):
        tournament = get_object_or_404(Tournament, id=pk)

        if tournament.stripe_account_id is None:
            account = stripe.Account.create(type="standard")
            tournament.payment_account = PaymentAccount.objects.create(
                scene=tournament.scene,
                stripe_account_id=account["id"],
                created_by=request.user,
            )

            tournament.save()

        account_link = stripe.AccountLink.create(
            account=tournament.stripe_account_id,
            refresh_url=settings.PUBLIC_URL("/stripe/refresh"),
            return_url=settings.PUBLIC_URL("/stripe/return"),
            type="account_onboarding",
        )

        return Response({"url": account_link["url"]})

    @action(detail=True, methods=[HTTPMethod.PUT], url_path="update-stripe",
            permission_classes=[TournamentPermission])
    def update_stripe(self, request, pk=None):
        tournament = get_object_or_404(Tournament, id=pk)
        payment_account = get_object_or_404(PaymentAccount, id=request.data.get("payment_account_id"))

        if payment_account.scene != tournament.scene:
            raise BadRequest("Invalid payment account ID")

        tournament.payment_account = payment_account
        tournament.save()

        return Response({"success": True})

    @action(detail=True, methods=[HTTPMethod.GET], url_path="payment-account",
            permission_classes=[TournamentPermission])
    def payment_account(self, request, pk=None):
        tournament = get_object_or_404(Tournament, id=pk)
        if not (request.user.is_authenticated and request.user.is_admin_of(tournament.scene)):
            raise PermissionDenied()

        if tournament.payment_account is None:
            return Response({})

        return Response(PaymentAccount.serializer()(tournament.payment_account).data)

    @action(detail=True, methods=[HTTPMethod.PUT], url_path="question-order")
    def update_order(self, request, pk=None):
        tournament = get_object_or_404(Tournament, id=pk)
        try:
            questions = PlayerInfoField.objects.filter(tournament=tournament)
            with transaction.atomic():
                for index, id in enumerate(request.data.get("order")):
                    questions.filter(id=id).update(order=index+1)
                return Response({"success": True})
        except PlayerInfoField.DoesNotExist:
            return Response({"error": "This tournament has no questions."})

    @action(detail=False, methods=[HTTPMethod.GET], url_path="tiebreaker-types")
    def tiebreaker_types(self, request):
        tiebreakers = [
            {
                "name": tb.name,
                "title": tb.title,
                "description": tb.description,
            }
            for tb in tiebreaker_types()
        ]

        return Response({ "tiebreaker_types": tiebreakers })

    @action(detail=True, methods=[HTTPMethod.GET, HTTPMethod.PUT], url_path="draft-status")
    def draft_status(self, request, pk=None):
        tournament = get_object_or_404(Tournament, id=pk)

        if request.method == HTTPMethod.GET:
            if tournament.draft_status is None:
                num_players = TournamentPlayer.objects.filter(tournament=tournament, team__isnull=True).count()
                num_teams = math.ceil(num_players / 5)

                tournament.draft_status = {
                    "draft_order": "straight",
                    "teams": [{
                        "id": i,
                        "name": f"Team {i+1}",
                        "selections": [],
                        "order": i,
                    } for i in range(num_teams)],
                    "current_idx": 0,
                    "selected_round": 0,
                }
                tournament.save()

            return Response(tournament.draft_status)

        if request.method == HTTPMethod.PUT:
            tournament.draft_status = request.data
            tournament.save()

            return Response(tournament.draft_status)

    @action(detail=True, methods=[HTTPMethod.PUT], url_path="finalize-draft")
    def finalize_draft(self, request, pk=None):
        tournament = get_object_or_404(Tournament, id=pk)

        for row in request.data.get("teams"):
            team = TournamentTeam.objects.create(tournament=tournament, name=row["name"])
            TournamentPlayer.objects.filter(id__in=[i["id"] for i in row["selections"]]).update(team_id=team.id)

        return Response({ "success": True })



class TournamentTemplateViewSet(BaseViewSet):
    queryset = TournamentTemplate.objects.order_by("name").all()
    serializer_class = TournamentTemplate.serializer()
    filterset_fields = ["scene_id"]
    permission_classes = [TournamentTemplatePermission]


class PlayerInfoFieldViewSet(BaseViewSet):
    queryset = PlayerInfoField.objects.order_by("order", "id").all()
    serializer_class = PlayerInfoFieldSerializer
    filterset_fields = ["tournament_id"]
    permission_classes = [PlayerInfoFieldPermission]


class PaymentOptionViewSet(BaseViewSet):
    queryset = PaymentOption.objects.order_by("price").all()
    serializer_class = PaymentOption.serializer()
    filterset_fields = ["tournament_id"]
    permission_classes = [PaymentOptionPermission]


class TournamentBracketViewSet(BaseViewSet):
    queryset = TournamentBracket.objects.order_by("id").all()
    serializer_class = TournamentBracketSerializer
    filterset_fields = ["tournament_id"]
    permission_classes = [TournamentBracketPermission]

    @action(detail=True, methods=[HTTPMethod.GET])
    def teams(self, request, pk=None):
        bracket = get_object_or_404(TournamentBracket, id=pk)
        serializer = TournamentTeamSerializer(bracket.get_placed_teams(), many=True)
        return Response(serializer.data)

    @action(detail=True, methods=[HTTPMethod.GET])
    def standings(self, request, pk=None):
        bracket = get_object_or_404(TournamentBracket, id=pk)
        return Response({"standings": bracket.standings()})

    @action(detail=True, methods=[HTTPMethod.PUT], permission_classes=[TournamentBracketPermission])
    def validate(self, request, pk=None):
        bracket = get_object_or_404(TournamentBracket, id=pk)
        errors = []

        try:
            if not bracket.validate():
                errors.append("Token not found in tournament description.")
            if not bracket.check_permissions():
                errors.append("Tournament admin access not shared with HiveMind user.")
        except requests.exceptions.HTTPError as err:
            if err.response is not None and err.response.status_code == http.client.NOT_FOUND:
                errors.append("Tournament not found. Check the organization and tournament ID and try again.")
            else:
                errors.append("Error validating tournament: {}".format(err))

        if errors:
            return Response({"success": False, "errors": errors})

        return Response({"success": True})

    @action(detail=True, methods=[HTTPMethod.PUT], url_path="set-teams",
            permission_classes=[TournamentBracketPermission])
    def set_teams(self, request, pk=None):
        bracket = get_object_or_404(TournamentBracket, id=pk)
        errors = []

        if bracket.tournament.link_type == Tournament.LinkType.CHALLONGE:
            if not bracket.check_permissions():
                errors.append("Tournament admin access not shared with HiveMind user.")
            if not bracket.is_valid:
                errors.append("Bracket not yet validated.")

            if errors:
                return Response({"success": False, "errors": errors})

        teams = [
            get_object_or_404(TournamentTeam, id=i, tournament=bracket.tournament)
            for i in request.data.get("teams")
        ]

        bracket.set_teams(teams)

        if bracket.tournament.link_type == Tournament.LinkType.HIVEMIND:
            bracket.tournamentmatch_set.all().delete()
            bracket.generate_matches()

        return Response({"success": True})

    @action(detail=True, methods=[HTTPMethod.PUT], url_path="create-matches",
            permission_classes=[TournamentBracketPermission])
    def create_matches(self, request, pk=None):
        bracket = get_object_or_404(TournamentBracket, id=pk)
        bracket.tournamentmatch_set.all().delete()
        bracket.generate_matches()

        return Response({"success": True})


class TournamentMatchViewSet(BaseViewSet):
    queryset = TournamentMatch.objects.order_by("id").all()
    serializer_class = TournamentMatchSerializer
    filterset_class = TournamentMatchFilter
    permission_classes = [TournamentMatchPermission]

    def create(self, request, *args, **kwargs):
        if request.data.get("match_num") is None:
            request.data["match_num"] = (
                TournamentMatch.objects.filter(bracket_id=request.data.get("bracket")) \
                .aggregate(Max("match_num")) \
                .get("match_num__max") or 0
            ) + 1

        return super().create(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        # If setting to active, check if both teams exist
        instance = self.get_object()
        if instance.active_cabinet_id is None and request.data.get("active_cabinet") is not None:
            if instance.blue_team is None and request.data.get("blue_team") is None:
                return Response({ "success": False, "error": "Cannot start match because blue team is not assigned." })
            if instance.gold_team is None and request.data.get("gold_team") is None:
                return Response({ "success": False, "error": "Cannot start match because gold team is not assigned." })

        response = super().update(request, *args, **kwargs)

        # If updating a completed match, the set_complete function may have to run again
        match = self.get_object()
        if match.is_complete:
            match.set_complete()

        publish_match.delay(match.id)
        publish_matches.delay(match.bracket.tournament_id)

        return response

    @action(detail=True)
    def stats(self, request, pk=None):
        match = TournamentMatch.objects.get(id=pk)
        if not match.is_complete:
            return Response({"error": "Match is not complete."})

        return Response(match.get_match_stats())

    @action(detail=True, methods=[HTTPMethod.PUT])
    def start(self, request, pk=None):
        match = get_object_or_404(TournamentMatch, id=pk)
        match.report_start()
        return Response({"success": True})

    @action(detail=True, methods=[HTTPMethod.PUT])
    def stop(self, request, pk=None):
        match = get_object_or_404(TournamentMatch, id=pk)
        match.report_stop()
        return Response({"success": True})

    @action(detail=True, methods=[HTTPMethod.PUT])
    def report(self, request, pk=None):
        match = get_object_or_404(TournamentMatch, id=pk)
        match.report_result()
        return Response({"success": True})

    @action(detail=True, methods=[HTTPMethod.PUT])
    def complete(self, request, pk=None):
        match = get_object_or_404(TournamentMatch, id=pk)
        match.set_complete()
        return Response({"success": True})

class MatchQueueViewSet(BaseViewSet):
    queryset = MatchQueue.objects.order_by("id")
    serializer_class = MatchQueueSerializer
    filterset_fields = ["tournament_id"]
    permission_classes = [MatchQueuePermission]

    def create(self, request):
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def update(self, request, *args, **kwargs):
        response = super().update(request, *args, **kwargs)

        queue = self.get_object()
        publish_queue.delay(queue.id)

        return response

    @action(detail=True, methods=[HTTPMethod.PUT], url_path="set-queue", permission_classes=[MatchQueuePermission])
    def set_queue(self, request, pk=None):
        queue = get_object_or_404(MatchQueue, id=pk)
        matches = [TournamentMatch.objects.get(id=i) for i in request.data.get("matches", [])]

        if any([i.bracket.tournament_id != queue.tournament_id for i in matches]):
            raise BadRequest("Invalid match")

        queue.set_list(matches)
        publish_queue.delay(queue.id)
        queue.tournament.publish_match_state()

        return Response(MatchQueueSerializer(queue).data)

    @action(detail=True, methods=[HTTPMethod.PUT], url_path="set-cabinets", permission_classes=[MatchQueuePermission])
    def set_cabinets(self, request, pk=None):
        queue = get_object_or_404(MatchQueue, id=pk)
        if queue.auto_created:
            raise BadRequest("Cannot edit an automatically created queue.")

        cabinets = [Cabinet.objects.get(scene=queue.tournament.scene, id=i) for i in request.data.get("cabinets")]
        queue.cabinets.set(cabinets)

        return Response({ "success": True })

    @action(detail=True, methods=[HTTPMethod.PUT], permission_classes=[MatchQueuePermission])
    def sort(self, request, pk=None):
        queue = get_object_or_404(MatchQueue, id=pk)
        to_assign = [i for i in queue.get_list()]
        new_queue = []
        last_match = {}

        matches = TournamentMatch.objects.filter(
            Q(blue_score__gt=0) | Q(gold_score__gt=0) | Q(active_cabinet_id__isnull=False),
            bracket__tournament_id=queue.tournament_id,
        )

        for i, match in enumerate(sorted(matches, key=lambda i: i.end_time())):
            last_match[match.blue_team_id] = last_match[match.gold_team_id] = i

        match_sort_key = lambda i: last_match.get(i.blue_team_id, 0) + last_match.get(i.gold_team_id, 0)

        idx = matches.count()
        while len(to_assign) > 0:
            next_match = sorted(to_assign, key=match_sort_key)[0]
            new_queue.append(next_match)
            to_assign.remove(next_match)
            last_match[next_match.blue_team_id] = last_match[next_match.gold_team_id] = idx
            idx += 1

        queue.set_list(new_queue)
        publish_queue.delay(queue.id)

        return Response({"success": True})


class TournamentTeamViewSet(BaseViewSet):
    queryset = TournamentTeam.objects.order_by("name").all()
    serializer_class = TournamentTeamSerializer
    filterset_fields = ["tournament_id"]
    permission_classes = [TournamentTeamPermission]


class StagePlacementViewSet(BaseViewSet):
    queryset = StagePlacement.objects.order_by("id").all()
    serializer_class = StagePlacement.serializer()
    filterset_fields = ["dest_bracket_id"]
    permission_classes = [StagePlacementPermission]


class HomeSceneViewSet(BaseViewSet):
    queryset = HomeScene.objects.order_by("name").all()
    serializer_class = HomeScene.serializer()


class TournamentPlayerViewSet(BaseViewSet):
    queryset = TournamentPlayer.objects.order_by("name").all()
    serializer_class = TournamentPlayerSerializer
    filterset_fields = ["tournament_id", "team_id"]
    permission_classes = [TournamentPlayerPermission]

    @action(detail=True, methods=[HTTPMethod.PUT], url_path="begin-checkout")
    def begin_checkout(self, request, pk=None):
        player = get_object_or_404(TournamentPlayer, id=pk)
        tournament = player.tournament
        payment_options = request.data.get("payment_options")

        if not tournament.accept_payments:
            return Response({ "error": "This tournament is not accepting payments." })
        if not tournament.stripe_account_id:
            return Response({ "error": "Stripe has not yet been configured for this tournament." })
        if not payment_options:
            return Response({ "error": "No payment options selected." })
        if player.paid:
            return Response({ "error": "Registration is already paid." })


        selected_payment = [SelectedPaymentOption.objects.create(
            payment_option=PaymentOption.objects.get(id=po.get("payment_option")),
            quantity=po.get("quantity"),
            custom_price=po.get("custom_price"),
        ) for po in payment_options]

        player.selected_payment_options.set(selected_payment)
        player.save()

        price = int(100 * sum([(i.payment_option.price or i.custom_price) * i.quantity for i in player.selected_payment_options.all()]))

        line_items = [{
            "quantity": po.quantity,
            "price_data": {
                "unit_amount": int(100 * (po.payment_option.price or po.custom_price)),
                "currency": "usd",
                "product_data": {
                    "name": "{} - {}".format(tournament.name, po.payment_option.name),
                },
            },
        } for po in player.selected_payment_options.all()]

        payment_intent_data = None
        if datetime.now() >= datetime(2025, 1, 1):
            payment_intent_data = {
                "application_fee_amount": math.floor(0.01 * price),
            }

        session = stripe.checkout.Session.create(
            line_items=line_items,
            stripe_account=tournament.stripe_account_id,
            mode="payment",
            success_url=settings.PUBLIC_URL(
                "/tournament/{}/paid".format(tournament.id),
            ),
            cancel_url=settings.PUBLIC_URL(
                "/tournament/{}/register".format(tournament.id),
            ),
            payment_intent_data=payment_intent_data,
        )

        player.price = price
        player.stripe_session_id = session.id
        player.save()
        return Response({"url": session.url})

    @action(detail=True, methods=[HTTPMethod.PUT], url_path="check-payment")
    def check_payment(self, request, pk=None):
        player = get_object_or_404(TournamentPlayer, id=pk)

        if not player.stripe_session_id:
            return Response({ "error": "No session found." })

        session = stripe.checkout.Session.retrieve(
            player.stripe_session_id,
            expand=["line_items"],
            stripe_account=player.tournament.stripe_account_id,
        )


        if not session:
            return Response({ "error": "No session found." })

        if session["status"] == "complete":
            player.paid = True
            player.registration_time = datetime.fromtimestamp(session['created'])
            player.save()

        return Response({})


class VideoViewSet(BaseViewSet):
    queryset = Video.objects.order_by("id").all()
    serializer_class = VideoSerializer
    permission_classes = [VideoPermission]
    filterset_fields = ["tournament_id"]

    @action(detail=False, methods=[HTTPMethod.POST], permission_classes=[VideoCreatePermission])
    def add(self, request):
        game = get_object_or_404(Game, id=request.data.get("game"))
        tournament = get_object_or_404(Tournament, id=request.data.get("tournament"))

        if request.data.get("reference_point") == "start":
            start_time = game.start_time - timedelta_parse(request.data.get("game_end_time"))
        elif request.data.get("reference_point") == "end":
            start_time = game.end_time - timedelta_parse(request.data.get("game_end_time"))
        else:
            start_time = game.end_time - timedelta_parse(request.data.get("game_end_time")) + timedelta(seconds=10)

        video = Video.objects.create(
            tournament=tournament,
            cabinet=game.cabinet,
            service=Video.VideoSite.YOUTUBE,
            video_id=request.data.get("video_id"),
            start_time=start_time,
            length=timedelta_parse(request.data.get("length")),
        )

        video.tag_matches()
        return Response(Video.serializer()(video).data)

    @action(detail=True, methods=[HTTPMethod.GET])
    def timestamps(self, request, pk=None):
        video = get_object_or_404(Video, id=pk)
        return Response({ "timestamps": video.match_timestamps() })


class PaymentAccountViewSet(BaseViewSet):
    serializer_class = PaymentAccount.serializer()
    permission_classes = [PaymentAccountPermission]
    filterset_fields = ["scene"]

    def get_queryset(self):
        user = self.request.user
        admin_scene_ids = [p.scene_id for p in user.permissions.filter(permission=PermissionType.ADMIN)]
        return PaymentAccount.objects.filter(scene_id__in=admin_scene_ids)


class TournamentLinkViewSet(BaseViewSet):
    queryset = TournamentLink.objects.order_by("order", "id")
    serializer_class = TournamentLinkSerializer
    permission_classes = [TournamentLinkPermission]
    filterset_fields = ["tournament"]

    @action(detail=False, methods=[HTTPMethod.POST], permission_classes=[IsAuthenticated])
    def opengraph(self, request, pk=None):
        url = request.data.get("url")
        data = opengraph_parse.parse_page(url)

        if data.get("og:image"):
            response = requests.get(data["og:image"])
            data["og:image"] = "data:{};base64,{}".format(
                response.headers.get("Content-Type"),
                base64.b64encode(response.content).decode("utf-8"),
            )

        return Response(data)
