from django.urls import include, path
from rest_framework import routers, serializers, viewsets

from . import views

router = routers.DefaultRouter()
router.register(r"tournament", views.TournamentViewSet)
router.register(r"payment-option", views.PaymentOptionViewSet)
router.register(r"template", views.TournamentTemplateViewSet)
router.register(r"bracket", views.TournamentBracketViewSet)
router.register(r"match", views.TournamentMatchViewSet)
router.register(r"team", views.TournamentTeamViewSet)
router.register(r"scene", views.HomeSceneViewSet)
router.register(r"player", views.TournamentPlayerViewSet)
router.register(r"video", views.VideoViewSet)
router.register(r"player-info-field", views.PlayerInfoFieldViewSet)
router.register(r"queue", views.MatchQueueViewSet)
router.register(r"stage-placement", views.StagePlacementViewSet)
router.register(r"payment-account", views.PaymentAccountViewSet, basename="payment-account")
router.register(r"links", views.TournamentLinkViewSet)

urlpatterns = [
    path("", include(router.urls)),
]
