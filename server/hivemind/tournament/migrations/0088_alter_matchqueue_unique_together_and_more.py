# Generated by Django 5.0.6 on 2024-10-29 14:39

from django.db import migrations, models


def create_default_cab_groups(apps, schema_editor):
    MatchQueue = apps.get_model("tournament", "MatchQueue")

    for queue in MatchQueue.objects.filter(cabinet__isnull=False):
        queue.auto_created = True
        queue.cabinets.set([queue.cabinet])
        queue.save()


def revert_default_cab_groups(apps, schema_editor):
    CabinetGroup = apps.get_model("tournament", "CabinetGroup")
    MatchQueue = apps.get_model("tournament", "MatchQueue")

    for queue in MatchQueue.objects.filter(cabinet_group__auto_created=True):
        if queue.cabinet_group.cabinets.count() == 1:
            queue.cabinet = queue.cabinet_group.cabinets.first()
            queue.save()


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0036_alter_heatmap_event_type'),
        ('tournament', '0087_tournament_slug'),
    ]

    operations = [
        migrations.AddField(
            model_name='matchqueue',
            name='auto_created',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='matchqueue',
            name='cabinets',
            field=models.ManyToManyField(blank=True, to='game.cabinet'),
        ),
        migrations.AddField(
            model_name='matchqueue',
            name='name',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AlterUniqueTogether(
            name='matchqueue',
            unique_together={('tournament', 'name')},
        ),
        migrations.RunPython(create_default_cab_groups, revert_default_cab_groups),
        migrations.RemoveField(
            model_name='matchqueue',
            name='cabinet',
        ),
    ]
