# Generated by Django 3.2.7 on 2021-10-26 19:33

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tournament', '0006_auto_20211026_1931'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='tournamentmatch',
            name='blue_team',
        ),
        migrations.RemoveField(
            model_name='tournamentmatch',
            name='gold_team',
        ),
        migrations.RenameField(
            model_name='tournamentmatch',
            old_name='blue_team_fk',
            new_name='blue_team',
        ),
        migrations.RenameField(
            model_name='tournamentmatch',
            old_name='gold_team_fk',
            new_name='gold_team',
        ),
    ]
