# Generated by Django 3.2.7 on 2023-11-08 02:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tournament', '0070_alter_playerstat_stat_type'),
    ]

    operations = [
        migrations.AlterField(
            model_name='playerinfofield',
            name='field_slug',
            field=models.CharField(blank=True, max_length=30, null=True),
        ),
        migrations.AlterUniqueTogether(
            name='playerinfofield',
            unique_together={('tournament', 'field_slug'), ('tournament', 'field_description')},
        ),
    ]
