# Generated by Django 5.0.6 on 2024-12-17 00:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tournament', '0088_alter_matchqueue_unique_together_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='tournamentbracket',
            name='third_place_match',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='tournamentmatch',
            name='match_type',
            field=models.CharField(choices=[('winners', 'Winners Bracket'), ('losers', 'Losers Bracket'), ('first_final', 'Finals (First Match)'), ('second_final', 'Finals (Second Match)'), ('third_place', 'Third Place Match')], max_length=20, null=True),
        ),
    ]
