# Generated by Django 3.2.7 on 2023-02-04 22:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tournament', '0041_tournament_accept_payments'),
    ]

    operations = [
        migrations.AddField(
            model_name='tournament',
            name='registration_close_date',
            field=models.DateField(null=True),
        ),
    ]
