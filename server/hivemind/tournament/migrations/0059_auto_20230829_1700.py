# Generated by Django 3.2.7 on 2023-08-29 17:00

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('tournament', '0058_tournamentbracket_auto_warmup'),
    ]

    operations = [
        migrations.AddField(
            model_name='tournamentbracket',
            name='stage_type',
            field=models.CharField(choices=[('round_robin', 'Round Robin (Group Stage)'), ('single_elim', 'Single Elimination'), ('double_elim', 'Double Elimination'), ('ladder', 'Ladder')], max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='tournamentmatch',
            name='match_num',
            field=models.IntegerField(null=True),
        ),
        migrations.AddField(
            model_name='tournamentmatch',
            name='match_type',
            field=models.CharField(choices=[('winners', 'Winners Bracket'), ('losers', 'Losers Bracket'), ('first_final', 'Finals (First Match)'), ('second_final', 'Finals (Second Match)')], max_length=20, null=True),
        ),
        migrations.AlterField(
            model_name='tournament',
            name='link_type',
            field=models.TextField(choices=[('manual', 'Manual'), ('hivemind', 'HiveMind Brackets'), ('challonge', 'Challonge')], default='challonge', max_length=20),
        ),
        migrations.AlterUniqueTogether(
            name='tournamentmatch',
            unique_together={('bracket', 'match_num')},
        ),
        migrations.CreateModel(
            name='StagePlacement',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('place', models.IntegerField()),
                ('bracket', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tournament.tournamentbracket')),
                ('team', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tournament.tournamentteam')),
            ],
            options={
                'unique_together': {('bracket', 'team')},
            },
        ),
    ]
