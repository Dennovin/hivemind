# Generated by Django 3.2.7 on 2024-02-03 18:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tournament', '0073_remove_tournamentmatch_stage_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='tournamentbracket',
            name='queue_timer',
            field=models.IntegerField(default=30),
        ),
    ]
