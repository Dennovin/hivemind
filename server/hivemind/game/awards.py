from datetime import datetime, timedelta

from ..constants import CabinetPosition, CabinetTeam
from ..stats.stattype import StatType, player_has_speed, player_is_warrior


class Award(object):
    title = None
    description = None
    weight = 0
    is_team_award = False

    @classmethod
    def all(cls):
        return sorted(cls.__subclasses__(), key=lambda i: i.__name__)

    @classmethod
    def choices(cls):
        return [(i.__name__, i.title) for i in cls.all()]

    @classmethod
    def test(cls, game, stats, player_id):
        return False

    @classmethod
    def get_title_for_game(cls, game, stats):
        return cls.title

    @classmethod
    def pick_some(cls, game, stats, number):
        """
        Selects a number of awards for a game, semi-randomly based on weight.
        For each one selected, returns the award and all the position IDs that earned it.
        Returns the same set for any particular game, as long as awards and
        their weights have not changed.
        """

        earned = cls.check_all(game, stats)
        selected = []
        count = min(number, len(earned))

        for i in range(count):
            total_weight = sum([2**s.award.weight for s in earned])
            modval = game.id % total_weight

            k = 0
            for idx, s in enumerate(earned):
                k += 2 ** s.award.weight
                if k > modval:
                    break

            selected.append(earned.pop(idx))

        return selected

    @classmethod
    def check_all(cls, game, stats):
        """
        Finds all awards earned in this game and all players who earned each one.
        """

        awards_earned = []

        for subcls in cls.all():
            if subcls.is_team_award:
                teams = set()
                for team in CabinetTeam:
                    if subcls.test(game, stats, CabinetTeam(team)):
                        teams.add(team.value)

                if len(teams) > 0:
                    awards_earned.append(AwardEarned(subcls, teams=teams))

            else:
                players = set()
                for position in CabinetPosition:
                    if subcls.test(game, stats, position):
                        players.add(position)

                if len(players) > 0:
                    awards_earned.append(AwardEarned(subcls, players=players))

        return awards_earned


class AwardEarned(object):
    def __init__(self, award, players=None, teams=None):
        self.award = award
        self.players = players
        self.teams = teams

    def __repr__(self):
        if self.players:
            return "<{}: [{}]>".format(
                self.award.__name__,
                ", ".join([str(i.value) for i in self.players]),
            )

        return "<{}>".format(self.award.__name__)


class StarHatTrick(Award):
    title = "★ Star Hat Trick"
    description = "Kill the enemy queen three times without dying."
    weight = 6

    @classmethod
    def test(cls, game, stats, position):
        return stats["by_player"][StatType.QUEEN_KILLS].get(position, 0) >= 3 and \
            stats["by_player"][StatType.DEATHS].get(position, 0) == 0 and \
            position in (CabinetPosition.BLUE_QUEEN, CabinetPosition.GOLD_QUEEN)


class HatTrick(Award):
    title = "Hat Trick"
    description = "Kill the enemy queen three times."
    weight = 5

    @classmethod
    def test(cls, game, stats, position):
        return stats["by_player"][StatType.QUEEN_KILLS].get(position, 0) >= 3 and \
            not StarHatTrick.test(game, stats, position)


class MajorityBerryholder(Award):
    title = "Majority Berryholder"
    description = "Deposit seven or more berries."
    weight = 3

    @classmethod
    def test(cls, game, stats, position):
        return stats["by_player"][StatType.BERRIES].get(position, 0) >= 7 \
            and stats["by_player"][StatType.BERRIES].get(position, 0) < 12


class OopsAllBerries(Award):
    title = "Oops! All Berries!"
    description = "Deposit twelve berries."
    weight = 7

    @classmethod
    def test(cls, game, stats, position):
        return stats["by_player"][StatType.BERRIES].get(position, 0) >= 12


class Conductor(Award):
    title = "Conductor"
    description = "Ride the snail for 100 meters or more."
    weight = 3

    @classmethod
    def test(cls, game, stats, position):
        return stats["by_player"][StatType.SNAIL_METERS].get(position, 0) >= 100


class LeaveMeAlone(Award):
    title = "Leave Me Alone!"
    description = "As Queen, have the most deaths."
    weight = 2

    @classmethod
    def test(cls, game, stats, position):
        return (position in (CabinetPosition.BLUE_QUEEN, CabinetPosition.GOLD_QUEEN) and
                position in stats.get("most_deaths", {}).get("players", []))


class EscapeArtist(Award):
    title = "Escape Artist"
    description = "Escape from the snail five or more times."
    weight = 4

    @classmethod
    def test(cls, game, stats, position):
        return game.gameevent_set.filter(event_type="snailEscape", values__2=position).count() >= 5


class MostlyHarmless(Award):
    title = "Space Holder"
    description = "With at least two minutes of warrior uptime, record less than one kill per minute."
    weight = 4

    @classmethod
    def test(cls, game, stats, position):
        uptime = stats["by_player"][StatType.WARRIOR_UPTIME].get(position, 0) / 1000
        return (uptime >= 120 and (60 * stats["by_player"][StatType.KILLS].get(position, 0) / uptime) < 1)


class HatThief(Award):
    title = "Hat Thief"
    description = "Get the third kill on the enemy Queen after a teammate records the first two."
    weight = 5

    @classmethod
    def test(cls, game, stats, position):
        if stats["by_player"][StatType.QUEEN_KILLS].get(position, 0) != 1:
            return False

        if max([stats["by_player"][StatType.QUEEN_KILLS].get(i, 0) for i in position.team.positions]) != 2:
            return False

        kill = game.gameevent_set.filter(event_type="playerKill", values__4="Queen").order_by("-timestamp").first()
        return int(kill.values[2]) == position


class Striker(Award):
    title = "Striker"
    description = "Kick in two or more berries."
    weight = 5

    @classmethod
    def test(cls, game, stats, position):
        return stats["by_player"][StatType.BERRIES_KICKED].get(position, 0) >= 2


class Immortal(Award):
    title = "Immortal"
    description = "As a warrior, survive for more than three minutes."
    weight = 5

    @classmethod
    def test(cls, game, stats, position):
        for up_event in game.gameevent_set.filter(
                event_type="useMaiden",
                values__2="maiden_wings",
                values__3=position,
                timestamp__gt=game.start_time,
        ).order_by("timestamp"):

            death_event = game.gameevent_set.filter(
                event_type="playerKill",
                values__3=position,
                timestamp__gt=up_event.timestamp,
            ).order_by("timestamp").first()

            if death_event is None:
                down_time = game.end_time
            else:
                down_time = death_event.timestamp

            if down_time > up_event.timestamp + timedelta(minutes=3):
                return True


class UtilityPlayer(Award):
    title = "Utility Player"
    description = "Record 25 snail meters, 3 berries, and 3 military kills."
    weight = 5

    @classmethod
    def test(cls, game, stats, position):
        total_berries = stats["by_player"][StatType.BERRIES].get(position, 0) + \
            stats["by_player"][StatType.BERRIES_KICKED].get(position, 0)

        return stats["by_player"][StatType.SNAIL_METERS].get(position, 0) >= 25 \
            and stats["by_player"][StatType.MILITARY_KILLS].get(position, 0) >= 3 \
            and total_berries >= 3


class Switzerland(Award):
    title = "Switzerland"
    description = "End a game with zero kills and zero deaths."
    weight = 7

    @classmethod
    def test(cls, game, stats, position):
        return str(position) in stats["human_players"] and \
            stats["by_player"][StatType.KILLS].get(position, 0) == 0 and \
            stats["by_player"][StatType.DEATHS].get(position, 0) == 0


class NonstopSnail(Award):
    title = "Nonstop Snail"
    description = "Win a snail victory without ever getting off the snail."
    weight = 7

    @classmethod
    def test(cls, game, stats, position):
        if game.win_condition != "snail":
            return False

        if game.winning_team != CabinetPosition(position).team.value:
            return False

        if game.gameevent_set.filter(event_type="getOnSnail", values__2=str(position)).count() != 1:
            return False

        positions = [str(i) for i in CabinetPosition(position).team.positions]
        if game.gameevent_set.filter(event_type="getOnSnail", values__2__in=positions).count() != 1:
            return False

        if game.gameevent_set.filter(event_type="getOffSnail", values__2=str(position)).count() > 0:
            return False

        return True


class SecondQueen(Award):
    title = "Illegal Second Queen"
    description = "As a warrior, record at least ten kills with fewer than three deaths."
    weight = 6

    @classmethod
    def test(cls, game, stats, position):
        if position in (CabinetPosition.BLUE_QUEEN, CabinetPosition.GOLD_QUEEN):
            return False

        if stats["by_player"][StatType.DEATHS].get(position, 0) >= 3:
            return False

        return stats["by_player"][StatType.KILLS].get(position, 0) >= 10


class Delicious(Award):
    title = "Delicious"
    description = "Get eaten by the snail five times."
    weight = 5

    @classmethod
    def test(cls, game, stats, position):
        return stats["by_player"][StatType.EATEN_BY_SNAIL].get(position, 0) >= 5


class OnlyKill(Award):
    title = "I Only Need One"
    description = "Win the game with your only kill."
    weight = 7

    @classmethod
    def test(cls, game, stats, position):
        if game.winning_team != CabinetPosition(position).team.value:
            return False

        if game.win_condition != "military":
            return False

        if stats["by_player"][StatType.KILLS].get(position, 0) != 1:
            return False

        kill = game.gameevent_set.filter(event_type="playerKill", values__4="Queen").order_by("-timestamp").first()
        return int(kill.values[2]) == position


class Exodia(Award):
    title = "EXODIA"
    description = "Win the game with three speed warriors and a speed drone."
    weight = 7
    is_team_award = True

    @classmethod
    def test(cls, game, stats, team):
        if team != game.winning_team:
            return False

        speed = 0
        warriors = 0
        end_ts = game.end_time - timedelta(milliseconds=1)
        for position in team.positions:
            if player_is_warrior(game, position, end_ts):
                warriors += 1
            if player_has_speed(game, position, end_ts):
                speed += 1

        return speed == 4 and warriors == 3


class PhotoFinish(Award):
    title = "Photo Finish"
    description = "Deposit your team's final berry within 1 second of the other team's final berry."
    weight = 7
    is_team_award = True

    @classmethod
    def get_time_diff(cls, game):
        last_deposit = [
            game.gameevent_set \
                .filter(event_type="berryDeposit", values__2__in=CabinetTeam(t).positions) \
                .order_by("-timestamp") \
                .first()
            for t in CabinetTeam
        ]
        max_ts = max([i.timestamp for i in last_deposit])
        min_ts = min([i.timestamp for i in last_deposit])

        last_kick_in = game.gameevent_set.filter(event_type="berryKickIn").order_by("-timestamp").first()
        if last_kick_in and last_kick_in.timestamp > max_ts:
            return None

        return max_ts - min_ts

    @classmethod
    def test(cls, game, stats, team):
        if team != game.winning_team:
            return False

        if not (stats["berries"][CabinetTeam.BLUE] == stats["berries"][CabinetTeam.GOLD] == 12):
            return False

        time_diff = cls.get_time_diff(game)
        return (time_diff and time_diff < timedelta(seconds=1))

    @classmethod
    def get_title_for_game(cls, game, stats):
        return "{} ({:.02f} seconds)".format(cls.title, cls.get_time_diff(game).total_seconds())


class TrickShot(Award):
    title = "Trick Shot"
    description = "Kick in two berries within one second."
    weight = 6

    @classmethod
    def test(cls, game, stats, position):
        for event in game.gameevent_set.filter(event_type="berryKickIn", values__2=position, values__3="True"):
            others = game.gameevent_set.filter(
                event_type="berryKickIn", values__2=position, values__3="True",
                timestamp__gte=event.timestamp - timedelta(seconds=1),
                timestamp__lt=event.timestamp,
            ).exclude(id=event.id)

            if others.count() > 0:
                return True
