from django_filters import rest_framework as filters

from .models import CabinetActivity, Game


class GameFilter(filters.FilterSet):
    scene_id = filters.NumberFilter(field_name="cabinet__scene_id")
    cabinet_id = filters.NumberFilter(field_name="cabinet__id")
    user_id = filters.NumberFilter(field_name="usergame__user_id")
    tournament_player_id = filters.NumberFilter(field_name="tournamentplayergame__tournament_player_id")

    class Meta:
        model = Game
        fields = [
            "cabinet_id",
            "scene_id",
            "match_id",
            "qp_match_id",
            "tournament_match_id",
            "tournament_player_id",
            "user_id",
        ]


class CabinetActivityFilter(filters.FilterSet):
    class Meta:
        model = CabinetActivity
        fields = ["cabinet_id"]
