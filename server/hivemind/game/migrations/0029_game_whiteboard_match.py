# Generated by Django 3.2.7 on 2023-11-23 02:48

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('whiteboard', '0002_playersignup_signup_time'),
        ('game', '0028_auto_20231121_1654'),
    ]

    operations = [
        migrations.AddField(
            model_name='game',
            name='whiteboard_match',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='whiteboard.match'),
        ),
    ]
