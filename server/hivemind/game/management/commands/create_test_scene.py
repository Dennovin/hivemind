from django.core.management.base import BaseCommand
from django.db import transaction
from hivemind.constants import PermissionType
from hivemind.game.models import Cabinet, Scene
from hivemind.overlay.models import Overlay
from hivemind.user.models import Message, Permission, User


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument("--admin", help="Email address of a user to set as the admin for the test scene.")

    def handle(self, *args, **kwargs):
        scene, _ = Scene.objects.get_or_create(
            name="test",
            display_name="HiveMind Test Lab",
            background_color="#fa86c4",
        )

        for i in range(4):
            cabinet, _ = Cabinet.objects.get_or_create(
                name="test{}".format(i+1),
                scene=scene,
                defaults={
                    "display_name": "Test Cabinet {}".format(i+1),
                    "token": "12345",
                    "time_zone": "UTC",
                },
            )

            Overlay.objects.get_or_create(
                cabinet=cabinet,
                defaults={"is_default": True},
            )

        if kwargs.get("admin"):
            user = User.objects.get(email=kwargs.get("admin"))
            Permission.objects.get_or_create(user=user, scene=scene, permission=PermissionType.ADMIN)
            Message.objects.create(user=user, message_text="Test scene created.", url="/scene/test")
