import http.client

from ..tests import HiveMindTest
from ..user.models import User
from .models import Scene


class SceneTestCase(HiveMindTest):
    def setUp(self):
        super().setUp()

        self._scene2 = Scene(name="test2", display_name="Test Scene 2")
        self._scene2.save()

        self._basic_user = User(username="test ID 2", email="test2@test.com", is_site_admin=False)
        self._basic_user.save()

        self._site_admin_user = User(username="test ID 3", email="test3@test.com", is_site_admin=True)
        self._site_admin_user.save()

        self.client.force_authenticate(None)

    def test_scene_list(self):
        response = self.client.get("/api/game/scene/")
        self.assertEqual(response.status_code, http.client.OK)

    def test_scene_read(self):
        response = self.client.get("/api/game/scene/{}/".format(self._scene.id))
        self.assertEqual(response.status_code, http.client.OK)

        response = self.client.get("/api/game/scene/{}/".format(self._scene2.id))
        self.assertEqual(response.status_code, http.client.OK)

    def test_scene_create(self):
        self.client.force_authenticate(self._site_admin_user)

        data = {"name": "test3", "display_name": "New Scene Name"}
        response = self.client.post("/api/game/scene/", data)
        self.assertEqual(response.status_code, http.client.CREATED)

        scene_id = response.data["id"]
        self.client.force_authenticate(None)

        response = self.client.get("/api/game/scene/{}/".format(scene_id))
        self.assertEqual(response.status_code, http.client.OK)
        self.assertEqual(response.data["display_name"], data["display_name"])

    def test_scene_create_no_admin(self):
        self.client.force_authenticate(self._user)

        data = {"name": "test3", "display_name": "New Scene Name"}
        response = self.client.post("/api/game/scene/", json=data)
        self.assertEqual(response.status_code, http.client.FORBIDDEN)

    def test_scene_update(self):
        self.client.force_authenticate(self._user)

        response = self.client.get("/api/game/scene/{}/".format(self._scene.id))

        data = response.data
        data["display_name"] = "New Scene Name"

        response = self.client.put("/api/game/scene/{}/".format(self._scene.id), data)
        self.assertEqual(response.status_code, http.client.OK)

        response = self.client.get("/api/game/scene/{}/".format(self._scene.id))
        self.assertEqual(response.data["display_name"], data["display_name"])

    def test_scene_update_no_admin(self):
        self.client.force_authenticate(self._basic_user)

        response = self.client.get("/api/game/scene/{}/".format(self._scene.id))

        data = response.data
        data["display_name"] = "New Scene Name"

        response = self.client.put("/api/game/scene/{}/".format(self._scene2.id), data)
        self.assertEqual(response.status_code, http.client.FORBIDDEN)

    def test_scene_delete(self):
        self.client.force_authenticate(self._site_admin_user)

        response = self.client.delete("/api/game/scene/{}/".format(self._scene.id))
        self.assertEqual(response.status_code, http.client.FORBIDDEN)


class CabinetTestCase(HiveMindTest):
    def test_cabinet_list(self):
        response = self.client.get("/api/game/cabinet/")
        self.assertEqual(response.status_code, http.client.OK)

    def test_cabinet_read(self):
        self.client.force_authenticate(None)

        response = self.client.get("/api/game/cabinet/{}/".format(self._cabinet.id))
        self.assertEqual(response.status_code, http.client.OK)
        self.assertNotIn("token", response.data.keys())

    def test_cabinet_admin_read(self):
        self.client.force_authenticate(self._user)

        response = self.client.get("/api/game/cabinet/{}/".format(self._cabinet.id))
        self.assertEqual(response.status_code, http.client.OK)
        self.assertIn("token", response.data.keys())

    def test_cabinet_create(self):
        self.client.force_authenticate(None)

        data = {
            "name": "test2",
            "scene": self._scene.id,
            "display_name": "Test Cabinet 2",
        }

        response = self.client.post("/api/game/cabinet/", data)
        self.assertEqual(response.status_code, http.client.UNAUTHORIZED)

        self.client.force_authenticate(self._user)

        response = self.client.post("/api/game/cabinet/", data)
        self.assertEqual(response.status_code, http.client.CREATED)

        cabinet_id = response.data["id"]
        response = self.client.get("/api/game/cabinet/{}/".format(cabinet_id))
        self.assertEqual(response.status_code, http.client.OK)
        self.assertEqual(response.data["name"], data["name"])

    def test_cabinet_update(self):
        self.client.force_authenticate(self._user)

        response = self.client.get("/api/game/cabinet/{}/".format(self._cabinet.id))
        self.assertEqual(response.status_code, http.client.OK)

        data = response.data
        data["display_name"] = "New Cabinet Name"

        self.client.force_authenticate(None)

        response = self.client.put("/api/game/cabinet/{}/".format(self._cabinet.id), data)
        self.assertEqual(response.status_code, http.client.UNAUTHORIZED)

        self.client.force_authenticate(self._user)

        response = self.client.put("/api/game/cabinet/{}/".format(self._cabinet.id), data)
        self.assertEqual(response.status_code, http.client.OK)

        response = self.client.get("/api/game/cabinet/{}/".format(self._cabinet.id))
        self.assertEqual(response.data["display_name"], data["display_name"])

    def test_cabinet_delete(self):
        self.client.force_authenticate(self._user)

        response = self.client.delete("/api/game/cabinet/{}/".format(self._cabinet.id))
        self.assertEqual(response.status_code, http.client.FORBIDDEN)
