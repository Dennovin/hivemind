import math

from django.db import migrations, transaction
from django.db.models.fields.related import ManyToManyField
from tqdm import tqdm


def copy_table(app_name, model_name):
    def func(apps, schema_editor):
        model = apps.get_model(app_name, model_name)
        old_table_name = "hivemind_" + model._meta.model_name

        if old_table_name not in schema_editor.connection.introspection.table_names():
            return

        cursor = schema_editor.connection.cursor()
        cursor.execute(f"SELECT COUNT(1) FROM {old_table_name}")
        result = cursor.fetchall()
        num_rows = result[0][0]
        num_pages = math.ceil(num_rows / 1000)

        for page_num in tqdm(range(num_pages), desc=model_name, unit_scale=1000):
            offset = page_num * 1000
            with transaction.atomic():
                for obj in model.objects.raw(f"SELECT * FROM {old_table_name} ORDER BY id LIMIT 1000 OFFSET {offset}"):
                    obj.save()

        schema_editor.execute(f"SELECT SETVAL(pg_get_serial_sequence('{model._meta.db_table}', 'id'), (SELECT MAX(id) FROM {old_table_name}))")

        for field in model._meta.get_fields():
            if isinstance(field, ManyToManyField):
                new_table_name = field.m2m_db_table()
                old_table_name = new_table_name.replace(app_name + "_", "hivemind_", 1)

                schema_editor.execute(f"INSERT INTO {new_table_name} SELECT * FROM {old_table_name}")
                schema_editor.execute(f"SELECT SETVAL(pg_get_serial_sequence('{new_table_name}', 'id'), (SELECT MAX(id) FROM {new_table_name}))")

    def reverse_func(apps, schema_editor):
        pass

    return migrations.RunPython(func, reverse_func)
