"""server URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.urls import include, path, re_path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions

schema_view = get_schema_view(
    openapi.Info(
        title="HiveMind API",
        default_version="v1",
        contact=openapi.Contact(email="contact@kqhivemind.com"),
        license=openapi.License(name="MIT License"),
    ),
    public=True,
    permission_classes=[permissions.AllowAny],
)

urlpatterns = [
    path("api/achievement/", include("hivemind.achievement.urls")),
    path("api/game/", include("hivemind.game.urls")),
    path("api/league/", include("hivemind.league.urls")),
    path("api/notifications/", include("hivemind.notifications.urls")),
    path("api/overlay/", include("hivemind.overlay.urls")),
    path("api/stats/", include("hivemind.stats.urls")),
    path("api/tournament/", include("hivemind.tournament.urls")),
    path("api/user/", include("hivemind.user.urls")),
    path("api/whiteboard/", include("hivemind.whiteboard.urls")),
    path("api/wiki/", include("hivemind.wiki.urls")),
    re_path(r"^api/swagger(?P<format>\.json|\.yaml)$", schema_view.without_ui(cache_timeout=0), name="schema-json"),
    re_path(r"^api/swagger/$", schema_view.with_ui("swagger", cache_timeout=0), name="schema-swagger-ui"),
    re_path(r"^api/redoc/$", schema_view.with_ui("redoc", cache_timeout=0), name="schema-redoc"),
]
