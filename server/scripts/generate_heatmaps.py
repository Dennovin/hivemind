#!/usr/bin/env python3
import io
import math
from datetime import timedelta

import matplotlib
import matplotlib.colors
import requests
from django.core.files.base import ContentFile
from django.utils import timezone
from heatmappy import Heatmapper
from PIL import Image, ImageDraw

from hivemind.constants import HeatmapType
from hivemind.game.models import Game, GameMap, Heatmap

heatmap_type = {
    "Worker": HeatmapType.WORKER,
    "Soldier": HeatmapType.WARRIOR,
    "Queen": HeatmapType.QUEEN,
    "Snail": HeatmapType.SNAIL,
}

def run(*args):
    Heatmap.objects.filter(date_generated__lt=timezone.now() - timedelta(days=30)).delete()

    for game_map in GameMap.objects.all():
        if args and game_map.name not in args:
            continue

        points = { "Worker": {}, "Soldier": {}, "Queen": {}, "Snail": {} }

        games = Game.objects.filter(
            map_name=game_map.name,
            start_time__gt=timezone.now() - timedelta(days=60),
            player_count=10,
            end_time__isnull=False,
        )

        for game in games:
            for event in game.gameevent_set.filter(event_type="playerKill"):
                loc = (int(event.values[0]), 1080-int(event.values[1]))
                points[event.values[4]][loc] = points[event.values[4]].get(loc, 0) + 1

                if event.values[4] == "Worker" and event.values[1] == str(game_map.snail_track_y_loc):
                    if game.gameevent_set.filter(event_type="getOffSnail", values__3=event.values[3],
                                                 timestamp__gt=event.timestamp - timedelta(milliseconds=50),
                                                 timestamp__lt=event.timestamp + timedelta(milliseconds=50),
                                                 ).count() > 0:
                        points["Snail"][loc] = points["Snail"].get(loc, 0) + 1

        for kill_type, kill_points in points.items():
            if kill_type == "Snail" and len(kill_points) > 0:
                heatmap_points = [(i[0], i[1]-j*5) for i, v in kill_points.items() for j in range(v)]
            else:
                heatmap_points = [(i[0], i[1]) for i, v in kill_points.items() for j in range(v)]

            if game_map.image:
                img = Image.open(game_map.image)
            else:
                img = Image.new("RGB", (1920, 1080))

            colormap_colors = matplotlib.colormaps["viridis_r"].colors
            colormap = matplotlib.colors.LinearSegmentedColormap.from_list("viridis", colormap_colors)

            heatmapper = Heatmapper(colours=colormap, opacity=0.55)
            heatmap = heatmapper.heatmap_on_img(heatmap_points, img)

            filename = "{}.{}.{:%Y%m%d}.png".format(
                game_map.name.replace("map_", ""),
                kill_type.lower(),
                timezone.now(),
            )

            img_bytes = io.BytesIO()
            heatmap.save(img_bytes, format="png")

            obj = Heatmap.objects.create(game_map=game_map, event_type=heatmap_type[kill_type])
            obj.image.save(filename, ContentFile(img_bytes.getvalue()), save=True)
