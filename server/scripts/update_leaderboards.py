#!/usr/bin/env python3
from hivemind.stats.leaderboards import refresh_leaderboards


def run():
    refresh_leaderboards()
