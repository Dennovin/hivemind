    resolver 127.0.0.11;

    set $app app:8080;
    set $nextjs nextjs:3000;
    set $stats_listener_v3 stats_listener_v3:8080;
    set $stats_listener_v4 stats_listener_v4:8080;
    set $gamestate gamestate:8080;
    set $ingame_stats ingame_stats:8080;
    set $signin signin:8080;
    set $event_relay event_relay:8080;
    set $tournament_relay tournament_relay:8080;
    set $whiteboard_relay whiteboard_relay:8080;

    client_max_body_size 100M;

    gzip on;
    gzip_types application/json application/javascript text/css;
    gzip_min_length 1000;
    proxy_intercept_errors on;

    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
    ssl_ciphers HIGH:!aNULL:!MD5;

    rewrite ^/api/overlay/html/(\d+)/(\w+)/$ /overlay/$1/$2 break;

    location /api/auth {
        proxy_pass http://$nextjs;
    }

    location /api {
        add_header "Access-Control-Allow-Origin" "*";
        add_header "Access-Control-Allow-Methods" "GET,OPTIONS";
        add_header "Access-Control-Allow-Headers" "User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range";
        proxy_set_header "X-Forwarded-For" $proxy_add_x_forwarded_for;
        proxy_set_header "X-Forwarded-Host" $http_host;
        proxy_set_header "X-Forwarded-Proto" $scheme;
        proxy_pass http://$app;
    }

    location /media {
        root /usr/share/nginx/media;
        rewrite ^/media/(.*)$ /$1 break;
    }

    location @api {
        add_header "Access-Control-Allow-Origin" "*";
        add_header "Access-Control-Allow-Methods" "GET,OPTIONS";
        add_header "Access-Control-Allow-Headers" "User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range";
        proxy_set_header "X-Forwarded-For" $proxy_add_x_forwarded_for;
        proxy_set_header "X-Forwarded-Host" $http_host;
        proxy_set_header "X-Forwarded-Proto" $scheme;
        proxy_pass http://$app;
    }

    location /ws/stats_listener/v3 {
        proxy_pass http://$stats_listener_v3;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "Upgrade";
    }

    location /ws/stats_listener/v4 {
        proxy_pass http://$stats_listener_v4;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "Upgrade";
    }

    location /ws/gamestate {
        proxy_pass http://$gamestate;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "Upgrade";
    }

    location /ws/ingame_stats {
        proxy_pass http://$ingame_stats;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "Upgrade";
    }

    location ~ ^/ws/ingame_stats/ {
        proxy_pass http://$ingame_stats;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "Upgrade";
    }

    location /ws/signin {
        proxy_pass http://$signin;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "Upgrade";
    }

    location ~ ^/ws/event-relay/ {
        proxy_pass http://$event_relay;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "Upgrade";
    }

    location ~ ^/ws/tournament-relay/ {
        proxy_pass http://$tournament_relay;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "Upgrade";
    }

    location ~ ^/ws/whiteboard/ {
        proxy_pass http://$whiteboard_relay;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "Upgrade";
    }

    location /favicon.ico {
        root /usr/share/nginx/html/static;
    }

    location /.well-known/acme-challenge {
        root /data/certbotroot;
    }

    location / {
        proxy_pass http://$nextjs;
    }
