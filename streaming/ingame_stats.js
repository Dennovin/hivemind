#!/usr/bin/env node
const http = require("http");
const request = require("request");
const WebSocket = require("ws");
const redis = require("redis");
const axios = require("axios");
const uuid = require("uuid");


const ALL_MAPS = {};

function mapValues(obj, itereeFn) {
  const keys = Object.keys(obj);
  const outObj = {};
  keys.forEach((key) => {
    outObj[key] = itereeFn(obj[key], key);
  });
  return outObj;
}

function log(message) {
  console.log((new Date()) + " " + message);
}

function heartbeat() {
  this.isAlive = true;
}

function redisKey(cabinetID) {
  return `livestats.cabinet.${cabinetID}`;
}

function emptyStats() {
  return {
    playerKills: {},
    playerDeaths: {},
    workerKills: {},
    workerDeaths: {},
    playerMilitaryKills: {},
    playerMilitaryDeaths: {},
    queenKills: {},
    berriesDeposited: {},
    berriesKickedIn: {},
    totalBerries: {},
    totalSnailDistance: {},
    lastSnailPosition: {},
    lastTimeSnailPositionWasAccurate: null, // This is only updated when the snail has a rider and is moving.  It is stale otherwise
    snailIsEating: false,
    snailIsBeingRidden: false,
    lastSnailRiderId: null,
    gameStartTime: null,
    gameEndTime: null,
    mapName: null,
    berries: 0,
    warriors: new Set(),
    speed: new Set(),
    haveBerries: new Set(),
    bumps: [],
    killFeed: [],
    finalKill: {},
    goldOnLeft: false,
    prediction: null,
  };
}

async function getCabinetStats(cabinetID) {
  if (stats[cabinetID]) {
    return stats[cabinetID];
  }

  const objString = await new Promise((resolve, reject) => {
    cache.get(redisKey(cabinetID), (err, reply) => {
      resolve(reply);
    });
  });

  if (objString === null) {
    stats[cabinetID] = emptyStats();
    stats[cabinetID].gameStartTime = Date.now();
  } else {
    stats[cabinetID] = {};
    const cachedValue = JSON.parse(objString);

    for (const [k, v] of Object.entries(emptyStats())) {
      if (v instanceof Set) {
        stats[cabinetID][k] = new Set(cachedValue[k]);
      } else {
        stats[cabinetID][k] = cachedValue[k];
      }
    }

    if (stats[cabinetID].mapName) {
      await getMapInfo(stats[cabinetID].mapName);
    }
  }

  return stats[cabinetID];
}

function cacheCabinetStats(cabinetID, cabinetStats) {
  const obj = {};
  for (const [k, v] of Object.entries(cabinetStats)) {
    if (v instanceof Set) {
      obj[k] = Array.from(v);
    } else {
      obj[k] = v;
    }
  }

  return new Promise((resolve, reject) => {
    cache.set(redisKey(cabinetID), JSON.stringify(obj), 'EX', 60, resolve);
  });
}

async function getMapInfo(mapName) {
  if (!(mapName in ALL_MAPS)) {
    try {
      const response = await axios({ method: 'GET', url: '/game/map/', baseURL: process.env.API_BASE_URL,
                                     params: { name: mapName } });
      if (response?.data?.results?.length > 0) {
        ALL_MAPS[mapName] = response.data.results[0];
      }
    } catch (err) {
      log(err);
    }
  }
}

const server = http.createServer(function(request, response) {
  response.writeHead(404);
  response.end();
});

const wss = new WebSocket.Server({ server });

const interval = setInterval(function ping() {
  wss.clients.forEach(function each(ws) {
    if(!ws.isAlive)
      return ws.terminate();

    ws.isAlive = false;
    ws.ping(() => {});
  });
}, 30000);

const redisClient = redis.createClient({host: process.env.REDIS_HOST, password: process.env.REDIS_PASSWORD});
const cache = redis.createClient({host: process.env.REDIS_HOST, password: process.env.REDIS_PASSWORD});
const subscriptions = {};
const stats = {};

function estimatedSnailDistance(cabinetStats) {
  try {
    if (cabinetStats.snailIsBeingRidden && !cabinetStats.snailIsEating) {
      var timeToCalculateUntil = cabinetStats.gameEndTime ? cabinetStats.gameEndTime : Date.now();
      var millisSinceLastUpdate = timeToCalculateUntil - cabinetStats.lastTimeSnailPositionWasAccurate;

      // Pixels per second provided by Kevin J - https://discord.com/channels/850091759824535552/877059615924301845/1021890909996064788
      var pixelsPerSecond = 20.896215463
      if (cabinetStats.speed.has(cabinetStats.lastSnailRiderId)) {
        pixelsPerSecond = 28.209890875
      }

      var newMap = { ...cabinetStats.totalSnailDistance }
      newMap[cabinetStats.lastSnailRiderId] += Math.floor(pixelsPerSecond * millisSinceLastUpdate / 1000)
      return newMap;
    }
  } catch (err) {
    log(err)
  }

  return cabinetStats.totalSnailDistance;
}

wss.on("connection", (ws, request) => {
  log("New connection.");
  ws.isAlive = true;
  ws.cabinetID = null;

  const [cabinetID] = request.url.split('/').reverse();
  if (parseInt(cabinetID, 10)) {
    ws.cabinetID = cabinetID;

    if (!subscriptions[ws.cabinetID]) {
      redisClient.subscribe(`events.cabinet.${ws.cabinetID}`);
      redisClient.subscribe(`predictions.cabinet.${ws.cabinetID}`);
      subscriptions[ws.cabinetID] = new Set();
    }

    subscriptions[ws.cabinetID].add(ws);
    log(`Subscribed to cabinet: ${ws.cabinetID}`);
  }

  ws.on("pong", heartbeat);

  ws.on("message", messageString => {
    message = JSON.parse(messageString);

    if(message.type == "subscribe") {
      if(!message.cabinet_id)
        return;

      ws.cabinetID = message.cabinet_id;
      if(!subscriptions[ws.cabinetID]) {
        redisClient.subscribe(`events.cabinet.${ws.cabinetID}`);
        redisClient.subscribe(`predictions.cabinet.${ws.cabinetID}`);
        subscriptions[ws.cabinetID] = new Set();
      }

      subscriptions[ws.cabinetID].add(ws);
      log(`Subscribed to cabinet: ${ws.cabinetID}`);
    }
  });

  ws.interval = setInterval(() => {
    let cabinetStats = stats[ws.cabinetID];
    if(!cabinetStats)
      return;

    let endTime = cabinetStats.gameEndTime ? cabinetStats.gameEndTime : Date.now();
    let gameTime = cabinetStats.gameStartTime ? (endTime - cabinetStats.gameStartTime) / 1000 : 0;

    try {
      ws.send(JSON.stringify({
        "message_type": "stats",
        "map_name": cabinetStats.mapName,
        "kills": cabinetStats.playerKills,
        "deaths": cabinetStats.playerDeaths,
        "worker_kills": mapValues(cabinetStats.playerKills, (stat, key) => { return stat - (cabinetStats.playerMilitaryKills[key] || 0)}),
        "worker_deaths": mapValues(cabinetStats.playerDeaths, (stat, key) => { return stat - (cabinetStats.playerMilitaryDeaths[key] || 0)}),
        "military_kills": cabinetStats.playerMilitaryKills,
        "military_deaths": cabinetStats.playerMilitaryDeaths,
        "queen_kills": cabinetStats.queenKills,
        "berries_deposited": cabinetStats.berriesDeposited,
        "berries_kicked_in": cabinetStats.berriesKickedIn, // Only for your team
        "total_berries": cabinetStats.totalBerries, // Only for your team
        "snail_distance": cabinetStats.totalSnailDistance,
        "estimated_snail_distance": estimatedSnailDistance(cabinetStats),
        "berries_remaining": cabinetStats.berries,
        "warriors": Array.from(cabinetStats.warriors),
        "speed": Array.from(cabinetStats.speed),
        "have_berries": Array.from(cabinetStats.haveBerries),
        "game_time": gameTime,
        "kill_feed": cabinetStats.killFeed.filter(i => i.ts > (new Date() - 15000)),
        "final_kill": cabinetStats.finalKill,
        "prediction": cabinetStats.prediction,
      }));
    } catch (err) {
      log(err);
      ws.close();
    }
  }, 500);

  ws.on("close", () => {
    log("Connection closed.");
    clearInterval(ws.interval);

    if(ws.cabinetID && subscriptions[ws.cabinetID])
      subscriptions[ws.cabinetID].delete(ws);
  });
});

const processEvent = async (channel, message) => {
  let cabinetID = channel.replace("events.cabinet.", "");
  let cabinetStats = await getCabinetStats(cabinetID);

  if(message.event_type == "mapstart") {
    Object.assign(cabinetStats, emptyStats());
    cabinetStats.mapName = message.values[0];

    await getMapInfo(cabinetStats.mapName);

    cabinetStats.berries = ALL_MAPS[cabinetStats.mapName]?.total_berries;

    cabinetStats.goldOnLeft = message.values[1].toLowerCase() == "true";
  }

  // NOTE(DRay, 2023-09-17): "gamestart" has historically been used to
  // determine the `gameStartTime` along with resetting in-game stats.
  // Stats are now reset on the earlier "mapstart" event when both queens tap
  // in, but for backwards compatibility or possibly only consistency, the
  // `gameStartTime` remains tied to the "gamestart" event type. This could be
  // changed in the future if requisite consideration is given to the potential
  // repercussions of setting `gameStartTime` on the earlier "mapstart" event.
  if(message.event_type == "gamestart") {
    cabinetStats.gameStartTime = Date.now();
  }

  // NOTE(chris, 2022-09-21): Starting with 17.26 (and some other 17.X betas), there is a bug where "gameend" isn't
  // being sent.  We can safely use "victory" here, so adding that as a workaround for now
  if(message.event_type == "gameend" || (message.event_type == "victory" && !cabinetStats.gameEndTime)) {
    cabinetStats.gameEndTime = Date.now();

    cabinetStats.finalKill = cabinetStats.killFeed[cabinetStats.killFeed.length - 1];
  }

  if (message.event_type == "victory") {
    if (message.values[1].trim().toLowerCase() == "snail") {
      await getMapInfo(cabinetStats.mapName);
      var snailTrackWidth = ALL_MAPS[cabinetStats.mapName]?.snail_track_width || 900;
      var leftTeamColor = cabinetStats.goldOnLeft ? "Gold" : "Blue"
      var finishedInLeftGoal = message.values[0] == leftTeamColor;
      var endGoalPosition = 960 + snailTrackWidth;
      if (finishedInLeftGoal) {
        endGoalPosition = 960 - snailTrackWidth;
      }
      cabinetStats.totalSnailDistance[cabinetStats.lastSnailRiderId] += Math.abs(
        endGoalPosition - cabinetStats.lastSnailPosition[cabinetStats.lastSnailRiderId]
      )
      cabinetStats.snailIsBeingRidden = false; // Map is over, and we know the true snail spot
    }
  }

  if(message.event_type == "playerKill") {
    cabinetStats.playerKills[message.values[2]] = (cabinetStats.playerKills[message.values[2]] || 0) + 1;
    cabinetStats.playerDeaths[message.values[3]] = (cabinetStats.playerDeaths[message.values[3]] || 0) + 1;

    if(message.values[4] == "Queen" || message.values[4] == "Soldier") {
      cabinetStats.playerMilitaryKills[message.values[2]] = (cabinetStats.playerMilitaryKills[message.values[2]] || 0) + 1;
      cabinetStats.playerMilitaryDeaths[message.values[3]] = (cabinetStats.playerMilitaryDeaths[message.values[3]] || 0) + 1;

      const assists = new Set();
      const ts = new Date();
      for (const bump of cabinetStats.bumps.filter(i => i.ts > ts - 2000)) {
        if (bump.players.includes(message.values[3])) {
          assists.add(bump.players.filter(i => i != message.values[3])[0]);
        }
      }

      assists.delete(message.values[2]);
      assists.delete(message.values[3]);

      cabinetStats.killFeed.push({
        killer: message.values[2],
        victim: message.values[3],
        ts,
        entryType: 'kill',
        gameTime: cabinetStats.gameStartTime ? (Date.now() - cabinetStats.gameStartTime) / 1000 : undefined,
        assists: Array.from(assists),
        id: uuid.v4(),
      });
    }

    cabinetStats.warriors.delete(message.values[3]);
    cabinetStats.speed.delete(message.values[3]);
    cabinetStats.haveBerries.delete(message.values[3]);

    if(message.values[3] == "1" || message.values[3] == "2") {
      cabinetStats.queenKills[message.values[2]] = (cabinetStats.queenKills[message.values[2]] || 0) + 1;
    }

    // playerKill is the only event generated when a snail eat is completed
    if(message.values[2] == cabinetStats.lastSnailRiderId && cabinetStats.snailIsEating) {
      cabinetStats.snailIsEating = false;
      cabinetStats.lastTimeSnailPositionWasAccurate = Date.now();
    }
  }

  if(["berryDeposit", "berryKickIn", "useMaiden"].includes(message.event_type)) {
    cabinetStats.berries--;
  }

  if(message.event_type == "useMaiden") {
    if(message.values[2] == "maiden_speed")
      cabinetStats.speed.add(message.values[3]);
    if(message.values[2] == "maiden_wings")
      cabinetStats.warriors.add(message.values[3]);
  }

  if(message.event_type == "berryDeposit") {
    cabinetStats.berriesDeposited[message.values[2]] = (cabinetStats.berriesDeposited[message.values[2]] || 0) + 1;
    cabinetStats.totalBerries[message.values[2]] = (cabinetStats.totalBerries[message.values[2]] || 0) + 1;
    cabinetStats.haveBerries.delete(message.values[2]);
  }

  if(message.event_type == "berryKickIn") {
    if (message.values[message.values.length - 1].substr(-4) == "True") {
      // Only count berries kicked in for your team.  Additionally, the reason we're doing this weird .substr thing
      // is because old API versions forgot the comma in the value, so it'd be "5True" in those builds.
      cabinetStats.berriesKickedIn[message.values[2]] = (cabinetStats.berriesKickedIn[message.values[2]] || 0) + 1;
      cabinetStats.totalBerries[message.values[2]] = (cabinetStats.totalBerries[message.values[2]] || 0) + 1;
    }
  }

  if(message.event_type == "useMaiden") {
    cabinetStats.haveBerries.delete(message.values[3]);
  }

  if(message.event_type == "carryFood") {
    cabinetStats.haveBerries.add(message.values[0]);
  }

  if(message.event_type == "carryFood" && cabinetStats.berries == 0) {
    cabinetStats.berries = ALL_MAPS[cabinetStats.mapName]?.total_berries;
  }

  if(message.event_type == "getOnSnail: " || message.event_type == "getOnSnail") {
    cabinetStats.lastSnailPosition[message.values[2]] = message.values[0] - 0;
    cabinetStats.totalSnailDistance[message.values[2]] = (cabinetStats.totalSnailDistance[message.values[2]] || 0);
    cabinetStats.lastSnailRiderId = message.values[2];
    cabinetStats.snailIsBeingRidden = true;
    cabinetStats.lastTimeSnailPositionWasAccurate = Date.now();
  }

  if(message.event_type == "getOffSnail: " || message.event_type == "getOffSnail") {
    cabinetStats.totalSnailDistance[message.values[3]] += Math.abs(
      message.values[0] - cabinetStats.lastSnailPosition[message.values[3]]
    );
    cabinetStats.snailIsBeingRidden = false;
  }

  if(message.event_type == "snailEat") { // Record incremental progress
    cabinetStats.totalSnailDistance[message.values[2]] += Math.abs(
      message.values[0] - cabinetStats.lastSnailPosition[message.values[2]]
    );
    cabinetStats.lastSnailPosition[message.values[2]] = message.values[0] - 0;
    cabinetStats.snailIsEating = true;
  }

  if(message.event_type == "snailEscape") {
    cabinetStats.snailIsEating = false;
  }

  if(message.event_type == "glance") {
    if (message.values.length >= 4) {
      cabinetStats.bumps.push({ ts: new Date(), players: [message.values[2], message.values[3]] });
    } else {
      cabinetStats.bumps.push({ ts: new Date(), players: [message.values[0], message.values[1]] });
    }
  }

  cacheCabinetStats(cabinetID, cabinetStats);
};

const processPrediction = async (channel, message) => {
  let cabinetID = channel.replace("predictions.cabinet.", "");
  if (cabinetID in stats) {
    stats[cabinetID].prediction = message.prediction;
  }
};

redisClient.on("message", async (channel, messageText) => {
  let message = JSON.parse(messageText);

  if (channel.startsWith("events.cabinet."))
    return await processEvent(channel, message);
  if (channel.startsWith("predictions.cabinet."))
    return await processPrediction(channel, message);
});


server.listen(8080);
