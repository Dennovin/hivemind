const log = message => {
  console.log(new Date() + ' ' + message);
};

const apiFetch = async (urlPart, params) => {
  const url = new URL(`api/${urlPart}`, process.env.API_BASE_URL);
  const response = await fetch(url, params);
  return await response.json();
};

const sendMessage = async (ws, subchannel, data) => {
  const messageText = JSON.stringify({
    type: subchannel,
    data,
  });

  try {
    ws.send(messageText);
  } catch (err) {
    log(err);
    ws.close();
  }
};

module.exports = { log, apiFetch, sendMessage };
