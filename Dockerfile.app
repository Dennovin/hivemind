FROM python:3.12 AS app

ADD . /src
WORKDIR /src/server
RUN pip install -r requirements.txt
RUN mkdir -p /src/logs
CMD ["python", "/src/server/manage.py", "runserver", "0.0.0.0:8080"]

FROM app AS app-dev
RUN pip install -r requirements_dev.txt
